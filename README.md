# smartFridge
SI1.2 LS2017      

## O Projektu

Projekt SmartFridge je postaven na míru zákazníkovi (Obchodní řetězec), který díky tomu bude moct kontrolovat své skladové zásoby, prodeje potravin a nebo například různé slevové akce. 

Lidé používající tento software budou moci při vkládání věcí do lednice načítat automaticky kódy z databáze obchodního řetězce. Poté na webových stránkách budou moci zobrazit obsah své lednice, budou dostávat upozornění na prošlé potraviny. A také budou moci si vyhledat recepty, které můžou z potravin uvařit. Důležitou funkcí je vyhledání receptů u kterých chybí nějaká surovina a možnost automatického objednávání (Košík na webových stránkách).

Tyto recepty budou vkládány zákazníkem. Také budou podléhat prioritnímu zobrazování (Top recepty, doporučené - pro objednání chybějících surovin se slevou,...)

##### Implementovali jsme pouze část tohoto velkého snu.

## Autoři

#### Jiří Kapoun  
#### Lubomír Oulehle   
#### Martina Fukalová
#### Pavla Ďuranová
#### Petr Pilař 

## Odkazy
  * https://trac.project.fit.cvut.cz/SF
  * GIT - https://github.com/kapoun/si1
  * ​FIT GITLAB(FIT) - https://gitlab.fit.cvut.cz/duranpav/sf
  * http://smartfridge.cz/