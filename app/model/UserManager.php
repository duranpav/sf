<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;
use SmartFridge\Base\UzivatelQuery;
use SmartFridge\Uzivatel;


/**
 * Users management.
 */
class UserManager implements Nette\Security\IAuthenticator
{
    use Nette\SmartObject;

    const
        TABLE_NAME = 'Uzivatel',
        COLUMN_ID = 'ID',
        COLUMN_NAME = 'prezdivka',
        COLUMN_PASSWORD_HASH = 'heslo',
        //COLUMN_EMAIL = 'email',
        COLUMN_ROLE = 'role';



    public function __construct() {
    }


    /**
     * Performs an authentication.
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;

        $uzivatel = UzivatelQuery::create()->findByPrezdivka($username);

        //dump($uzivatel->getColumnValues('heslo'));

        if (!$uzivatel->getColumnValues()) {
            throw new Nette\Security\AuthenticationException('Přihlašovací jméno nebylo nalezeno.', self::IDENTITY_NOT_FOUND);

        } elseif (!Passwords::verify($password, $uzivatel->getColumnValues(self::COLUMN_PASSWORD_HASH)[0])) {
            throw new Nette\Security\AuthenticationException('Heslo je nesprávné.', self::INVALID_CREDENTIAL);

        } elseif (Passwords::needsRehash($uzivatel->getColumnValues(self::COLUMN_PASSWORD_HASH)[0])) {
            $uzivatel->setData([self::COLUMN_PASSWORD_HASH => Passwords::hash($password)]);
            $uzivatel->save();
        }

        //unset($arr[self::COLUMN_PASSWORD_HASH]);
        return new Nette\Security\Identity($uzivatel->getColumnValues(self::COLUMN_ID)[0], $uzivatel->getColumnValues(self::COLUMN_ROLE)[0]);
    }


    /**
     * Adds new user.
     * @throws DuplicateNameException
     */
    public function add($username, $role, $password)
    {
        try {
            $uzivatel = new Uzivatel();
            $uzivatel->setHeslo(Passwords::hash($password));
            $uzivatel->setPrezdivka($username);
            $uzivatel->setRole($role);
            $uzivatel->save();

        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }
}



class DuplicateNameException extends \Exception
{}