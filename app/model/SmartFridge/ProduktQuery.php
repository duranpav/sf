<?php

namespace SmartFridge;

use SmartFridge\Base\ProduktQuery as BaseProduktQuery;
use SmartFridge\Base\KategorieQuery as BaseKategorieQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'Produkt' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProduktQuery extends BaseProduktQuery
{

    /**
     * ProduktQuery - getINOperator
     * Metoda sestavuje string z pole pro sql příkaz IN
     * @param $arr
     * @return string
     */
    public function getINOperator($arr) {

        $string = "(";
        $first = TRUE;
        foreach ($arr  as $a) {
            if (!$first)
                $string .= ',';
            else
                $first = FALSE;
            $string .= $a;
        }

        $string .= ")";
        return $string;
    }

    /**
     * ProduktQuery - getAllProductsFromCID
     * Metoda vrací všechny produktu od zadané kategorie
     * @param $cid
     * @return array|mixed|\Propel\Runtime\ActiveRecord\ActiveRecordInterface[]|\Propel\Runtime\Collection\ObjectCollection|Produkt[]
     */
    public function getAllProductsFromCID($cid) {
        $allCategories = KategorieQuery::create()->findAllSubcategoryByKategorieid($cid);
        $products = BaseProduktQuery::create()
            ->find();
        if ($cid) {
            $products = BaseProduktQuery::create()
                ->where("kategorieID IN " . $this->getINOperator($allCategories))
                ->find();
        }
        return $products;
    }
}
