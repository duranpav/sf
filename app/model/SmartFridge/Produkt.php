<?php

namespace SmartFridge;

use SmartFridge\Base\Produkt as BaseProdukt;
use SmartFridge\Base\ZboziQuery as BaseZboziQuery;
use SmartFridge\Base\KategorieQuery as BaseKategorieQuery;

/**
 * Skeleton subclass for representing a row from the 'Produkt' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Produkt extends BaseProdukt
{
    /**
     * Produkt - getPrice
     * Metoda vrací cenu nebo indikátor false
     * @param $id
     * @return bool|float
     */
    public function getPrice($id) {
        $property = BaseZboziQuery::create()
            ->orderByCena()
            ->findOneByProduktid($id);
        if($property)
            return $property->getCena();
        else
            return false;
    }
}
