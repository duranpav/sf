<?php

namespace SmartFridge;

use SmartFridge\Base\WatchdogQuery as BaseWatchdogQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'Watchdog' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class WatchdogQuery extends BaseWatchdogQuery
{

}
