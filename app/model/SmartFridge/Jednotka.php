<?php

namespace SmartFridge;

use SmartFridge\Base\Jednotka as BaseJednotka;

/**
 * Skeleton subclass for representing a row from the 'Jednotka' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Jednotka extends BaseJednotka
{

}
