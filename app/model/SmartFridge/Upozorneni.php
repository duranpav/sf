<?php

namespace SmartFridge;

use SmartFridge\Base\Upozorneni as BaseUpozorneni;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for representing a row from the 'Upozorneni' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Upozorneni extends BaseUpozorneni
{
    /**
     * Upozorneni - findActive
     * Metoda vrací všechny aktivní upozorneni
     * @param $userId
     * @return \Propel\Runtime\Collection\ObjectCollection|Upozorneni[]
     */
    public static function findActive($userId)
    {
        return UpozorneniQuery::create()
            ->filterByUzivatelid($userId)
            ->filterByAktivni(1)
            ->orderByDatum(Criteria::DESC)
            ->find();
    }
    /**
     * Upozorneni - findNonActive
     * Metoda vrací všechny neaktivní upozorneni
     * @param $userId
     * @return \Propel\Runtime\Collection\ObjectCollection|Upozorneni[]
     */
    public static function findNonActive($userId)
    {
        return UpozorneniQuery::create()
            ->filterByUzivatelid($userId)
            ->filterByAktivni(0)
            ->orderByDatum(Criteria::DESC)
            ->find();
    }
    /**
     * Upozorneni - findAll
     * Metoda vrací všechny upozorneni
     * @param $userId
     * @return \Propel\Runtime\Collection\ObjectCollection|Upozorneni[]
     */
    public static function findAll($userId)
    {
        return UpozorneniQuery::create()
            ->filterByUzivatelid($userId)
            ->orderByDatum(Criteria::DESC)
            ->find();
    }
}
