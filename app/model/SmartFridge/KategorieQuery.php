<?php

namespace SmartFridge;

use SmartFridge\Base\KategorieQuery as BaseKategorieQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'Kategorie' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class KategorieQuery extends BaseKategorieQuery
{

    /**
     * KategorieQuery - findAllSubcategoryByKategorieid
     * Najde všechny subkategorie od KategorieId
     * @param $cid
     * @return mixed
     */
    public function findAllSubcategoryByKategorieid($cid) {
        // vsechny kategorie od kategorie $cid
        $allCategories[$cid] = $cid;

        // prvni podkategorie od $cid
        $categories = BaseKategorieQuery::create()
            ->findByKategorieid($cid);
        // upraveni formatu
        $categoriesArr = array();
        foreach ($categories as $s) {
            $categoriesArr[$s->getId()] = $s->getId();
        }

        // pomocne pole pro subkategorie
        $subCategoryArr = array();

        // cyklus
        while ($categoriesArr) {
            foreach ($categoriesArr as $c) {
                // pridani subkategorii
                $allCategories[$c] = $c;

                // projiti subkategorie
                $subCategory = BaseKategorieQuery::create()
                    ->findByKategorieid($c);

                // pridani subsbukategorii
                foreach ($subCategory as $s) {
                    $subCategoryArr[$s->getId()] = $s->getId();
                }
            }
            $categoriesArr = $subCategoryArr;
            unset($subCategoryArr);
            $subCategoryArr = array();
        }

        return $allCategories;
    }
}
