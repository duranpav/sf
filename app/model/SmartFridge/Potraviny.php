<?php

namespace SmartFridge;

use SmartFridge\Base\Potraviny as BasePotraviny;

/**
 * Skeleton subclass for representing a row from the 'Potraviny' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Potraviny extends BasePotraviny
{
    /**
     * Potraviny - getNazevProduktu
     * Metoda vrací název produktu
     * @param $id
     * @return string
     */
    public function getNazevProduktu($id) {
        $food = PotravinyQuery::create()->findOneById($id);
        $produkt = ProduktQuery::create()
            ->findOneById($food->getProduktId());
        return $produkt->getNazev();
    }
}
