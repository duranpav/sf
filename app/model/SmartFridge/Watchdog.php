<?php

namespace SmartFridge;

use SmartFridge\Base\Watchdog as BaseWatchdog;

/**
 * Skeleton subclass for representing a row from the 'Watchdog' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Watchdog extends BaseWatchdog
{

}
