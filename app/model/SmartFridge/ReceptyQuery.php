<?php

namespace SmartFridge;

use Nette\Http\Session;
use SmartFridge\Base\ReceptyQuery as BaseReceptyQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'Recepty' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ReceptyQuery extends BaseReceptyQuery
{

    /**
     * ReceptyQuery - getCountMissingIngredience
     * Tato metoda vrací pole, kde klíčem je idReceptu a hodnotou je počet chybějících ingrediencí.
     * @param null $uid
     * @param null $filter
     * @return array|NULL
     */
    public function getCountMissingIngredience($uid = NULL, $filter = NULL) {
        $countMissI = array();
        $food = array();

        // kontrola navolených ingrediencí v cache paměti

        if (!($filter && (isset($filter->products) || count($filter->products) == 0))) {
            // pokud není $uid
            if (!$uid) {
                return NULL;
            }
            $foodInFridge = PotravinyQuery::create()->where('uzivatelID = ' . $uid)->find();
            $food = array();
            foreach ($foodInFridge as $f) {
                $food[$f->getProduktid()] = $f->getProduktid();
            }
        } else {
            $food = $filter->products;
        }

        $allRecipes = ReceptyQuery::create()->find();

        foreach ($allRecipes as $r) {
            $countMissI[$r->getId()] = 0;
            $ingredience = IngredienceQuery::create()->findByReceptid($r->getId());
            foreach($ingredience as $i) {
                if (!isset($food[$i->getProduktid()])) {
                    $countMissI[$r->getId()]++;
                }
            }
        }

        return $countMissI;
    }

    /**
     * ReceptyQuery - getRecipes
     * Tato metoda pro přihlášeného uživatele vrátí recepty, které může vytvořir podle věcí v lednici
     * jinak vrátí všechny recepty seřazené.
     *
     * @param null $uid
     * @param null $filter
     * @return array|mixed|\Propel\Runtime\ActiveRecord\ActiveRecordInterface[]|\Propel\Runtime\Collection\ObjectCollection|Recepty[]
     */
    public function getRecipes($uid = NULL, $filter = NULL) {

        // obycejne vyhledavani podle priority a poté podle ohodnocení
        $recipes = BaseReceptyQuery::create()
            ->leftJoinWithRecenze()
            ->orderByPriorita('DESC')
            ->orderBy('Recenze.pocetHvezdicek','DESC')->find();


        // pokud je prihlaseny uzivatel
        if ($uid != NULL) {
            $missI = $this->getCountMissingIngredience($uid, $filter);
            // nove serazene pole
            $new = array();
            foreach ($recipes as $r) {
                // ohodnoceni
                $stars = 0;
                foreach ($r->getRecenzes() as $recenze) {
                    $stars += $recenze->getPocethvezdicek();
                }
                if ($r->countRecenzes() != 0)
                    $stars = $stars / $r->countRecenzes();

                // serazovaci pomocná
                $order = 0;
                // pridame do ni chybející ingredience vynásobené hodnotou 5 odecteme od ni prioritu a pocet hvezd
                // tzn čím vyšší priorita a vyšší hodnocení tím bude výš recept
                $order += (int)ceil(($missI[$r->getId()] * 5) - ceil($r->getPriorita() * 0.2) - $stars);

                // podmínka pokud už existuje recept se stejným serazovacím číslem nebo číslo je menší než 0
                while (isset($new[$order]) || $order < 0) {
                    $order++;
                }

                // uložení hodnoty
                $new[$order] = $r;

                // serazení podel klíče
                ksort($new);
            }
            $recipes = $new;
        }

        return $recipes;
    }
}
