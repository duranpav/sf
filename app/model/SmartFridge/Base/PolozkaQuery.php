<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SmartFridge\Polozka as ChildPolozka;
use SmartFridge\PolozkaQuery as ChildPolozkaQuery;
use SmartFridge\Map\PolozkaTableMap;

/**
 * Base class that represents a query for the 'Polozka' table.
 *
 *
 *
 * @method     ChildPolozkaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPolozkaQuery orderByCenapolozky($order = Criteria::ASC) Order by the cenaPolozky column
 * @method     ChildPolozkaQuery orderByPocet($order = Criteria::ASC) Order by the pocet column
 * @method     ChildPolozkaQuery orderByZboziid($order = Criteria::ASC) Order by the ZboziID column
 * @method     ChildPolozkaQuery orderByObjednavkaid($order = Criteria::ASC) Order by the ObjednavkaID column
 *
 * @method     ChildPolozkaQuery groupById() Group by the id column
 * @method     ChildPolozkaQuery groupByCenapolozky() Group by the cenaPolozky column
 * @method     ChildPolozkaQuery groupByPocet() Group by the pocet column
 * @method     ChildPolozkaQuery groupByZboziid() Group by the ZboziID column
 * @method     ChildPolozkaQuery groupByObjednavkaid() Group by the ObjednavkaID column
 *
 * @method     ChildPolozkaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPolozkaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPolozkaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPolozkaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPolozkaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPolozkaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPolozkaQuery leftJoinObjednavka($relationAlias = null) Adds a LEFT JOIN clause to the query using the Objednavka relation
 * @method     ChildPolozkaQuery rightJoinObjednavka($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Objednavka relation
 * @method     ChildPolozkaQuery innerJoinObjednavka($relationAlias = null) Adds a INNER JOIN clause to the query using the Objednavka relation
 *
 * @method     ChildPolozkaQuery joinWithObjednavka($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Objednavka relation
 *
 * @method     ChildPolozkaQuery leftJoinWithObjednavka() Adds a LEFT JOIN clause and with to the query using the Objednavka relation
 * @method     ChildPolozkaQuery rightJoinWithObjednavka() Adds a RIGHT JOIN clause and with to the query using the Objednavka relation
 * @method     ChildPolozkaQuery innerJoinWithObjednavka() Adds a INNER JOIN clause and with to the query using the Objednavka relation
 *
 * @method     ChildPolozkaQuery leftJoinZbozi($relationAlias = null) Adds a LEFT JOIN clause to the query using the Zbozi relation
 * @method     ChildPolozkaQuery rightJoinZbozi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Zbozi relation
 * @method     ChildPolozkaQuery innerJoinZbozi($relationAlias = null) Adds a INNER JOIN clause to the query using the Zbozi relation
 *
 * @method     ChildPolozkaQuery joinWithZbozi($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Zbozi relation
 *
 * @method     ChildPolozkaQuery leftJoinWithZbozi() Adds a LEFT JOIN clause and with to the query using the Zbozi relation
 * @method     ChildPolozkaQuery rightJoinWithZbozi() Adds a RIGHT JOIN clause and with to the query using the Zbozi relation
 * @method     ChildPolozkaQuery innerJoinWithZbozi() Adds a INNER JOIN clause and with to the query using the Zbozi relation
 *
 * @method     \SmartFridge\ObjednavkaQuery|\SmartFridge\ZboziQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPolozka findOne(ConnectionInterface $con = null) Return the first ChildPolozka matching the query
 * @method     ChildPolozka findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPolozka matching the query, or a new ChildPolozka object populated from the query conditions when no match is found
 *
 * @method     ChildPolozka findOneById(int $id) Return the first ChildPolozka filtered by the id column
 * @method     ChildPolozka findOneByCenapolozky(double $cenaPolozky) Return the first ChildPolozka filtered by the cenaPolozky column
 * @method     ChildPolozka findOneByPocet(int $pocet) Return the first ChildPolozka filtered by the pocet column
 * @method     ChildPolozka findOneByZboziid(int $ZboziID) Return the first ChildPolozka filtered by the ZboziID column
 * @method     ChildPolozka findOneByObjednavkaid(int $ObjednavkaID) Return the first ChildPolozka filtered by the ObjednavkaID column *

 * @method     ChildPolozka requirePk($key, ConnectionInterface $con = null) Return the ChildPolozka by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPolozka requireOne(ConnectionInterface $con = null) Return the first ChildPolozka matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPolozka requireOneById(int $id) Return the first ChildPolozka filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPolozka requireOneByCenapolozky(double $cenaPolozky) Return the first ChildPolozka filtered by the cenaPolozky column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPolozka requireOneByPocet(int $pocet) Return the first ChildPolozka filtered by the pocet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPolozka requireOneByZboziid(int $ZboziID) Return the first ChildPolozka filtered by the ZboziID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPolozka requireOneByObjednavkaid(int $ObjednavkaID) Return the first ChildPolozka filtered by the ObjednavkaID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPolozka[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPolozka objects based on current ModelCriteria
 * @method     ChildPolozka[]|ObjectCollection findById(int $id) Return ChildPolozka objects filtered by the id column
 * @method     ChildPolozka[]|ObjectCollection findByCenapolozky(double $cenaPolozky) Return ChildPolozka objects filtered by the cenaPolozky column
 * @method     ChildPolozka[]|ObjectCollection findByPocet(int $pocet) Return ChildPolozka objects filtered by the pocet column
 * @method     ChildPolozka[]|ObjectCollection findByZboziid(int $ZboziID) Return ChildPolozka objects filtered by the ZboziID column
 * @method     ChildPolozka[]|ObjectCollection findByObjednavkaid(int $ObjednavkaID) Return ChildPolozka objects filtered by the ObjednavkaID column
 * @method     ChildPolozka[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PolozkaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SmartFridge\Base\PolozkaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SmartFridge\\Polozka', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPolozkaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPolozkaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPolozkaQuery) {
            return $criteria;
        }
        $query = new ChildPolozkaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPolozka|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PolozkaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PolozkaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPolozka A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, cenaPolozky, pocet, ZboziID, ObjednavkaID FROM Polozka WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPolozka $obj */
            $obj = new ChildPolozka();
            $obj->hydrate($row);
            PolozkaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPolozka|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPolozkaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PolozkaTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPolozkaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PolozkaTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPolozkaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PolozkaTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PolozkaTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PolozkaTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the cenaPolozky column
     *
     * Example usage:
     * <code>
     * $query->filterByCenapolozky(1234); // WHERE cenaPolozky = 1234
     * $query->filterByCenapolozky(array(12, 34)); // WHERE cenaPolozky IN (12, 34)
     * $query->filterByCenapolozky(array('min' => 12)); // WHERE cenaPolozky > 12
     * </code>
     *
     * @param     mixed $cenapolozky The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPolozkaQuery The current query, for fluid interface
     */
    public function filterByCenapolozky($cenapolozky = null, $comparison = null)
    {
        if (is_array($cenapolozky)) {
            $useMinMax = false;
            if (isset($cenapolozky['min'])) {
                $this->addUsingAlias(PolozkaTableMap::COL_CENAPOLOZKY, $cenapolozky['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cenapolozky['max'])) {
                $this->addUsingAlias(PolozkaTableMap::COL_CENAPOLOZKY, $cenapolozky['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PolozkaTableMap::COL_CENAPOLOZKY, $cenapolozky, $comparison);
    }

    /**
     * Filter the query on the pocet column
     *
     * Example usage:
     * <code>
     * $query->filterByPocet(1234); // WHERE pocet = 1234
     * $query->filterByPocet(array(12, 34)); // WHERE pocet IN (12, 34)
     * $query->filterByPocet(array('min' => 12)); // WHERE pocet > 12
     * </code>
     *
     * @param     mixed $pocet The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPolozkaQuery The current query, for fluid interface
     */
    public function filterByPocet($pocet = null, $comparison = null)
    {
        if (is_array($pocet)) {
            $useMinMax = false;
            if (isset($pocet['min'])) {
                $this->addUsingAlias(PolozkaTableMap::COL_POCET, $pocet['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pocet['max'])) {
                $this->addUsingAlias(PolozkaTableMap::COL_POCET, $pocet['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PolozkaTableMap::COL_POCET, $pocet, $comparison);
    }

    /**
     * Filter the query on the ZboziID column
     *
     * Example usage:
     * <code>
     * $query->filterByZboziid(1234); // WHERE ZboziID = 1234
     * $query->filterByZboziid(array(12, 34)); // WHERE ZboziID IN (12, 34)
     * $query->filterByZboziid(array('min' => 12)); // WHERE ZboziID > 12
     * </code>
     *
     * @see       filterByZbozi()
     *
     * @param     mixed $zboziid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPolozkaQuery The current query, for fluid interface
     */
    public function filterByZboziid($zboziid = null, $comparison = null)
    {
        if (is_array($zboziid)) {
            $useMinMax = false;
            if (isset($zboziid['min'])) {
                $this->addUsingAlias(PolozkaTableMap::COL_ZBOZIID, $zboziid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($zboziid['max'])) {
                $this->addUsingAlias(PolozkaTableMap::COL_ZBOZIID, $zboziid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PolozkaTableMap::COL_ZBOZIID, $zboziid, $comparison);
    }

    /**
     * Filter the query on the ObjednavkaID column
     *
     * Example usage:
     * <code>
     * $query->filterByObjednavkaid(1234); // WHERE ObjednavkaID = 1234
     * $query->filterByObjednavkaid(array(12, 34)); // WHERE ObjednavkaID IN (12, 34)
     * $query->filterByObjednavkaid(array('min' => 12)); // WHERE ObjednavkaID > 12
     * </code>
     *
     * @see       filterByObjednavka()
     *
     * @param     mixed $objednavkaid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPolozkaQuery The current query, for fluid interface
     */
    public function filterByObjednavkaid($objednavkaid = null, $comparison = null)
    {
        if (is_array($objednavkaid)) {
            $useMinMax = false;
            if (isset($objednavkaid['min'])) {
                $this->addUsingAlias(PolozkaTableMap::COL_OBJEDNAVKAID, $objednavkaid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($objednavkaid['max'])) {
                $this->addUsingAlias(PolozkaTableMap::COL_OBJEDNAVKAID, $objednavkaid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PolozkaTableMap::COL_OBJEDNAVKAID, $objednavkaid, $comparison);
    }

    /**
     * Filter the query by a related \SmartFridge\Objednavka object
     *
     * @param \SmartFridge\Objednavka|ObjectCollection $objednavka The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPolozkaQuery The current query, for fluid interface
     */
    public function filterByObjednavka($objednavka, $comparison = null)
    {
        if ($objednavka instanceof \SmartFridge\Objednavka) {
            return $this
                ->addUsingAlias(PolozkaTableMap::COL_OBJEDNAVKAID, $objednavka->getId(), $comparison);
        } elseif ($objednavka instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PolozkaTableMap::COL_OBJEDNAVKAID, $objednavka->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByObjednavka() only accepts arguments of type \SmartFridge\Objednavka or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Objednavka relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPolozkaQuery The current query, for fluid interface
     */
    public function joinObjednavka($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Objednavka');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Objednavka');
        }

        return $this;
    }

    /**
     * Use the Objednavka relation Objednavka object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\ObjednavkaQuery A secondary query class using the current class as primary query
     */
    public function useObjednavkaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinObjednavka($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Objednavka', '\SmartFridge\ObjednavkaQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Zbozi object
     *
     * @param \SmartFridge\Zbozi|ObjectCollection $zbozi The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPolozkaQuery The current query, for fluid interface
     */
    public function filterByZbozi($zbozi, $comparison = null)
    {
        if ($zbozi instanceof \SmartFridge\Zbozi) {
            return $this
                ->addUsingAlias(PolozkaTableMap::COL_ZBOZIID, $zbozi->getId(), $comparison);
        } elseif ($zbozi instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PolozkaTableMap::COL_ZBOZIID, $zbozi->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByZbozi() only accepts arguments of type \SmartFridge\Zbozi or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Zbozi relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPolozkaQuery The current query, for fluid interface
     */
    public function joinZbozi($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Zbozi');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Zbozi');
        }

        return $this;
    }

    /**
     * Use the Zbozi relation Zbozi object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\ZboziQuery A secondary query class using the current class as primary query
     */
    public function useZboziQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinZbozi($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Zbozi', '\SmartFridge\ZboziQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPolozka $polozka Object to remove from the list of results
     *
     * @return $this|ChildPolozkaQuery The current query, for fluid interface
     */
    public function prune($polozka = null)
    {
        if ($polozka) {
            $this->addUsingAlias(PolozkaTableMap::COL_ID, $polozka->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Polozka table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PolozkaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PolozkaTableMap::clearInstancePool();
            PolozkaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PolozkaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PolozkaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PolozkaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PolozkaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PolozkaQuery
