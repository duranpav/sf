<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SmartFridge\Potraviny as ChildPotraviny;
use SmartFridge\PotravinyQuery as ChildPotravinyQuery;
use SmartFridge\Map\PotravinyTableMap;

/**
 * Base class that represents a query for the 'Potraviny' table.
 *
 *
 *
 * @method     ChildPotravinyQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPotravinyQuery orderBySpotrebovatdo($order = Criteria::ASC) Order by the spotrebovatDo column
 * @method     ChildPotravinyQuery orderByTrvanlivostdo($order = Criteria::ASC) Order by the trvanlivostDo column
 * @method     ChildPotravinyQuery orderByUzivatelid($order = Criteria::ASC) Order by the uzivatelID column
 * @method     ChildPotravinyQuery orderByProduktid($order = Criteria::ASC) Order by the produktID column
 * @method     ChildPotravinyQuery orderByPocet($order = Criteria::ASC) Order by the pocet column
 *
 * @method     ChildPotravinyQuery groupById() Group by the id column
 * @method     ChildPotravinyQuery groupBySpotrebovatdo() Group by the spotrebovatDo column
 * @method     ChildPotravinyQuery groupByTrvanlivostdo() Group by the trvanlivostDo column
 * @method     ChildPotravinyQuery groupByUzivatelid() Group by the uzivatelID column
 * @method     ChildPotravinyQuery groupByProduktid() Group by the produktID column
 * @method     ChildPotravinyQuery groupByPocet() Group by the pocet column
 *
 * @method     ChildPotravinyQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPotravinyQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPotravinyQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPotravinyQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPotravinyQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPotravinyQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPotravinyQuery leftJoinProdukt($relationAlias = null) Adds a LEFT JOIN clause to the query using the Produkt relation
 * @method     ChildPotravinyQuery rightJoinProdukt($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Produkt relation
 * @method     ChildPotravinyQuery innerJoinProdukt($relationAlias = null) Adds a INNER JOIN clause to the query using the Produkt relation
 *
 * @method     ChildPotravinyQuery joinWithProdukt($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Produkt relation
 *
 * @method     ChildPotravinyQuery leftJoinWithProdukt() Adds a LEFT JOIN clause and with to the query using the Produkt relation
 * @method     ChildPotravinyQuery rightJoinWithProdukt() Adds a RIGHT JOIN clause and with to the query using the Produkt relation
 * @method     ChildPotravinyQuery innerJoinWithProdukt() Adds a INNER JOIN clause and with to the query using the Produkt relation
 *
 * @method     ChildPotravinyQuery leftJoinUzivatel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Uzivatel relation
 * @method     ChildPotravinyQuery rightJoinUzivatel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Uzivatel relation
 * @method     ChildPotravinyQuery innerJoinUzivatel($relationAlias = null) Adds a INNER JOIN clause to the query using the Uzivatel relation
 *
 * @method     ChildPotravinyQuery joinWithUzivatel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Uzivatel relation
 *
 * @method     ChildPotravinyQuery leftJoinWithUzivatel() Adds a LEFT JOIN clause and with to the query using the Uzivatel relation
 * @method     ChildPotravinyQuery rightJoinWithUzivatel() Adds a RIGHT JOIN clause and with to the query using the Uzivatel relation
 * @method     ChildPotravinyQuery innerJoinWithUzivatel() Adds a INNER JOIN clause and with to the query using the Uzivatel relation
 *
 * @method     \SmartFridge\ProduktQuery|\SmartFridge\UzivatelQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPotraviny findOne(ConnectionInterface $con = null) Return the first ChildPotraviny matching the query
 * @method     ChildPotraviny findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPotraviny matching the query, or a new ChildPotraviny object populated from the query conditions when no match is found
 *
 * @method     ChildPotraviny findOneById(int $id) Return the first ChildPotraviny filtered by the id column
 * @method     ChildPotraviny findOneBySpotrebovatdo(string $spotrebovatDo) Return the first ChildPotraviny filtered by the spotrebovatDo column
 * @method     ChildPotraviny findOneByTrvanlivostdo(int $trvanlivostDo) Return the first ChildPotraviny filtered by the trvanlivostDo column
 * @method     ChildPotraviny findOneByUzivatelid(int $uzivatelID) Return the first ChildPotraviny filtered by the uzivatelID column
 * @method     ChildPotraviny findOneByProduktid(int $produktID) Return the first ChildPotraviny filtered by the produktID column
 * @method     ChildPotraviny findOneByPocet(int $pocet) Return the first ChildPotraviny filtered by the pocet column *

 * @method     ChildPotraviny requirePk($key, ConnectionInterface $con = null) Return the ChildPotraviny by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPotraviny requireOne(ConnectionInterface $con = null) Return the first ChildPotraviny matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPotraviny requireOneById(int $id) Return the first ChildPotraviny filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPotraviny requireOneBySpotrebovatdo(string $spotrebovatDo) Return the first ChildPotraviny filtered by the spotrebovatDo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPotraviny requireOneByTrvanlivostdo(int $trvanlivostDo) Return the first ChildPotraviny filtered by the trvanlivostDo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPotraviny requireOneByUzivatelid(int $uzivatelID) Return the first ChildPotraviny filtered by the uzivatelID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPotraviny requireOneByProduktid(int $produktID) Return the first ChildPotraviny filtered by the produktID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPotraviny requireOneByPocet(int $pocet) Return the first ChildPotraviny filtered by the pocet column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPotraviny[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPotraviny objects based on current ModelCriteria
 * @method     ChildPotraviny[]|ObjectCollection findById(int $id) Return ChildPotraviny objects filtered by the id column
 * @method     ChildPotraviny[]|ObjectCollection findBySpotrebovatdo(string $spotrebovatDo) Return ChildPotraviny objects filtered by the spotrebovatDo column
 * @method     ChildPotraviny[]|ObjectCollection findByTrvanlivostdo(int $trvanlivostDo) Return ChildPotraviny objects filtered by the trvanlivostDo column
 * @method     ChildPotraviny[]|ObjectCollection findByUzivatelid(int $uzivatelID) Return ChildPotraviny objects filtered by the uzivatelID column
 * @method     ChildPotraviny[]|ObjectCollection findByProduktid(int $produktID) Return ChildPotraviny objects filtered by the produktID column
 * @method     ChildPotraviny[]|ObjectCollection findByPocet(int $pocet) Return ChildPotraviny objects filtered by the pocet column
 * @method     ChildPotraviny[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PotravinyQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SmartFridge\Base\PotravinyQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SmartFridge\\Potraviny', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPotravinyQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPotravinyQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPotravinyQuery) {
            return $criteria;
        }
        $query = new ChildPotravinyQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPotraviny|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PotravinyTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PotravinyTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPotraviny A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, spotrebovatDo, trvanlivostDo, uzivatelID, produktID, pocet FROM Potraviny WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPotraviny $obj */
            $obj = new ChildPotraviny();
            $obj->hydrate($row);
            PotravinyTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPotraviny|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PotravinyTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PotravinyTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PotravinyTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the spotrebovatDo column
     *
     * Example usage:
     * <code>
     * $query->filterBySpotrebovatdo('2011-03-14'); // WHERE spotrebovatDo = '2011-03-14'
     * $query->filterBySpotrebovatdo('now'); // WHERE spotrebovatDo = '2011-03-14'
     * $query->filterBySpotrebovatdo(array('max' => 'yesterday')); // WHERE spotrebovatDo > '2011-03-13'
     * </code>
     *
     * @param     mixed $spotrebovatdo The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function filterBySpotrebovatdo($spotrebovatdo = null, $comparison = null)
    {
        if (is_array($spotrebovatdo)) {
            $useMinMax = false;
            if (isset($spotrebovatdo['min'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_SPOTREBOVATDO, $spotrebovatdo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($spotrebovatdo['max'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_SPOTREBOVATDO, $spotrebovatdo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PotravinyTableMap::COL_SPOTREBOVATDO, $spotrebovatdo, $comparison);
    }

    /**
     * Filter the query on the trvanlivostDo column
     *
     * Example usage:
     * <code>
     * $query->filterByTrvanlivostdo(1234); // WHERE trvanlivostDo = 1234
     * $query->filterByTrvanlivostdo(array(12, 34)); // WHERE trvanlivostDo IN (12, 34)
     * $query->filterByTrvanlivostdo(array('min' => 12)); // WHERE trvanlivostDo > 12
     * </code>
     *
     * @param     mixed $trvanlivostdo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function filterByTrvanlivostdo($trvanlivostdo = null, $comparison = null)
    {
        if (is_array($trvanlivostdo)) {
            $useMinMax = false;
            if (isset($trvanlivostdo['min'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_TRVANLIVOSTDO, $trvanlivostdo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($trvanlivostdo['max'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_TRVANLIVOSTDO, $trvanlivostdo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PotravinyTableMap::COL_TRVANLIVOSTDO, $trvanlivostdo, $comparison);
    }

    /**
     * Filter the query on the uzivatelID column
     *
     * Example usage:
     * <code>
     * $query->filterByUzivatelid(1234); // WHERE uzivatelID = 1234
     * $query->filterByUzivatelid(array(12, 34)); // WHERE uzivatelID IN (12, 34)
     * $query->filterByUzivatelid(array('min' => 12)); // WHERE uzivatelID > 12
     * </code>
     *
     * @see       filterByUzivatel()
     *
     * @param     mixed $uzivatelid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function filterByUzivatelid($uzivatelid = null, $comparison = null)
    {
        if (is_array($uzivatelid)) {
            $useMinMax = false;
            if (isset($uzivatelid['min'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_UZIVATELID, $uzivatelid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uzivatelid['max'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_UZIVATELID, $uzivatelid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PotravinyTableMap::COL_UZIVATELID, $uzivatelid, $comparison);
    }

    /**
     * Filter the query on the produktID column
     *
     * Example usage:
     * <code>
     * $query->filterByProduktid(1234); // WHERE produktID = 1234
     * $query->filterByProduktid(array(12, 34)); // WHERE produktID IN (12, 34)
     * $query->filterByProduktid(array('min' => 12)); // WHERE produktID > 12
     * </code>
     *
     * @see       filterByProdukt()
     *
     * @param     mixed $produktid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function filterByProduktid($produktid = null, $comparison = null)
    {
        if (is_array($produktid)) {
            $useMinMax = false;
            if (isset($produktid['min'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_PRODUKTID, $produktid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produktid['max'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_PRODUKTID, $produktid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PotravinyTableMap::COL_PRODUKTID, $produktid, $comparison);
    }

    /**
     * Filter the query on the pocet column
     *
     * Example usage:
     * <code>
     * $query->filterByPocet(1234); // WHERE pocet = 1234
     * $query->filterByPocet(array(12, 34)); // WHERE pocet IN (12, 34)
     * $query->filterByPocet(array('min' => 12)); // WHERE pocet > 12
     * </code>
     *
     * @param     mixed $pocet The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function filterByPocet($pocet = null, $comparison = null)
    {
        if (is_array($pocet)) {
            $useMinMax = false;
            if (isset($pocet['min'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_POCET, $pocet['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pocet['max'])) {
                $this->addUsingAlias(PotravinyTableMap::COL_POCET, $pocet['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PotravinyTableMap::COL_POCET, $pocet, $comparison);
    }

    /**
     * Filter the query by a related \SmartFridge\Produkt object
     *
     * @param \SmartFridge\Produkt|ObjectCollection $produkt The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPotravinyQuery The current query, for fluid interface
     */
    public function filterByProdukt($produkt, $comparison = null)
    {
        if ($produkt instanceof \SmartFridge\Produkt) {
            return $this
                ->addUsingAlias(PotravinyTableMap::COL_PRODUKTID, $produkt->getId(), $comparison);
        } elseif ($produkt instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PotravinyTableMap::COL_PRODUKTID, $produkt->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProdukt() only accepts arguments of type \SmartFridge\Produkt or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Produkt relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function joinProdukt($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Produkt');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Produkt');
        }

        return $this;
    }

    /**
     * Use the Produkt relation Produkt object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\ProduktQuery A secondary query class using the current class as primary query
     */
    public function useProduktQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProdukt($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Produkt', '\SmartFridge\ProduktQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Uzivatel object
     *
     * @param \SmartFridge\Uzivatel|ObjectCollection $uzivatel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPotravinyQuery The current query, for fluid interface
     */
    public function filterByUzivatel($uzivatel, $comparison = null)
    {
        if ($uzivatel instanceof \SmartFridge\Uzivatel) {
            return $this
                ->addUsingAlias(PotravinyTableMap::COL_UZIVATELID, $uzivatel->getId(), $comparison);
        } elseif ($uzivatel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PotravinyTableMap::COL_UZIVATELID, $uzivatel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUzivatel() only accepts arguments of type \SmartFridge\Uzivatel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Uzivatel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function joinUzivatel($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Uzivatel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Uzivatel');
        }

        return $this;
    }

    /**
     * Use the Uzivatel relation Uzivatel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\UzivatelQuery A secondary query class using the current class as primary query
     */
    public function useUzivatelQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUzivatel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Uzivatel', '\SmartFridge\UzivatelQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPotraviny $potraviny Object to remove from the list of results
     *
     * @return $this|ChildPotravinyQuery The current query, for fluid interface
     */
    public function prune($potraviny = null)
    {
        if ($potraviny) {
            $this->addUsingAlias(PotravinyTableMap::COL_ID, $potraviny->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Potraviny table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PotravinyTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PotravinyTableMap::clearInstancePool();
            PotravinyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PotravinyTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PotravinyTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PotravinyTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PotravinyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PotravinyQuery
