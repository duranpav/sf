<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use SmartFridge\Objednavka as ChildObjednavka;
use SmartFridge\ObjednavkaQuery as ChildObjednavkaQuery;
use SmartFridge\Potraviny as ChildPotraviny;
use SmartFridge\PotravinyQuery as ChildPotravinyQuery;
use SmartFridge\Recenze as ChildRecenze;
use SmartFridge\RecenzeQuery as ChildRecenzeQuery;
use SmartFridge\Upozorneni as ChildUpozorneni;
use SmartFridge\UpozorneniQuery as ChildUpozorneniQuery;
use SmartFridge\Uzivatel as ChildUzivatel;
use SmartFridge\UzivatelQuery as ChildUzivatelQuery;
use SmartFridge\Watchdog as ChildWatchdog;
use SmartFridge\WatchdogQuery as ChildWatchdogQuery;
use SmartFridge\Map\ObjednavkaTableMap;
use SmartFridge\Map\PotravinyTableMap;
use SmartFridge\Map\RecenzeTableMap;
use SmartFridge\Map\UpozorneniTableMap;
use SmartFridge\Map\UzivatelTableMap;
use SmartFridge\Map\WatchdogTableMap;

/**
 * Base class that represents a row from the 'Uzivatel' table.
 *
 *
 *
 * @package    propel.generator.SmartFridge.Base
 */
abstract class Uzivatel implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\SmartFridge\\Map\\UzivatelTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the prezdivka field.
     *
     * @var        string
     */
    protected $prezdivka;

    /**
     * The value for the heslo field.
     *
     * @var        string
     */
    protected $heslo;

    /**
     * The value for the jmeno field.
     *
     * @var        string
     */
    protected $jmeno;

    /**
     * The value for the prijmeni field.
     *
     * @var        string
     */
    protected $prijmeni;

    /**
     * The value for the ulice field.
     *
     * @var        string
     */
    protected $ulice;

    /**
     * The value for the mesto field.
     *
     * @var        string
     */
    protected $mesto;

    /**
     * The value for the cislopopisne field.
     *
     * @var        string
     */
    protected $cislopopisne;

    /**
     * The value for the postovnismerovacicislo field.
     *
     * @var        string
     */
    protected $postovnismerovacicislo;

    /**
     * The value for the role field.
     *
     * @var        string
     */
    protected $role;

    /**
     * @var        ObjectCollection|ChildObjednavka[] Collection to store aggregation of ChildObjednavka objects.
     */
    protected $collObjednavkas;
    protected $collObjednavkasPartial;

    /**
     * @var        ObjectCollection|ChildPotraviny[] Collection to store aggregation of ChildPotraviny objects.
     */
    protected $collPotravinies;
    protected $collPotraviniesPartial;

    /**
     * @var        ObjectCollection|ChildRecenze[] Collection to store aggregation of ChildRecenze objects.
     */
    protected $collRecenzes;
    protected $collRecenzesPartial;

    /**
     * @var        ObjectCollection|ChildUpozorneni[] Collection to store aggregation of ChildUpozorneni objects.
     */
    protected $collUpozornenis;
    protected $collUpozornenisPartial;

    /**
     * @var        ObjectCollection|ChildWatchdog[] Collection to store aggregation of ChildWatchdog objects.
     */
    protected $collWatchdogs;
    protected $collWatchdogsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildObjednavka[]
     */
    protected $objednavkasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPotraviny[]
     */
    protected $potraviniesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRecenze[]
     */
    protected $recenzesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUpozorneni[]
     */
    protected $upozornenisScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildWatchdog[]
     */
    protected $watchdogsScheduledForDeletion = null;

    /**
     * Initializes internal state of SmartFridge\Base\Uzivatel object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Uzivatel</code> instance.  If
     * <code>obj</code> is an instance of <code>Uzivatel</code>, delegates to
     * <code>equals(Uzivatel)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Uzivatel The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [prezdivka] column value.
     *
     * @return string
     */
    public function getPrezdivka()
    {
        return $this->prezdivka;
    }

    /**
     * Get the [heslo] column value.
     *
     * @return string
     */
    public function getHeslo()
    {
        return $this->heslo;
    }

    /**
     * Get the [jmeno] column value.
     *
     * @return string
     */
    public function getJmeno()
    {
        return $this->jmeno;
    }

    /**
     * Get the [prijmeni] column value.
     *
     * @return string
     */
    public function getPrijmeni()
    {
        return $this->prijmeni;
    }

    /**
     * Get the [ulice] column value.
     *
     * @return string
     */
    public function getUlice()
    {
        return $this->ulice;
    }

    /**
     * Get the [mesto] column value.
     *
     * @return string
     */
    public function getMesto()
    {
        return $this->mesto;
    }

    /**
     * Get the [cislopopisne] column value.
     *
     * @return string
     */
    public function getCislopopisne()
    {
        return $this->cislopopisne;
    }

    /**
     * Get the [postovnismerovacicislo] column value.
     *
     * @return string
     */
    public function getPostovnismerovacicislo()
    {
        return $this->postovnismerovacicislo;
    }

    /**
     * Get the [role] column value.
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UzivatelTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [prezdivka] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function setPrezdivka($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prezdivka !== $v) {
            $this->prezdivka = $v;
            $this->modifiedColumns[UzivatelTableMap::COL_PREZDIVKA] = true;
        }

        return $this;
    } // setPrezdivka()

    /**
     * Set the value of [heslo] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function setHeslo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->heslo !== $v) {
            $this->heslo = $v;
            $this->modifiedColumns[UzivatelTableMap::COL_HESLO] = true;
        }

        return $this;
    } // setHeslo()

    /**
     * Set the value of [jmeno] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function setJmeno($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->jmeno !== $v) {
            $this->jmeno = $v;
            $this->modifiedColumns[UzivatelTableMap::COL_JMENO] = true;
        }

        return $this;
    } // setJmeno()

    /**
     * Set the value of [prijmeni] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function setPrijmeni($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prijmeni !== $v) {
            $this->prijmeni = $v;
            $this->modifiedColumns[UzivatelTableMap::COL_PRIJMENI] = true;
        }

        return $this;
    } // setPrijmeni()

    /**
     * Set the value of [ulice] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function setUlice($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ulice !== $v) {
            $this->ulice = $v;
            $this->modifiedColumns[UzivatelTableMap::COL_ULICE] = true;
        }

        return $this;
    } // setUlice()

    /**
     * Set the value of [mesto] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function setMesto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mesto !== $v) {
            $this->mesto = $v;
            $this->modifiedColumns[UzivatelTableMap::COL_MESTO] = true;
        }

        return $this;
    } // setMesto()

    /**
     * Set the value of [cislopopisne] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function setCislopopisne($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cislopopisne !== $v) {
            $this->cislopopisne = $v;
            $this->modifiedColumns[UzivatelTableMap::COL_CISLOPOPISNE] = true;
        }

        return $this;
    } // setCislopopisne()

    /**
     * Set the value of [postovnismerovacicislo] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function setPostovnismerovacicislo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->postovnismerovacicislo !== $v) {
            $this->postovnismerovacicislo = $v;
            $this->modifiedColumns[UzivatelTableMap::COL_POSTOVNISMEROVACICISLO] = true;
        }

        return $this;
    } // setPostovnismerovacicislo()

    /**
     * Set the value of [role] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function setRole($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->role !== $v) {
            $this->role = $v;
            $this->modifiedColumns[UzivatelTableMap::COL_ROLE] = true;
        }

        return $this;
    } // setRole()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UzivatelTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UzivatelTableMap::translateFieldName('Prezdivka', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prezdivka = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UzivatelTableMap::translateFieldName('Heslo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->heslo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UzivatelTableMap::translateFieldName('Jmeno', TableMap::TYPE_PHPNAME, $indexType)];
            $this->jmeno = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UzivatelTableMap::translateFieldName('Prijmeni', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prijmeni = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UzivatelTableMap::translateFieldName('Ulice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ulice = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UzivatelTableMap::translateFieldName('Mesto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mesto = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UzivatelTableMap::translateFieldName('Cislopopisne', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cislopopisne = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UzivatelTableMap::translateFieldName('Postovnismerovacicislo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->postovnismerovacicislo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UzivatelTableMap::translateFieldName('Role', TableMap::TYPE_PHPNAME, $indexType)];
            $this->role = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = UzivatelTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\SmartFridge\\Uzivatel'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UzivatelTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUzivatelQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collObjednavkas = null;

            $this->collPotravinies = null;

            $this->collRecenzes = null;

            $this->collUpozornenis = null;

            $this->collWatchdogs = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Uzivatel::setDeleted()
     * @see Uzivatel::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UzivatelTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUzivatelQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UzivatelTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UzivatelTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->objednavkasScheduledForDeletion !== null) {
                if (!$this->objednavkasScheduledForDeletion->isEmpty()) {
                    foreach ($this->objednavkasScheduledForDeletion as $objednavka) {
                        // need to save related object because we set the relation to null
                        $objednavka->save($con);
                    }
                    $this->objednavkasScheduledForDeletion = null;
                }
            }

            if ($this->collObjednavkas !== null) {
                foreach ($this->collObjednavkas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->potraviniesScheduledForDeletion !== null) {
                if (!$this->potraviniesScheduledForDeletion->isEmpty()) {
                    foreach ($this->potraviniesScheduledForDeletion as $potraviny) {
                        // need to save related object because we set the relation to null
                        $potraviny->save($con);
                    }
                    $this->potraviniesScheduledForDeletion = null;
                }
            }

            if ($this->collPotravinies !== null) {
                foreach ($this->collPotravinies as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->recenzesScheduledForDeletion !== null) {
                if (!$this->recenzesScheduledForDeletion->isEmpty()) {
                    foreach ($this->recenzesScheduledForDeletion as $recenze) {
                        // need to save related object because we set the relation to null
                        $recenze->save($con);
                    }
                    $this->recenzesScheduledForDeletion = null;
                }
            }

            if ($this->collRecenzes !== null) {
                foreach ($this->collRecenzes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->upozornenisScheduledForDeletion !== null) {
                if (!$this->upozornenisScheduledForDeletion->isEmpty()) {
                    foreach ($this->upozornenisScheduledForDeletion as $upozorneni) {
                        // need to save related object because we set the relation to null
                        $upozorneni->save($con);
                    }
                    $this->upozornenisScheduledForDeletion = null;
                }
            }

            if ($this->collUpozornenis !== null) {
                foreach ($this->collUpozornenis as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->watchdogsScheduledForDeletion !== null) {
                if (!$this->watchdogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->watchdogsScheduledForDeletion as $watchdog) {
                        // need to save related object because we set the relation to null
                        $watchdog->save($con);
                    }
                    $this->watchdogsScheduledForDeletion = null;
                }
            }

            if ($this->collWatchdogs !== null) {
                foreach ($this->collWatchdogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UzivatelTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UzivatelTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UzivatelTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_PREZDIVKA)) {
            $modifiedColumns[':p' . $index++]  = 'prezdivka';
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_HESLO)) {
            $modifiedColumns[':p' . $index++]  = 'heslo';
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_JMENO)) {
            $modifiedColumns[':p' . $index++]  = 'jmeno';
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_PRIJMENI)) {
            $modifiedColumns[':p' . $index++]  = 'prijmeni';
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_ULICE)) {
            $modifiedColumns[':p' . $index++]  = 'ulice';
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_MESTO)) {
            $modifiedColumns[':p' . $index++]  = 'mesto';
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_CISLOPOPISNE)) {
            $modifiedColumns[':p' . $index++]  = 'cisloPopisne';
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_POSTOVNISMEROVACICISLO)) {
            $modifiedColumns[':p' . $index++]  = 'PostovniSmerovaciCislo';
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_ROLE)) {
            $modifiedColumns[':p' . $index++]  = 'role';
        }

        $sql = sprintf(
            'INSERT INTO Uzivatel (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'prezdivka':
                        $stmt->bindValue($identifier, $this->prezdivka, PDO::PARAM_STR);
                        break;
                    case 'heslo':
                        $stmt->bindValue($identifier, $this->heslo, PDO::PARAM_STR);
                        break;
                    case 'jmeno':
                        $stmt->bindValue($identifier, $this->jmeno, PDO::PARAM_STR);
                        break;
                    case 'prijmeni':
                        $stmt->bindValue($identifier, $this->prijmeni, PDO::PARAM_STR);
                        break;
                    case 'ulice':
                        $stmt->bindValue($identifier, $this->ulice, PDO::PARAM_STR);
                        break;
                    case 'mesto':
                        $stmt->bindValue($identifier, $this->mesto, PDO::PARAM_STR);
                        break;
                    case 'cisloPopisne':
                        $stmt->bindValue($identifier, $this->cislopopisne, PDO::PARAM_STR);
                        break;
                    case 'PostovniSmerovaciCislo':
                        $stmt->bindValue($identifier, $this->postovnismerovacicislo, PDO::PARAM_STR);
                        break;
                    case 'role':
                        $stmt->bindValue($identifier, $this->role, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UzivatelTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getPrezdivka();
                break;
            case 2:
                return $this->getHeslo();
                break;
            case 3:
                return $this->getJmeno();
                break;
            case 4:
                return $this->getPrijmeni();
                break;
            case 5:
                return $this->getUlice();
                break;
            case 6:
                return $this->getMesto();
                break;
            case 7:
                return $this->getCislopopisne();
                break;
            case 8:
                return $this->getPostovnismerovacicislo();
                break;
            case 9:
                return $this->getRole();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Uzivatel'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Uzivatel'][$this->hashCode()] = true;
        $keys = UzivatelTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getPrezdivka(),
            $keys[2] => $this->getHeslo(),
            $keys[3] => $this->getJmeno(),
            $keys[4] => $this->getPrijmeni(),
            $keys[5] => $this->getUlice(),
            $keys[6] => $this->getMesto(),
            $keys[7] => $this->getCislopopisne(),
            $keys[8] => $this->getPostovnismerovacicislo(),
            $keys[9] => $this->getRole(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collObjednavkas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'objednavkas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Objednavkas';
                        break;
                    default:
                        $key = 'Objednavkas';
                }

                $result[$key] = $this->collObjednavkas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPotravinies) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'potravinies';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Potravinies';
                        break;
                    default:
                        $key = 'Potravinies';
                }

                $result[$key] = $this->collPotravinies->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRecenzes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'recenzes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Recenzes';
                        break;
                    default:
                        $key = 'Recenzes';
                }

                $result[$key] = $this->collRecenzes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUpozornenis) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'upozornenis';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Upozornenis';
                        break;
                    default:
                        $key = 'Upozornenis';
                }

                $result[$key] = $this->collUpozornenis->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWatchdogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'watchdogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Watchdogs';
                        break;
                    default:
                        $key = 'Watchdogs';
                }

                $result[$key] = $this->collWatchdogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\SmartFridge\Uzivatel
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UzivatelTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\SmartFridge\Uzivatel
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setPrezdivka($value);
                break;
            case 2:
                $this->setHeslo($value);
                break;
            case 3:
                $this->setJmeno($value);
                break;
            case 4:
                $this->setPrijmeni($value);
                break;
            case 5:
                $this->setUlice($value);
                break;
            case 6:
                $this->setMesto($value);
                break;
            case 7:
                $this->setCislopopisne($value);
                break;
            case 8:
                $this->setPostovnismerovacicislo($value);
                break;
            case 9:
                $this->setRole($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UzivatelTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPrezdivka($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setHeslo($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setJmeno($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPrijmeni($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setUlice($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setMesto($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCislopopisne($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setPostovnismerovacicislo($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setRole($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\SmartFridge\Uzivatel The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UzivatelTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UzivatelTableMap::COL_ID)) {
            $criteria->add(UzivatelTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_PREZDIVKA)) {
            $criteria->add(UzivatelTableMap::COL_PREZDIVKA, $this->prezdivka);
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_HESLO)) {
            $criteria->add(UzivatelTableMap::COL_HESLO, $this->heslo);
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_JMENO)) {
            $criteria->add(UzivatelTableMap::COL_JMENO, $this->jmeno);
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_PRIJMENI)) {
            $criteria->add(UzivatelTableMap::COL_PRIJMENI, $this->prijmeni);
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_ULICE)) {
            $criteria->add(UzivatelTableMap::COL_ULICE, $this->ulice);
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_MESTO)) {
            $criteria->add(UzivatelTableMap::COL_MESTO, $this->mesto);
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_CISLOPOPISNE)) {
            $criteria->add(UzivatelTableMap::COL_CISLOPOPISNE, $this->cislopopisne);
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_POSTOVNISMEROVACICISLO)) {
            $criteria->add(UzivatelTableMap::COL_POSTOVNISMEROVACICISLO, $this->postovnismerovacicislo);
        }
        if ($this->isColumnModified(UzivatelTableMap::COL_ROLE)) {
            $criteria->add(UzivatelTableMap::COL_ROLE, $this->role);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUzivatelQuery::create();
        $criteria->add(UzivatelTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \SmartFridge\Uzivatel (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPrezdivka($this->getPrezdivka());
        $copyObj->setHeslo($this->getHeslo());
        $copyObj->setJmeno($this->getJmeno());
        $copyObj->setPrijmeni($this->getPrijmeni());
        $copyObj->setUlice($this->getUlice());
        $copyObj->setMesto($this->getMesto());
        $copyObj->setCislopopisne($this->getCislopopisne());
        $copyObj->setPostovnismerovacicislo($this->getPostovnismerovacicislo());
        $copyObj->setRole($this->getRole());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getObjednavkas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addObjednavka($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPotravinies() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPotraviny($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRecenzes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRecenze($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUpozornenis() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUpozorneni($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWatchdogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWatchdog($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \SmartFridge\Uzivatel Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Objednavka' == $relationName) {
            return $this->initObjednavkas();
        }
        if ('Potraviny' == $relationName) {
            return $this->initPotravinies();
        }
        if ('Recenze' == $relationName) {
            return $this->initRecenzes();
        }
        if ('Upozorneni' == $relationName) {
            return $this->initUpozornenis();
        }
        if ('Watchdog' == $relationName) {
            return $this->initWatchdogs();
        }
    }

    /**
     * Clears out the collObjednavkas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addObjednavkas()
     */
    public function clearObjednavkas()
    {
        $this->collObjednavkas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collObjednavkas collection loaded partially.
     */
    public function resetPartialObjednavkas($v = true)
    {
        $this->collObjednavkasPartial = $v;
    }

    /**
     * Initializes the collObjednavkas collection.
     *
     * By default this just sets the collObjednavkas collection to an empty array (like clearcollObjednavkas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initObjednavkas($overrideExisting = true)
    {
        if (null !== $this->collObjednavkas && !$overrideExisting) {
            return;
        }

        $collectionClassName = ObjednavkaTableMap::getTableMap()->getCollectionClassName();

        $this->collObjednavkas = new $collectionClassName;
        $this->collObjednavkas->setModel('\SmartFridge\Objednavka');
    }

    /**
     * Gets an array of ChildObjednavka objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUzivatel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildObjednavka[] List of ChildObjednavka objects
     * @throws PropelException
     */
    public function getObjednavkas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collObjednavkasPartial && !$this->isNew();
        if (null === $this->collObjednavkas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collObjednavkas) {
                // return empty collection
                $this->initObjednavkas();
            } else {
                $collObjednavkas = ChildObjednavkaQuery::create(null, $criteria)
                    ->filterByUzivatel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collObjednavkasPartial && count($collObjednavkas)) {
                        $this->initObjednavkas(false);

                        foreach ($collObjednavkas as $obj) {
                            if (false == $this->collObjednavkas->contains($obj)) {
                                $this->collObjednavkas->append($obj);
                            }
                        }

                        $this->collObjednavkasPartial = true;
                    }

                    return $collObjednavkas;
                }

                if ($partial && $this->collObjednavkas) {
                    foreach ($this->collObjednavkas as $obj) {
                        if ($obj->isNew()) {
                            $collObjednavkas[] = $obj;
                        }
                    }
                }

                $this->collObjednavkas = $collObjednavkas;
                $this->collObjednavkasPartial = false;
            }
        }

        return $this->collObjednavkas;
    }

    /**
     * Sets a collection of ChildObjednavka objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $objednavkas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUzivatel The current object (for fluent API support)
     */
    public function setObjednavkas(Collection $objednavkas, ConnectionInterface $con = null)
    {
        /** @var ChildObjednavka[] $objednavkasToDelete */
        $objednavkasToDelete = $this->getObjednavkas(new Criteria(), $con)->diff($objednavkas);


        $this->objednavkasScheduledForDeletion = $objednavkasToDelete;

        foreach ($objednavkasToDelete as $objednavkaRemoved) {
            $objednavkaRemoved->setUzivatel(null);
        }

        $this->collObjednavkas = null;
        foreach ($objednavkas as $objednavka) {
            $this->addObjednavka($objednavka);
        }

        $this->collObjednavkas = $objednavkas;
        $this->collObjednavkasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Objednavka objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Objednavka objects.
     * @throws PropelException
     */
    public function countObjednavkas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collObjednavkasPartial && !$this->isNew();
        if (null === $this->collObjednavkas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collObjednavkas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getObjednavkas());
            }

            $query = ChildObjednavkaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUzivatel($this)
                ->count($con);
        }

        return count($this->collObjednavkas);
    }

    /**
     * Method called to associate a ChildObjednavka object to this object
     * through the ChildObjednavka foreign key attribute.
     *
     * @param  ChildObjednavka $l ChildObjednavka
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function addObjednavka(ChildObjednavka $l)
    {
        if ($this->collObjednavkas === null) {
            $this->initObjednavkas();
            $this->collObjednavkasPartial = true;
        }

        if (!$this->collObjednavkas->contains($l)) {
            $this->doAddObjednavka($l);

            if ($this->objednavkasScheduledForDeletion and $this->objednavkasScheduledForDeletion->contains($l)) {
                $this->objednavkasScheduledForDeletion->remove($this->objednavkasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildObjednavka $objednavka The ChildObjednavka object to add.
     */
    protected function doAddObjednavka(ChildObjednavka $objednavka)
    {
        $this->collObjednavkas[]= $objednavka;
        $objednavka->setUzivatel($this);
    }

    /**
     * @param  ChildObjednavka $objednavka The ChildObjednavka object to remove.
     * @return $this|ChildUzivatel The current object (for fluent API support)
     */
    public function removeObjednavka(ChildObjednavka $objednavka)
    {
        if ($this->getObjednavkas()->contains($objednavka)) {
            $pos = $this->collObjednavkas->search($objednavka);
            $this->collObjednavkas->remove($pos);
            if (null === $this->objednavkasScheduledForDeletion) {
                $this->objednavkasScheduledForDeletion = clone $this->collObjednavkas;
                $this->objednavkasScheduledForDeletion->clear();
            }
            $this->objednavkasScheduledForDeletion[]= $objednavka;
            $objednavka->setUzivatel(null);
        }

        return $this;
    }

    /**
     * Clears out the collPotravinies collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPotravinies()
     */
    public function clearPotravinies()
    {
        $this->collPotravinies = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPotravinies collection loaded partially.
     */
    public function resetPartialPotravinies($v = true)
    {
        $this->collPotraviniesPartial = $v;
    }

    /**
     * Initializes the collPotravinies collection.
     *
     * By default this just sets the collPotravinies collection to an empty array (like clearcollPotravinies());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPotravinies($overrideExisting = true)
    {
        if (null !== $this->collPotravinies && !$overrideExisting) {
            return;
        }

        $collectionClassName = PotravinyTableMap::getTableMap()->getCollectionClassName();

        $this->collPotravinies = new $collectionClassName;
        $this->collPotravinies->setModel('\SmartFridge\Potraviny');
    }

    /**
     * Gets an array of ChildPotraviny objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUzivatel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPotraviny[] List of ChildPotraviny objects
     * @throws PropelException
     */
    public function getPotravinies(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPotraviniesPartial && !$this->isNew();
        if (null === $this->collPotravinies || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPotravinies) {
                // return empty collection
                $this->initPotravinies();
            } else {
                $collPotravinies = ChildPotravinyQuery::create(null, $criteria)
                    ->filterByUzivatel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPotraviniesPartial && count($collPotravinies)) {
                        $this->initPotravinies(false);

                        foreach ($collPotravinies as $obj) {
                            if (false == $this->collPotravinies->contains($obj)) {
                                $this->collPotravinies->append($obj);
                            }
                        }

                        $this->collPotraviniesPartial = true;
                    }

                    return $collPotravinies;
                }

                if ($partial && $this->collPotravinies) {
                    foreach ($this->collPotravinies as $obj) {
                        if ($obj->isNew()) {
                            $collPotravinies[] = $obj;
                        }
                    }
                }

                $this->collPotravinies = $collPotravinies;
                $this->collPotraviniesPartial = false;
            }
        }

        return $this->collPotravinies;
    }

    /**
     * Sets a collection of ChildPotraviny objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $potravinies A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUzivatel The current object (for fluent API support)
     */
    public function setPotravinies(Collection $potravinies, ConnectionInterface $con = null)
    {
        /** @var ChildPotraviny[] $potraviniesToDelete */
        $potraviniesToDelete = $this->getPotravinies(new Criteria(), $con)->diff($potravinies);


        $this->potraviniesScheduledForDeletion = $potraviniesToDelete;

        foreach ($potraviniesToDelete as $potravinyRemoved) {
            $potravinyRemoved->setUzivatel(null);
        }

        $this->collPotravinies = null;
        foreach ($potravinies as $potraviny) {
            $this->addPotraviny($potraviny);
        }

        $this->collPotravinies = $potravinies;
        $this->collPotraviniesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Potraviny objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Potraviny objects.
     * @throws PropelException
     */
    public function countPotravinies(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPotraviniesPartial && !$this->isNew();
        if (null === $this->collPotravinies || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPotravinies) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPotravinies());
            }

            $query = ChildPotravinyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUzivatel($this)
                ->count($con);
        }

        return count($this->collPotravinies);
    }

    /**
     * Method called to associate a ChildPotraviny object to this object
     * through the ChildPotraviny foreign key attribute.
     *
     * @param  ChildPotraviny $l ChildPotraviny
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function addPotraviny(ChildPotraviny $l)
    {
        if ($this->collPotravinies === null) {
            $this->initPotravinies();
            $this->collPotraviniesPartial = true;
        }

        if (!$this->collPotravinies->contains($l)) {
            $this->doAddPotraviny($l);

            if ($this->potraviniesScheduledForDeletion and $this->potraviniesScheduledForDeletion->contains($l)) {
                $this->potraviniesScheduledForDeletion->remove($this->potraviniesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPotraviny $potraviny The ChildPotraviny object to add.
     */
    protected function doAddPotraviny(ChildPotraviny $potraviny)
    {
        $this->collPotravinies[]= $potraviny;
        $potraviny->setUzivatel($this);
    }

    /**
     * @param  ChildPotraviny $potraviny The ChildPotraviny object to remove.
     * @return $this|ChildUzivatel The current object (for fluent API support)
     */
    public function removePotraviny(ChildPotraviny $potraviny)
    {
        if ($this->getPotravinies()->contains($potraviny)) {
            $pos = $this->collPotravinies->search($potraviny);
            $this->collPotravinies->remove($pos);
            if (null === $this->potraviniesScheduledForDeletion) {
                $this->potraviniesScheduledForDeletion = clone $this->collPotravinies;
                $this->potraviniesScheduledForDeletion->clear();
            }
            $this->potraviniesScheduledForDeletion[]= $potraviny;
            $potraviny->setUzivatel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Uzivatel is new, it will return
     * an empty collection; or if this Uzivatel has previously
     * been saved, it will retrieve related Potravinies from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Uzivatel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPotraviny[] List of ChildPotraviny objects
     */
    public function getPotraviniesJoinProdukt(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPotravinyQuery::create(null, $criteria);
        $query->joinWith('Produkt', $joinBehavior);

        return $this->getPotravinies($query, $con);
    }

    /**
     * Clears out the collRecenzes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRecenzes()
     */
    public function clearRecenzes()
    {
        $this->collRecenzes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRecenzes collection loaded partially.
     */
    public function resetPartialRecenzes($v = true)
    {
        $this->collRecenzesPartial = $v;
    }

    /**
     * Initializes the collRecenzes collection.
     *
     * By default this just sets the collRecenzes collection to an empty array (like clearcollRecenzes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRecenzes($overrideExisting = true)
    {
        if (null !== $this->collRecenzes && !$overrideExisting) {
            return;
        }

        $collectionClassName = RecenzeTableMap::getTableMap()->getCollectionClassName();

        $this->collRecenzes = new $collectionClassName;
        $this->collRecenzes->setModel('\SmartFridge\Recenze');
    }

    /**
     * Gets an array of ChildRecenze objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUzivatel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRecenze[] List of ChildRecenze objects
     * @throws PropelException
     */
    public function getRecenzes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRecenzesPartial && !$this->isNew();
        if (null === $this->collRecenzes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRecenzes) {
                // return empty collection
                $this->initRecenzes();
            } else {
                $collRecenzes = ChildRecenzeQuery::create(null, $criteria)
                    ->filterByUzivatel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRecenzesPartial && count($collRecenzes)) {
                        $this->initRecenzes(false);

                        foreach ($collRecenzes as $obj) {
                            if (false == $this->collRecenzes->contains($obj)) {
                                $this->collRecenzes->append($obj);
                            }
                        }

                        $this->collRecenzesPartial = true;
                    }

                    return $collRecenzes;
                }

                if ($partial && $this->collRecenzes) {
                    foreach ($this->collRecenzes as $obj) {
                        if ($obj->isNew()) {
                            $collRecenzes[] = $obj;
                        }
                    }
                }

                $this->collRecenzes = $collRecenzes;
                $this->collRecenzesPartial = false;
            }
        }

        return $this->collRecenzes;
    }

    /**
     * Sets a collection of ChildRecenze objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $recenzes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUzivatel The current object (for fluent API support)
     */
    public function setRecenzes(Collection $recenzes, ConnectionInterface $con = null)
    {
        /** @var ChildRecenze[] $recenzesToDelete */
        $recenzesToDelete = $this->getRecenzes(new Criteria(), $con)->diff($recenzes);


        $this->recenzesScheduledForDeletion = $recenzesToDelete;

        foreach ($recenzesToDelete as $recenzeRemoved) {
            $recenzeRemoved->setUzivatel(null);
        }

        $this->collRecenzes = null;
        foreach ($recenzes as $recenze) {
            $this->addRecenze($recenze);
        }

        $this->collRecenzes = $recenzes;
        $this->collRecenzesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Recenze objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Recenze objects.
     * @throws PropelException
     */
    public function countRecenzes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRecenzesPartial && !$this->isNew();
        if (null === $this->collRecenzes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRecenzes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRecenzes());
            }

            $query = ChildRecenzeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUzivatel($this)
                ->count($con);
        }

        return count($this->collRecenzes);
    }

    /**
     * Method called to associate a ChildRecenze object to this object
     * through the ChildRecenze foreign key attribute.
     *
     * @param  ChildRecenze $l ChildRecenze
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function addRecenze(ChildRecenze $l)
    {
        if ($this->collRecenzes === null) {
            $this->initRecenzes();
            $this->collRecenzesPartial = true;
        }

        if (!$this->collRecenzes->contains($l)) {
            $this->doAddRecenze($l);

            if ($this->recenzesScheduledForDeletion and $this->recenzesScheduledForDeletion->contains($l)) {
                $this->recenzesScheduledForDeletion->remove($this->recenzesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRecenze $recenze The ChildRecenze object to add.
     */
    protected function doAddRecenze(ChildRecenze $recenze)
    {
        $this->collRecenzes[]= $recenze;
        $recenze->setUzivatel($this);
    }

    /**
     * @param  ChildRecenze $recenze The ChildRecenze object to remove.
     * @return $this|ChildUzivatel The current object (for fluent API support)
     */
    public function removeRecenze(ChildRecenze $recenze)
    {
        if ($this->getRecenzes()->contains($recenze)) {
            $pos = $this->collRecenzes->search($recenze);
            $this->collRecenzes->remove($pos);
            if (null === $this->recenzesScheduledForDeletion) {
                $this->recenzesScheduledForDeletion = clone $this->collRecenzes;
                $this->recenzesScheduledForDeletion->clear();
            }
            $this->recenzesScheduledForDeletion[]= $recenze;
            $recenze->setUzivatel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Uzivatel is new, it will return
     * an empty collection; or if this Uzivatel has previously
     * been saved, it will retrieve related Recenzes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Uzivatel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildRecenze[] List of ChildRecenze objects
     */
    public function getRecenzesJoinRecepty(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildRecenzeQuery::create(null, $criteria);
        $query->joinWith('Recepty', $joinBehavior);

        return $this->getRecenzes($query, $con);
    }

    /**
     * Clears out the collUpozornenis collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUpozornenis()
     */
    public function clearUpozornenis()
    {
        $this->collUpozornenis = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUpozornenis collection loaded partially.
     */
    public function resetPartialUpozornenis($v = true)
    {
        $this->collUpozornenisPartial = $v;
    }

    /**
     * Initializes the collUpozornenis collection.
     *
     * By default this just sets the collUpozornenis collection to an empty array (like clearcollUpozornenis());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUpozornenis($overrideExisting = true)
    {
        if (null !== $this->collUpozornenis && !$overrideExisting) {
            return;
        }

        $collectionClassName = UpozorneniTableMap::getTableMap()->getCollectionClassName();

        $this->collUpozornenis = new $collectionClassName;
        $this->collUpozornenis->setModel('\SmartFridge\Upozorneni');
    }

    /**
     * Gets an array of ChildUpozorneni objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUzivatel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUpozorneni[] List of ChildUpozorneni objects
     * @throws PropelException
     */
    public function getUpozornenis(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUpozornenisPartial && !$this->isNew();
        if (null === $this->collUpozornenis || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUpozornenis) {
                // return empty collection
                $this->initUpozornenis();
            } else {
                $collUpozornenis = ChildUpozorneniQuery::create(null, $criteria)
                    ->filterByUzivatel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUpozornenisPartial && count($collUpozornenis)) {
                        $this->initUpozornenis(false);

                        foreach ($collUpozornenis as $obj) {
                            if (false == $this->collUpozornenis->contains($obj)) {
                                $this->collUpozornenis->append($obj);
                            }
                        }

                        $this->collUpozornenisPartial = true;
                    }

                    return $collUpozornenis;
                }

                if ($partial && $this->collUpozornenis) {
                    foreach ($this->collUpozornenis as $obj) {
                        if ($obj->isNew()) {
                            $collUpozornenis[] = $obj;
                        }
                    }
                }

                $this->collUpozornenis = $collUpozornenis;
                $this->collUpozornenisPartial = false;
            }
        }

        return $this->collUpozornenis;
    }

    /**
     * Sets a collection of ChildUpozorneni objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $upozornenis A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUzivatel The current object (for fluent API support)
     */
    public function setUpozornenis(Collection $upozornenis, ConnectionInterface $con = null)
    {
        /** @var ChildUpozorneni[] $upozornenisToDelete */
        $upozornenisToDelete = $this->getUpozornenis(new Criteria(), $con)->diff($upozornenis);


        $this->upozornenisScheduledForDeletion = $upozornenisToDelete;

        foreach ($upozornenisToDelete as $upozorneniRemoved) {
            $upozorneniRemoved->setUzivatel(null);
        }

        $this->collUpozornenis = null;
        foreach ($upozornenis as $upozorneni) {
            $this->addUpozorneni($upozorneni);
        }

        $this->collUpozornenis = $upozornenis;
        $this->collUpozornenisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Upozorneni objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Upozorneni objects.
     * @throws PropelException
     */
    public function countUpozornenis(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUpozornenisPartial && !$this->isNew();
        if (null === $this->collUpozornenis || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUpozornenis) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUpozornenis());
            }

            $query = ChildUpozorneniQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUzivatel($this)
                ->count($con);
        }

        return count($this->collUpozornenis);
    }

    /**
     * Method called to associate a ChildUpozorneni object to this object
     * through the ChildUpozorneni foreign key attribute.
     *
     * @param  ChildUpozorneni $l ChildUpozorneni
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function addUpozorneni(ChildUpozorneni $l)
    {
        if ($this->collUpozornenis === null) {
            $this->initUpozornenis();
            $this->collUpozornenisPartial = true;
        }

        if (!$this->collUpozornenis->contains($l)) {
            $this->doAddUpozorneni($l);

            if ($this->upozornenisScheduledForDeletion and $this->upozornenisScheduledForDeletion->contains($l)) {
                $this->upozornenisScheduledForDeletion->remove($this->upozornenisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUpozorneni $upozorneni The ChildUpozorneni object to add.
     */
    protected function doAddUpozorneni(ChildUpozorneni $upozorneni)
    {
        $this->collUpozornenis[]= $upozorneni;
        $upozorneni->setUzivatel($this);
    }

    /**
     * @param  ChildUpozorneni $upozorneni The ChildUpozorneni object to remove.
     * @return $this|ChildUzivatel The current object (for fluent API support)
     */
    public function removeUpozorneni(ChildUpozorneni $upozorneni)
    {
        if ($this->getUpozornenis()->contains($upozorneni)) {
            $pos = $this->collUpozornenis->search($upozorneni);
            $this->collUpozornenis->remove($pos);
            if (null === $this->upozornenisScheduledForDeletion) {
                $this->upozornenisScheduledForDeletion = clone $this->collUpozornenis;
                $this->upozornenisScheduledForDeletion->clear();
            }
            $this->upozornenisScheduledForDeletion[]= $upozorneni;
            $upozorneni->setUzivatel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Uzivatel is new, it will return
     * an empty collection; or if this Uzivatel has previously
     * been saved, it will retrieve related Upozornenis from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Uzivatel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUpozorneni[] List of ChildUpozorneni objects
     */
    public function getUpozornenisJoinProdukt(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUpozorneniQuery::create(null, $criteria);
        $query->joinWith('Produkt', $joinBehavior);

        return $this->getUpozornenis($query, $con);
    }

    /**
     * Clears out the collWatchdogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addWatchdogs()
     */
    public function clearWatchdogs()
    {
        $this->collWatchdogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collWatchdogs collection loaded partially.
     */
    public function resetPartialWatchdogs($v = true)
    {
        $this->collWatchdogsPartial = $v;
    }

    /**
     * Initializes the collWatchdogs collection.
     *
     * By default this just sets the collWatchdogs collection to an empty array (like clearcollWatchdogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWatchdogs($overrideExisting = true)
    {
        if (null !== $this->collWatchdogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = WatchdogTableMap::getTableMap()->getCollectionClassName();

        $this->collWatchdogs = new $collectionClassName;
        $this->collWatchdogs->setModel('\SmartFridge\Watchdog');
    }

    /**
     * Gets an array of ChildWatchdog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUzivatel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildWatchdog[] List of ChildWatchdog objects
     * @throws PropelException
     */
    public function getWatchdogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collWatchdogsPartial && !$this->isNew();
        if (null === $this->collWatchdogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWatchdogs) {
                // return empty collection
                $this->initWatchdogs();
            } else {
                $collWatchdogs = ChildWatchdogQuery::create(null, $criteria)
                    ->filterByUzivatel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collWatchdogsPartial && count($collWatchdogs)) {
                        $this->initWatchdogs(false);

                        foreach ($collWatchdogs as $obj) {
                            if (false == $this->collWatchdogs->contains($obj)) {
                                $this->collWatchdogs->append($obj);
                            }
                        }

                        $this->collWatchdogsPartial = true;
                    }

                    return $collWatchdogs;
                }

                if ($partial && $this->collWatchdogs) {
                    foreach ($this->collWatchdogs as $obj) {
                        if ($obj->isNew()) {
                            $collWatchdogs[] = $obj;
                        }
                    }
                }

                $this->collWatchdogs = $collWatchdogs;
                $this->collWatchdogsPartial = false;
            }
        }

        return $this->collWatchdogs;
    }

    /**
     * Sets a collection of ChildWatchdog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $watchdogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUzivatel The current object (for fluent API support)
     */
    public function setWatchdogs(Collection $watchdogs, ConnectionInterface $con = null)
    {
        /** @var ChildWatchdog[] $watchdogsToDelete */
        $watchdogsToDelete = $this->getWatchdogs(new Criteria(), $con)->diff($watchdogs);


        $this->watchdogsScheduledForDeletion = $watchdogsToDelete;

        foreach ($watchdogsToDelete as $watchdogRemoved) {
            $watchdogRemoved->setUzivatel(null);
        }

        $this->collWatchdogs = null;
        foreach ($watchdogs as $watchdog) {
            $this->addWatchdog($watchdog);
        }

        $this->collWatchdogs = $watchdogs;
        $this->collWatchdogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Watchdog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Watchdog objects.
     * @throws PropelException
     */
    public function countWatchdogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collWatchdogsPartial && !$this->isNew();
        if (null === $this->collWatchdogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWatchdogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWatchdogs());
            }

            $query = ChildWatchdogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUzivatel($this)
                ->count($con);
        }

        return count($this->collWatchdogs);
    }

    /**
     * Method called to associate a ChildWatchdog object to this object
     * through the ChildWatchdog foreign key attribute.
     *
     * @param  ChildWatchdog $l ChildWatchdog
     * @return $this|\SmartFridge\Uzivatel The current object (for fluent API support)
     */
    public function addWatchdog(ChildWatchdog $l)
    {
        if ($this->collWatchdogs === null) {
            $this->initWatchdogs();
            $this->collWatchdogsPartial = true;
        }

        if (!$this->collWatchdogs->contains($l)) {
            $this->doAddWatchdog($l);

            if ($this->watchdogsScheduledForDeletion and $this->watchdogsScheduledForDeletion->contains($l)) {
                $this->watchdogsScheduledForDeletion->remove($this->watchdogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildWatchdog $watchdog The ChildWatchdog object to add.
     */
    protected function doAddWatchdog(ChildWatchdog $watchdog)
    {
        $this->collWatchdogs[]= $watchdog;
        $watchdog->setUzivatel($this);
    }

    /**
     * @param  ChildWatchdog $watchdog The ChildWatchdog object to remove.
     * @return $this|ChildUzivatel The current object (for fluent API support)
     */
    public function removeWatchdog(ChildWatchdog $watchdog)
    {
        if ($this->getWatchdogs()->contains($watchdog)) {
            $pos = $this->collWatchdogs->search($watchdog);
            $this->collWatchdogs->remove($pos);
            if (null === $this->watchdogsScheduledForDeletion) {
                $this->watchdogsScheduledForDeletion = clone $this->collWatchdogs;
                $this->watchdogsScheduledForDeletion->clear();
            }
            $this->watchdogsScheduledForDeletion[]= $watchdog;
            $watchdog->setUzivatel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Uzivatel is new, it will return
     * an empty collection; or if this Uzivatel has previously
     * been saved, it will retrieve related Watchdogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Uzivatel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildWatchdog[] List of ChildWatchdog objects
     */
    public function getWatchdogsJoinProdukt(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildWatchdogQuery::create(null, $criteria);
        $query->joinWith('Produkt', $joinBehavior);

        return $this->getWatchdogs($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->prezdivka = null;
        $this->heslo = null;
        $this->jmeno = null;
        $this->prijmeni = null;
        $this->ulice = null;
        $this->mesto = null;
        $this->cislopopisne = null;
        $this->postovnismerovacicislo = null;
        $this->role = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collObjednavkas) {
                foreach ($this->collObjednavkas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPotravinies) {
                foreach ($this->collPotravinies as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRecenzes) {
                foreach ($this->collRecenzes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUpozornenis) {
                foreach ($this->collUpozornenis as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWatchdogs) {
                foreach ($this->collWatchdogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collObjednavkas = null;
        $this->collPotravinies = null;
        $this->collRecenzes = null;
        $this->collUpozornenis = null;
        $this->collWatchdogs = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UzivatelTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
