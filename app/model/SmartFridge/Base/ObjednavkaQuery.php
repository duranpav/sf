<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SmartFridge\Objednavka as ChildObjednavka;
use SmartFridge\ObjednavkaQuery as ChildObjednavkaQuery;
use SmartFridge\Map\ObjednavkaTableMap;

/**
 * Base class that represents a query for the 'Objednavka' table.
 *
 *
 *
 * @method     ChildObjednavkaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildObjednavkaQuery orderByDatumzalozeni($order = Criteria::ASC) Order by the datumZalozeni column
 * @method     ChildObjednavkaQuery orderByDatumzaplaceni($order = Criteria::ASC) Order by the datumZaplaceni column
 * @method     ChildObjednavkaQuery orderByDatumdorucení($order = Criteria::ASC) Order by the datumDorucení column
 * @method     ChildObjednavkaQuery orderByZpusobplatby($order = Criteria::ASC) Order by the zpusobPlatby column
 * @method     ChildObjednavkaQuery orderByCelkovacena($order = Criteria::ASC) Order by the celkovaCena column
 * @method     ChildObjednavkaQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildObjednavkaQuery orderBySoubor($order = Criteria::ASC) Order by the soubor column
 * @method     ChildObjednavkaQuery orderByUzivatelid($order = Criteria::ASC) Order by the uzivatelID column
 *
 * @method     ChildObjednavkaQuery groupById() Group by the id column
 * @method     ChildObjednavkaQuery groupByDatumzalozeni() Group by the datumZalozeni column
 * @method     ChildObjednavkaQuery groupByDatumzaplaceni() Group by the datumZaplaceni column
 * @method     ChildObjednavkaQuery groupByDatumdorucení() Group by the datumDorucení column
 * @method     ChildObjednavkaQuery groupByZpusobplatby() Group by the zpusobPlatby column
 * @method     ChildObjednavkaQuery groupByCelkovacena() Group by the celkovaCena column
 * @method     ChildObjednavkaQuery groupByStatus() Group by the status column
 * @method     ChildObjednavkaQuery groupBySoubor() Group by the soubor column
 * @method     ChildObjednavkaQuery groupByUzivatelid() Group by the uzivatelID column
 *
 * @method     ChildObjednavkaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildObjednavkaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildObjednavkaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildObjednavkaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildObjednavkaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildObjednavkaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildObjednavkaQuery leftJoinUzivatel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Uzivatel relation
 * @method     ChildObjednavkaQuery rightJoinUzivatel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Uzivatel relation
 * @method     ChildObjednavkaQuery innerJoinUzivatel($relationAlias = null) Adds a INNER JOIN clause to the query using the Uzivatel relation
 *
 * @method     ChildObjednavkaQuery joinWithUzivatel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Uzivatel relation
 *
 * @method     ChildObjednavkaQuery leftJoinWithUzivatel() Adds a LEFT JOIN clause and with to the query using the Uzivatel relation
 * @method     ChildObjednavkaQuery rightJoinWithUzivatel() Adds a RIGHT JOIN clause and with to the query using the Uzivatel relation
 * @method     ChildObjednavkaQuery innerJoinWithUzivatel() Adds a INNER JOIN clause and with to the query using the Uzivatel relation
 *
 * @method     ChildObjednavkaQuery leftJoinPolozka($relationAlias = null) Adds a LEFT JOIN clause to the query using the Polozka relation
 * @method     ChildObjednavkaQuery rightJoinPolozka($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Polozka relation
 * @method     ChildObjednavkaQuery innerJoinPolozka($relationAlias = null) Adds a INNER JOIN clause to the query using the Polozka relation
 *
 * @method     ChildObjednavkaQuery joinWithPolozka($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Polozka relation
 *
 * @method     ChildObjednavkaQuery leftJoinWithPolozka() Adds a LEFT JOIN clause and with to the query using the Polozka relation
 * @method     ChildObjednavkaQuery rightJoinWithPolozka() Adds a RIGHT JOIN clause and with to the query using the Polozka relation
 * @method     ChildObjednavkaQuery innerJoinWithPolozka() Adds a INNER JOIN clause and with to the query using the Polozka relation
 *
 * @method     \SmartFridge\UzivatelQuery|\SmartFridge\PolozkaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildObjednavka findOne(ConnectionInterface $con = null) Return the first ChildObjednavka matching the query
 * @method     ChildObjednavka findOneOrCreate(ConnectionInterface $con = null) Return the first ChildObjednavka matching the query, or a new ChildObjednavka object populated from the query conditions when no match is found
 *
 * @method     ChildObjednavka findOneById(int $id) Return the first ChildObjednavka filtered by the id column
 * @method     ChildObjednavka findOneByDatumzalozeni(string $datumZalozeni) Return the first ChildObjednavka filtered by the datumZalozeni column
 * @method     ChildObjednavka findOneByDatumzaplaceni(string $datumZaplaceni) Return the first ChildObjednavka filtered by the datumZaplaceni column
 * @method     ChildObjednavka findOneByDatumdorucení(string $datumDorucení) Return the first ChildObjednavka filtered by the datumDorucení column
 * @method     ChildObjednavka findOneByZpusobplatby(string $zpusobPlatby) Return the first ChildObjednavka filtered by the zpusobPlatby column
 * @method     ChildObjednavka findOneByCelkovacena(double $celkovaCena) Return the first ChildObjednavka filtered by the celkovaCena column
 * @method     ChildObjednavka findOneByStatus(string $status) Return the first ChildObjednavka filtered by the status column
 * @method     ChildObjednavka findOneBySoubor(string $soubor) Return the first ChildObjednavka filtered by the soubor column
 * @method     ChildObjednavka findOneByUzivatelid(int $uzivatelID) Return the first ChildObjednavka filtered by the uzivatelID column *

 * @method     ChildObjednavka requirePk($key, ConnectionInterface $con = null) Return the ChildObjednavka by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildObjednavka requireOne(ConnectionInterface $con = null) Return the first ChildObjednavka matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildObjednavka requireOneById(int $id) Return the first ChildObjednavka filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildObjednavka requireOneByDatumzalozeni(string $datumZalozeni) Return the first ChildObjednavka filtered by the datumZalozeni column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildObjednavka requireOneByDatumzaplaceni(string $datumZaplaceni) Return the first ChildObjednavka filtered by the datumZaplaceni column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildObjednavka requireOneByDatumdorucení(string $datumDorucení) Return the first ChildObjednavka filtered by the datumDorucení column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildObjednavka requireOneByZpusobplatby(string $zpusobPlatby) Return the first ChildObjednavka filtered by the zpusobPlatby column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildObjednavka requireOneByCelkovacena(double $celkovaCena) Return the first ChildObjednavka filtered by the celkovaCena column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildObjednavka requireOneByStatus(string $status) Return the first ChildObjednavka filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildObjednavka requireOneBySoubor(string $soubor) Return the first ChildObjednavka filtered by the soubor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildObjednavka requireOneByUzivatelid(int $uzivatelID) Return the first ChildObjednavka filtered by the uzivatelID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildObjednavka[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildObjednavka objects based on current ModelCriteria
 * @method     ChildObjednavka[]|ObjectCollection findById(int $id) Return ChildObjednavka objects filtered by the id column
 * @method     ChildObjednavka[]|ObjectCollection findByDatumzalozeni(string $datumZalozeni) Return ChildObjednavka objects filtered by the datumZalozeni column
 * @method     ChildObjednavka[]|ObjectCollection findByDatumzaplaceni(string $datumZaplaceni) Return ChildObjednavka objects filtered by the datumZaplaceni column
 * @method     ChildObjednavka[]|ObjectCollection findByDatumdorucení(string $datumDorucení) Return ChildObjednavka objects filtered by the datumDorucení column
 * @method     ChildObjednavka[]|ObjectCollection findByZpusobplatby(string $zpusobPlatby) Return ChildObjednavka objects filtered by the zpusobPlatby column
 * @method     ChildObjednavka[]|ObjectCollection findByCelkovacena(double $celkovaCena) Return ChildObjednavka objects filtered by the celkovaCena column
 * @method     ChildObjednavka[]|ObjectCollection findByStatus(string $status) Return ChildObjednavka objects filtered by the status column
 * @method     ChildObjednavka[]|ObjectCollection findBySoubor(string $soubor) Return ChildObjednavka objects filtered by the soubor column
 * @method     ChildObjednavka[]|ObjectCollection findByUzivatelid(int $uzivatelID) Return ChildObjednavka objects filtered by the uzivatelID column
 * @method     ChildObjednavka[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ObjednavkaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SmartFridge\Base\ObjednavkaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SmartFridge\\Objednavka', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildObjednavkaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildObjednavkaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildObjednavkaQuery) {
            return $criteria;
        }
        $query = new ChildObjednavkaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildObjednavka|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ObjednavkaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ObjednavkaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildObjednavka A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, datumZalozeni, datumZaplaceni, datumDorucení, zpusobPlatby, celkovaCena, status, soubor, uzivatelID FROM Objednavka WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildObjednavka $obj */
            $obj = new ChildObjednavka();
            $obj->hydrate($row);
            ObjednavkaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildObjednavka|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ObjednavkaTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ObjednavkaTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ObjednavkaTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the datumZalozeni column
     *
     * Example usage:
     * <code>
     * $query->filterByDatumzalozeni('2011-03-14'); // WHERE datumZalozeni = '2011-03-14'
     * $query->filterByDatumzalozeni('now'); // WHERE datumZalozeni = '2011-03-14'
     * $query->filterByDatumzalozeni(array('max' => 'yesterday')); // WHERE datumZalozeni > '2011-03-13'
     * </code>
     *
     * @param     mixed $datumzalozeni The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByDatumzalozeni($datumzalozeni = null, $comparison = null)
    {
        if (is_array($datumzalozeni)) {
            $useMinMax = false;
            if (isset($datumzalozeni['min'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_DATUMZALOZENI, $datumzalozeni['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datumzalozeni['max'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_DATUMZALOZENI, $datumzalozeni['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ObjednavkaTableMap::COL_DATUMZALOZENI, $datumzalozeni, $comparison);
    }

    /**
     * Filter the query on the datumZaplaceni column
     *
     * Example usage:
     * <code>
     * $query->filterByDatumzaplaceni('2011-03-14'); // WHERE datumZaplaceni = '2011-03-14'
     * $query->filterByDatumzaplaceni('now'); // WHERE datumZaplaceni = '2011-03-14'
     * $query->filterByDatumzaplaceni(array('max' => 'yesterday')); // WHERE datumZaplaceni > '2011-03-13'
     * </code>
     *
     * @param     mixed $datumzaplaceni The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByDatumzaplaceni($datumzaplaceni = null, $comparison = null)
    {
        if (is_array($datumzaplaceni)) {
            $useMinMax = false;
            if (isset($datumzaplaceni['min'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_DATUMZAPLACENI, $datumzaplaceni['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datumzaplaceni['max'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_DATUMZAPLACENI, $datumzaplaceni['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ObjednavkaTableMap::COL_DATUMZAPLACENI, $datumzaplaceni, $comparison);
    }

    /**
     * Filter the query on the datumDorucení column
     *
     * Example usage:
     * <code>
     * $query->filterByDatumdorucení('2011-03-14'); // WHERE datumDorucení = '2011-03-14'
     * $query->filterByDatumdorucení('now'); // WHERE datumDorucení = '2011-03-14'
     * $query->filterByDatumdorucení(array('max' => 'yesterday')); // WHERE datumDorucení > '2011-03-13'
     * </code>
     *
     * @param     mixed $datumdorucení The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByDatumdorucení($datumdorucení = null, $comparison = null)
    {
        if (is_array($datumdorucení)) {
            $useMinMax = false;
            if (isset($datumdorucení['min'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_DATUMDORUCENí, $datumdorucení['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datumdorucení['max'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_DATUMDORUCENí, $datumdorucení['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ObjednavkaTableMap::COL_DATUMDORUCENí, $datumdorucení, $comparison);
    }

    /**
     * Filter the query on the zpusobPlatby column
     *
     * Example usage:
     * <code>
     * $query->filterByZpusobplatby('fooValue');   // WHERE zpusobPlatby = 'fooValue'
     * $query->filterByZpusobplatby('%fooValue%', Criteria::LIKE); // WHERE zpusobPlatby LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zpusobplatby The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByZpusobplatby($zpusobplatby = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zpusobplatby)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ObjednavkaTableMap::COL_ZPUSOBPLATBY, $zpusobplatby, $comparison);
    }

    /**
     * Filter the query on the celkovaCena column
     *
     * Example usage:
     * <code>
     * $query->filterByCelkovacena(1234); // WHERE celkovaCena = 1234
     * $query->filterByCelkovacena(array(12, 34)); // WHERE celkovaCena IN (12, 34)
     * $query->filterByCelkovacena(array('min' => 12)); // WHERE celkovaCena > 12
     * </code>
     *
     * @param     mixed $celkovacena The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByCelkovacena($celkovacena = null, $comparison = null)
    {
        if (is_array($celkovacena)) {
            $useMinMax = false;
            if (isset($celkovacena['min'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_CELKOVACENA, $celkovacena['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($celkovacena['max'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_CELKOVACENA, $celkovacena['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ObjednavkaTableMap::COL_CELKOVACENA, $celkovacena, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ObjednavkaTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the soubor column
     *
     * Example usage:
     * <code>
     * $query->filterBySoubor('fooValue');   // WHERE soubor = 'fooValue'
     * $query->filterBySoubor('%fooValue%', Criteria::LIKE); // WHERE soubor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $soubor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterBySoubor($soubor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($soubor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ObjednavkaTableMap::COL_SOUBOR, $soubor, $comparison);
    }

    /**
     * Filter the query on the uzivatelID column
     *
     * Example usage:
     * <code>
     * $query->filterByUzivatelid(1234); // WHERE uzivatelID = 1234
     * $query->filterByUzivatelid(array(12, 34)); // WHERE uzivatelID IN (12, 34)
     * $query->filterByUzivatelid(array('min' => 12)); // WHERE uzivatelID > 12
     * </code>
     *
     * @see       filterByUzivatel()
     *
     * @param     mixed $uzivatelid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByUzivatelid($uzivatelid = null, $comparison = null)
    {
        if (is_array($uzivatelid)) {
            $useMinMax = false;
            if (isset($uzivatelid['min'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_UZIVATELID, $uzivatelid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uzivatelid['max'])) {
                $this->addUsingAlias(ObjednavkaTableMap::COL_UZIVATELID, $uzivatelid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ObjednavkaTableMap::COL_UZIVATELID, $uzivatelid, $comparison);
    }

    /**
     * Filter the query by a related \SmartFridge\Uzivatel object
     *
     * @param \SmartFridge\Uzivatel|ObjectCollection $uzivatel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByUzivatel($uzivatel, $comparison = null)
    {
        if ($uzivatel instanceof \SmartFridge\Uzivatel) {
            return $this
                ->addUsingAlias(ObjednavkaTableMap::COL_UZIVATELID, $uzivatel->getId(), $comparison);
        } elseif ($uzivatel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ObjednavkaTableMap::COL_UZIVATELID, $uzivatel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUzivatel() only accepts arguments of type \SmartFridge\Uzivatel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Uzivatel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function joinUzivatel($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Uzivatel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Uzivatel');
        }

        return $this;
    }

    /**
     * Use the Uzivatel relation Uzivatel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\UzivatelQuery A secondary query class using the current class as primary query
     */
    public function useUzivatelQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUzivatel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Uzivatel', '\SmartFridge\UzivatelQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Polozka object
     *
     * @param \SmartFridge\Polozka|ObjectCollection $polozka the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildObjednavkaQuery The current query, for fluid interface
     */
    public function filterByPolozka($polozka, $comparison = null)
    {
        if ($polozka instanceof \SmartFridge\Polozka) {
            return $this
                ->addUsingAlias(ObjednavkaTableMap::COL_ID, $polozka->getObjednavkaid(), $comparison);
        } elseif ($polozka instanceof ObjectCollection) {
            return $this
                ->usePolozkaQuery()
                ->filterByPrimaryKeys($polozka->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPolozka() only accepts arguments of type \SmartFridge\Polozka or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Polozka relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function joinPolozka($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Polozka');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Polozka');
        }

        return $this;
    }

    /**
     * Use the Polozka relation Polozka object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\PolozkaQuery A secondary query class using the current class as primary query
     */
    public function usePolozkaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPolozka($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Polozka', '\SmartFridge\PolozkaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildObjednavka $objednavka Object to remove from the list of results
     *
     * @return $this|ChildObjednavkaQuery The current query, for fluid interface
     */
    public function prune($objednavka = null)
    {
        if ($objednavka) {
            $this->addUsingAlias(ObjednavkaTableMap::COL_ID, $objednavka->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Objednavka table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ObjednavkaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ObjednavkaTableMap::clearInstancePool();
            ObjednavkaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ObjednavkaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ObjednavkaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ObjednavkaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ObjednavkaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ObjednavkaQuery
