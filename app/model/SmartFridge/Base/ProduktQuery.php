<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SmartFridge\Produkt as ChildProdukt;
use SmartFridge\ProduktQuery as ChildProduktQuery;
use SmartFridge\Map\ProduktTableMap;

/**
 * Base class that represents a query for the 'Produkt' table.
 *
 *
 *
 * @method     ChildProduktQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildProduktQuery orderByNazev($order = Criteria::ASC) Order by the nazev column
 * @method     ChildProduktQuery orderByFotka($order = Criteria::ASC) Order by the fotka column
 * @method     ChildProduktQuery orderByPopis($order = Criteria::ASC) Order by the popis column
 * @method     ChildProduktQuery orderByKategorieid($order = Criteria::ASC) Order by the kategorieID column
 * @method     ChildProduktQuery orderByJednotkaid($order = Criteria::ASC) Order by the jednotkaID column
 * @method     ChildProduktQuery orderByMnozstvi($order = Criteria::ASC) Order by the mnozstvi column
 *
 * @method     ChildProduktQuery groupById() Group by the id column
 * @method     ChildProduktQuery groupByNazev() Group by the nazev column
 * @method     ChildProduktQuery groupByFotka() Group by the fotka column
 * @method     ChildProduktQuery groupByPopis() Group by the popis column
 * @method     ChildProduktQuery groupByKategorieid() Group by the kategorieID column
 * @method     ChildProduktQuery groupByJednotkaid() Group by the jednotkaID column
 * @method     ChildProduktQuery groupByMnozstvi() Group by the mnozstvi column
 *
 * @method     ChildProduktQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProduktQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProduktQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProduktQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProduktQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProduktQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProduktQuery leftJoinJednotka($relationAlias = null) Adds a LEFT JOIN clause to the query using the Jednotka relation
 * @method     ChildProduktQuery rightJoinJednotka($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Jednotka relation
 * @method     ChildProduktQuery innerJoinJednotka($relationAlias = null) Adds a INNER JOIN clause to the query using the Jednotka relation
 *
 * @method     ChildProduktQuery joinWithJednotka($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Jednotka relation
 *
 * @method     ChildProduktQuery leftJoinWithJednotka() Adds a LEFT JOIN clause and with to the query using the Jednotka relation
 * @method     ChildProduktQuery rightJoinWithJednotka() Adds a RIGHT JOIN clause and with to the query using the Jednotka relation
 * @method     ChildProduktQuery innerJoinWithJednotka() Adds a INNER JOIN clause and with to the query using the Jednotka relation
 *
 * @method     ChildProduktQuery leftJoinKategorie($relationAlias = null) Adds a LEFT JOIN clause to the query using the Kategorie relation
 * @method     ChildProduktQuery rightJoinKategorie($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Kategorie relation
 * @method     ChildProduktQuery innerJoinKategorie($relationAlias = null) Adds a INNER JOIN clause to the query using the Kategorie relation
 *
 * @method     ChildProduktQuery joinWithKategorie($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Kategorie relation
 *
 * @method     ChildProduktQuery leftJoinWithKategorie() Adds a LEFT JOIN clause and with to the query using the Kategorie relation
 * @method     ChildProduktQuery rightJoinWithKategorie() Adds a RIGHT JOIN clause and with to the query using the Kategorie relation
 * @method     ChildProduktQuery innerJoinWithKategorie() Adds a INNER JOIN clause and with to the query using the Kategorie relation
 *
 * @method     ChildProduktQuery leftJoinIngredience($relationAlias = null) Adds a LEFT JOIN clause to the query using the Ingredience relation
 * @method     ChildProduktQuery rightJoinIngredience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Ingredience relation
 * @method     ChildProduktQuery innerJoinIngredience($relationAlias = null) Adds a INNER JOIN clause to the query using the Ingredience relation
 *
 * @method     ChildProduktQuery joinWithIngredience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Ingredience relation
 *
 * @method     ChildProduktQuery leftJoinWithIngredience() Adds a LEFT JOIN clause and with to the query using the Ingredience relation
 * @method     ChildProduktQuery rightJoinWithIngredience() Adds a RIGHT JOIN clause and with to the query using the Ingredience relation
 * @method     ChildProduktQuery innerJoinWithIngredience() Adds a INNER JOIN clause and with to the query using the Ingredience relation
 *
 * @method     ChildProduktQuery leftJoinPotraviny($relationAlias = null) Adds a LEFT JOIN clause to the query using the Potraviny relation
 * @method     ChildProduktQuery rightJoinPotraviny($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Potraviny relation
 * @method     ChildProduktQuery innerJoinPotraviny($relationAlias = null) Adds a INNER JOIN clause to the query using the Potraviny relation
 *
 * @method     ChildProduktQuery joinWithPotraviny($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Potraviny relation
 *
 * @method     ChildProduktQuery leftJoinWithPotraviny() Adds a LEFT JOIN clause and with to the query using the Potraviny relation
 * @method     ChildProduktQuery rightJoinWithPotraviny() Adds a RIGHT JOIN clause and with to the query using the Potraviny relation
 * @method     ChildProduktQuery innerJoinWithPotraviny() Adds a INNER JOIN clause and with to the query using the Potraviny relation
 *
 * @method     ChildProduktQuery leftJoinUpozorneni($relationAlias = null) Adds a LEFT JOIN clause to the query using the Upozorneni relation
 * @method     ChildProduktQuery rightJoinUpozorneni($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Upozorneni relation
 * @method     ChildProduktQuery innerJoinUpozorneni($relationAlias = null) Adds a INNER JOIN clause to the query using the Upozorneni relation
 *
 * @method     ChildProduktQuery joinWithUpozorneni($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Upozorneni relation
 *
 * @method     ChildProduktQuery leftJoinWithUpozorneni() Adds a LEFT JOIN clause and with to the query using the Upozorneni relation
 * @method     ChildProduktQuery rightJoinWithUpozorneni() Adds a RIGHT JOIN clause and with to the query using the Upozorneni relation
 * @method     ChildProduktQuery innerJoinWithUpozorneni() Adds a INNER JOIN clause and with to the query using the Upozorneni relation
 *
 * @method     ChildProduktQuery leftJoinWatchdog($relationAlias = null) Adds a LEFT JOIN clause to the query using the Watchdog relation
 * @method     ChildProduktQuery rightJoinWatchdog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Watchdog relation
 * @method     ChildProduktQuery innerJoinWatchdog($relationAlias = null) Adds a INNER JOIN clause to the query using the Watchdog relation
 *
 * @method     ChildProduktQuery joinWithWatchdog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Watchdog relation
 *
 * @method     ChildProduktQuery leftJoinWithWatchdog() Adds a LEFT JOIN clause and with to the query using the Watchdog relation
 * @method     ChildProduktQuery rightJoinWithWatchdog() Adds a RIGHT JOIN clause and with to the query using the Watchdog relation
 * @method     ChildProduktQuery innerJoinWithWatchdog() Adds a INNER JOIN clause and with to the query using the Watchdog relation
 *
 * @method     ChildProduktQuery leftJoinZbozi($relationAlias = null) Adds a LEFT JOIN clause to the query using the Zbozi relation
 * @method     ChildProduktQuery rightJoinZbozi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Zbozi relation
 * @method     ChildProduktQuery innerJoinZbozi($relationAlias = null) Adds a INNER JOIN clause to the query using the Zbozi relation
 *
 * @method     ChildProduktQuery joinWithZbozi($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Zbozi relation
 *
 * @method     ChildProduktQuery leftJoinWithZbozi() Adds a LEFT JOIN clause and with to the query using the Zbozi relation
 * @method     ChildProduktQuery rightJoinWithZbozi() Adds a RIGHT JOIN clause and with to the query using the Zbozi relation
 * @method     ChildProduktQuery innerJoinWithZbozi() Adds a INNER JOIN clause and with to the query using the Zbozi relation
 *
 * @method     \SmartFridge\JednotkaQuery|\SmartFridge\KategorieQuery|\SmartFridge\IngredienceQuery|\SmartFridge\PotravinyQuery|\SmartFridge\UpozorneniQuery|\SmartFridge\WatchdogQuery|\SmartFridge\ZboziQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProdukt findOne(ConnectionInterface $con = null) Return the first ChildProdukt matching the query
 * @method     ChildProdukt findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProdukt matching the query, or a new ChildProdukt object populated from the query conditions when no match is found
 *
 * @method     ChildProdukt findOneById(int $id) Return the first ChildProdukt filtered by the id column
 * @method     ChildProdukt findOneByNazev(string $nazev) Return the first ChildProdukt filtered by the nazev column
 * @method     ChildProdukt findOneByFotka(string $fotka) Return the first ChildProdukt filtered by the fotka column
 * @method     ChildProdukt findOneByPopis(string $popis) Return the first ChildProdukt filtered by the popis column
 * @method     ChildProdukt findOneByKategorieid(int $kategorieID) Return the first ChildProdukt filtered by the kategorieID column
 * @method     ChildProdukt findOneByJednotkaid(int $jednotkaID) Return the first ChildProdukt filtered by the jednotkaID column
 * @method     ChildProdukt findOneByMnozstvi(int $mnozstvi) Return the first ChildProdukt filtered by the mnozstvi column *

 * @method     ChildProdukt requirePk($key, ConnectionInterface $con = null) Return the ChildProdukt by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProdukt requireOne(ConnectionInterface $con = null) Return the first ChildProdukt matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProdukt requireOneById(int $id) Return the first ChildProdukt filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProdukt requireOneByNazev(string $nazev) Return the first ChildProdukt filtered by the nazev column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProdukt requireOneByFotka(string $fotka) Return the first ChildProdukt filtered by the fotka column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProdukt requireOneByPopis(string $popis) Return the first ChildProdukt filtered by the popis column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProdukt requireOneByKategorieid(int $kategorieID) Return the first ChildProdukt filtered by the kategorieID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProdukt requireOneByJednotkaid(int $jednotkaID) Return the first ChildProdukt filtered by the jednotkaID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProdukt requireOneByMnozstvi(int $mnozstvi) Return the first ChildProdukt filtered by the mnozstvi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProdukt[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProdukt objects based on current ModelCriteria
 * @method     ChildProdukt[]|ObjectCollection findById(int $id) Return ChildProdukt objects filtered by the id column
 * @method     ChildProdukt[]|ObjectCollection findByNazev(string $nazev) Return ChildProdukt objects filtered by the nazev column
 * @method     ChildProdukt[]|ObjectCollection findByFotka(string $fotka) Return ChildProdukt objects filtered by the fotka column
 * @method     ChildProdukt[]|ObjectCollection findByPopis(string $popis) Return ChildProdukt objects filtered by the popis column
 * @method     ChildProdukt[]|ObjectCollection findByKategorieid(int $kategorieID) Return ChildProdukt objects filtered by the kategorieID column
 * @method     ChildProdukt[]|ObjectCollection findByJednotkaid(int $jednotkaID) Return ChildProdukt objects filtered by the jednotkaID column
 * @method     ChildProdukt[]|ObjectCollection findByMnozstvi(int $mnozstvi) Return ChildProdukt objects filtered by the mnozstvi column
 * @method     ChildProdukt[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProduktQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SmartFridge\Base\ProduktQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SmartFridge\\Produkt', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProduktQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProduktQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProduktQuery) {
            return $criteria;
        }
        $query = new ChildProduktQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProdukt|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProduktTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProduktTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProdukt A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, nazev, fotka, popis, kategorieID, jednotkaID, mnozstvi FROM Produkt WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProdukt $obj */
            $obj = new ChildProdukt();
            $obj->hydrate($row);
            ProduktTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProdukt|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProduktTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProduktTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProduktTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProduktTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduktTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nazev column
     *
     * Example usage:
     * <code>
     * $query->filterByNazev('fooValue');   // WHERE nazev = 'fooValue'
     * $query->filterByNazev('%fooValue%', Criteria::LIKE); // WHERE nazev LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nazev The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function filterByNazev($nazev = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nazev)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduktTableMap::COL_NAZEV, $nazev, $comparison);
    }

    /**
     * Filter the query on the fotka column
     *
     * Example usage:
     * <code>
     * $query->filterByFotka('fooValue');   // WHERE fotka = 'fooValue'
     * $query->filterByFotka('%fooValue%', Criteria::LIKE); // WHERE fotka LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fotka The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function filterByFotka($fotka = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fotka)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduktTableMap::COL_FOTKA, $fotka, $comparison);
    }

    /**
     * Filter the query on the popis column
     *
     * Example usage:
     * <code>
     * $query->filterByPopis('fooValue');   // WHERE popis = 'fooValue'
     * $query->filterByPopis('%fooValue%', Criteria::LIKE); // WHERE popis LIKE '%fooValue%'
     * </code>
     *
     * @param     string $popis The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function filterByPopis($popis = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($popis)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduktTableMap::COL_POPIS, $popis, $comparison);
    }

    /**
     * Filter the query on the kategorieID column
     *
     * Example usage:
     * <code>
     * $query->filterByKategorieid(1234); // WHERE kategorieID = 1234
     * $query->filterByKategorieid(array(12, 34)); // WHERE kategorieID IN (12, 34)
     * $query->filterByKategorieid(array('min' => 12)); // WHERE kategorieID > 12
     * </code>
     *
     * @see       filterByKategorie()
     *
     * @param     mixed $kategorieid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function filterByKategorieid($kategorieid = null, $comparison = null)
    {
        if (is_array($kategorieid)) {
            $useMinMax = false;
            if (isset($kategorieid['min'])) {
                $this->addUsingAlias(ProduktTableMap::COL_KATEGORIEID, $kategorieid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kategorieid['max'])) {
                $this->addUsingAlias(ProduktTableMap::COL_KATEGORIEID, $kategorieid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduktTableMap::COL_KATEGORIEID, $kategorieid, $comparison);
    }

    /**
     * Filter the query on the jednotkaID column
     *
     * Example usage:
     * <code>
     * $query->filterByJednotkaid(1234); // WHERE jednotkaID = 1234
     * $query->filterByJednotkaid(array(12, 34)); // WHERE jednotkaID IN (12, 34)
     * $query->filterByJednotkaid(array('min' => 12)); // WHERE jednotkaID > 12
     * </code>
     *
     * @see       filterByJednotka()
     *
     * @param     mixed $jednotkaid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function filterByJednotkaid($jednotkaid = null, $comparison = null)
    {
        if (is_array($jednotkaid)) {
            $useMinMax = false;
            if (isset($jednotkaid['min'])) {
                $this->addUsingAlias(ProduktTableMap::COL_JEDNOTKAID, $jednotkaid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jednotkaid['max'])) {
                $this->addUsingAlias(ProduktTableMap::COL_JEDNOTKAID, $jednotkaid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduktTableMap::COL_JEDNOTKAID, $jednotkaid, $comparison);
    }

    /**
     * Filter the query on the mnozstvi column
     *
     * Example usage:
     * <code>
     * $query->filterByMnozstvi(1234); // WHERE mnozstvi = 1234
     * $query->filterByMnozstvi(array(12, 34)); // WHERE mnozstvi IN (12, 34)
     * $query->filterByMnozstvi(array('min' => 12)); // WHERE mnozstvi > 12
     * </code>
     *
     * @param     mixed $mnozstvi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function filterByMnozstvi($mnozstvi = null, $comparison = null)
    {
        if (is_array($mnozstvi)) {
            $useMinMax = false;
            if (isset($mnozstvi['min'])) {
                $this->addUsingAlias(ProduktTableMap::COL_MNOZSTVI, $mnozstvi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mnozstvi['max'])) {
                $this->addUsingAlias(ProduktTableMap::COL_MNOZSTVI, $mnozstvi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduktTableMap::COL_MNOZSTVI, $mnozstvi, $comparison);
    }

    /**
     * Filter the query by a related \SmartFridge\Jednotka object
     *
     * @param \SmartFridge\Jednotka|ObjectCollection $jednotka The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProduktQuery The current query, for fluid interface
     */
    public function filterByJednotka($jednotka, $comparison = null)
    {
        if ($jednotka instanceof \SmartFridge\Jednotka) {
            return $this
                ->addUsingAlias(ProduktTableMap::COL_JEDNOTKAID, $jednotka->getId(), $comparison);
        } elseif ($jednotka instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProduktTableMap::COL_JEDNOTKAID, $jednotka->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByJednotka() only accepts arguments of type \SmartFridge\Jednotka or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Jednotka relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function joinJednotka($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Jednotka');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Jednotka');
        }

        return $this;
    }

    /**
     * Use the Jednotka relation Jednotka object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\JednotkaQuery A secondary query class using the current class as primary query
     */
    public function useJednotkaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJednotka($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Jednotka', '\SmartFridge\JednotkaQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Kategorie object
     *
     * @param \SmartFridge\Kategorie|ObjectCollection $kategorie The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProduktQuery The current query, for fluid interface
     */
    public function filterByKategorie($kategorie, $comparison = null)
    {
        if ($kategorie instanceof \SmartFridge\Kategorie) {
            return $this
                ->addUsingAlias(ProduktTableMap::COL_KATEGORIEID, $kategorie->getId(), $comparison);
        } elseif ($kategorie instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProduktTableMap::COL_KATEGORIEID, $kategorie->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByKategorie() only accepts arguments of type \SmartFridge\Kategorie or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Kategorie relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function joinKategorie($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Kategorie');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Kategorie');
        }

        return $this;
    }

    /**
     * Use the Kategorie relation Kategorie object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\KategorieQuery A secondary query class using the current class as primary query
     */
    public function useKategorieQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinKategorie($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Kategorie', '\SmartFridge\KategorieQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Ingredience object
     *
     * @param \SmartFridge\Ingredience|ObjectCollection $ingredience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProduktQuery The current query, for fluid interface
     */
    public function filterByIngredience($ingredience, $comparison = null)
    {
        if ($ingredience instanceof \SmartFridge\Ingredience) {
            return $this
                ->addUsingAlias(ProduktTableMap::COL_ID, $ingredience->getProduktid(), $comparison);
        } elseif ($ingredience instanceof ObjectCollection) {
            return $this
                ->useIngredienceQuery()
                ->filterByPrimaryKeys($ingredience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByIngredience() only accepts arguments of type \SmartFridge\Ingredience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Ingredience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function joinIngredience($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Ingredience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Ingredience');
        }

        return $this;
    }

    /**
     * Use the Ingredience relation Ingredience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\IngredienceQuery A secondary query class using the current class as primary query
     */
    public function useIngredienceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinIngredience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Ingredience', '\SmartFridge\IngredienceQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Potraviny object
     *
     * @param \SmartFridge\Potraviny|ObjectCollection $potraviny the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProduktQuery The current query, for fluid interface
     */
    public function filterByPotraviny($potraviny, $comparison = null)
    {
        if ($potraviny instanceof \SmartFridge\Potraviny) {
            return $this
                ->addUsingAlias(ProduktTableMap::COL_ID, $potraviny->getProduktid(), $comparison);
        } elseif ($potraviny instanceof ObjectCollection) {
            return $this
                ->usePotravinyQuery()
                ->filterByPrimaryKeys($potraviny->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPotraviny() only accepts arguments of type \SmartFridge\Potraviny or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Potraviny relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function joinPotraviny($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Potraviny');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Potraviny');
        }

        return $this;
    }

    /**
     * Use the Potraviny relation Potraviny object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\PotravinyQuery A secondary query class using the current class as primary query
     */
    public function usePotravinyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPotraviny($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Potraviny', '\SmartFridge\PotravinyQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Upozorneni object
     *
     * @param \SmartFridge\Upozorneni|ObjectCollection $upozorneni the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProduktQuery The current query, for fluid interface
     */
    public function filterByUpozorneni($upozorneni, $comparison = null)
    {
        if ($upozorneni instanceof \SmartFridge\Upozorneni) {
            return $this
                ->addUsingAlias(ProduktTableMap::COL_ID, $upozorneni->getProduktid(), $comparison);
        } elseif ($upozorneni instanceof ObjectCollection) {
            return $this
                ->useUpozorneniQuery()
                ->filterByPrimaryKeys($upozorneni->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUpozorneni() only accepts arguments of type \SmartFridge\Upozorneni or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Upozorneni relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function joinUpozorneni($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Upozorneni');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Upozorneni');
        }

        return $this;
    }

    /**
     * Use the Upozorneni relation Upozorneni object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\UpozorneniQuery A secondary query class using the current class as primary query
     */
    public function useUpozorneniQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpozorneni($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Upozorneni', '\SmartFridge\UpozorneniQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Watchdog object
     *
     * @param \SmartFridge\Watchdog|ObjectCollection $watchdog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProduktQuery The current query, for fluid interface
     */
    public function filterByWatchdog($watchdog, $comparison = null)
    {
        if ($watchdog instanceof \SmartFridge\Watchdog) {
            return $this
                ->addUsingAlias(ProduktTableMap::COL_ID, $watchdog->getProduktid(), $comparison);
        } elseif ($watchdog instanceof ObjectCollection) {
            return $this
                ->useWatchdogQuery()
                ->filterByPrimaryKeys($watchdog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWatchdog() only accepts arguments of type \SmartFridge\Watchdog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Watchdog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function joinWatchdog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Watchdog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Watchdog');
        }

        return $this;
    }

    /**
     * Use the Watchdog relation Watchdog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\WatchdogQuery A secondary query class using the current class as primary query
     */
    public function useWatchdogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinWatchdog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Watchdog', '\SmartFridge\WatchdogQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Zbozi object
     *
     * @param \SmartFridge\Zbozi|ObjectCollection $zbozi the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProduktQuery The current query, for fluid interface
     */
    public function filterByZbozi($zbozi, $comparison = null)
    {
        if ($zbozi instanceof \SmartFridge\Zbozi) {
            return $this
                ->addUsingAlias(ProduktTableMap::COL_ID, $zbozi->getProduktid(), $comparison);
        } elseif ($zbozi instanceof ObjectCollection) {
            return $this
                ->useZboziQuery()
                ->filterByPrimaryKeys($zbozi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByZbozi() only accepts arguments of type \SmartFridge\Zbozi or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Zbozi relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function joinZbozi($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Zbozi');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Zbozi');
        }

        return $this;
    }

    /**
     * Use the Zbozi relation Zbozi object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\ZboziQuery A secondary query class using the current class as primary query
     */
    public function useZboziQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinZbozi($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Zbozi', '\SmartFridge\ZboziQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProdukt $produkt Object to remove from the list of results
     *
     * @return $this|ChildProduktQuery The current query, for fluid interface
     */
    public function prune($produkt = null)
    {
        if ($produkt) {
            $this->addUsingAlias(ProduktTableMap::COL_ID, $produkt->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Produkt table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduktTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProduktTableMap::clearInstancePool();
            ProduktTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduktTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProduktTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProduktTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProduktTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ProduktQuery
