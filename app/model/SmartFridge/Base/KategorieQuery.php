<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SmartFridge\Kategorie as ChildKategorie;
use SmartFridge\KategorieQuery as ChildKategorieQuery;
use SmartFridge\Map\KategorieTableMap;

/**
 * Base class that represents a query for the 'Kategorie' table.
 *
 *
 *
 * @method     ChildKategorieQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildKategorieQuery orderByIkona($order = Criteria::ASC) Order by the ikona column
 * @method     ChildKategorieQuery orderByNazev($order = Criteria::ASC) Order by the nazev column
 * @method     ChildKategorieQuery orderByKategorieid($order = Criteria::ASC) Order by the kategorieID column
 *
 * @method     ChildKategorieQuery groupById() Group by the id column
 * @method     ChildKategorieQuery groupByIkona() Group by the ikona column
 * @method     ChildKategorieQuery groupByNazev() Group by the nazev column
 * @method     ChildKategorieQuery groupByKategorieid() Group by the kategorieID column
 *
 * @method     ChildKategorieQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildKategorieQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildKategorieQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildKategorieQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildKategorieQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildKategorieQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildKategorieQuery leftJoinKategorieRelatedByKategorieid($relationAlias = null) Adds a LEFT JOIN clause to the query using the KategorieRelatedByKategorieid relation
 * @method     ChildKategorieQuery rightJoinKategorieRelatedByKategorieid($relationAlias = null) Adds a RIGHT JOIN clause to the query using the KategorieRelatedByKategorieid relation
 * @method     ChildKategorieQuery innerJoinKategorieRelatedByKategorieid($relationAlias = null) Adds a INNER JOIN clause to the query using the KategorieRelatedByKategorieid relation
 *
 * @method     ChildKategorieQuery joinWithKategorieRelatedByKategorieid($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the KategorieRelatedByKategorieid relation
 *
 * @method     ChildKategorieQuery leftJoinWithKategorieRelatedByKategorieid() Adds a LEFT JOIN clause and with to the query using the KategorieRelatedByKategorieid relation
 * @method     ChildKategorieQuery rightJoinWithKategorieRelatedByKategorieid() Adds a RIGHT JOIN clause and with to the query using the KategorieRelatedByKategorieid relation
 * @method     ChildKategorieQuery innerJoinWithKategorieRelatedByKategorieid() Adds a INNER JOIN clause and with to the query using the KategorieRelatedByKategorieid relation
 *
 * @method     ChildKategorieQuery leftJoinIngredience($relationAlias = null) Adds a LEFT JOIN clause to the query using the Ingredience relation
 * @method     ChildKategorieQuery rightJoinIngredience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Ingredience relation
 * @method     ChildKategorieQuery innerJoinIngredience($relationAlias = null) Adds a INNER JOIN clause to the query using the Ingredience relation
 *
 * @method     ChildKategorieQuery joinWithIngredience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Ingredience relation
 *
 * @method     ChildKategorieQuery leftJoinWithIngredience() Adds a LEFT JOIN clause and with to the query using the Ingredience relation
 * @method     ChildKategorieQuery rightJoinWithIngredience() Adds a RIGHT JOIN clause and with to the query using the Ingredience relation
 * @method     ChildKategorieQuery innerJoinWithIngredience() Adds a INNER JOIN clause and with to the query using the Ingredience relation
 *
 * @method     ChildKategorieQuery leftJoinKategorieRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the KategorieRelatedById relation
 * @method     ChildKategorieQuery rightJoinKategorieRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the KategorieRelatedById relation
 * @method     ChildKategorieQuery innerJoinKategorieRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the KategorieRelatedById relation
 *
 * @method     ChildKategorieQuery joinWithKategorieRelatedById($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the KategorieRelatedById relation
 *
 * @method     ChildKategorieQuery leftJoinWithKategorieRelatedById() Adds a LEFT JOIN clause and with to the query using the KategorieRelatedById relation
 * @method     ChildKategorieQuery rightJoinWithKategorieRelatedById() Adds a RIGHT JOIN clause and with to the query using the KategorieRelatedById relation
 * @method     ChildKategorieQuery innerJoinWithKategorieRelatedById() Adds a INNER JOIN clause and with to the query using the KategorieRelatedById relation
 *
 * @method     ChildKategorieQuery leftJoinProdukt($relationAlias = null) Adds a LEFT JOIN clause to the query using the Produkt relation
 * @method     ChildKategorieQuery rightJoinProdukt($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Produkt relation
 * @method     ChildKategorieQuery innerJoinProdukt($relationAlias = null) Adds a INNER JOIN clause to the query using the Produkt relation
 *
 * @method     ChildKategorieQuery joinWithProdukt($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Produkt relation
 *
 * @method     ChildKategorieQuery leftJoinWithProdukt() Adds a LEFT JOIN clause and with to the query using the Produkt relation
 * @method     ChildKategorieQuery rightJoinWithProdukt() Adds a RIGHT JOIN clause and with to the query using the Produkt relation
 * @method     ChildKategorieQuery innerJoinWithProdukt() Adds a INNER JOIN clause and with to the query using the Produkt relation
 *
 * @method     \SmartFridge\KategorieQuery|\SmartFridge\IngredienceQuery|\SmartFridge\ProduktQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildKategorie findOne(ConnectionInterface $con = null) Return the first ChildKategorie matching the query
 * @method     ChildKategorie findOneOrCreate(ConnectionInterface $con = null) Return the first ChildKategorie matching the query, or a new ChildKategorie object populated from the query conditions when no match is found
 *
 * @method     ChildKategorie findOneById(int $id) Return the first ChildKategorie filtered by the id column
 * @method     ChildKategorie findOneByIkona(string $ikona) Return the first ChildKategorie filtered by the ikona column
 * @method     ChildKategorie findOneByNazev(string $nazev) Return the first ChildKategorie filtered by the nazev column
 * @method     ChildKategorie findOneByKategorieid(int $kategorieID) Return the first ChildKategorie filtered by the kategorieID column *

 * @method     ChildKategorie requirePk($key, ConnectionInterface $con = null) Return the ChildKategorie by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKategorie requireOne(ConnectionInterface $con = null) Return the first ChildKategorie matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildKategorie requireOneById(int $id) Return the first ChildKategorie filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKategorie requireOneByIkona(string $ikona) Return the first ChildKategorie filtered by the ikona column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKategorie requireOneByNazev(string $nazev) Return the first ChildKategorie filtered by the nazev column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildKategorie requireOneByKategorieid(int $kategorieID) Return the first ChildKategorie filtered by the kategorieID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildKategorie[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildKategorie objects based on current ModelCriteria
 * @method     ChildKategorie[]|ObjectCollection findById(int $id) Return ChildKategorie objects filtered by the id column
 * @method     ChildKategorie[]|ObjectCollection findByIkona(string $ikona) Return ChildKategorie objects filtered by the ikona column
 * @method     ChildKategorie[]|ObjectCollection findByNazev(string $nazev) Return ChildKategorie objects filtered by the nazev column
 * @method     ChildKategorie[]|ObjectCollection findByKategorieid(int $kategorieID) Return ChildKategorie objects filtered by the kategorieID column
 * @method     ChildKategorie[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class KategorieQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SmartFridge\Base\KategorieQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SmartFridge\\Kategorie', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildKategorieQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildKategorieQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildKategorieQuery) {
            return $criteria;
        }
        $query = new ChildKategorieQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildKategorie|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(KategorieTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = KategorieTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildKategorie A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, ikona, nazev, kategorieID FROM Kategorie WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildKategorie $obj */
            $obj = new ChildKategorie();
            $obj->hydrate($row);
            KategorieTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildKategorie|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(KategorieTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(KategorieTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(KategorieTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(KategorieTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KategorieTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the ikona column
     *
     * Example usage:
     * <code>
     * $query->filterByIkona('fooValue');   // WHERE ikona = 'fooValue'
     * $query->filterByIkona('%fooValue%', Criteria::LIKE); // WHERE ikona LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ikona The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function filterByIkona($ikona = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ikona)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KategorieTableMap::COL_IKONA, $ikona, $comparison);
    }

    /**
     * Filter the query on the nazev column
     *
     * Example usage:
     * <code>
     * $query->filterByNazev('fooValue');   // WHERE nazev = 'fooValue'
     * $query->filterByNazev('%fooValue%', Criteria::LIKE); // WHERE nazev LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nazev The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function filterByNazev($nazev = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nazev)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KategorieTableMap::COL_NAZEV, $nazev, $comparison);
    }

    /**
     * Filter the query on the kategorieID column
     *
     * Example usage:
     * <code>
     * $query->filterByKategorieid(1234); // WHERE kategorieID = 1234
     * $query->filterByKategorieid(array(12, 34)); // WHERE kategorieID IN (12, 34)
     * $query->filterByKategorieid(array('min' => 12)); // WHERE kategorieID > 12
     * </code>
     *
     * @see       filterByKategorieRelatedByKategorieid()
     *
     * @param     mixed $kategorieid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function filterByKategorieid($kategorieid = null, $comparison = null)
    {
        if (is_array($kategorieid)) {
            $useMinMax = false;
            if (isset($kategorieid['min'])) {
                $this->addUsingAlias(KategorieTableMap::COL_KATEGORIEID, $kategorieid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kategorieid['max'])) {
                $this->addUsingAlias(KategorieTableMap::COL_KATEGORIEID, $kategorieid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(KategorieTableMap::COL_KATEGORIEID, $kategorieid, $comparison);
    }

    /**
     * Filter the query by a related \SmartFridge\Kategorie object
     *
     * @param \SmartFridge\Kategorie|ObjectCollection $kategorie The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildKategorieQuery The current query, for fluid interface
     */
    public function filterByKategorieRelatedByKategorieid($kategorie, $comparison = null)
    {
        if ($kategorie instanceof \SmartFridge\Kategorie) {
            return $this
                ->addUsingAlias(KategorieTableMap::COL_KATEGORIEID, $kategorie->getId(), $comparison);
        } elseif ($kategorie instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(KategorieTableMap::COL_KATEGORIEID, $kategorie->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByKategorieRelatedByKategorieid() only accepts arguments of type \SmartFridge\Kategorie or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the KategorieRelatedByKategorieid relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function joinKategorieRelatedByKategorieid($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('KategorieRelatedByKategorieid');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'KategorieRelatedByKategorieid');
        }

        return $this;
    }

    /**
     * Use the KategorieRelatedByKategorieid relation Kategorie object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\KategorieQuery A secondary query class using the current class as primary query
     */
    public function useKategorieRelatedByKategorieidQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinKategorieRelatedByKategorieid($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'KategorieRelatedByKategorieid', '\SmartFridge\KategorieQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Ingredience object
     *
     * @param \SmartFridge\Ingredience|ObjectCollection $ingredience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildKategorieQuery The current query, for fluid interface
     */
    public function filterByIngredience($ingredience, $comparison = null)
    {
        if ($ingredience instanceof \SmartFridge\Ingredience) {
            return $this
                ->addUsingAlias(KategorieTableMap::COL_ID, $ingredience->getKategorieid(), $comparison);
        } elseif ($ingredience instanceof ObjectCollection) {
            return $this
                ->useIngredienceQuery()
                ->filterByPrimaryKeys($ingredience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByIngredience() only accepts arguments of type \SmartFridge\Ingredience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Ingredience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function joinIngredience($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Ingredience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Ingredience');
        }

        return $this;
    }

    /**
     * Use the Ingredience relation Ingredience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\IngredienceQuery A secondary query class using the current class as primary query
     */
    public function useIngredienceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinIngredience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Ingredience', '\SmartFridge\IngredienceQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Kategorie object
     *
     * @param \SmartFridge\Kategorie|ObjectCollection $kategorie the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildKategorieQuery The current query, for fluid interface
     */
    public function filterByKategorieRelatedById($kategorie, $comparison = null)
    {
        if ($kategorie instanceof \SmartFridge\Kategorie) {
            return $this
                ->addUsingAlias(KategorieTableMap::COL_ID, $kategorie->getKategorieid(), $comparison);
        } elseif ($kategorie instanceof ObjectCollection) {
            return $this
                ->useKategorieRelatedByIdQuery()
                ->filterByPrimaryKeys($kategorie->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByKategorieRelatedById() only accepts arguments of type \SmartFridge\Kategorie or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the KategorieRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function joinKategorieRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('KategorieRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'KategorieRelatedById');
        }

        return $this;
    }

    /**
     * Use the KategorieRelatedById relation Kategorie object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\KategorieQuery A secondary query class using the current class as primary query
     */
    public function useKategorieRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinKategorieRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'KategorieRelatedById', '\SmartFridge\KategorieQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Produkt object
     *
     * @param \SmartFridge\Produkt|ObjectCollection $produkt the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildKategorieQuery The current query, for fluid interface
     */
    public function filterByProdukt($produkt, $comparison = null)
    {
        if ($produkt instanceof \SmartFridge\Produkt) {
            return $this
                ->addUsingAlias(KategorieTableMap::COL_ID, $produkt->getKategorieid(), $comparison);
        } elseif ($produkt instanceof ObjectCollection) {
            return $this
                ->useProduktQuery()
                ->filterByPrimaryKeys($produkt->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProdukt() only accepts arguments of type \SmartFridge\Produkt or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Produkt relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function joinProdukt($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Produkt');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Produkt');
        }

        return $this;
    }

    /**
     * Use the Produkt relation Produkt object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\ProduktQuery A secondary query class using the current class as primary query
     */
    public function useProduktQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProdukt($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Produkt', '\SmartFridge\ProduktQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildKategorie $kategorie Object to remove from the list of results
     *
     * @return $this|ChildKategorieQuery The current query, for fluid interface
     */
    public function prune($kategorie = null)
    {
        if ($kategorie) {
            $this->addUsingAlias(KategorieTableMap::COL_ID, $kategorie->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Kategorie table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KategorieTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            KategorieTableMap::clearInstancePool();
            KategorieTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(KategorieTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(KategorieTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            KategorieTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            KategorieTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // KategorieQuery
