<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use SmartFridge\Ingredience as ChildIngredience;
use SmartFridge\IngredienceQuery as ChildIngredienceQuery;
use SmartFridge\Kategorie as ChildKategorie;
use SmartFridge\KategorieQuery as ChildKategorieQuery;
use SmartFridge\Produkt as ChildProdukt;
use SmartFridge\ProduktQuery as ChildProduktQuery;
use SmartFridge\Map\IngredienceTableMap;
use SmartFridge\Map\KategorieTableMap;
use SmartFridge\Map\ProduktTableMap;

/**
 * Base class that represents a row from the 'Kategorie' table.
 *
 *
 *
 * @package    propel.generator.SmartFridge.Base
 */
abstract class Kategorie implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\SmartFridge\\Map\\KategorieTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the ikona field.
     *
     * @var        string
     */
    protected $ikona;

    /**
     * The value for the nazev field.
     *
     * @var        string
     */
    protected $nazev;

    /**
     * The value for the kategorieid field.
     *
     * @var        int
     */
    protected $kategorieid;

    /**
     * @var        ChildKategorie
     */
    protected $aKategorieRelatedByKategorieid;

    /**
     * @var        ObjectCollection|ChildIngredience[] Collection to store aggregation of ChildIngredience objects.
     */
    protected $collIngrediences;
    protected $collIngrediencesPartial;

    /**
     * @var        ObjectCollection|ChildKategorie[] Collection to store aggregation of ChildKategorie objects.
     */
    protected $collKategoriesRelatedById;
    protected $collKategoriesRelatedByIdPartial;

    /**
     * @var        ObjectCollection|ChildProdukt[] Collection to store aggregation of ChildProdukt objects.
     */
    protected $collProdukts;
    protected $collProduktsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildIngredience[]
     */
    protected $ingrediencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildKategorie[]
     */
    protected $kategoriesRelatedByIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProdukt[]
     */
    protected $produktsScheduledForDeletion = null;

    /**
     * Initializes internal state of SmartFridge\Base\Kategorie object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Kategorie</code> instance.  If
     * <code>obj</code> is an instance of <code>Kategorie</code>, delegates to
     * <code>equals(Kategorie)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Kategorie The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [ikona] column value.
     *
     * @return string
     */
    public function getIkona()
    {
        return $this->ikona;
    }

    /**
     * Get the [nazev] column value.
     *
     * @return string
     */
    public function getNazev()
    {
        return $this->nazev;
    }

    /**
     * Get the [kategorieid] column value.
     *
     * @return int
     */
    public function getKategorieid()
    {
        return $this->kategorieid;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Kategorie The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[KategorieTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [ikona] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Kategorie The current object (for fluent API support)
     */
    public function setIkona($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ikona !== $v) {
            $this->ikona = $v;
            $this->modifiedColumns[KategorieTableMap::COL_IKONA] = true;
        }

        return $this;
    } // setIkona()

    /**
     * Set the value of [nazev] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Kategorie The current object (for fluent API support)
     */
    public function setNazev($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nazev !== $v) {
            $this->nazev = $v;
            $this->modifiedColumns[KategorieTableMap::COL_NAZEV] = true;
        }

        return $this;
    } // setNazev()

    /**
     * Set the value of [kategorieid] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Kategorie The current object (for fluent API support)
     */
    public function setKategorieid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->kategorieid !== $v) {
            $this->kategorieid = $v;
            $this->modifiedColumns[KategorieTableMap::COL_KATEGORIEID] = true;
        }

        if ($this->aKategorieRelatedByKategorieid !== null && $this->aKategorieRelatedByKategorieid->getId() !== $v) {
            $this->aKategorieRelatedByKategorieid = null;
        }

        return $this;
    } // setKategorieid()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : KategorieTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : KategorieTableMap::translateFieldName('Ikona', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ikona = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : KategorieTableMap::translateFieldName('Nazev', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nazev = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : KategorieTableMap::translateFieldName('Kategorieid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->kategorieid = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 4; // 4 = KategorieTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\SmartFridge\\Kategorie'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aKategorieRelatedByKategorieid !== null && $this->kategorieid !== $this->aKategorieRelatedByKategorieid->getId()) {
            $this->aKategorieRelatedByKategorieid = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(KategorieTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildKategorieQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aKategorieRelatedByKategorieid = null;
            $this->collIngrediences = null;

            $this->collKategoriesRelatedById = null;

            $this->collProdukts = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Kategorie::setDeleted()
     * @see Kategorie::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(KategorieTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildKategorieQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(KategorieTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                KategorieTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aKategorieRelatedByKategorieid !== null) {
                if ($this->aKategorieRelatedByKategorieid->isModified() || $this->aKategorieRelatedByKategorieid->isNew()) {
                    $affectedRows += $this->aKategorieRelatedByKategorieid->save($con);
                }
                $this->setKategorieRelatedByKategorieid($this->aKategorieRelatedByKategorieid);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->ingrediencesScheduledForDeletion !== null) {
                if (!$this->ingrediencesScheduledForDeletion->isEmpty()) {
                    foreach ($this->ingrediencesScheduledForDeletion as $ingredience) {
                        // need to save related object because we set the relation to null
                        $ingredience->save($con);
                    }
                    $this->ingrediencesScheduledForDeletion = null;
                }
            }

            if ($this->collIngrediences !== null) {
                foreach ($this->collIngrediences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->kategoriesRelatedByIdScheduledForDeletion !== null) {
                if (!$this->kategoriesRelatedByIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->kategoriesRelatedByIdScheduledForDeletion as $kategorieRelatedById) {
                        // need to save related object because we set the relation to null
                        $kategorieRelatedById->save($con);
                    }
                    $this->kategoriesRelatedByIdScheduledForDeletion = null;
                }
            }

            if ($this->collKategoriesRelatedById !== null) {
                foreach ($this->collKategoriesRelatedById as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->produktsScheduledForDeletion !== null) {
                if (!$this->produktsScheduledForDeletion->isEmpty()) {
                    foreach ($this->produktsScheduledForDeletion as $produkt) {
                        // need to save related object because we set the relation to null
                        $produkt->save($con);
                    }
                    $this->produktsScheduledForDeletion = null;
                }
            }

            if ($this->collProdukts !== null) {
                foreach ($this->collProdukts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[KategorieTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . KategorieTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(KategorieTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(KategorieTableMap::COL_IKONA)) {
            $modifiedColumns[':p' . $index++]  = 'ikona';
        }
        if ($this->isColumnModified(KategorieTableMap::COL_NAZEV)) {
            $modifiedColumns[':p' . $index++]  = 'nazev';
        }
        if ($this->isColumnModified(KategorieTableMap::COL_KATEGORIEID)) {
            $modifiedColumns[':p' . $index++]  = 'kategorieID';
        }

        $sql = sprintf(
            'INSERT INTO Kategorie (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'ikona':
                        $stmt->bindValue($identifier, $this->ikona, PDO::PARAM_STR);
                        break;
                    case 'nazev':
                        $stmt->bindValue($identifier, $this->nazev, PDO::PARAM_STR);
                        break;
                    case 'kategorieID':
                        $stmt->bindValue($identifier, $this->kategorieid, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = KategorieTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIkona();
                break;
            case 2:
                return $this->getNazev();
                break;
            case 3:
                return $this->getKategorieid();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Kategorie'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Kategorie'][$this->hashCode()] = true;
        $keys = KategorieTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getIkona(),
            $keys[2] => $this->getNazev(),
            $keys[3] => $this->getKategorieid(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aKategorieRelatedByKategorieid) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'kategorie';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Kategorie';
                        break;
                    default:
                        $key = 'Kategorie';
                }

                $result[$key] = $this->aKategorieRelatedByKategorieid->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collIngrediences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ingrediences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Ingrediences';
                        break;
                    default:
                        $key = 'Ingrediences';
                }

                $result[$key] = $this->collIngrediences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collKategoriesRelatedById) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'kategories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Kategories';
                        break;
                    default:
                        $key = 'Kategories';
                }

                $result[$key] = $this->collKategoriesRelatedById->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProdukts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'produkts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Produkts';
                        break;
                    default:
                        $key = 'Produkts';
                }

                $result[$key] = $this->collProdukts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\SmartFridge\Kategorie
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = KategorieTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\SmartFridge\Kategorie
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIkona($value);
                break;
            case 2:
                $this->setNazev($value);
                break;
            case 3:
                $this->setKategorieid($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = KategorieTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIkona($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNazev($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setKategorieid($arr[$keys[3]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\SmartFridge\Kategorie The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(KategorieTableMap::DATABASE_NAME);

        if ($this->isColumnModified(KategorieTableMap::COL_ID)) {
            $criteria->add(KategorieTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(KategorieTableMap::COL_IKONA)) {
            $criteria->add(KategorieTableMap::COL_IKONA, $this->ikona);
        }
        if ($this->isColumnModified(KategorieTableMap::COL_NAZEV)) {
            $criteria->add(KategorieTableMap::COL_NAZEV, $this->nazev);
        }
        if ($this->isColumnModified(KategorieTableMap::COL_KATEGORIEID)) {
            $criteria->add(KategorieTableMap::COL_KATEGORIEID, $this->kategorieid);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildKategorieQuery::create();
        $criteria->add(KategorieTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \SmartFridge\Kategorie (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIkona($this->getIkona());
        $copyObj->setNazev($this->getNazev());
        $copyObj->setKategorieid($this->getKategorieid());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getIngrediences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addIngredience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getKategoriesRelatedById() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addKategorieRelatedById($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProdukts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProdukt($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \SmartFridge\Kategorie Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildKategorie object.
     *
     * @param  ChildKategorie $v
     * @return $this|\SmartFridge\Kategorie The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKategorieRelatedByKategorieid(ChildKategorie $v = null)
    {
        if ($v === null) {
            $this->setKategorieid(NULL);
        } else {
            $this->setKategorieid($v->getId());
        }

        $this->aKategorieRelatedByKategorieid = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildKategorie object, it will not be re-added.
        if ($v !== null) {
            $v->addKategorieRelatedById($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildKategorie object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildKategorie The associated ChildKategorie object.
     * @throws PropelException
     */
    public function getKategorieRelatedByKategorieid(ConnectionInterface $con = null)
    {
        if ($this->aKategorieRelatedByKategorieid === null && ($this->kategorieid !== null)) {
            $this->aKategorieRelatedByKategorieid = ChildKategorieQuery::create()->findPk($this->kategorieid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKategorieRelatedByKategorieid->addKategoriesRelatedById($this);
             */
        }

        return $this->aKategorieRelatedByKategorieid;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Ingredience' == $relationName) {
            return $this->initIngrediences();
        }
        if ('KategorieRelatedById' == $relationName) {
            return $this->initKategoriesRelatedById();
        }
        if ('Produkt' == $relationName) {
            return $this->initProdukts();
        }
    }

    /**
     * Clears out the collIngrediences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addIngrediences()
     */
    public function clearIngrediences()
    {
        $this->collIngrediences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collIngrediences collection loaded partially.
     */
    public function resetPartialIngrediences($v = true)
    {
        $this->collIngrediencesPartial = $v;
    }

    /**
     * Initializes the collIngrediences collection.
     *
     * By default this just sets the collIngrediences collection to an empty array (like clearcollIngrediences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initIngrediences($overrideExisting = true)
    {
        if (null !== $this->collIngrediences && !$overrideExisting) {
            return;
        }

        $collectionClassName = IngredienceTableMap::getTableMap()->getCollectionClassName();

        $this->collIngrediences = new $collectionClassName;
        $this->collIngrediences->setModel('\SmartFridge\Ingredience');
    }

    /**
     * Gets an array of ChildIngredience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildKategorie is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     * @throws PropelException
     */
    public function getIngrediences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collIngrediencesPartial && !$this->isNew();
        if (null === $this->collIngrediences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collIngrediences) {
                // return empty collection
                $this->initIngrediences();
            } else {
                $collIngrediences = ChildIngredienceQuery::create(null, $criteria)
                    ->filterByKategorie($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collIngrediencesPartial && count($collIngrediences)) {
                        $this->initIngrediences(false);

                        foreach ($collIngrediences as $obj) {
                            if (false == $this->collIngrediences->contains($obj)) {
                                $this->collIngrediences->append($obj);
                            }
                        }

                        $this->collIngrediencesPartial = true;
                    }

                    return $collIngrediences;
                }

                if ($partial && $this->collIngrediences) {
                    foreach ($this->collIngrediences as $obj) {
                        if ($obj->isNew()) {
                            $collIngrediences[] = $obj;
                        }
                    }
                }

                $this->collIngrediences = $collIngrediences;
                $this->collIngrediencesPartial = false;
            }
        }

        return $this->collIngrediences;
    }

    /**
     * Sets a collection of ChildIngredience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $ingrediences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildKategorie The current object (for fluent API support)
     */
    public function setIngrediences(Collection $ingrediences, ConnectionInterface $con = null)
    {
        /** @var ChildIngredience[] $ingrediencesToDelete */
        $ingrediencesToDelete = $this->getIngrediences(new Criteria(), $con)->diff($ingrediences);


        $this->ingrediencesScheduledForDeletion = $ingrediencesToDelete;

        foreach ($ingrediencesToDelete as $ingredienceRemoved) {
            $ingredienceRemoved->setKategorie(null);
        }

        $this->collIngrediences = null;
        foreach ($ingrediences as $ingredience) {
            $this->addIngredience($ingredience);
        }

        $this->collIngrediences = $ingrediences;
        $this->collIngrediencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ingredience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Ingredience objects.
     * @throws PropelException
     */
    public function countIngrediences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collIngrediencesPartial && !$this->isNew();
        if (null === $this->collIngrediences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collIngrediences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getIngrediences());
            }

            $query = ChildIngredienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByKategorie($this)
                ->count($con);
        }

        return count($this->collIngrediences);
    }

    /**
     * Method called to associate a ChildIngredience object to this object
     * through the ChildIngredience foreign key attribute.
     *
     * @param  ChildIngredience $l ChildIngredience
     * @return $this|\SmartFridge\Kategorie The current object (for fluent API support)
     */
    public function addIngredience(ChildIngredience $l)
    {
        if ($this->collIngrediences === null) {
            $this->initIngrediences();
            $this->collIngrediencesPartial = true;
        }

        if (!$this->collIngrediences->contains($l)) {
            $this->doAddIngredience($l);

            if ($this->ingrediencesScheduledForDeletion and $this->ingrediencesScheduledForDeletion->contains($l)) {
                $this->ingrediencesScheduledForDeletion->remove($this->ingrediencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildIngredience $ingredience The ChildIngredience object to add.
     */
    protected function doAddIngredience(ChildIngredience $ingredience)
    {
        $this->collIngrediences[]= $ingredience;
        $ingredience->setKategorie($this);
    }

    /**
     * @param  ChildIngredience $ingredience The ChildIngredience object to remove.
     * @return $this|ChildKategorie The current object (for fluent API support)
     */
    public function removeIngredience(ChildIngredience $ingredience)
    {
        if ($this->getIngrediences()->contains($ingredience)) {
            $pos = $this->collIngrediences->search($ingredience);
            $this->collIngrediences->remove($pos);
            if (null === $this->ingrediencesScheduledForDeletion) {
                $this->ingrediencesScheduledForDeletion = clone $this->collIngrediences;
                $this->ingrediencesScheduledForDeletion->clear();
            }
            $this->ingrediencesScheduledForDeletion[]= $ingredience;
            $ingredience->setKategorie(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Kategorie is new, it will return
     * an empty collection; or if this Kategorie has previously
     * been saved, it will retrieve related Ingrediences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Kategorie.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     */
    public function getIngrediencesJoinJednotka(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildIngredienceQuery::create(null, $criteria);
        $query->joinWith('Jednotka', $joinBehavior);

        return $this->getIngrediences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Kategorie is new, it will return
     * an empty collection; or if this Kategorie has previously
     * been saved, it will retrieve related Ingrediences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Kategorie.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     */
    public function getIngrediencesJoinProdukt(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildIngredienceQuery::create(null, $criteria);
        $query->joinWith('Produkt', $joinBehavior);

        return $this->getIngrediences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Kategorie is new, it will return
     * an empty collection; or if this Kategorie has previously
     * been saved, it will retrieve related Ingrediences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Kategorie.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     */
    public function getIngrediencesJoinRecepty(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildIngredienceQuery::create(null, $criteria);
        $query->joinWith('Recepty', $joinBehavior);

        return $this->getIngrediences($query, $con);
    }

    /**
     * Clears out the collKategoriesRelatedById collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addKategoriesRelatedById()
     */
    public function clearKategoriesRelatedById()
    {
        $this->collKategoriesRelatedById = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collKategoriesRelatedById collection loaded partially.
     */
    public function resetPartialKategoriesRelatedById($v = true)
    {
        $this->collKategoriesRelatedByIdPartial = $v;
    }

    /**
     * Initializes the collKategoriesRelatedById collection.
     *
     * By default this just sets the collKategoriesRelatedById collection to an empty array (like clearcollKategoriesRelatedById());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initKategoriesRelatedById($overrideExisting = true)
    {
        if (null !== $this->collKategoriesRelatedById && !$overrideExisting) {
            return;
        }

        $collectionClassName = KategorieTableMap::getTableMap()->getCollectionClassName();

        $this->collKategoriesRelatedById = new $collectionClassName;
        $this->collKategoriesRelatedById->setModel('\SmartFridge\Kategorie');
    }

    /**
     * Gets an array of ChildKategorie objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildKategorie is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildKategorie[] List of ChildKategorie objects
     * @throws PropelException
     */
    public function getKategoriesRelatedById(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collKategoriesRelatedByIdPartial && !$this->isNew();
        if (null === $this->collKategoriesRelatedById || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collKategoriesRelatedById) {
                // return empty collection
                $this->initKategoriesRelatedById();
            } else {
                $collKategoriesRelatedById = ChildKategorieQuery::create(null, $criteria)
                    ->filterByKategorieRelatedByKategorieid($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collKategoriesRelatedByIdPartial && count($collKategoriesRelatedById)) {
                        $this->initKategoriesRelatedById(false);

                        foreach ($collKategoriesRelatedById as $obj) {
                            if (false == $this->collKategoriesRelatedById->contains($obj)) {
                                $this->collKategoriesRelatedById->append($obj);
                            }
                        }

                        $this->collKategoriesRelatedByIdPartial = true;
                    }

                    return $collKategoriesRelatedById;
                }

                if ($partial && $this->collKategoriesRelatedById) {
                    foreach ($this->collKategoriesRelatedById as $obj) {
                        if ($obj->isNew()) {
                            $collKategoriesRelatedById[] = $obj;
                        }
                    }
                }

                $this->collKategoriesRelatedById = $collKategoriesRelatedById;
                $this->collKategoriesRelatedByIdPartial = false;
            }
        }

        return $this->collKategoriesRelatedById;
    }

    /**
     * Sets a collection of ChildKategorie objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $kategoriesRelatedById A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildKategorie The current object (for fluent API support)
     */
    public function setKategoriesRelatedById(Collection $kategoriesRelatedById, ConnectionInterface $con = null)
    {
        /** @var ChildKategorie[] $kategoriesRelatedByIdToDelete */
        $kategoriesRelatedByIdToDelete = $this->getKategoriesRelatedById(new Criteria(), $con)->diff($kategoriesRelatedById);


        $this->kategoriesRelatedByIdScheduledForDeletion = $kategoriesRelatedByIdToDelete;

        foreach ($kategoriesRelatedByIdToDelete as $kategorieRelatedByIdRemoved) {
            $kategorieRelatedByIdRemoved->setKategorieRelatedByKategorieid(null);
        }

        $this->collKategoriesRelatedById = null;
        foreach ($kategoriesRelatedById as $kategorieRelatedById) {
            $this->addKategorieRelatedById($kategorieRelatedById);
        }

        $this->collKategoriesRelatedById = $kategoriesRelatedById;
        $this->collKategoriesRelatedByIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Kategorie objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Kategorie objects.
     * @throws PropelException
     */
    public function countKategoriesRelatedById(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collKategoriesRelatedByIdPartial && !$this->isNew();
        if (null === $this->collKategoriesRelatedById || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collKategoriesRelatedById) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getKategoriesRelatedById());
            }

            $query = ChildKategorieQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByKategorieRelatedByKategorieid($this)
                ->count($con);
        }

        return count($this->collKategoriesRelatedById);
    }

    /**
     * Method called to associate a ChildKategorie object to this object
     * through the ChildKategorie foreign key attribute.
     *
     * @param  ChildKategorie $l ChildKategorie
     * @return $this|\SmartFridge\Kategorie The current object (for fluent API support)
     */
    public function addKategorieRelatedById(ChildKategorie $l)
    {
        if ($this->collKategoriesRelatedById === null) {
            $this->initKategoriesRelatedById();
            $this->collKategoriesRelatedByIdPartial = true;
        }

        if (!$this->collKategoriesRelatedById->contains($l)) {
            $this->doAddKategorieRelatedById($l);

            if ($this->kategoriesRelatedByIdScheduledForDeletion and $this->kategoriesRelatedByIdScheduledForDeletion->contains($l)) {
                $this->kategoriesRelatedByIdScheduledForDeletion->remove($this->kategoriesRelatedByIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildKategorie $kategorieRelatedById The ChildKategorie object to add.
     */
    protected function doAddKategorieRelatedById(ChildKategorie $kategorieRelatedById)
    {
        $this->collKategoriesRelatedById[]= $kategorieRelatedById;
        $kategorieRelatedById->setKategorieRelatedByKategorieid($this);
    }

    /**
     * @param  ChildKategorie $kategorieRelatedById The ChildKategorie object to remove.
     * @return $this|ChildKategorie The current object (for fluent API support)
     */
    public function removeKategorieRelatedById(ChildKategorie $kategorieRelatedById)
    {
        if ($this->getKategoriesRelatedById()->contains($kategorieRelatedById)) {
            $pos = $this->collKategoriesRelatedById->search($kategorieRelatedById);
            $this->collKategoriesRelatedById->remove($pos);
            if (null === $this->kategoriesRelatedByIdScheduledForDeletion) {
                $this->kategoriesRelatedByIdScheduledForDeletion = clone $this->collKategoriesRelatedById;
                $this->kategoriesRelatedByIdScheduledForDeletion->clear();
            }
            $this->kategoriesRelatedByIdScheduledForDeletion[]= $kategorieRelatedById;
            $kategorieRelatedById->setKategorieRelatedByKategorieid(null);
        }

        return $this;
    }

    /**
     * Clears out the collProdukts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProdukts()
     */
    public function clearProdukts()
    {
        $this->collProdukts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProdukts collection loaded partially.
     */
    public function resetPartialProdukts($v = true)
    {
        $this->collProduktsPartial = $v;
    }

    /**
     * Initializes the collProdukts collection.
     *
     * By default this just sets the collProdukts collection to an empty array (like clearcollProdukts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProdukts($overrideExisting = true)
    {
        if (null !== $this->collProdukts && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProduktTableMap::getTableMap()->getCollectionClassName();

        $this->collProdukts = new $collectionClassName;
        $this->collProdukts->setModel('\SmartFridge\Produkt');
    }

    /**
     * Gets an array of ChildProdukt objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildKategorie is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProdukt[] List of ChildProdukt objects
     * @throws PropelException
     */
    public function getProdukts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProduktsPartial && !$this->isNew();
        if (null === $this->collProdukts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProdukts) {
                // return empty collection
                $this->initProdukts();
            } else {
                $collProdukts = ChildProduktQuery::create(null, $criteria)
                    ->filterByKategorie($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProduktsPartial && count($collProdukts)) {
                        $this->initProdukts(false);

                        foreach ($collProdukts as $obj) {
                            if (false == $this->collProdukts->contains($obj)) {
                                $this->collProdukts->append($obj);
                            }
                        }

                        $this->collProduktsPartial = true;
                    }

                    return $collProdukts;
                }

                if ($partial && $this->collProdukts) {
                    foreach ($this->collProdukts as $obj) {
                        if ($obj->isNew()) {
                            $collProdukts[] = $obj;
                        }
                    }
                }

                $this->collProdukts = $collProdukts;
                $this->collProduktsPartial = false;
            }
        }

        return $this->collProdukts;
    }

    /**
     * Sets a collection of ChildProdukt objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $produkts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildKategorie The current object (for fluent API support)
     */
    public function setProdukts(Collection $produkts, ConnectionInterface $con = null)
    {
        /** @var ChildProdukt[] $produktsToDelete */
        $produktsToDelete = $this->getProdukts(new Criteria(), $con)->diff($produkts);


        $this->produktsScheduledForDeletion = $produktsToDelete;

        foreach ($produktsToDelete as $produktRemoved) {
            $produktRemoved->setKategorie(null);
        }

        $this->collProdukts = null;
        foreach ($produkts as $produkt) {
            $this->addProdukt($produkt);
        }

        $this->collProdukts = $produkts;
        $this->collProduktsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Produkt objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Produkt objects.
     * @throws PropelException
     */
    public function countProdukts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProduktsPartial && !$this->isNew();
        if (null === $this->collProdukts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProdukts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProdukts());
            }

            $query = ChildProduktQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByKategorie($this)
                ->count($con);
        }

        return count($this->collProdukts);
    }

    /**
     * Method called to associate a ChildProdukt object to this object
     * through the ChildProdukt foreign key attribute.
     *
     * @param  ChildProdukt $l ChildProdukt
     * @return $this|\SmartFridge\Kategorie The current object (for fluent API support)
     */
    public function addProdukt(ChildProdukt $l)
    {
        if ($this->collProdukts === null) {
            $this->initProdukts();
            $this->collProduktsPartial = true;
        }

        if (!$this->collProdukts->contains($l)) {
            $this->doAddProdukt($l);

            if ($this->produktsScheduledForDeletion and $this->produktsScheduledForDeletion->contains($l)) {
                $this->produktsScheduledForDeletion->remove($this->produktsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProdukt $produkt The ChildProdukt object to add.
     */
    protected function doAddProdukt(ChildProdukt $produkt)
    {
        $this->collProdukts[]= $produkt;
        $produkt->setKategorie($this);
    }

    /**
     * @param  ChildProdukt $produkt The ChildProdukt object to remove.
     * @return $this|ChildKategorie The current object (for fluent API support)
     */
    public function removeProdukt(ChildProdukt $produkt)
    {
        if ($this->getProdukts()->contains($produkt)) {
            $pos = $this->collProdukts->search($produkt);
            $this->collProdukts->remove($pos);
            if (null === $this->produktsScheduledForDeletion) {
                $this->produktsScheduledForDeletion = clone $this->collProdukts;
                $this->produktsScheduledForDeletion->clear();
            }
            $this->produktsScheduledForDeletion[]= $produkt;
            $produkt->setKategorie(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Kategorie is new, it will return
     * an empty collection; or if this Kategorie has previously
     * been saved, it will retrieve related Produkts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Kategorie.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProdukt[] List of ChildProdukt objects
     */
    public function getProduktsJoinJednotka(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProduktQuery::create(null, $criteria);
        $query->joinWith('Jednotka', $joinBehavior);

        return $this->getProdukts($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aKategorieRelatedByKategorieid) {
            $this->aKategorieRelatedByKategorieid->removeKategorieRelatedById($this);
        }
        $this->id = null;
        $this->ikona = null;
        $this->nazev = null;
        $this->kategorieid = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collIngrediences) {
                foreach ($this->collIngrediences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collKategoriesRelatedById) {
                foreach ($this->collKategoriesRelatedById as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProdukts) {
                foreach ($this->collProdukts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collIngrediences = null;
        $this->collKategoriesRelatedById = null;
        $this->collProdukts = null;
        $this->aKategorieRelatedByKategorieid = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(KategorieTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
