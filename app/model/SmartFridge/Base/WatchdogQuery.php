<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SmartFridge\Watchdog as ChildWatchdog;
use SmartFridge\WatchdogQuery as ChildWatchdogQuery;
use SmartFridge\Map\WatchdogTableMap;

/**
 * Base class that represents a query for the 'Watchdog' table.
 *
 *
 *
 * @method     ChildWatchdogQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildWatchdogQuery orderByProduktid($order = Criteria::ASC) Order by the produktID column
 * @method     ChildWatchdogQuery orderByUzivatelid($order = Criteria::ASC) Order by the uzivatelID column
 * @method     ChildWatchdogQuery orderByCena($order = Criteria::ASC) Order by the cena column
 *
 * @method     ChildWatchdogQuery groupById() Group by the id column
 * @method     ChildWatchdogQuery groupByProduktid() Group by the produktID column
 * @method     ChildWatchdogQuery groupByUzivatelid() Group by the uzivatelID column
 * @method     ChildWatchdogQuery groupByCena() Group by the cena column
 *
 * @method     ChildWatchdogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildWatchdogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildWatchdogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildWatchdogQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildWatchdogQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildWatchdogQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildWatchdogQuery leftJoinProdukt($relationAlias = null) Adds a LEFT JOIN clause to the query using the Produkt relation
 * @method     ChildWatchdogQuery rightJoinProdukt($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Produkt relation
 * @method     ChildWatchdogQuery innerJoinProdukt($relationAlias = null) Adds a INNER JOIN clause to the query using the Produkt relation
 *
 * @method     ChildWatchdogQuery joinWithProdukt($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Produkt relation
 *
 * @method     ChildWatchdogQuery leftJoinWithProdukt() Adds a LEFT JOIN clause and with to the query using the Produkt relation
 * @method     ChildWatchdogQuery rightJoinWithProdukt() Adds a RIGHT JOIN clause and with to the query using the Produkt relation
 * @method     ChildWatchdogQuery innerJoinWithProdukt() Adds a INNER JOIN clause and with to the query using the Produkt relation
 *
 * @method     ChildWatchdogQuery leftJoinUzivatel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Uzivatel relation
 * @method     ChildWatchdogQuery rightJoinUzivatel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Uzivatel relation
 * @method     ChildWatchdogQuery innerJoinUzivatel($relationAlias = null) Adds a INNER JOIN clause to the query using the Uzivatel relation
 *
 * @method     ChildWatchdogQuery joinWithUzivatel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Uzivatel relation
 *
 * @method     ChildWatchdogQuery leftJoinWithUzivatel() Adds a LEFT JOIN clause and with to the query using the Uzivatel relation
 * @method     ChildWatchdogQuery rightJoinWithUzivatel() Adds a RIGHT JOIN clause and with to the query using the Uzivatel relation
 * @method     ChildWatchdogQuery innerJoinWithUzivatel() Adds a INNER JOIN clause and with to the query using the Uzivatel relation
 *
 * @method     \SmartFridge\ProduktQuery|\SmartFridge\UzivatelQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildWatchdog findOne(ConnectionInterface $con = null) Return the first ChildWatchdog matching the query
 * @method     ChildWatchdog findOneOrCreate(ConnectionInterface $con = null) Return the first ChildWatchdog matching the query, or a new ChildWatchdog object populated from the query conditions when no match is found
 *
 * @method     ChildWatchdog findOneById(int $id) Return the first ChildWatchdog filtered by the id column
 * @method     ChildWatchdog findOneByProduktid(int $produktID) Return the first ChildWatchdog filtered by the produktID column
 * @method     ChildWatchdog findOneByUzivatelid(int $uzivatelID) Return the first ChildWatchdog filtered by the uzivatelID column
 * @method     ChildWatchdog findOneByCena(double $cena) Return the first ChildWatchdog filtered by the cena column *

 * @method     ChildWatchdog requirePk($key, ConnectionInterface $con = null) Return the ChildWatchdog by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWatchdog requireOne(ConnectionInterface $con = null) Return the first ChildWatchdog matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWatchdog requireOneById(int $id) Return the first ChildWatchdog filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWatchdog requireOneByProduktid(int $produktID) Return the first ChildWatchdog filtered by the produktID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWatchdog requireOneByUzivatelid(int $uzivatelID) Return the first ChildWatchdog filtered by the uzivatelID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWatchdog requireOneByCena(double $cena) Return the first ChildWatchdog filtered by the cena column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWatchdog[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildWatchdog objects based on current ModelCriteria
 * @method     ChildWatchdog[]|ObjectCollection findById(int $id) Return ChildWatchdog objects filtered by the id column
 * @method     ChildWatchdog[]|ObjectCollection findByProduktid(int $produktID) Return ChildWatchdog objects filtered by the produktID column
 * @method     ChildWatchdog[]|ObjectCollection findByUzivatelid(int $uzivatelID) Return ChildWatchdog objects filtered by the uzivatelID column
 * @method     ChildWatchdog[]|ObjectCollection findByCena(double $cena) Return ChildWatchdog objects filtered by the cena column
 * @method     ChildWatchdog[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class WatchdogQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SmartFridge\Base\WatchdogQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SmartFridge\\Watchdog', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildWatchdogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildWatchdogQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildWatchdogQuery) {
            return $criteria;
        }
        $query = new ChildWatchdogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildWatchdog|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(WatchdogTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = WatchdogTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWatchdog A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, produktID, uzivatelID, cena FROM Watchdog WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildWatchdog $obj */
            $obj = new ChildWatchdog();
            $obj->hydrate($row);
            WatchdogTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildWatchdog|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildWatchdogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(WatchdogTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildWatchdogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(WatchdogTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWatchdogQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(WatchdogTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(WatchdogTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WatchdogTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the produktID column
     *
     * Example usage:
     * <code>
     * $query->filterByProduktid(1234); // WHERE produktID = 1234
     * $query->filterByProduktid(array(12, 34)); // WHERE produktID IN (12, 34)
     * $query->filterByProduktid(array('min' => 12)); // WHERE produktID > 12
     * </code>
     *
     * @see       filterByProdukt()
     *
     * @param     mixed $produktid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWatchdogQuery The current query, for fluid interface
     */
    public function filterByProduktid($produktid = null, $comparison = null)
    {
        if (is_array($produktid)) {
            $useMinMax = false;
            if (isset($produktid['min'])) {
                $this->addUsingAlias(WatchdogTableMap::COL_PRODUKTID, $produktid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produktid['max'])) {
                $this->addUsingAlias(WatchdogTableMap::COL_PRODUKTID, $produktid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WatchdogTableMap::COL_PRODUKTID, $produktid, $comparison);
    }

    /**
     * Filter the query on the uzivatelID column
     *
     * Example usage:
     * <code>
     * $query->filterByUzivatelid(1234); // WHERE uzivatelID = 1234
     * $query->filterByUzivatelid(array(12, 34)); // WHERE uzivatelID IN (12, 34)
     * $query->filterByUzivatelid(array('min' => 12)); // WHERE uzivatelID > 12
     * </code>
     *
     * @see       filterByUzivatel()
     *
     * @param     mixed $uzivatelid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWatchdogQuery The current query, for fluid interface
     */
    public function filterByUzivatelid($uzivatelid = null, $comparison = null)
    {
        if (is_array($uzivatelid)) {
            $useMinMax = false;
            if (isset($uzivatelid['min'])) {
                $this->addUsingAlias(WatchdogTableMap::COL_UZIVATELID, $uzivatelid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uzivatelid['max'])) {
                $this->addUsingAlias(WatchdogTableMap::COL_UZIVATELID, $uzivatelid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WatchdogTableMap::COL_UZIVATELID, $uzivatelid, $comparison);
    }

    /**
     * Filter the query on the cena column
     *
     * Example usage:
     * <code>
     * $query->filterByCena(1234); // WHERE cena = 1234
     * $query->filterByCena(array(12, 34)); // WHERE cena IN (12, 34)
     * $query->filterByCena(array('min' => 12)); // WHERE cena > 12
     * </code>
     *
     * @param     mixed $cena The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWatchdogQuery The current query, for fluid interface
     */
    public function filterByCena($cena = null, $comparison = null)
    {
        if (is_array($cena)) {
            $useMinMax = false;
            if (isset($cena['min'])) {
                $this->addUsingAlias(WatchdogTableMap::COL_CENA, $cena['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cena['max'])) {
                $this->addUsingAlias(WatchdogTableMap::COL_CENA, $cena['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WatchdogTableMap::COL_CENA, $cena, $comparison);
    }

    /**
     * Filter the query by a related \SmartFridge\Produkt object
     *
     * @param \SmartFridge\Produkt|ObjectCollection $produkt The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWatchdogQuery The current query, for fluid interface
     */
    public function filterByProdukt($produkt, $comparison = null)
    {
        if ($produkt instanceof \SmartFridge\Produkt) {
            return $this
                ->addUsingAlias(WatchdogTableMap::COL_PRODUKTID, $produkt->getId(), $comparison);
        } elseif ($produkt instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WatchdogTableMap::COL_PRODUKTID, $produkt->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProdukt() only accepts arguments of type \SmartFridge\Produkt or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Produkt relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWatchdogQuery The current query, for fluid interface
     */
    public function joinProdukt($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Produkt');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Produkt');
        }

        return $this;
    }

    /**
     * Use the Produkt relation Produkt object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\ProduktQuery A secondary query class using the current class as primary query
     */
    public function useProduktQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProdukt($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Produkt', '\SmartFridge\ProduktQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Uzivatel object
     *
     * @param \SmartFridge\Uzivatel|ObjectCollection $uzivatel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWatchdogQuery The current query, for fluid interface
     */
    public function filterByUzivatel($uzivatel, $comparison = null)
    {
        if ($uzivatel instanceof \SmartFridge\Uzivatel) {
            return $this
                ->addUsingAlias(WatchdogTableMap::COL_UZIVATELID, $uzivatel->getId(), $comparison);
        } elseif ($uzivatel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WatchdogTableMap::COL_UZIVATELID, $uzivatel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUzivatel() only accepts arguments of type \SmartFridge\Uzivatel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Uzivatel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWatchdogQuery The current query, for fluid interface
     */
    public function joinUzivatel($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Uzivatel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Uzivatel');
        }

        return $this;
    }

    /**
     * Use the Uzivatel relation Uzivatel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\UzivatelQuery A secondary query class using the current class as primary query
     */
    public function useUzivatelQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUzivatel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Uzivatel', '\SmartFridge\UzivatelQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildWatchdog $watchdog Object to remove from the list of results
     *
     * @return $this|ChildWatchdogQuery The current query, for fluid interface
     */
    public function prune($watchdog = null)
    {
        if ($watchdog) {
            $this->addUsingAlias(WatchdogTableMap::COL_ID, $watchdog->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Watchdog table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WatchdogTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            WatchdogTableMap::clearInstancePool();
            WatchdogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WatchdogTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(WatchdogTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            WatchdogTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            WatchdogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // WatchdogQuery
