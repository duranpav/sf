<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use SmartFridge\Ingredience as ChildIngredience;
use SmartFridge\IngredienceQuery as ChildIngredienceQuery;
use SmartFridge\Recenze as ChildRecenze;
use SmartFridge\RecenzeQuery as ChildRecenzeQuery;
use SmartFridge\Recepty as ChildRecepty;
use SmartFridge\ReceptyQuery as ChildReceptyQuery;
use SmartFridge\Map\IngredienceTableMap;
use SmartFridge\Map\RecenzeTableMap;
use SmartFridge\Map\ReceptyTableMap;

/**
 * Base class that represents a row from the 'Recepty' table.
 *
 *
 *
 * @package    propel.generator.SmartFridge.Base
 */
abstract class Recepty implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\SmartFridge\\Map\\ReceptyTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the nazev field.
     *
     * @var        string
     */
    protected $nazev;

    /**
     * The value for the pocetporci field.
     *
     * @var        int
     */
    protected $pocetporci;

    /**
     * The value for the postup field.
     *
     * @var        string
     */
    protected $postup;

    /**
     * The value for the pribliznycas field.
     *
     * @var        int
     */
    protected $pribliznycas;

    /**
     * The value for the priorita field.
     *
     * @var        int
     */
    protected $priorita;

    /**
     * The value for the slozitost field.
     *
     * @var        string
     */
    protected $slozitost;

    /**
     * The value for the fotka field.
     *
     * @var        string
     */
    protected $fotka;

    /**
     * @var        ObjectCollection|ChildIngredience[] Collection to store aggregation of ChildIngredience objects.
     */
    protected $collIngrediences;
    protected $collIngrediencesPartial;

    /**
     * @var        ObjectCollection|ChildRecenze[] Collection to store aggregation of ChildRecenze objects.
     */
    protected $collRecenzes;
    protected $collRecenzesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildIngredience[]
     */
    protected $ingrediencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRecenze[]
     */
    protected $recenzesScheduledForDeletion = null;

    /**
     * Initializes internal state of SmartFridge\Base\Recepty object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Recepty</code> instance.  If
     * <code>obj</code> is an instance of <code>Recepty</code>, delegates to
     * <code>equals(Recepty)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Recepty The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [nazev] column value.
     *
     * @return string
     */
    public function getNazev()
    {
        return $this->nazev;
    }

    /**
     * Get the [pocetporci] column value.
     *
     * @return int
     */
    public function getPocetporci()
    {
        return $this->pocetporci;
    }

    /**
     * Get the [postup] column value.
     *
     * @return string
     */
    public function getPostup()
    {
        return $this->postup;
    }

    /**
     * Get the [pribliznycas] column value.
     *
     * @return int
     */
    public function getPribliznycas()
    {
        return $this->pribliznycas;
    }

    /**
     * Get the [priorita] column value.
     *
     * @return int
     */
    public function getPriorita()
    {
        return $this->priorita;
    }

    /**
     * Get the [slozitost] column value.
     *
     * @return string
     */
    public function getSlozitost()
    {
        return $this->slozitost;
    }

    /**
     * Get the [fotka] column value.
     *
     * @return string
     */
    public function getFotka()
    {
        return $this->fotka;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Recepty The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[ReceptyTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [nazev] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Recepty The current object (for fluent API support)
     */
    public function setNazev($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nazev !== $v) {
            $this->nazev = $v;
            $this->modifiedColumns[ReceptyTableMap::COL_NAZEV] = true;
        }

        return $this;
    } // setNazev()

    /**
     * Set the value of [pocetporci] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Recepty The current object (for fluent API support)
     */
    public function setPocetporci($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pocetporci !== $v) {
            $this->pocetporci = $v;
            $this->modifiedColumns[ReceptyTableMap::COL_POCETPORCI] = true;
        }

        return $this;
    } // setPocetporci()

    /**
     * Set the value of [postup] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Recepty The current object (for fluent API support)
     */
    public function setPostup($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->postup !== $v) {
            $this->postup = $v;
            $this->modifiedColumns[ReceptyTableMap::COL_POSTUP] = true;
        }

        return $this;
    } // setPostup()

    /**
     * Set the value of [pribliznycas] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Recepty The current object (for fluent API support)
     */
    public function setPribliznycas($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pribliznycas !== $v) {
            $this->pribliznycas = $v;
            $this->modifiedColumns[ReceptyTableMap::COL_PRIBLIZNYCAS] = true;
        }

        return $this;
    } // setPribliznycas()

    /**
     * Set the value of [priorita] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Recepty The current object (for fluent API support)
     */
    public function setPriorita($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->priorita !== $v) {
            $this->priorita = $v;
            $this->modifiedColumns[ReceptyTableMap::COL_PRIORITA] = true;
        }

        return $this;
    } // setPriorita()

    /**
     * Set the value of [slozitost] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Recepty The current object (for fluent API support)
     */
    public function setSlozitost($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->slozitost !== $v) {
            $this->slozitost = $v;
            $this->modifiedColumns[ReceptyTableMap::COL_SLOZITOST] = true;
        }

        return $this;
    } // setSlozitost()

    /**
     * Set the value of [fotka] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Recepty The current object (for fluent API support)
     */
    public function setFotka($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fotka !== $v) {
            $this->fotka = $v;
            $this->modifiedColumns[ReceptyTableMap::COL_FOTKA] = true;
        }

        return $this;
    } // setFotka()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ReceptyTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ReceptyTableMap::translateFieldName('Nazev', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nazev = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ReceptyTableMap::translateFieldName('Pocetporci', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pocetporci = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ReceptyTableMap::translateFieldName('Postup', TableMap::TYPE_PHPNAME, $indexType)];
            $this->postup = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ReceptyTableMap::translateFieldName('Pribliznycas', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pribliznycas = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ReceptyTableMap::translateFieldName('Priorita', TableMap::TYPE_PHPNAME, $indexType)];
            $this->priorita = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ReceptyTableMap::translateFieldName('Slozitost', TableMap::TYPE_PHPNAME, $indexType)];
            $this->slozitost = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ReceptyTableMap::translateFieldName('Fotka', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fotka = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 8; // 8 = ReceptyTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\SmartFridge\\Recepty'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ReceptyTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildReceptyQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collIngrediences = null;

            $this->collRecenzes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Recepty::setDeleted()
     * @see Recepty::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ReceptyTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildReceptyQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ReceptyTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ReceptyTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->ingrediencesScheduledForDeletion !== null) {
                if (!$this->ingrediencesScheduledForDeletion->isEmpty()) {
                    foreach ($this->ingrediencesScheduledForDeletion as $ingredience) {
                        // need to save related object because we set the relation to null
                        $ingredience->save($con);
                    }
                    $this->ingrediencesScheduledForDeletion = null;
                }
            }

            if ($this->collIngrediences !== null) {
                foreach ($this->collIngrediences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->recenzesScheduledForDeletion !== null) {
                if (!$this->recenzesScheduledForDeletion->isEmpty()) {
                    foreach ($this->recenzesScheduledForDeletion as $recenze) {
                        // need to save related object because we set the relation to null
                        $recenze->save($con);
                    }
                    $this->recenzesScheduledForDeletion = null;
                }
            }

            if ($this->collRecenzes !== null) {
                foreach ($this->collRecenzes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ReceptyTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ReceptyTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ReceptyTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_NAZEV)) {
            $modifiedColumns[':p' . $index++]  = 'nazev';
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_POCETPORCI)) {
            $modifiedColumns[':p' . $index++]  = 'pocetPorci';
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_POSTUP)) {
            $modifiedColumns[':p' . $index++]  = 'postup';
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_PRIBLIZNYCAS)) {
            $modifiedColumns[':p' . $index++]  = 'pribliznyCas';
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_PRIORITA)) {
            $modifiedColumns[':p' . $index++]  = 'priorita';
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_SLOZITOST)) {
            $modifiedColumns[':p' . $index++]  = 'slozitost';
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_FOTKA)) {
            $modifiedColumns[':p' . $index++]  = 'fotka';
        }

        $sql = sprintf(
            'INSERT INTO Recepty (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'nazev':
                        $stmt->bindValue($identifier, $this->nazev, PDO::PARAM_STR);
                        break;
                    case 'pocetPorci':
                        $stmt->bindValue($identifier, $this->pocetporci, PDO::PARAM_INT);
                        break;
                    case 'postup':
                        $stmt->bindValue($identifier, $this->postup, PDO::PARAM_STR);
                        break;
                    case 'pribliznyCas':
                        $stmt->bindValue($identifier, $this->pribliznycas, PDO::PARAM_INT);
                        break;
                    case 'priorita':
                        $stmt->bindValue($identifier, $this->priorita, PDO::PARAM_INT);
                        break;
                    case 'slozitost':
                        $stmt->bindValue($identifier, $this->slozitost, PDO::PARAM_STR);
                        break;
                    case 'fotka':
                        $stmt->bindValue($identifier, $this->fotka, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ReceptyTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNazev();
                break;
            case 2:
                return $this->getPocetporci();
                break;
            case 3:
                return $this->getPostup();
                break;
            case 4:
                return $this->getPribliznycas();
                break;
            case 5:
                return $this->getPriorita();
                break;
            case 6:
                return $this->getSlozitost();
                break;
            case 7:
                return $this->getFotka();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Recepty'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Recepty'][$this->hashCode()] = true;
        $keys = ReceptyTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNazev(),
            $keys[2] => $this->getPocetporci(),
            $keys[3] => $this->getPostup(),
            $keys[4] => $this->getPribliznycas(),
            $keys[5] => $this->getPriorita(),
            $keys[6] => $this->getSlozitost(),
            $keys[7] => $this->getFotka(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collIngrediences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ingrediences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Ingrediences';
                        break;
                    default:
                        $key = 'Ingrediences';
                }

                $result[$key] = $this->collIngrediences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRecenzes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'recenzes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Recenzes';
                        break;
                    default:
                        $key = 'Recenzes';
                }

                $result[$key] = $this->collRecenzes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\SmartFridge\Recepty
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ReceptyTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\SmartFridge\Recepty
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNazev($value);
                break;
            case 2:
                $this->setPocetporci($value);
                break;
            case 3:
                $this->setPostup($value);
                break;
            case 4:
                $this->setPribliznycas($value);
                break;
            case 5:
                $this->setPriorita($value);
                break;
            case 6:
                $this->setSlozitost($value);
                break;
            case 7:
                $this->setFotka($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ReceptyTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNazev($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPocetporci($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPostup($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPribliznycas($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPriorita($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSlozitost($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setFotka($arr[$keys[7]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\SmartFridge\Recepty The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ReceptyTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ReceptyTableMap::COL_ID)) {
            $criteria->add(ReceptyTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_NAZEV)) {
            $criteria->add(ReceptyTableMap::COL_NAZEV, $this->nazev);
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_POCETPORCI)) {
            $criteria->add(ReceptyTableMap::COL_POCETPORCI, $this->pocetporci);
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_POSTUP)) {
            $criteria->add(ReceptyTableMap::COL_POSTUP, $this->postup);
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_PRIBLIZNYCAS)) {
            $criteria->add(ReceptyTableMap::COL_PRIBLIZNYCAS, $this->pribliznycas);
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_PRIORITA)) {
            $criteria->add(ReceptyTableMap::COL_PRIORITA, $this->priorita);
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_SLOZITOST)) {
            $criteria->add(ReceptyTableMap::COL_SLOZITOST, $this->slozitost);
        }
        if ($this->isColumnModified(ReceptyTableMap::COL_FOTKA)) {
            $criteria->add(ReceptyTableMap::COL_FOTKA, $this->fotka);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildReceptyQuery::create();
        $criteria->add(ReceptyTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \SmartFridge\Recepty (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNazev($this->getNazev());
        $copyObj->setPocetporci($this->getPocetporci());
        $copyObj->setPostup($this->getPostup());
        $copyObj->setPribliznycas($this->getPribliznycas());
        $copyObj->setPriorita($this->getPriorita());
        $copyObj->setSlozitost($this->getSlozitost());
        $copyObj->setFotka($this->getFotka());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getIngrediences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addIngredience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRecenzes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRecenze($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \SmartFridge\Recepty Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Ingredience' == $relationName) {
            return $this->initIngrediences();
        }
        if ('Recenze' == $relationName) {
            return $this->initRecenzes();
        }
    }

    /**
     * Clears out the collIngrediences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addIngrediences()
     */
    public function clearIngrediences()
    {
        $this->collIngrediences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collIngrediences collection loaded partially.
     */
    public function resetPartialIngrediences($v = true)
    {
        $this->collIngrediencesPartial = $v;
    }

    /**
     * Initializes the collIngrediences collection.
     *
     * By default this just sets the collIngrediences collection to an empty array (like clearcollIngrediences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initIngrediences($overrideExisting = true)
    {
        if (null !== $this->collIngrediences && !$overrideExisting) {
            return;
        }

        $collectionClassName = IngredienceTableMap::getTableMap()->getCollectionClassName();

        $this->collIngrediences = new $collectionClassName;
        $this->collIngrediences->setModel('\SmartFridge\Ingredience');
    }

    /**
     * Gets an array of ChildIngredience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildRecepty is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     * @throws PropelException
     */
    public function getIngrediences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collIngrediencesPartial && !$this->isNew();
        if (null === $this->collIngrediences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collIngrediences) {
                // return empty collection
                $this->initIngrediences();
            } else {
                $collIngrediences = ChildIngredienceQuery::create(null, $criteria)
                    ->filterByRecepty($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collIngrediencesPartial && count($collIngrediences)) {
                        $this->initIngrediences(false);

                        foreach ($collIngrediences as $obj) {
                            if (false == $this->collIngrediences->contains($obj)) {
                                $this->collIngrediences->append($obj);
                            }
                        }

                        $this->collIngrediencesPartial = true;
                    }

                    return $collIngrediences;
                }

                if ($partial && $this->collIngrediences) {
                    foreach ($this->collIngrediences as $obj) {
                        if ($obj->isNew()) {
                            $collIngrediences[] = $obj;
                        }
                    }
                }

                $this->collIngrediences = $collIngrediences;
                $this->collIngrediencesPartial = false;
            }
        }

        return $this->collIngrediences;
    }

    /**
     * Sets a collection of ChildIngredience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $ingrediences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildRecepty The current object (for fluent API support)
     */
    public function setIngrediences(Collection $ingrediences, ConnectionInterface $con = null)
    {
        /** @var ChildIngredience[] $ingrediencesToDelete */
        $ingrediencesToDelete = $this->getIngrediences(new Criteria(), $con)->diff($ingrediences);


        $this->ingrediencesScheduledForDeletion = $ingrediencesToDelete;

        foreach ($ingrediencesToDelete as $ingredienceRemoved) {
            $ingredienceRemoved->setRecepty(null);
        }

        $this->collIngrediences = null;
        foreach ($ingrediences as $ingredience) {
            $this->addIngredience($ingredience);
        }

        $this->collIngrediences = $ingrediences;
        $this->collIngrediencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ingredience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Ingredience objects.
     * @throws PropelException
     */
    public function countIngrediences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collIngrediencesPartial && !$this->isNew();
        if (null === $this->collIngrediences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collIngrediences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getIngrediences());
            }

            $query = ChildIngredienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRecepty($this)
                ->count($con);
        }

        return count($this->collIngrediences);
    }

    /**
     * Method called to associate a ChildIngredience object to this object
     * through the ChildIngredience foreign key attribute.
     *
     * @param  ChildIngredience $l ChildIngredience
     * @return $this|\SmartFridge\Recepty The current object (for fluent API support)
     */
    public function addIngredience(ChildIngredience $l)
    {
        if ($this->collIngrediences === null) {
            $this->initIngrediences();
            $this->collIngrediencesPartial = true;
        }

        if (!$this->collIngrediences->contains($l)) {
            $this->doAddIngredience($l);

            if ($this->ingrediencesScheduledForDeletion and $this->ingrediencesScheduledForDeletion->contains($l)) {
                $this->ingrediencesScheduledForDeletion->remove($this->ingrediencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildIngredience $ingredience The ChildIngredience object to add.
     */
    protected function doAddIngredience(ChildIngredience $ingredience)
    {
        $this->collIngrediences[]= $ingredience;
        $ingredience->setRecepty($this);
    }

    /**
     * @param  ChildIngredience $ingredience The ChildIngredience object to remove.
     * @return $this|ChildRecepty The current object (for fluent API support)
     */
    public function removeIngredience(ChildIngredience $ingredience)
    {
        if ($this->getIngrediences()->contains($ingredience)) {
            $pos = $this->collIngrediences->search($ingredience);
            $this->collIngrediences->remove($pos);
            if (null === $this->ingrediencesScheduledForDeletion) {
                $this->ingrediencesScheduledForDeletion = clone $this->collIngrediences;
                $this->ingrediencesScheduledForDeletion->clear();
            }
            $this->ingrediencesScheduledForDeletion[]= $ingredience;
            $ingredience->setRecepty(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Recepty is new, it will return
     * an empty collection; or if this Recepty has previously
     * been saved, it will retrieve related Ingrediences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Recepty.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     */
    public function getIngrediencesJoinJednotka(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildIngredienceQuery::create(null, $criteria);
        $query->joinWith('Jednotka', $joinBehavior);

        return $this->getIngrediences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Recepty is new, it will return
     * an empty collection; or if this Recepty has previously
     * been saved, it will retrieve related Ingrediences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Recepty.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     */
    public function getIngrediencesJoinKategorie(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildIngredienceQuery::create(null, $criteria);
        $query->joinWith('Kategorie', $joinBehavior);

        return $this->getIngrediences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Recepty is new, it will return
     * an empty collection; or if this Recepty has previously
     * been saved, it will retrieve related Ingrediences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Recepty.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     */
    public function getIngrediencesJoinProdukt(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildIngredienceQuery::create(null, $criteria);
        $query->joinWith('Produkt', $joinBehavior);

        return $this->getIngrediences($query, $con);
    }

    /**
     * Clears out the collRecenzes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRecenzes()
     */
    public function clearRecenzes()
    {
        $this->collRecenzes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRecenzes collection loaded partially.
     */
    public function resetPartialRecenzes($v = true)
    {
        $this->collRecenzesPartial = $v;
    }

    /**
     * Initializes the collRecenzes collection.
     *
     * By default this just sets the collRecenzes collection to an empty array (like clearcollRecenzes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRecenzes($overrideExisting = true)
    {
        if (null !== $this->collRecenzes && !$overrideExisting) {
            return;
        }

        $collectionClassName = RecenzeTableMap::getTableMap()->getCollectionClassName();

        $this->collRecenzes = new $collectionClassName;
        $this->collRecenzes->setModel('\SmartFridge\Recenze');
    }

    /**
     * Gets an array of ChildRecenze objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildRecepty is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRecenze[] List of ChildRecenze objects
     * @throws PropelException
     */
    public function getRecenzes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRecenzesPartial && !$this->isNew();
        if (null === $this->collRecenzes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRecenzes) {
                // return empty collection
                $this->initRecenzes();
            } else {
                $collRecenzes = ChildRecenzeQuery::create(null, $criteria)
                    ->filterByRecepty($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRecenzesPartial && count($collRecenzes)) {
                        $this->initRecenzes(false);

                        foreach ($collRecenzes as $obj) {
                            if (false == $this->collRecenzes->contains($obj)) {
                                $this->collRecenzes->append($obj);
                            }
                        }

                        $this->collRecenzesPartial = true;
                    }

                    return $collRecenzes;
                }

                if ($partial && $this->collRecenzes) {
                    foreach ($this->collRecenzes as $obj) {
                        if ($obj->isNew()) {
                            $collRecenzes[] = $obj;
                        }
                    }
                }

                $this->collRecenzes = $collRecenzes;
                $this->collRecenzesPartial = false;
            }
        }

        return $this->collRecenzes;
    }

    /**
     * Sets a collection of ChildRecenze objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $recenzes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildRecepty The current object (for fluent API support)
     */
    public function setRecenzes(Collection $recenzes, ConnectionInterface $con = null)
    {
        /** @var ChildRecenze[] $recenzesToDelete */
        $recenzesToDelete = $this->getRecenzes(new Criteria(), $con)->diff($recenzes);


        $this->recenzesScheduledForDeletion = $recenzesToDelete;

        foreach ($recenzesToDelete as $recenzeRemoved) {
            $recenzeRemoved->setRecepty(null);
        }

        $this->collRecenzes = null;
        foreach ($recenzes as $recenze) {
            $this->addRecenze($recenze);
        }

        $this->collRecenzes = $recenzes;
        $this->collRecenzesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Recenze objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Recenze objects.
     * @throws PropelException
     */
    public function countRecenzes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRecenzesPartial && !$this->isNew();
        if (null === $this->collRecenzes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRecenzes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRecenzes());
            }

            $query = ChildRecenzeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRecepty($this)
                ->count($con);
        }

        return count($this->collRecenzes);
    }

    /**
     * Method called to associate a ChildRecenze object to this object
     * through the ChildRecenze foreign key attribute.
     *
     * @param  ChildRecenze $l ChildRecenze
     * @return $this|\SmartFridge\Recepty The current object (for fluent API support)
     */
    public function addRecenze(ChildRecenze $l)
    {
        if ($this->collRecenzes === null) {
            $this->initRecenzes();
            $this->collRecenzesPartial = true;
        }

        if (!$this->collRecenzes->contains($l)) {
            $this->doAddRecenze($l);

            if ($this->recenzesScheduledForDeletion and $this->recenzesScheduledForDeletion->contains($l)) {
                $this->recenzesScheduledForDeletion->remove($this->recenzesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRecenze $recenze The ChildRecenze object to add.
     */
    protected function doAddRecenze(ChildRecenze $recenze)
    {
        $this->collRecenzes[]= $recenze;
        $recenze->setRecepty($this);
    }

    /**
     * @param  ChildRecenze $recenze The ChildRecenze object to remove.
     * @return $this|ChildRecepty The current object (for fluent API support)
     */
    public function removeRecenze(ChildRecenze $recenze)
    {
        if ($this->getRecenzes()->contains($recenze)) {
            $pos = $this->collRecenzes->search($recenze);
            $this->collRecenzes->remove($pos);
            if (null === $this->recenzesScheduledForDeletion) {
                $this->recenzesScheduledForDeletion = clone $this->collRecenzes;
                $this->recenzesScheduledForDeletion->clear();
            }
            $this->recenzesScheduledForDeletion[]= $recenze;
            $recenze->setRecepty(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Recepty is new, it will return
     * an empty collection; or if this Recepty has previously
     * been saved, it will retrieve related Recenzes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Recepty.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildRecenze[] List of ChildRecenze objects
     */
    public function getRecenzesJoinUzivatel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildRecenzeQuery::create(null, $criteria);
        $query->joinWith('Uzivatel', $joinBehavior);

        return $this->getRecenzes($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->nazev = null;
        $this->pocetporci = null;
        $this->postup = null;
        $this->pribliznycas = null;
        $this->priorita = null;
        $this->slozitost = null;
        $this->fotka = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collIngrediences) {
                foreach ($this->collIngrediences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRecenzes) {
                foreach ($this->collRecenzes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collIngrediences = null;
        $this->collRecenzes = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ReceptyTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
