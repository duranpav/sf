<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SmartFridge\Recepty as ChildRecepty;
use SmartFridge\ReceptyQuery as ChildReceptyQuery;
use SmartFridge\Map\ReceptyTableMap;

/**
 * Base class that represents a query for the 'Recepty' table.
 *
 *
 *
 * @method     ChildReceptyQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildReceptyQuery orderByNazev($order = Criteria::ASC) Order by the nazev column
 * @method     ChildReceptyQuery orderByPocetporci($order = Criteria::ASC) Order by the pocetPorci column
 * @method     ChildReceptyQuery orderByPostup($order = Criteria::ASC) Order by the postup column
 * @method     ChildReceptyQuery orderByPribliznycas($order = Criteria::ASC) Order by the pribliznyCas column
 * @method     ChildReceptyQuery orderByPriorita($order = Criteria::ASC) Order by the priorita column
 * @method     ChildReceptyQuery orderBySlozitost($order = Criteria::ASC) Order by the slozitost column
 * @method     ChildReceptyQuery orderByFotka($order = Criteria::ASC) Order by the fotka column
 *
 * @method     ChildReceptyQuery groupById() Group by the id column
 * @method     ChildReceptyQuery groupByNazev() Group by the nazev column
 * @method     ChildReceptyQuery groupByPocetporci() Group by the pocetPorci column
 * @method     ChildReceptyQuery groupByPostup() Group by the postup column
 * @method     ChildReceptyQuery groupByPribliznycas() Group by the pribliznyCas column
 * @method     ChildReceptyQuery groupByPriorita() Group by the priorita column
 * @method     ChildReceptyQuery groupBySlozitost() Group by the slozitost column
 * @method     ChildReceptyQuery groupByFotka() Group by the fotka column
 *
 * @method     ChildReceptyQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildReceptyQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildReceptyQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildReceptyQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildReceptyQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildReceptyQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildReceptyQuery leftJoinIngredience($relationAlias = null) Adds a LEFT JOIN clause to the query using the Ingredience relation
 * @method     ChildReceptyQuery rightJoinIngredience($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Ingredience relation
 * @method     ChildReceptyQuery innerJoinIngredience($relationAlias = null) Adds a INNER JOIN clause to the query using the Ingredience relation
 *
 * @method     ChildReceptyQuery joinWithIngredience($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Ingredience relation
 *
 * @method     ChildReceptyQuery leftJoinWithIngredience() Adds a LEFT JOIN clause and with to the query using the Ingredience relation
 * @method     ChildReceptyQuery rightJoinWithIngredience() Adds a RIGHT JOIN clause and with to the query using the Ingredience relation
 * @method     ChildReceptyQuery innerJoinWithIngredience() Adds a INNER JOIN clause and with to the query using the Ingredience relation
 *
 * @method     ChildReceptyQuery leftJoinRecenze($relationAlias = null) Adds a LEFT JOIN clause to the query using the Recenze relation
 * @method     ChildReceptyQuery rightJoinRecenze($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Recenze relation
 * @method     ChildReceptyQuery innerJoinRecenze($relationAlias = null) Adds a INNER JOIN clause to the query using the Recenze relation
 *
 * @method     ChildReceptyQuery joinWithRecenze($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Recenze relation
 *
 * @method     ChildReceptyQuery leftJoinWithRecenze() Adds a LEFT JOIN clause and with to the query using the Recenze relation
 * @method     ChildReceptyQuery rightJoinWithRecenze() Adds a RIGHT JOIN clause and with to the query using the Recenze relation
 * @method     ChildReceptyQuery innerJoinWithRecenze() Adds a INNER JOIN clause and with to the query using the Recenze relation
 *
 * @method     \SmartFridge\IngredienceQuery|\SmartFridge\RecenzeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRecepty findOne(ConnectionInterface $con = null) Return the first ChildRecepty matching the query
 * @method     ChildRecepty findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRecepty matching the query, or a new ChildRecepty object populated from the query conditions when no match is found
 *
 * @method     ChildRecepty findOneById(int $id) Return the first ChildRecepty filtered by the id column
 * @method     ChildRecepty findOneByNazev(string $nazev) Return the first ChildRecepty filtered by the nazev column
 * @method     ChildRecepty findOneByPocetporci(int $pocetPorci) Return the first ChildRecepty filtered by the pocetPorci column
 * @method     ChildRecepty findOneByPostup(string $postup) Return the first ChildRecepty filtered by the postup column
 * @method     ChildRecepty findOneByPribliznycas(int $pribliznyCas) Return the first ChildRecepty filtered by the pribliznyCas column
 * @method     ChildRecepty findOneByPriorita(int $priorita) Return the first ChildRecepty filtered by the priorita column
 * @method     ChildRecepty findOneBySlozitost(string $slozitost) Return the first ChildRecepty filtered by the slozitost column
 * @method     ChildRecepty findOneByFotka(string $fotka) Return the first ChildRecepty filtered by the fotka column *

 * @method     ChildRecepty requirePk($key, ConnectionInterface $con = null) Return the ChildRecepty by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecepty requireOne(ConnectionInterface $con = null) Return the first ChildRecepty matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRecepty requireOneById(int $id) Return the first ChildRecepty filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecepty requireOneByNazev(string $nazev) Return the first ChildRecepty filtered by the nazev column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecepty requireOneByPocetporci(int $pocetPorci) Return the first ChildRecepty filtered by the pocetPorci column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecepty requireOneByPostup(string $postup) Return the first ChildRecepty filtered by the postup column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecepty requireOneByPribliznycas(int $pribliznyCas) Return the first ChildRecepty filtered by the pribliznyCas column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecepty requireOneByPriorita(int $priorita) Return the first ChildRecepty filtered by the priorita column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecepty requireOneBySlozitost(string $slozitost) Return the first ChildRecepty filtered by the slozitost column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecepty requireOneByFotka(string $fotka) Return the first ChildRecepty filtered by the fotka column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRecepty[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRecepty objects based on current ModelCriteria
 * @method     ChildRecepty[]|ObjectCollection findById(int $id) Return ChildRecepty objects filtered by the id column
 * @method     ChildRecepty[]|ObjectCollection findByNazev(string $nazev) Return ChildRecepty objects filtered by the nazev column
 * @method     ChildRecepty[]|ObjectCollection findByPocetporci(int $pocetPorci) Return ChildRecepty objects filtered by the pocetPorci column
 * @method     ChildRecepty[]|ObjectCollection findByPostup(string $postup) Return ChildRecepty objects filtered by the postup column
 * @method     ChildRecepty[]|ObjectCollection findByPribliznycas(int $pribliznyCas) Return ChildRecepty objects filtered by the pribliznyCas column
 * @method     ChildRecepty[]|ObjectCollection findByPriorita(int $priorita) Return ChildRecepty objects filtered by the priorita column
 * @method     ChildRecepty[]|ObjectCollection findBySlozitost(string $slozitost) Return ChildRecepty objects filtered by the slozitost column
 * @method     ChildRecepty[]|ObjectCollection findByFotka(string $fotka) Return ChildRecepty objects filtered by the fotka column
 * @method     ChildRecepty[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ReceptyQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SmartFridge\Base\ReceptyQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SmartFridge\\Recepty', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildReceptyQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildReceptyQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildReceptyQuery) {
            return $criteria;
        }
        $query = new ChildReceptyQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRecepty|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ReceptyTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ReceptyTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRecepty A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, nazev, pocetPorci, postup, pribliznyCas, priorita, slozitost, fotka FROM Recepty WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRecepty $obj */
            $obj = new ChildRecepty();
            $obj->hydrate($row);
            ReceptyTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRecepty|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ReceptyTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ReceptyTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ReceptyTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ReceptyTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceptyTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nazev column
     *
     * Example usage:
     * <code>
     * $query->filterByNazev('fooValue');   // WHERE nazev = 'fooValue'
     * $query->filterByNazev('%fooValue%', Criteria::LIKE); // WHERE nazev LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nazev The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function filterByNazev($nazev = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nazev)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceptyTableMap::COL_NAZEV, $nazev, $comparison);
    }

    /**
     * Filter the query on the pocetPorci column
     *
     * Example usage:
     * <code>
     * $query->filterByPocetporci(1234); // WHERE pocetPorci = 1234
     * $query->filterByPocetporci(array(12, 34)); // WHERE pocetPorci IN (12, 34)
     * $query->filterByPocetporci(array('min' => 12)); // WHERE pocetPorci > 12
     * </code>
     *
     * @param     mixed $pocetporci The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function filterByPocetporci($pocetporci = null, $comparison = null)
    {
        if (is_array($pocetporci)) {
            $useMinMax = false;
            if (isset($pocetporci['min'])) {
                $this->addUsingAlias(ReceptyTableMap::COL_POCETPORCI, $pocetporci['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pocetporci['max'])) {
                $this->addUsingAlias(ReceptyTableMap::COL_POCETPORCI, $pocetporci['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceptyTableMap::COL_POCETPORCI, $pocetporci, $comparison);
    }

    /**
     * Filter the query on the postup column
     *
     * Example usage:
     * <code>
     * $query->filterByPostup('fooValue');   // WHERE postup = 'fooValue'
     * $query->filterByPostup('%fooValue%', Criteria::LIKE); // WHERE postup LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postup The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function filterByPostup($postup = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postup)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceptyTableMap::COL_POSTUP, $postup, $comparison);
    }

    /**
     * Filter the query on the pribliznyCas column
     *
     * Example usage:
     * <code>
     * $query->filterByPribliznycas(1234); // WHERE pribliznyCas = 1234
     * $query->filterByPribliznycas(array(12, 34)); // WHERE pribliznyCas IN (12, 34)
     * $query->filterByPribliznycas(array('min' => 12)); // WHERE pribliznyCas > 12
     * </code>
     *
     * @param     mixed $pribliznycas The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function filterByPribliznycas($pribliznycas = null, $comparison = null)
    {
        if (is_array($pribliznycas)) {
            $useMinMax = false;
            if (isset($pribliznycas['min'])) {
                $this->addUsingAlias(ReceptyTableMap::COL_PRIBLIZNYCAS, $pribliznycas['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pribliznycas['max'])) {
                $this->addUsingAlias(ReceptyTableMap::COL_PRIBLIZNYCAS, $pribliznycas['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceptyTableMap::COL_PRIBLIZNYCAS, $pribliznycas, $comparison);
    }

    /**
     * Filter the query on the priorita column
     *
     * Example usage:
     * <code>
     * $query->filterByPriorita(1234); // WHERE priorita = 1234
     * $query->filterByPriorita(array(12, 34)); // WHERE priorita IN (12, 34)
     * $query->filterByPriorita(array('min' => 12)); // WHERE priorita > 12
     * </code>
     *
     * @param     mixed $priorita The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function filterByPriorita($priorita = null, $comparison = null)
    {
        if (is_array($priorita)) {
            $useMinMax = false;
            if (isset($priorita['min'])) {
                $this->addUsingAlias(ReceptyTableMap::COL_PRIORITA, $priorita['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($priorita['max'])) {
                $this->addUsingAlias(ReceptyTableMap::COL_PRIORITA, $priorita['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceptyTableMap::COL_PRIORITA, $priorita, $comparison);
    }

    /**
     * Filter the query on the slozitost column
     *
     * Example usage:
     * <code>
     * $query->filterBySlozitost('fooValue');   // WHERE slozitost = 'fooValue'
     * $query->filterBySlozitost('%fooValue%', Criteria::LIKE); // WHERE slozitost LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slozitost The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function filterBySlozitost($slozitost = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slozitost)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceptyTableMap::COL_SLOZITOST, $slozitost, $comparison);
    }

    /**
     * Filter the query on the fotka column
     *
     * Example usage:
     * <code>
     * $query->filterByFotka('fooValue');   // WHERE fotka = 'fooValue'
     * $query->filterByFotka('%fooValue%', Criteria::LIKE); // WHERE fotka LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fotka The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function filterByFotka($fotka = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fotka)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReceptyTableMap::COL_FOTKA, $fotka, $comparison);
    }

    /**
     * Filter the query by a related \SmartFridge\Ingredience object
     *
     * @param \SmartFridge\Ingredience|ObjectCollection $ingredience the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildReceptyQuery The current query, for fluid interface
     */
    public function filterByIngredience($ingredience, $comparison = null)
    {
        if ($ingredience instanceof \SmartFridge\Ingredience) {
            return $this
                ->addUsingAlias(ReceptyTableMap::COL_ID, $ingredience->getReceptid(), $comparison);
        } elseif ($ingredience instanceof ObjectCollection) {
            return $this
                ->useIngredienceQuery()
                ->filterByPrimaryKeys($ingredience->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByIngredience() only accepts arguments of type \SmartFridge\Ingredience or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Ingredience relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function joinIngredience($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Ingredience');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Ingredience');
        }

        return $this;
    }

    /**
     * Use the Ingredience relation Ingredience object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\IngredienceQuery A secondary query class using the current class as primary query
     */
    public function useIngredienceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinIngredience($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Ingredience', '\SmartFridge\IngredienceQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Recenze object
     *
     * @param \SmartFridge\Recenze|ObjectCollection $recenze the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildReceptyQuery The current query, for fluid interface
     */
    public function filterByRecenze($recenze, $comparison = null)
    {
        if ($recenze instanceof \SmartFridge\Recenze) {
            return $this
                ->addUsingAlias(ReceptyTableMap::COL_ID, $recenze->getReceptid(), $comparison);
        } elseif ($recenze instanceof ObjectCollection) {
            return $this
                ->useRecenzeQuery()
                ->filterByPrimaryKeys($recenze->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRecenze() only accepts arguments of type \SmartFridge\Recenze or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Recenze relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function joinRecenze($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Recenze');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Recenze');
        }

        return $this;
    }

    /**
     * Use the Recenze relation Recenze object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\RecenzeQuery A secondary query class using the current class as primary query
     */
    public function useRecenzeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRecenze($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Recenze', '\SmartFridge\RecenzeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRecepty $recepty Object to remove from the list of results
     *
     * @return $this|ChildReceptyQuery The current query, for fluid interface
     */
    public function prune($recepty = null)
    {
        if ($recepty) {
            $this->addUsingAlias(ReceptyTableMap::COL_ID, $recepty->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Recepty table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ReceptyTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ReceptyTableMap::clearInstancePool();
            ReceptyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ReceptyTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ReceptyTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ReceptyTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ReceptyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ReceptyQuery
