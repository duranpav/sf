<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use SmartFridge\Ingredience as ChildIngredience;
use SmartFridge\IngredienceQuery as ChildIngredienceQuery;
use SmartFridge\Jednotka as ChildJednotka;
use SmartFridge\JednotkaQuery as ChildJednotkaQuery;
use SmartFridge\Kategorie as ChildKategorie;
use SmartFridge\KategorieQuery as ChildKategorieQuery;
use SmartFridge\Potraviny as ChildPotraviny;
use SmartFridge\PotravinyQuery as ChildPotravinyQuery;
use SmartFridge\Produkt as ChildProdukt;
use SmartFridge\ProduktQuery as ChildProduktQuery;
use SmartFridge\Upozorneni as ChildUpozorneni;
use SmartFridge\UpozorneniQuery as ChildUpozorneniQuery;
use SmartFridge\Watchdog as ChildWatchdog;
use SmartFridge\WatchdogQuery as ChildWatchdogQuery;
use SmartFridge\Zbozi as ChildZbozi;
use SmartFridge\ZboziQuery as ChildZboziQuery;
use SmartFridge\Map\IngredienceTableMap;
use SmartFridge\Map\PotravinyTableMap;
use SmartFridge\Map\ProduktTableMap;
use SmartFridge\Map\UpozorneniTableMap;
use SmartFridge\Map\WatchdogTableMap;
use SmartFridge\Map\ZboziTableMap;

/**
 * Base class that represents a row from the 'Produkt' table.
 *
 *
 *
 * @package    propel.generator.SmartFridge.Base
 */
abstract class Produkt implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\SmartFridge\\Map\\ProduktTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the nazev field.
     *
     * @var        string
     */
    protected $nazev;

    /**
     * The value for the fotka field.
     *
     * @var        string
     */
    protected $fotka;

    /**
     * The value for the popis field.
     *
     * @var        string
     */
    protected $popis;

    /**
     * The value for the kategorieid field.
     *
     * @var        int
     */
    protected $kategorieid;

    /**
     * The value for the jednotkaid field.
     *
     * @var        int
     */
    protected $jednotkaid;

    /**
     * The value for the mnozstvi field.
     *
     * @var        int
     */
    protected $mnozstvi;

    /**
     * @var        ChildJednotka
     */
    protected $aJednotka;

    /**
     * @var        ChildKategorie
     */
    protected $aKategorie;

    /**
     * @var        ObjectCollection|ChildIngredience[] Collection to store aggregation of ChildIngredience objects.
     */
    protected $collIngrediences;
    protected $collIngrediencesPartial;

    /**
     * @var        ObjectCollection|ChildPotraviny[] Collection to store aggregation of ChildPotraviny objects.
     */
    protected $collPotravinies;
    protected $collPotraviniesPartial;

    /**
     * @var        ObjectCollection|ChildUpozorneni[] Collection to store aggregation of ChildUpozorneni objects.
     */
    protected $collUpozornenis;
    protected $collUpozornenisPartial;

    /**
     * @var        ObjectCollection|ChildWatchdog[] Collection to store aggregation of ChildWatchdog objects.
     */
    protected $collWatchdogs;
    protected $collWatchdogsPartial;

    /**
     * @var        ObjectCollection|ChildZbozi[] Collection to store aggregation of ChildZbozi objects.
     */
    protected $collZbozis;
    protected $collZbozisPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildIngredience[]
     */
    protected $ingrediencesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPotraviny[]
     */
    protected $potraviniesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUpozorneni[]
     */
    protected $upozornenisScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildWatchdog[]
     */
    protected $watchdogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildZbozi[]
     */
    protected $zbozisScheduledForDeletion = null;

    /**
     * Initializes internal state of SmartFridge\Base\Produkt object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Produkt</code> instance.  If
     * <code>obj</code> is an instance of <code>Produkt</code>, delegates to
     * <code>equals(Produkt)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Produkt The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [nazev] column value.
     *
     * @return string
     */
    public function getNazev()
    {
        return $this->nazev;
    }

    /**
     * Get the [fotka] column value.
     *
     * @return string
     */
    public function getFotka()
    {
        return $this->fotka;
    }

    /**
     * Get the [popis] column value.
     *
     * @return string
     */
    public function getPopis()
    {
        return $this->popis;
    }

    /**
     * Get the [kategorieid] column value.
     *
     * @return int
     */
    public function getKategorieid()
    {
        return $this->kategorieid;
    }

    /**
     * Get the [jednotkaid] column value.
     *
     * @return int
     */
    public function getJednotkaid()
    {
        return $this->jednotkaid;
    }

    /**
     * Get the [mnozstvi] column value.
     *
     * @return int
     */
    public function getMnozstvi()
    {
        return $this->mnozstvi;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[ProduktTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [nazev] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function setNazev($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nazev !== $v) {
            $this->nazev = $v;
            $this->modifiedColumns[ProduktTableMap::COL_NAZEV] = true;
        }

        return $this;
    } // setNazev()

    /**
     * Set the value of [fotka] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function setFotka($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fotka !== $v) {
            $this->fotka = $v;
            $this->modifiedColumns[ProduktTableMap::COL_FOTKA] = true;
        }

        return $this;
    } // setFotka()

    /**
     * Set the value of [popis] column.
     *
     * @param string $v new value
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function setPopis($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->popis !== $v) {
            $this->popis = $v;
            $this->modifiedColumns[ProduktTableMap::COL_POPIS] = true;
        }

        return $this;
    } // setPopis()

    /**
     * Set the value of [kategorieid] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function setKategorieid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->kategorieid !== $v) {
            $this->kategorieid = $v;
            $this->modifiedColumns[ProduktTableMap::COL_KATEGORIEID] = true;
        }

        if ($this->aKategorie !== null && $this->aKategorie->getId() !== $v) {
            $this->aKategorie = null;
        }

        return $this;
    } // setKategorieid()

    /**
     * Set the value of [jednotkaid] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function setJednotkaid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->jednotkaid !== $v) {
            $this->jednotkaid = $v;
            $this->modifiedColumns[ProduktTableMap::COL_JEDNOTKAID] = true;
        }

        if ($this->aJednotka !== null && $this->aJednotka->getId() !== $v) {
            $this->aJednotka = null;
        }

        return $this;
    } // setJednotkaid()

    /**
     * Set the value of [mnozstvi] column.
     *
     * @param int $v new value
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function setMnozstvi($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->mnozstvi !== $v) {
            $this->mnozstvi = $v;
            $this->modifiedColumns[ProduktTableMap::COL_MNOZSTVI] = true;
        }

        return $this;
    } // setMnozstvi()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ProduktTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ProduktTableMap::translateFieldName('Nazev', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nazev = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ProduktTableMap::translateFieldName('Fotka', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fotka = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ProduktTableMap::translateFieldName('Popis', TableMap::TYPE_PHPNAME, $indexType)];
            $this->popis = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ProduktTableMap::translateFieldName('Kategorieid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->kategorieid = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ProduktTableMap::translateFieldName('Jednotkaid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->jednotkaid = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ProduktTableMap::translateFieldName('Mnozstvi', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mnozstvi = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = ProduktTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\SmartFridge\\Produkt'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aKategorie !== null && $this->kategorieid !== $this->aKategorie->getId()) {
            $this->aKategorie = null;
        }
        if ($this->aJednotka !== null && $this->jednotkaid !== $this->aJednotka->getId()) {
            $this->aJednotka = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProduktTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildProduktQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aJednotka = null;
            $this->aKategorie = null;
            $this->collIngrediences = null;

            $this->collPotravinies = null;

            $this->collUpozornenis = null;

            $this->collWatchdogs = null;

            $this->collZbozis = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Produkt::setDeleted()
     * @see Produkt::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduktTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildProduktQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduktTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProduktTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aJednotka !== null) {
                if ($this->aJednotka->isModified() || $this->aJednotka->isNew()) {
                    $affectedRows += $this->aJednotka->save($con);
                }
                $this->setJednotka($this->aJednotka);
            }

            if ($this->aKategorie !== null) {
                if ($this->aKategorie->isModified() || $this->aKategorie->isNew()) {
                    $affectedRows += $this->aKategorie->save($con);
                }
                $this->setKategorie($this->aKategorie);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->ingrediencesScheduledForDeletion !== null) {
                if (!$this->ingrediencesScheduledForDeletion->isEmpty()) {
                    foreach ($this->ingrediencesScheduledForDeletion as $ingredience) {
                        // need to save related object because we set the relation to null
                        $ingredience->save($con);
                    }
                    $this->ingrediencesScheduledForDeletion = null;
                }
            }

            if ($this->collIngrediences !== null) {
                foreach ($this->collIngrediences as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->potraviniesScheduledForDeletion !== null) {
                if (!$this->potraviniesScheduledForDeletion->isEmpty()) {
                    foreach ($this->potraviniesScheduledForDeletion as $potraviny) {
                        // need to save related object because we set the relation to null
                        $potraviny->save($con);
                    }
                    $this->potraviniesScheduledForDeletion = null;
                }
            }

            if ($this->collPotravinies !== null) {
                foreach ($this->collPotravinies as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->upozornenisScheduledForDeletion !== null) {
                if (!$this->upozornenisScheduledForDeletion->isEmpty()) {
                    foreach ($this->upozornenisScheduledForDeletion as $upozorneni) {
                        // need to save related object because we set the relation to null
                        $upozorneni->save($con);
                    }
                    $this->upozornenisScheduledForDeletion = null;
                }
            }

            if ($this->collUpozornenis !== null) {
                foreach ($this->collUpozornenis as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->watchdogsScheduledForDeletion !== null) {
                if (!$this->watchdogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->watchdogsScheduledForDeletion as $watchdog) {
                        // need to save related object because we set the relation to null
                        $watchdog->save($con);
                    }
                    $this->watchdogsScheduledForDeletion = null;
                }
            }

            if ($this->collWatchdogs !== null) {
                foreach ($this->collWatchdogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->zbozisScheduledForDeletion !== null) {
                if (!$this->zbozisScheduledForDeletion->isEmpty()) {
                    foreach ($this->zbozisScheduledForDeletion as $zbozi) {
                        // need to save related object because we set the relation to null
                        $zbozi->save($con);
                    }
                    $this->zbozisScheduledForDeletion = null;
                }
            }

            if ($this->collZbozis !== null) {
                foreach ($this->collZbozis as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ProduktTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ProduktTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProduktTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(ProduktTableMap::COL_NAZEV)) {
            $modifiedColumns[':p' . $index++]  = 'nazev';
        }
        if ($this->isColumnModified(ProduktTableMap::COL_FOTKA)) {
            $modifiedColumns[':p' . $index++]  = 'fotka';
        }
        if ($this->isColumnModified(ProduktTableMap::COL_POPIS)) {
            $modifiedColumns[':p' . $index++]  = 'popis';
        }
        if ($this->isColumnModified(ProduktTableMap::COL_KATEGORIEID)) {
            $modifiedColumns[':p' . $index++]  = 'kategorieID';
        }
        if ($this->isColumnModified(ProduktTableMap::COL_JEDNOTKAID)) {
            $modifiedColumns[':p' . $index++]  = 'jednotkaID';
        }
        if ($this->isColumnModified(ProduktTableMap::COL_MNOZSTVI)) {
            $modifiedColumns[':p' . $index++]  = 'mnozstvi';
        }

        $sql = sprintf(
            'INSERT INTO Produkt (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'nazev':
                        $stmt->bindValue($identifier, $this->nazev, PDO::PARAM_STR);
                        break;
                    case 'fotka':
                        $stmt->bindValue($identifier, $this->fotka, PDO::PARAM_STR);
                        break;
                    case 'popis':
                        $stmt->bindValue($identifier, $this->popis, PDO::PARAM_STR);
                        break;
                    case 'kategorieID':
                        $stmt->bindValue($identifier, $this->kategorieid, PDO::PARAM_INT);
                        break;
                    case 'jednotkaID':
                        $stmt->bindValue($identifier, $this->jednotkaid, PDO::PARAM_INT);
                        break;
                    case 'mnozstvi':
                        $stmt->bindValue($identifier, $this->mnozstvi, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProduktTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNazev();
                break;
            case 2:
                return $this->getFotka();
                break;
            case 3:
                return $this->getPopis();
                break;
            case 4:
                return $this->getKategorieid();
                break;
            case 5:
                return $this->getJednotkaid();
                break;
            case 6:
                return $this->getMnozstvi();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Produkt'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Produkt'][$this->hashCode()] = true;
        $keys = ProduktTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNazev(),
            $keys[2] => $this->getFotka(),
            $keys[3] => $this->getPopis(),
            $keys[4] => $this->getKategorieid(),
            $keys[5] => $this->getJednotkaid(),
            $keys[6] => $this->getMnozstvi(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aJednotka) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'jednotka';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Jednotka';
                        break;
                    default:
                        $key = 'Jednotka';
                }

                $result[$key] = $this->aJednotka->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKategorie) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'kategorie';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Kategorie';
                        break;
                    default:
                        $key = 'Kategorie';
                }

                $result[$key] = $this->aKategorie->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collIngrediences) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ingrediences';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Ingrediences';
                        break;
                    default:
                        $key = 'Ingrediences';
                }

                $result[$key] = $this->collIngrediences->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPotravinies) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'potravinies';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Potravinies';
                        break;
                    default:
                        $key = 'Potravinies';
                }

                $result[$key] = $this->collPotravinies->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUpozornenis) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'upozornenis';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Upozornenis';
                        break;
                    default:
                        $key = 'Upozornenis';
                }

                $result[$key] = $this->collUpozornenis->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWatchdogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'watchdogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Watchdogs';
                        break;
                    default:
                        $key = 'Watchdogs';
                }

                $result[$key] = $this->collWatchdogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collZbozis) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'zbozis';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Zbozis';
                        break;
                    default:
                        $key = 'Zbozis';
                }

                $result[$key] = $this->collZbozis->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\SmartFridge\Produkt
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProduktTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\SmartFridge\Produkt
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNazev($value);
                break;
            case 2:
                $this->setFotka($value);
                break;
            case 3:
                $this->setPopis($value);
                break;
            case 4:
                $this->setKategorieid($value);
                break;
            case 5:
                $this->setJednotkaid($value);
                break;
            case 6:
                $this->setMnozstvi($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ProduktTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNazev($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setFotka($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPopis($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setKategorieid($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setJednotkaid($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setMnozstvi($arr[$keys[6]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\SmartFridge\Produkt The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProduktTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ProduktTableMap::COL_ID)) {
            $criteria->add(ProduktTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(ProduktTableMap::COL_NAZEV)) {
            $criteria->add(ProduktTableMap::COL_NAZEV, $this->nazev);
        }
        if ($this->isColumnModified(ProduktTableMap::COL_FOTKA)) {
            $criteria->add(ProduktTableMap::COL_FOTKA, $this->fotka);
        }
        if ($this->isColumnModified(ProduktTableMap::COL_POPIS)) {
            $criteria->add(ProduktTableMap::COL_POPIS, $this->popis);
        }
        if ($this->isColumnModified(ProduktTableMap::COL_KATEGORIEID)) {
            $criteria->add(ProduktTableMap::COL_KATEGORIEID, $this->kategorieid);
        }
        if ($this->isColumnModified(ProduktTableMap::COL_JEDNOTKAID)) {
            $criteria->add(ProduktTableMap::COL_JEDNOTKAID, $this->jednotkaid);
        }
        if ($this->isColumnModified(ProduktTableMap::COL_MNOZSTVI)) {
            $criteria->add(ProduktTableMap::COL_MNOZSTVI, $this->mnozstvi);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildProduktQuery::create();
        $criteria->add(ProduktTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \SmartFridge\Produkt (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNazev($this->getNazev());
        $copyObj->setFotka($this->getFotka());
        $copyObj->setPopis($this->getPopis());
        $copyObj->setKategorieid($this->getKategorieid());
        $copyObj->setJednotkaid($this->getJednotkaid());
        $copyObj->setMnozstvi($this->getMnozstvi());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getIngrediences() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addIngredience($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPotravinies() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPotraviny($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUpozornenis() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUpozorneni($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWatchdogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWatchdog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getZbozis() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addZbozi($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \SmartFridge\Produkt Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildJednotka object.
     *
     * @param  ChildJednotka $v
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJednotka(ChildJednotka $v = null)
    {
        if ($v === null) {
            $this->setJednotkaid(NULL);
        } else {
            $this->setJednotkaid($v->getId());
        }

        $this->aJednotka = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildJednotka object, it will not be re-added.
        if ($v !== null) {
            $v->addProdukt($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildJednotka object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildJednotka The associated ChildJednotka object.
     * @throws PropelException
     */
    public function getJednotka(ConnectionInterface $con = null)
    {
        if ($this->aJednotka === null && ($this->jednotkaid !== null)) {
            $this->aJednotka = ChildJednotkaQuery::create()->findPk($this->jednotkaid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJednotka->addProdukts($this);
             */
        }

        return $this->aJednotka;
    }

    /**
     * Declares an association between this object and a ChildKategorie object.
     *
     * @param  ChildKategorie $v
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKategorie(ChildKategorie $v = null)
    {
        if ($v === null) {
            $this->setKategorieid(NULL);
        } else {
            $this->setKategorieid($v->getId());
        }

        $this->aKategorie = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildKategorie object, it will not be re-added.
        if ($v !== null) {
            $v->addProdukt($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildKategorie object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildKategorie The associated ChildKategorie object.
     * @throws PropelException
     */
    public function getKategorie(ConnectionInterface $con = null)
    {
        if ($this->aKategorie === null && ($this->kategorieid !== null)) {
            $this->aKategorie = ChildKategorieQuery::create()->findPk($this->kategorieid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKategorie->addProdukts($this);
             */
        }

        return $this->aKategorie;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Ingredience' == $relationName) {
            return $this->initIngrediences();
        }
        if ('Potraviny' == $relationName) {
            return $this->initPotravinies();
        }
        if ('Upozorneni' == $relationName) {
            return $this->initUpozornenis();
        }
        if ('Watchdog' == $relationName) {
            return $this->initWatchdogs();
        }
        if ('Zbozi' == $relationName) {
            return $this->initZbozis();
        }
    }

    /**
     * Clears out the collIngrediences collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addIngrediences()
     */
    public function clearIngrediences()
    {
        $this->collIngrediences = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collIngrediences collection loaded partially.
     */
    public function resetPartialIngrediences($v = true)
    {
        $this->collIngrediencesPartial = $v;
    }

    /**
     * Initializes the collIngrediences collection.
     *
     * By default this just sets the collIngrediences collection to an empty array (like clearcollIngrediences());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initIngrediences($overrideExisting = true)
    {
        if (null !== $this->collIngrediences && !$overrideExisting) {
            return;
        }

        $collectionClassName = IngredienceTableMap::getTableMap()->getCollectionClassName();

        $this->collIngrediences = new $collectionClassName;
        $this->collIngrediences->setModel('\SmartFridge\Ingredience');
    }

    /**
     * Gets an array of ChildIngredience objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProdukt is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     * @throws PropelException
     */
    public function getIngrediences(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collIngrediencesPartial && !$this->isNew();
        if (null === $this->collIngrediences || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collIngrediences) {
                // return empty collection
                $this->initIngrediences();
            } else {
                $collIngrediences = ChildIngredienceQuery::create(null, $criteria)
                    ->filterByProdukt($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collIngrediencesPartial && count($collIngrediences)) {
                        $this->initIngrediences(false);

                        foreach ($collIngrediences as $obj) {
                            if (false == $this->collIngrediences->contains($obj)) {
                                $this->collIngrediences->append($obj);
                            }
                        }

                        $this->collIngrediencesPartial = true;
                    }

                    return $collIngrediences;
                }

                if ($partial && $this->collIngrediences) {
                    foreach ($this->collIngrediences as $obj) {
                        if ($obj->isNew()) {
                            $collIngrediences[] = $obj;
                        }
                    }
                }

                $this->collIngrediences = $collIngrediences;
                $this->collIngrediencesPartial = false;
            }
        }

        return $this->collIngrediences;
    }

    /**
     * Sets a collection of ChildIngredience objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $ingrediences A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProdukt The current object (for fluent API support)
     */
    public function setIngrediences(Collection $ingrediences, ConnectionInterface $con = null)
    {
        /** @var ChildIngredience[] $ingrediencesToDelete */
        $ingrediencesToDelete = $this->getIngrediences(new Criteria(), $con)->diff($ingrediences);


        $this->ingrediencesScheduledForDeletion = $ingrediencesToDelete;

        foreach ($ingrediencesToDelete as $ingredienceRemoved) {
            $ingredienceRemoved->setProdukt(null);
        }

        $this->collIngrediences = null;
        foreach ($ingrediences as $ingredience) {
            $this->addIngredience($ingredience);
        }

        $this->collIngrediences = $ingrediences;
        $this->collIngrediencesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Ingredience objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Ingredience objects.
     * @throws PropelException
     */
    public function countIngrediences(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collIngrediencesPartial && !$this->isNew();
        if (null === $this->collIngrediences || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collIngrediences) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getIngrediences());
            }

            $query = ChildIngredienceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProdukt($this)
                ->count($con);
        }

        return count($this->collIngrediences);
    }

    /**
     * Method called to associate a ChildIngredience object to this object
     * through the ChildIngredience foreign key attribute.
     *
     * @param  ChildIngredience $l ChildIngredience
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function addIngredience(ChildIngredience $l)
    {
        if ($this->collIngrediences === null) {
            $this->initIngrediences();
            $this->collIngrediencesPartial = true;
        }

        if (!$this->collIngrediences->contains($l)) {
            $this->doAddIngredience($l);

            if ($this->ingrediencesScheduledForDeletion and $this->ingrediencesScheduledForDeletion->contains($l)) {
                $this->ingrediencesScheduledForDeletion->remove($this->ingrediencesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildIngredience $ingredience The ChildIngredience object to add.
     */
    protected function doAddIngredience(ChildIngredience $ingredience)
    {
        $this->collIngrediences[]= $ingredience;
        $ingredience->setProdukt($this);
    }

    /**
     * @param  ChildIngredience $ingredience The ChildIngredience object to remove.
     * @return $this|ChildProdukt The current object (for fluent API support)
     */
    public function removeIngredience(ChildIngredience $ingredience)
    {
        if ($this->getIngrediences()->contains($ingredience)) {
            $pos = $this->collIngrediences->search($ingredience);
            $this->collIngrediences->remove($pos);
            if (null === $this->ingrediencesScheduledForDeletion) {
                $this->ingrediencesScheduledForDeletion = clone $this->collIngrediences;
                $this->ingrediencesScheduledForDeletion->clear();
            }
            $this->ingrediencesScheduledForDeletion[]= $ingredience;
            $ingredience->setProdukt(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Produkt is new, it will return
     * an empty collection; or if this Produkt has previously
     * been saved, it will retrieve related Ingrediences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Produkt.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     */
    public function getIngrediencesJoinJednotka(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildIngredienceQuery::create(null, $criteria);
        $query->joinWith('Jednotka', $joinBehavior);

        return $this->getIngrediences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Produkt is new, it will return
     * an empty collection; or if this Produkt has previously
     * been saved, it will retrieve related Ingrediences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Produkt.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     */
    public function getIngrediencesJoinKategorie(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildIngredienceQuery::create(null, $criteria);
        $query->joinWith('Kategorie', $joinBehavior);

        return $this->getIngrediences($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Produkt is new, it will return
     * an empty collection; or if this Produkt has previously
     * been saved, it will retrieve related Ingrediences from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Produkt.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildIngredience[] List of ChildIngredience objects
     */
    public function getIngrediencesJoinRecepty(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildIngredienceQuery::create(null, $criteria);
        $query->joinWith('Recepty', $joinBehavior);

        return $this->getIngrediences($query, $con);
    }

    /**
     * Clears out the collPotravinies collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPotravinies()
     */
    public function clearPotravinies()
    {
        $this->collPotravinies = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPotravinies collection loaded partially.
     */
    public function resetPartialPotravinies($v = true)
    {
        $this->collPotraviniesPartial = $v;
    }

    /**
     * Initializes the collPotravinies collection.
     *
     * By default this just sets the collPotravinies collection to an empty array (like clearcollPotravinies());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPotravinies($overrideExisting = true)
    {
        if (null !== $this->collPotravinies && !$overrideExisting) {
            return;
        }

        $collectionClassName = PotravinyTableMap::getTableMap()->getCollectionClassName();

        $this->collPotravinies = new $collectionClassName;
        $this->collPotravinies->setModel('\SmartFridge\Potraviny');
    }

    /**
     * Gets an array of ChildPotraviny objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProdukt is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPotraviny[] List of ChildPotraviny objects
     * @throws PropelException
     */
    public function getPotravinies(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPotraviniesPartial && !$this->isNew();
        if (null === $this->collPotravinies || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPotravinies) {
                // return empty collection
                $this->initPotravinies();
            } else {
                $collPotravinies = ChildPotravinyQuery::create(null, $criteria)
                    ->filterByProdukt($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPotraviniesPartial && count($collPotravinies)) {
                        $this->initPotravinies(false);

                        foreach ($collPotravinies as $obj) {
                            if (false == $this->collPotravinies->contains($obj)) {
                                $this->collPotravinies->append($obj);
                            }
                        }

                        $this->collPotraviniesPartial = true;
                    }

                    return $collPotravinies;
                }

                if ($partial && $this->collPotravinies) {
                    foreach ($this->collPotravinies as $obj) {
                        if ($obj->isNew()) {
                            $collPotravinies[] = $obj;
                        }
                    }
                }

                $this->collPotravinies = $collPotravinies;
                $this->collPotraviniesPartial = false;
            }
        }

        return $this->collPotravinies;
    }

    /**
     * Sets a collection of ChildPotraviny objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $potravinies A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProdukt The current object (for fluent API support)
     */
    public function setPotravinies(Collection $potravinies, ConnectionInterface $con = null)
    {
        /** @var ChildPotraviny[] $potraviniesToDelete */
        $potraviniesToDelete = $this->getPotravinies(new Criteria(), $con)->diff($potravinies);


        $this->potraviniesScheduledForDeletion = $potraviniesToDelete;

        foreach ($potraviniesToDelete as $potravinyRemoved) {
            $potravinyRemoved->setProdukt(null);
        }

        $this->collPotravinies = null;
        foreach ($potravinies as $potraviny) {
            $this->addPotraviny($potraviny);
        }

        $this->collPotravinies = $potravinies;
        $this->collPotraviniesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Potraviny objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Potraviny objects.
     * @throws PropelException
     */
    public function countPotravinies(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPotraviniesPartial && !$this->isNew();
        if (null === $this->collPotravinies || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPotravinies) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPotravinies());
            }

            $query = ChildPotravinyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProdukt($this)
                ->count($con);
        }

        return count($this->collPotravinies);
    }

    /**
     * Method called to associate a ChildPotraviny object to this object
     * through the ChildPotraviny foreign key attribute.
     *
     * @param  ChildPotraviny $l ChildPotraviny
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function addPotraviny(ChildPotraviny $l)
    {
        if ($this->collPotravinies === null) {
            $this->initPotravinies();
            $this->collPotraviniesPartial = true;
        }

        if (!$this->collPotravinies->contains($l)) {
            $this->doAddPotraviny($l);

            if ($this->potraviniesScheduledForDeletion and $this->potraviniesScheduledForDeletion->contains($l)) {
                $this->potraviniesScheduledForDeletion->remove($this->potraviniesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPotraviny $potraviny The ChildPotraviny object to add.
     */
    protected function doAddPotraviny(ChildPotraviny $potraviny)
    {
        $this->collPotravinies[]= $potraviny;
        $potraviny->setProdukt($this);
    }

    /**
     * @param  ChildPotraviny $potraviny The ChildPotraviny object to remove.
     * @return $this|ChildProdukt The current object (for fluent API support)
     */
    public function removePotraviny(ChildPotraviny $potraviny)
    {
        if ($this->getPotravinies()->contains($potraviny)) {
            $pos = $this->collPotravinies->search($potraviny);
            $this->collPotravinies->remove($pos);
            if (null === $this->potraviniesScheduledForDeletion) {
                $this->potraviniesScheduledForDeletion = clone $this->collPotravinies;
                $this->potraviniesScheduledForDeletion->clear();
            }
            $this->potraviniesScheduledForDeletion[]= $potraviny;
            $potraviny->setProdukt(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Produkt is new, it will return
     * an empty collection; or if this Produkt has previously
     * been saved, it will retrieve related Potravinies from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Produkt.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPotraviny[] List of ChildPotraviny objects
     */
    public function getPotraviniesJoinUzivatel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPotravinyQuery::create(null, $criteria);
        $query->joinWith('Uzivatel', $joinBehavior);

        return $this->getPotravinies($query, $con);
    }

    /**
     * Clears out the collUpozornenis collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUpozornenis()
     */
    public function clearUpozornenis()
    {
        $this->collUpozornenis = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUpozornenis collection loaded partially.
     */
    public function resetPartialUpozornenis($v = true)
    {
        $this->collUpozornenisPartial = $v;
    }

    /**
     * Initializes the collUpozornenis collection.
     *
     * By default this just sets the collUpozornenis collection to an empty array (like clearcollUpozornenis());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUpozornenis($overrideExisting = true)
    {
        if (null !== $this->collUpozornenis && !$overrideExisting) {
            return;
        }

        $collectionClassName = UpozorneniTableMap::getTableMap()->getCollectionClassName();

        $this->collUpozornenis = new $collectionClassName;
        $this->collUpozornenis->setModel('\SmartFridge\Upozorneni');
    }

    /**
     * Gets an array of ChildUpozorneni objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProdukt is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUpozorneni[] List of ChildUpozorneni objects
     * @throws PropelException
     */
    public function getUpozornenis(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUpozornenisPartial && !$this->isNew();
        if (null === $this->collUpozornenis || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUpozornenis) {
                // return empty collection
                $this->initUpozornenis();
            } else {
                $collUpozornenis = ChildUpozorneniQuery::create(null, $criteria)
                    ->filterByProdukt($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUpozornenisPartial && count($collUpozornenis)) {
                        $this->initUpozornenis(false);

                        foreach ($collUpozornenis as $obj) {
                            if (false == $this->collUpozornenis->contains($obj)) {
                                $this->collUpozornenis->append($obj);
                            }
                        }

                        $this->collUpozornenisPartial = true;
                    }

                    return $collUpozornenis;
                }

                if ($partial && $this->collUpozornenis) {
                    foreach ($this->collUpozornenis as $obj) {
                        if ($obj->isNew()) {
                            $collUpozornenis[] = $obj;
                        }
                    }
                }

                $this->collUpozornenis = $collUpozornenis;
                $this->collUpozornenisPartial = false;
            }
        }

        return $this->collUpozornenis;
    }

    /**
     * Sets a collection of ChildUpozorneni objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $upozornenis A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProdukt The current object (for fluent API support)
     */
    public function setUpozornenis(Collection $upozornenis, ConnectionInterface $con = null)
    {
        /** @var ChildUpozorneni[] $upozornenisToDelete */
        $upozornenisToDelete = $this->getUpozornenis(new Criteria(), $con)->diff($upozornenis);


        $this->upozornenisScheduledForDeletion = $upozornenisToDelete;

        foreach ($upozornenisToDelete as $upozorneniRemoved) {
            $upozorneniRemoved->setProdukt(null);
        }

        $this->collUpozornenis = null;
        foreach ($upozornenis as $upozorneni) {
            $this->addUpozorneni($upozorneni);
        }

        $this->collUpozornenis = $upozornenis;
        $this->collUpozornenisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Upozorneni objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Upozorneni objects.
     * @throws PropelException
     */
    public function countUpozornenis(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUpozornenisPartial && !$this->isNew();
        if (null === $this->collUpozornenis || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUpozornenis) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUpozornenis());
            }

            $query = ChildUpozorneniQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProdukt($this)
                ->count($con);
        }

        return count($this->collUpozornenis);
    }

    /**
     * Method called to associate a ChildUpozorneni object to this object
     * through the ChildUpozorneni foreign key attribute.
     *
     * @param  ChildUpozorneni $l ChildUpozorneni
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function addUpozorneni(ChildUpozorneni $l)
    {
        if ($this->collUpozornenis === null) {
            $this->initUpozornenis();
            $this->collUpozornenisPartial = true;
        }

        if (!$this->collUpozornenis->contains($l)) {
            $this->doAddUpozorneni($l);

            if ($this->upozornenisScheduledForDeletion and $this->upozornenisScheduledForDeletion->contains($l)) {
                $this->upozornenisScheduledForDeletion->remove($this->upozornenisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUpozorneni $upozorneni The ChildUpozorneni object to add.
     */
    protected function doAddUpozorneni(ChildUpozorneni $upozorneni)
    {
        $this->collUpozornenis[]= $upozorneni;
        $upozorneni->setProdukt($this);
    }

    /**
     * @param  ChildUpozorneni $upozorneni The ChildUpozorneni object to remove.
     * @return $this|ChildProdukt The current object (for fluent API support)
     */
    public function removeUpozorneni(ChildUpozorneni $upozorneni)
    {
        if ($this->getUpozornenis()->contains($upozorneni)) {
            $pos = $this->collUpozornenis->search($upozorneni);
            $this->collUpozornenis->remove($pos);
            if (null === $this->upozornenisScheduledForDeletion) {
                $this->upozornenisScheduledForDeletion = clone $this->collUpozornenis;
                $this->upozornenisScheduledForDeletion->clear();
            }
            $this->upozornenisScheduledForDeletion[]= $upozorneni;
            $upozorneni->setProdukt(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Produkt is new, it will return
     * an empty collection; or if this Produkt has previously
     * been saved, it will retrieve related Upozornenis from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Produkt.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUpozorneni[] List of ChildUpozorneni objects
     */
    public function getUpozornenisJoinUzivatel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUpozorneniQuery::create(null, $criteria);
        $query->joinWith('Uzivatel', $joinBehavior);

        return $this->getUpozornenis($query, $con);
    }

    /**
     * Clears out the collWatchdogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addWatchdogs()
     */
    public function clearWatchdogs()
    {
        $this->collWatchdogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collWatchdogs collection loaded partially.
     */
    public function resetPartialWatchdogs($v = true)
    {
        $this->collWatchdogsPartial = $v;
    }

    /**
     * Initializes the collWatchdogs collection.
     *
     * By default this just sets the collWatchdogs collection to an empty array (like clearcollWatchdogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWatchdogs($overrideExisting = true)
    {
        if (null !== $this->collWatchdogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = WatchdogTableMap::getTableMap()->getCollectionClassName();

        $this->collWatchdogs = new $collectionClassName;
        $this->collWatchdogs->setModel('\SmartFridge\Watchdog');
    }

    /**
     * Gets an array of ChildWatchdog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProdukt is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildWatchdog[] List of ChildWatchdog objects
     * @throws PropelException
     */
    public function getWatchdogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collWatchdogsPartial && !$this->isNew();
        if (null === $this->collWatchdogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWatchdogs) {
                // return empty collection
                $this->initWatchdogs();
            } else {
                $collWatchdogs = ChildWatchdogQuery::create(null, $criteria)
                    ->filterByProdukt($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collWatchdogsPartial && count($collWatchdogs)) {
                        $this->initWatchdogs(false);

                        foreach ($collWatchdogs as $obj) {
                            if (false == $this->collWatchdogs->contains($obj)) {
                                $this->collWatchdogs->append($obj);
                            }
                        }

                        $this->collWatchdogsPartial = true;
                    }

                    return $collWatchdogs;
                }

                if ($partial && $this->collWatchdogs) {
                    foreach ($this->collWatchdogs as $obj) {
                        if ($obj->isNew()) {
                            $collWatchdogs[] = $obj;
                        }
                    }
                }

                $this->collWatchdogs = $collWatchdogs;
                $this->collWatchdogsPartial = false;
            }
        }

        return $this->collWatchdogs;
    }

    /**
     * Sets a collection of ChildWatchdog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $watchdogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProdukt The current object (for fluent API support)
     */
    public function setWatchdogs(Collection $watchdogs, ConnectionInterface $con = null)
    {
        /** @var ChildWatchdog[] $watchdogsToDelete */
        $watchdogsToDelete = $this->getWatchdogs(new Criteria(), $con)->diff($watchdogs);


        $this->watchdogsScheduledForDeletion = $watchdogsToDelete;

        foreach ($watchdogsToDelete as $watchdogRemoved) {
            $watchdogRemoved->setProdukt(null);
        }

        $this->collWatchdogs = null;
        foreach ($watchdogs as $watchdog) {
            $this->addWatchdog($watchdog);
        }

        $this->collWatchdogs = $watchdogs;
        $this->collWatchdogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Watchdog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Watchdog objects.
     * @throws PropelException
     */
    public function countWatchdogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collWatchdogsPartial && !$this->isNew();
        if (null === $this->collWatchdogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWatchdogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWatchdogs());
            }

            $query = ChildWatchdogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProdukt($this)
                ->count($con);
        }

        return count($this->collWatchdogs);
    }

    /**
     * Method called to associate a ChildWatchdog object to this object
     * through the ChildWatchdog foreign key attribute.
     *
     * @param  ChildWatchdog $l ChildWatchdog
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function addWatchdog(ChildWatchdog $l)
    {
        if ($this->collWatchdogs === null) {
            $this->initWatchdogs();
            $this->collWatchdogsPartial = true;
        }

        if (!$this->collWatchdogs->contains($l)) {
            $this->doAddWatchdog($l);

            if ($this->watchdogsScheduledForDeletion and $this->watchdogsScheduledForDeletion->contains($l)) {
                $this->watchdogsScheduledForDeletion->remove($this->watchdogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildWatchdog $watchdog The ChildWatchdog object to add.
     */
    protected function doAddWatchdog(ChildWatchdog $watchdog)
    {
        $this->collWatchdogs[]= $watchdog;
        $watchdog->setProdukt($this);
    }

    /**
     * @param  ChildWatchdog $watchdog The ChildWatchdog object to remove.
     * @return $this|ChildProdukt The current object (for fluent API support)
     */
    public function removeWatchdog(ChildWatchdog $watchdog)
    {
        if ($this->getWatchdogs()->contains($watchdog)) {
            $pos = $this->collWatchdogs->search($watchdog);
            $this->collWatchdogs->remove($pos);
            if (null === $this->watchdogsScheduledForDeletion) {
                $this->watchdogsScheduledForDeletion = clone $this->collWatchdogs;
                $this->watchdogsScheduledForDeletion->clear();
            }
            $this->watchdogsScheduledForDeletion[]= $watchdog;
            $watchdog->setProdukt(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Produkt is new, it will return
     * an empty collection; or if this Produkt has previously
     * been saved, it will retrieve related Watchdogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Produkt.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildWatchdog[] List of ChildWatchdog objects
     */
    public function getWatchdogsJoinUzivatel(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildWatchdogQuery::create(null, $criteria);
        $query->joinWith('Uzivatel', $joinBehavior);

        return $this->getWatchdogs($query, $con);
    }

    /**
     * Clears out the collZbozis collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addZbozis()
     */
    public function clearZbozis()
    {
        $this->collZbozis = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collZbozis collection loaded partially.
     */
    public function resetPartialZbozis($v = true)
    {
        $this->collZbozisPartial = $v;
    }

    /**
     * Initializes the collZbozis collection.
     *
     * By default this just sets the collZbozis collection to an empty array (like clearcollZbozis());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initZbozis($overrideExisting = true)
    {
        if (null !== $this->collZbozis && !$overrideExisting) {
            return;
        }

        $collectionClassName = ZboziTableMap::getTableMap()->getCollectionClassName();

        $this->collZbozis = new $collectionClassName;
        $this->collZbozis->setModel('\SmartFridge\Zbozi');
    }

    /**
     * Gets an array of ChildZbozi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProdukt is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildZbozi[] List of ChildZbozi objects
     * @throws PropelException
     */
    public function getZbozis(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collZbozisPartial && !$this->isNew();
        if (null === $this->collZbozis || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collZbozis) {
                // return empty collection
                $this->initZbozis();
            } else {
                $collZbozis = ChildZboziQuery::create(null, $criteria)
                    ->filterByProdukt($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collZbozisPartial && count($collZbozis)) {
                        $this->initZbozis(false);

                        foreach ($collZbozis as $obj) {
                            if (false == $this->collZbozis->contains($obj)) {
                                $this->collZbozis->append($obj);
                            }
                        }

                        $this->collZbozisPartial = true;
                    }

                    return $collZbozis;
                }

                if ($partial && $this->collZbozis) {
                    foreach ($this->collZbozis as $obj) {
                        if ($obj->isNew()) {
                            $collZbozis[] = $obj;
                        }
                    }
                }

                $this->collZbozis = $collZbozis;
                $this->collZbozisPartial = false;
            }
        }

        return $this->collZbozis;
    }

    /**
     * Sets a collection of ChildZbozi objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $zbozis A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProdukt The current object (for fluent API support)
     */
    public function setZbozis(Collection $zbozis, ConnectionInterface $con = null)
    {
        /** @var ChildZbozi[] $zbozisToDelete */
        $zbozisToDelete = $this->getZbozis(new Criteria(), $con)->diff($zbozis);


        $this->zbozisScheduledForDeletion = $zbozisToDelete;

        foreach ($zbozisToDelete as $zboziRemoved) {
            $zboziRemoved->setProdukt(null);
        }

        $this->collZbozis = null;
        foreach ($zbozis as $zbozi) {
            $this->addZbozi($zbozi);
        }

        $this->collZbozis = $zbozis;
        $this->collZbozisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Zbozi objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Zbozi objects.
     * @throws PropelException
     */
    public function countZbozis(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collZbozisPartial && !$this->isNew();
        if (null === $this->collZbozis || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collZbozis) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getZbozis());
            }

            $query = ChildZboziQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProdukt($this)
                ->count($con);
        }

        return count($this->collZbozis);
    }

    /**
     * Method called to associate a ChildZbozi object to this object
     * through the ChildZbozi foreign key attribute.
     *
     * @param  ChildZbozi $l ChildZbozi
     * @return $this|\SmartFridge\Produkt The current object (for fluent API support)
     */
    public function addZbozi(ChildZbozi $l)
    {
        if ($this->collZbozis === null) {
            $this->initZbozis();
            $this->collZbozisPartial = true;
        }

        if (!$this->collZbozis->contains($l)) {
            $this->doAddZbozi($l);

            if ($this->zbozisScheduledForDeletion and $this->zbozisScheduledForDeletion->contains($l)) {
                $this->zbozisScheduledForDeletion->remove($this->zbozisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildZbozi $zbozi The ChildZbozi object to add.
     */
    protected function doAddZbozi(ChildZbozi $zbozi)
    {
        $this->collZbozis[]= $zbozi;
        $zbozi->setProdukt($this);
    }

    /**
     * @param  ChildZbozi $zbozi The ChildZbozi object to remove.
     * @return $this|ChildProdukt The current object (for fluent API support)
     */
    public function removeZbozi(ChildZbozi $zbozi)
    {
        if ($this->getZbozis()->contains($zbozi)) {
            $pos = $this->collZbozis->search($zbozi);
            $this->collZbozis->remove($pos);
            if (null === $this->zbozisScheduledForDeletion) {
                $this->zbozisScheduledForDeletion = clone $this->collZbozis;
                $this->zbozisScheduledForDeletion->clear();
            }
            $this->zbozisScheduledForDeletion[]= $zbozi;
            $zbozi->setProdukt(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aJednotka) {
            $this->aJednotka->removeProdukt($this);
        }
        if (null !== $this->aKategorie) {
            $this->aKategorie->removeProdukt($this);
        }
        $this->id = null;
        $this->nazev = null;
        $this->fotka = null;
        $this->popis = null;
        $this->kategorieid = null;
        $this->jednotkaid = null;
        $this->mnozstvi = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collIngrediences) {
                foreach ($this->collIngrediences as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPotravinies) {
                foreach ($this->collPotravinies as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUpozornenis) {
                foreach ($this->collUpozornenis as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWatchdogs) {
                foreach ($this->collWatchdogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collZbozis) {
                foreach ($this->collZbozis as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collIngrediences = null;
        $this->collPotravinies = null;
        $this->collUpozornenis = null;
        $this->collWatchdogs = null;
        $this->collZbozis = null;
        $this->aJednotka = null;
        $this->aKategorie = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProduktTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
