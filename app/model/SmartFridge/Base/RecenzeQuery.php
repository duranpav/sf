<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SmartFridge\Recenze as ChildRecenze;
use SmartFridge\RecenzeQuery as ChildRecenzeQuery;
use SmartFridge\Map\RecenzeTableMap;

/**
 * Base class that represents a query for the 'Recenze' table.
 *
 *
 *
 * @method     ChildRecenzeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildRecenzeQuery orderByTextrecenze($order = Criteria::ASC) Order by the textRecenze column
 * @method     ChildRecenzeQuery orderByPocethvezdicek($order = Criteria::ASC) Order by the pocetHvezdicek column
 * @method     ChildRecenzeQuery orderBySoubor($order = Criteria::ASC) Order by the soubor column
 * @method     ChildRecenzeQuery orderByReceptid($order = Criteria::ASC) Order by the receptID column
 * @method     ChildRecenzeQuery orderByUzivatelid($order = Criteria::ASC) Order by the uzivatelID column
 *
 * @method     ChildRecenzeQuery groupById() Group by the id column
 * @method     ChildRecenzeQuery groupByTextrecenze() Group by the textRecenze column
 * @method     ChildRecenzeQuery groupByPocethvezdicek() Group by the pocetHvezdicek column
 * @method     ChildRecenzeQuery groupBySoubor() Group by the soubor column
 * @method     ChildRecenzeQuery groupByReceptid() Group by the receptID column
 * @method     ChildRecenzeQuery groupByUzivatelid() Group by the uzivatelID column
 *
 * @method     ChildRecenzeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRecenzeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRecenzeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRecenzeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRecenzeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRecenzeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRecenzeQuery leftJoinRecepty($relationAlias = null) Adds a LEFT JOIN clause to the query using the Recepty relation
 * @method     ChildRecenzeQuery rightJoinRecepty($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Recepty relation
 * @method     ChildRecenzeQuery innerJoinRecepty($relationAlias = null) Adds a INNER JOIN clause to the query using the Recepty relation
 *
 * @method     ChildRecenzeQuery joinWithRecepty($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Recepty relation
 *
 * @method     ChildRecenzeQuery leftJoinWithRecepty() Adds a LEFT JOIN clause and with to the query using the Recepty relation
 * @method     ChildRecenzeQuery rightJoinWithRecepty() Adds a RIGHT JOIN clause and with to the query using the Recepty relation
 * @method     ChildRecenzeQuery innerJoinWithRecepty() Adds a INNER JOIN clause and with to the query using the Recepty relation
 *
 * @method     ChildRecenzeQuery leftJoinUzivatel($relationAlias = null) Adds a LEFT JOIN clause to the query using the Uzivatel relation
 * @method     ChildRecenzeQuery rightJoinUzivatel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Uzivatel relation
 * @method     ChildRecenzeQuery innerJoinUzivatel($relationAlias = null) Adds a INNER JOIN clause to the query using the Uzivatel relation
 *
 * @method     ChildRecenzeQuery joinWithUzivatel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Uzivatel relation
 *
 * @method     ChildRecenzeQuery leftJoinWithUzivatel() Adds a LEFT JOIN clause and with to the query using the Uzivatel relation
 * @method     ChildRecenzeQuery rightJoinWithUzivatel() Adds a RIGHT JOIN clause and with to the query using the Uzivatel relation
 * @method     ChildRecenzeQuery innerJoinWithUzivatel() Adds a INNER JOIN clause and with to the query using the Uzivatel relation
 *
 * @method     \SmartFridge\ReceptyQuery|\SmartFridge\UzivatelQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRecenze findOne(ConnectionInterface $con = null) Return the first ChildRecenze matching the query
 * @method     ChildRecenze findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRecenze matching the query, or a new ChildRecenze object populated from the query conditions when no match is found
 *
 * @method     ChildRecenze findOneById(int $id) Return the first ChildRecenze filtered by the id column
 * @method     ChildRecenze findOneByTextrecenze(string $textRecenze) Return the first ChildRecenze filtered by the textRecenze column
 * @method     ChildRecenze findOneByPocethvezdicek(int $pocetHvezdicek) Return the first ChildRecenze filtered by the pocetHvezdicek column
 * @method     ChildRecenze findOneBySoubor(string $soubor) Return the first ChildRecenze filtered by the soubor column
 * @method     ChildRecenze findOneByReceptid(int $receptID) Return the first ChildRecenze filtered by the receptID column
 * @method     ChildRecenze findOneByUzivatelid(int $uzivatelID) Return the first ChildRecenze filtered by the uzivatelID column *

 * @method     ChildRecenze requirePk($key, ConnectionInterface $con = null) Return the ChildRecenze by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecenze requireOne(ConnectionInterface $con = null) Return the first ChildRecenze matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRecenze requireOneById(int $id) Return the first ChildRecenze filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecenze requireOneByTextrecenze(string $textRecenze) Return the first ChildRecenze filtered by the textRecenze column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecenze requireOneByPocethvezdicek(int $pocetHvezdicek) Return the first ChildRecenze filtered by the pocetHvezdicek column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecenze requireOneBySoubor(string $soubor) Return the first ChildRecenze filtered by the soubor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecenze requireOneByReceptid(int $receptID) Return the first ChildRecenze filtered by the receptID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRecenze requireOneByUzivatelid(int $uzivatelID) Return the first ChildRecenze filtered by the uzivatelID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRecenze[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRecenze objects based on current ModelCriteria
 * @method     ChildRecenze[]|ObjectCollection findById(int $id) Return ChildRecenze objects filtered by the id column
 * @method     ChildRecenze[]|ObjectCollection findByTextrecenze(string $textRecenze) Return ChildRecenze objects filtered by the textRecenze column
 * @method     ChildRecenze[]|ObjectCollection findByPocethvezdicek(int $pocetHvezdicek) Return ChildRecenze objects filtered by the pocetHvezdicek column
 * @method     ChildRecenze[]|ObjectCollection findBySoubor(string $soubor) Return ChildRecenze objects filtered by the soubor column
 * @method     ChildRecenze[]|ObjectCollection findByReceptid(int $receptID) Return ChildRecenze objects filtered by the receptID column
 * @method     ChildRecenze[]|ObjectCollection findByUzivatelid(int $uzivatelID) Return ChildRecenze objects filtered by the uzivatelID column
 * @method     ChildRecenze[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RecenzeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SmartFridge\Base\RecenzeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SmartFridge\\Recenze', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRecenzeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRecenzeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRecenzeQuery) {
            return $criteria;
        }
        $query = new ChildRecenzeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRecenze|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RecenzeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RecenzeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRecenze A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, textRecenze, pocetHvezdicek, soubor, receptID, uzivatelID FROM Recenze WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRecenze $obj */
            $obj = new ChildRecenze();
            $obj->hydrate($row);
            RecenzeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRecenze|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RecenzeTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RecenzeTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RecenzeTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RecenzeTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RecenzeTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the textRecenze column
     *
     * Example usage:
     * <code>
     * $query->filterByTextrecenze('fooValue');   // WHERE textRecenze = 'fooValue'
     * $query->filterByTextrecenze('%fooValue%', Criteria::LIKE); // WHERE textRecenze LIKE '%fooValue%'
     * </code>
     *
     * @param     string $textrecenze The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function filterByTextrecenze($textrecenze = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($textrecenze)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RecenzeTableMap::COL_TEXTRECENZE, $textrecenze, $comparison);
    }

    /**
     * Filter the query on the pocetHvezdicek column
     *
     * Example usage:
     * <code>
     * $query->filterByPocethvezdicek(1234); // WHERE pocetHvezdicek = 1234
     * $query->filterByPocethvezdicek(array(12, 34)); // WHERE pocetHvezdicek IN (12, 34)
     * $query->filterByPocethvezdicek(array('min' => 12)); // WHERE pocetHvezdicek > 12
     * </code>
     *
     * @param     mixed $pocethvezdicek The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function filterByPocethvezdicek($pocethvezdicek = null, $comparison = null)
    {
        if (is_array($pocethvezdicek)) {
            $useMinMax = false;
            if (isset($pocethvezdicek['min'])) {
                $this->addUsingAlias(RecenzeTableMap::COL_POCETHVEZDICEK, $pocethvezdicek['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pocethvezdicek['max'])) {
                $this->addUsingAlias(RecenzeTableMap::COL_POCETHVEZDICEK, $pocethvezdicek['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RecenzeTableMap::COL_POCETHVEZDICEK, $pocethvezdicek, $comparison);
    }

    /**
     * Filter the query on the soubor column
     *
     * Example usage:
     * <code>
     * $query->filterBySoubor('fooValue');   // WHERE soubor = 'fooValue'
     * $query->filterBySoubor('%fooValue%', Criteria::LIKE); // WHERE soubor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $soubor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function filterBySoubor($soubor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($soubor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RecenzeTableMap::COL_SOUBOR, $soubor, $comparison);
    }

    /**
     * Filter the query on the receptID column
     *
     * Example usage:
     * <code>
     * $query->filterByReceptid(1234); // WHERE receptID = 1234
     * $query->filterByReceptid(array(12, 34)); // WHERE receptID IN (12, 34)
     * $query->filterByReceptid(array('min' => 12)); // WHERE receptID > 12
     * </code>
     *
     * @see       filterByRecepty()
     *
     * @param     mixed $receptid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function filterByReceptid($receptid = null, $comparison = null)
    {
        if (is_array($receptid)) {
            $useMinMax = false;
            if (isset($receptid['min'])) {
                $this->addUsingAlias(RecenzeTableMap::COL_RECEPTID, $receptid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($receptid['max'])) {
                $this->addUsingAlias(RecenzeTableMap::COL_RECEPTID, $receptid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RecenzeTableMap::COL_RECEPTID, $receptid, $comparison);
    }

    /**
     * Filter the query on the uzivatelID column
     *
     * Example usage:
     * <code>
     * $query->filterByUzivatelid(1234); // WHERE uzivatelID = 1234
     * $query->filterByUzivatelid(array(12, 34)); // WHERE uzivatelID IN (12, 34)
     * $query->filterByUzivatelid(array('min' => 12)); // WHERE uzivatelID > 12
     * </code>
     *
     * @see       filterByUzivatel()
     *
     * @param     mixed $uzivatelid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function filterByUzivatelid($uzivatelid = null, $comparison = null)
    {
        if (is_array($uzivatelid)) {
            $useMinMax = false;
            if (isset($uzivatelid['min'])) {
                $this->addUsingAlias(RecenzeTableMap::COL_UZIVATELID, $uzivatelid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uzivatelid['max'])) {
                $this->addUsingAlias(RecenzeTableMap::COL_UZIVATELID, $uzivatelid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RecenzeTableMap::COL_UZIVATELID, $uzivatelid, $comparison);
    }

    /**
     * Filter the query by a related \SmartFridge\Recepty object
     *
     * @param \SmartFridge\Recepty|ObjectCollection $recepty The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRecenzeQuery The current query, for fluid interface
     */
    public function filterByRecepty($recepty, $comparison = null)
    {
        if ($recepty instanceof \SmartFridge\Recepty) {
            return $this
                ->addUsingAlias(RecenzeTableMap::COL_RECEPTID, $recepty->getId(), $comparison);
        } elseif ($recepty instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RecenzeTableMap::COL_RECEPTID, $recepty->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRecepty() only accepts arguments of type \SmartFridge\Recepty or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Recepty relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function joinRecepty($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Recepty');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Recepty');
        }

        return $this;
    }

    /**
     * Use the Recepty relation Recepty object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\ReceptyQuery A secondary query class using the current class as primary query
     */
    public function useReceptyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRecepty($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Recepty', '\SmartFridge\ReceptyQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Uzivatel object
     *
     * @param \SmartFridge\Uzivatel|ObjectCollection $uzivatel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRecenzeQuery The current query, for fluid interface
     */
    public function filterByUzivatel($uzivatel, $comparison = null)
    {
        if ($uzivatel instanceof \SmartFridge\Uzivatel) {
            return $this
                ->addUsingAlias(RecenzeTableMap::COL_UZIVATELID, $uzivatel->getId(), $comparison);
        } elseif ($uzivatel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RecenzeTableMap::COL_UZIVATELID, $uzivatel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUzivatel() only accepts arguments of type \SmartFridge\Uzivatel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Uzivatel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function joinUzivatel($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Uzivatel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Uzivatel');
        }

        return $this;
    }

    /**
     * Use the Uzivatel relation Uzivatel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\UzivatelQuery A secondary query class using the current class as primary query
     */
    public function useUzivatelQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUzivatel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Uzivatel', '\SmartFridge\UzivatelQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRecenze $recenze Object to remove from the list of results
     *
     * @return $this|ChildRecenzeQuery The current query, for fluid interface
     */
    public function prune($recenze = null)
    {
        if ($recenze) {
            $this->addUsingAlias(RecenzeTableMap::COL_ID, $recenze->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Recenze table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RecenzeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RecenzeTableMap::clearInstancePool();
            RecenzeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RecenzeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RecenzeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RecenzeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RecenzeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RecenzeQuery
