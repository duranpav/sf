<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SmartFridge\Ingredience as ChildIngredience;
use SmartFridge\IngredienceQuery as ChildIngredienceQuery;
use SmartFridge\Map\IngredienceTableMap;

/**
 * Base class that represents a query for the 'Ingredience' table.
 *
 *
 *
 * @method     ChildIngredienceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildIngredienceQuery orderByMnozstvi($order = Criteria::ASC) Order by the mnozstvi column
 * @method     ChildIngredienceQuery orderByReceptid($order = Criteria::ASC) Order by the receptID column
 * @method     ChildIngredienceQuery orderByJednotkaid($order = Criteria::ASC) Order by the jednotkaID column
 * @method     ChildIngredienceQuery orderByKategorieid($order = Criteria::ASC) Order by the kategorieID column
 * @method     ChildIngredienceQuery orderByProduktid($order = Criteria::ASC) Order by the produktID column
 *
 * @method     ChildIngredienceQuery groupById() Group by the id column
 * @method     ChildIngredienceQuery groupByMnozstvi() Group by the mnozstvi column
 * @method     ChildIngredienceQuery groupByReceptid() Group by the receptID column
 * @method     ChildIngredienceQuery groupByJednotkaid() Group by the jednotkaID column
 * @method     ChildIngredienceQuery groupByKategorieid() Group by the kategorieID column
 * @method     ChildIngredienceQuery groupByProduktid() Group by the produktID column
 *
 * @method     ChildIngredienceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildIngredienceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildIngredienceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildIngredienceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildIngredienceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildIngredienceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildIngredienceQuery leftJoinJednotka($relationAlias = null) Adds a LEFT JOIN clause to the query using the Jednotka relation
 * @method     ChildIngredienceQuery rightJoinJednotka($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Jednotka relation
 * @method     ChildIngredienceQuery innerJoinJednotka($relationAlias = null) Adds a INNER JOIN clause to the query using the Jednotka relation
 *
 * @method     ChildIngredienceQuery joinWithJednotka($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Jednotka relation
 *
 * @method     ChildIngredienceQuery leftJoinWithJednotka() Adds a LEFT JOIN clause and with to the query using the Jednotka relation
 * @method     ChildIngredienceQuery rightJoinWithJednotka() Adds a RIGHT JOIN clause and with to the query using the Jednotka relation
 * @method     ChildIngredienceQuery innerJoinWithJednotka() Adds a INNER JOIN clause and with to the query using the Jednotka relation
 *
 * @method     ChildIngredienceQuery leftJoinKategorie($relationAlias = null) Adds a LEFT JOIN clause to the query using the Kategorie relation
 * @method     ChildIngredienceQuery rightJoinKategorie($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Kategorie relation
 * @method     ChildIngredienceQuery innerJoinKategorie($relationAlias = null) Adds a INNER JOIN clause to the query using the Kategorie relation
 *
 * @method     ChildIngredienceQuery joinWithKategorie($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Kategorie relation
 *
 * @method     ChildIngredienceQuery leftJoinWithKategorie() Adds a LEFT JOIN clause and with to the query using the Kategorie relation
 * @method     ChildIngredienceQuery rightJoinWithKategorie() Adds a RIGHT JOIN clause and with to the query using the Kategorie relation
 * @method     ChildIngredienceQuery innerJoinWithKategorie() Adds a INNER JOIN clause and with to the query using the Kategorie relation
 *
 * @method     ChildIngredienceQuery leftJoinProdukt($relationAlias = null) Adds a LEFT JOIN clause to the query using the Produkt relation
 * @method     ChildIngredienceQuery rightJoinProdukt($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Produkt relation
 * @method     ChildIngredienceQuery innerJoinProdukt($relationAlias = null) Adds a INNER JOIN clause to the query using the Produkt relation
 *
 * @method     ChildIngredienceQuery joinWithProdukt($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Produkt relation
 *
 * @method     ChildIngredienceQuery leftJoinWithProdukt() Adds a LEFT JOIN clause and with to the query using the Produkt relation
 * @method     ChildIngredienceQuery rightJoinWithProdukt() Adds a RIGHT JOIN clause and with to the query using the Produkt relation
 * @method     ChildIngredienceQuery innerJoinWithProdukt() Adds a INNER JOIN clause and with to the query using the Produkt relation
 *
 * @method     ChildIngredienceQuery leftJoinRecepty($relationAlias = null) Adds a LEFT JOIN clause to the query using the Recepty relation
 * @method     ChildIngredienceQuery rightJoinRecepty($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Recepty relation
 * @method     ChildIngredienceQuery innerJoinRecepty($relationAlias = null) Adds a INNER JOIN clause to the query using the Recepty relation
 *
 * @method     ChildIngredienceQuery joinWithRecepty($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Recepty relation
 *
 * @method     ChildIngredienceQuery leftJoinWithRecepty() Adds a LEFT JOIN clause and with to the query using the Recepty relation
 * @method     ChildIngredienceQuery rightJoinWithRecepty() Adds a RIGHT JOIN clause and with to the query using the Recepty relation
 * @method     ChildIngredienceQuery innerJoinWithRecepty() Adds a INNER JOIN clause and with to the query using the Recepty relation
 *
 * @method     \SmartFridge\JednotkaQuery|\SmartFridge\KategorieQuery|\SmartFridge\ProduktQuery|\SmartFridge\ReceptyQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildIngredience findOne(ConnectionInterface $con = null) Return the first ChildIngredience matching the query
 * @method     ChildIngredience findOneOrCreate(ConnectionInterface $con = null) Return the first ChildIngredience matching the query, or a new ChildIngredience object populated from the query conditions when no match is found
 *
 * @method     ChildIngredience findOneById(int $id) Return the first ChildIngredience filtered by the id column
 * @method     ChildIngredience findOneByMnozstvi(int $mnozstvi) Return the first ChildIngredience filtered by the mnozstvi column
 * @method     ChildIngredience findOneByReceptid(int $receptID) Return the first ChildIngredience filtered by the receptID column
 * @method     ChildIngredience findOneByJednotkaid(int $jednotkaID) Return the first ChildIngredience filtered by the jednotkaID column
 * @method     ChildIngredience findOneByKategorieid(int $kategorieID) Return the first ChildIngredience filtered by the kategorieID column
 * @method     ChildIngredience findOneByProduktid(int $produktID) Return the first ChildIngredience filtered by the produktID column *

 * @method     ChildIngredience requirePk($key, ConnectionInterface $con = null) Return the ChildIngredience by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIngredience requireOne(ConnectionInterface $con = null) Return the first ChildIngredience matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildIngredience requireOneById(int $id) Return the first ChildIngredience filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIngredience requireOneByMnozstvi(int $mnozstvi) Return the first ChildIngredience filtered by the mnozstvi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIngredience requireOneByReceptid(int $receptID) Return the first ChildIngredience filtered by the receptID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIngredience requireOneByJednotkaid(int $jednotkaID) Return the first ChildIngredience filtered by the jednotkaID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIngredience requireOneByKategorieid(int $kategorieID) Return the first ChildIngredience filtered by the kategorieID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIngredience requireOneByProduktid(int $produktID) Return the first ChildIngredience filtered by the produktID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildIngredience[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildIngredience objects based on current ModelCriteria
 * @method     ChildIngredience[]|ObjectCollection findById(int $id) Return ChildIngredience objects filtered by the id column
 * @method     ChildIngredience[]|ObjectCollection findByMnozstvi(int $mnozstvi) Return ChildIngredience objects filtered by the mnozstvi column
 * @method     ChildIngredience[]|ObjectCollection findByReceptid(int $receptID) Return ChildIngredience objects filtered by the receptID column
 * @method     ChildIngredience[]|ObjectCollection findByJednotkaid(int $jednotkaID) Return ChildIngredience objects filtered by the jednotkaID column
 * @method     ChildIngredience[]|ObjectCollection findByKategorieid(int $kategorieID) Return ChildIngredience objects filtered by the kategorieID column
 * @method     ChildIngredience[]|ObjectCollection findByProduktid(int $produktID) Return ChildIngredience objects filtered by the produktID column
 * @method     ChildIngredience[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class IngredienceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SmartFridge\Base\IngredienceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SmartFridge\\Ingredience', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildIngredienceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildIngredienceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildIngredienceQuery) {
            return $criteria;
        }
        $query = new ChildIngredienceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildIngredience|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(IngredienceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = IngredienceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildIngredience A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, mnozstvi, receptID, jednotkaID, kategorieID, produktID FROM Ingredience WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildIngredience $obj */
            $obj = new ChildIngredience();
            $obj->hydrate($row);
            IngredienceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildIngredience|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(IngredienceTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(IngredienceTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IngredienceTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the mnozstvi column
     *
     * Example usage:
     * <code>
     * $query->filterByMnozstvi(1234); // WHERE mnozstvi = 1234
     * $query->filterByMnozstvi(array(12, 34)); // WHERE mnozstvi IN (12, 34)
     * $query->filterByMnozstvi(array('min' => 12)); // WHERE mnozstvi > 12
     * </code>
     *
     * @param     mixed $mnozstvi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByMnozstvi($mnozstvi = null, $comparison = null)
    {
        if (is_array($mnozstvi)) {
            $useMinMax = false;
            if (isset($mnozstvi['min'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_MNOZSTVI, $mnozstvi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mnozstvi['max'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_MNOZSTVI, $mnozstvi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IngredienceTableMap::COL_MNOZSTVI, $mnozstvi, $comparison);
    }

    /**
     * Filter the query on the receptID column
     *
     * Example usage:
     * <code>
     * $query->filterByReceptid(1234); // WHERE receptID = 1234
     * $query->filterByReceptid(array(12, 34)); // WHERE receptID IN (12, 34)
     * $query->filterByReceptid(array('min' => 12)); // WHERE receptID > 12
     * </code>
     *
     * @see       filterByRecepty()
     *
     * @param     mixed $receptid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByReceptid($receptid = null, $comparison = null)
    {
        if (is_array($receptid)) {
            $useMinMax = false;
            if (isset($receptid['min'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_RECEPTID, $receptid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($receptid['max'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_RECEPTID, $receptid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IngredienceTableMap::COL_RECEPTID, $receptid, $comparison);
    }

    /**
     * Filter the query on the jednotkaID column
     *
     * Example usage:
     * <code>
     * $query->filterByJednotkaid(1234); // WHERE jednotkaID = 1234
     * $query->filterByJednotkaid(array(12, 34)); // WHERE jednotkaID IN (12, 34)
     * $query->filterByJednotkaid(array('min' => 12)); // WHERE jednotkaID > 12
     * </code>
     *
     * @see       filterByJednotka()
     *
     * @param     mixed $jednotkaid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByJednotkaid($jednotkaid = null, $comparison = null)
    {
        if (is_array($jednotkaid)) {
            $useMinMax = false;
            if (isset($jednotkaid['min'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_JEDNOTKAID, $jednotkaid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jednotkaid['max'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_JEDNOTKAID, $jednotkaid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IngredienceTableMap::COL_JEDNOTKAID, $jednotkaid, $comparison);
    }

    /**
     * Filter the query on the kategorieID column
     *
     * Example usage:
     * <code>
     * $query->filterByKategorieid(1234); // WHERE kategorieID = 1234
     * $query->filterByKategorieid(array(12, 34)); // WHERE kategorieID IN (12, 34)
     * $query->filterByKategorieid(array('min' => 12)); // WHERE kategorieID > 12
     * </code>
     *
     * @see       filterByKategorie()
     *
     * @param     mixed $kategorieid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByKategorieid($kategorieid = null, $comparison = null)
    {
        if (is_array($kategorieid)) {
            $useMinMax = false;
            if (isset($kategorieid['min'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_KATEGORIEID, $kategorieid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kategorieid['max'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_KATEGORIEID, $kategorieid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IngredienceTableMap::COL_KATEGORIEID, $kategorieid, $comparison);
    }

    /**
     * Filter the query on the produktID column
     *
     * Example usage:
     * <code>
     * $query->filterByProduktid(1234); // WHERE produktID = 1234
     * $query->filterByProduktid(array(12, 34)); // WHERE produktID IN (12, 34)
     * $query->filterByProduktid(array('min' => 12)); // WHERE produktID > 12
     * </code>
     *
     * @see       filterByProdukt()
     *
     * @param     mixed $produktid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByProduktid($produktid = null, $comparison = null)
    {
        if (is_array($produktid)) {
            $useMinMax = false;
            if (isset($produktid['min'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_PRODUKTID, $produktid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produktid['max'])) {
                $this->addUsingAlias(IngredienceTableMap::COL_PRODUKTID, $produktid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IngredienceTableMap::COL_PRODUKTID, $produktid, $comparison);
    }

    /**
     * Filter the query by a related \SmartFridge\Jednotka object
     *
     * @param \SmartFridge\Jednotka|ObjectCollection $jednotka The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByJednotka($jednotka, $comparison = null)
    {
        if ($jednotka instanceof \SmartFridge\Jednotka) {
            return $this
                ->addUsingAlias(IngredienceTableMap::COL_JEDNOTKAID, $jednotka->getId(), $comparison);
        } elseif ($jednotka instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(IngredienceTableMap::COL_JEDNOTKAID, $jednotka->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByJednotka() only accepts arguments of type \SmartFridge\Jednotka or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Jednotka relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function joinJednotka($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Jednotka');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Jednotka');
        }

        return $this;
    }

    /**
     * Use the Jednotka relation Jednotka object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\JednotkaQuery A secondary query class using the current class as primary query
     */
    public function useJednotkaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJednotka($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Jednotka', '\SmartFridge\JednotkaQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Kategorie object
     *
     * @param \SmartFridge\Kategorie|ObjectCollection $kategorie The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByKategorie($kategorie, $comparison = null)
    {
        if ($kategorie instanceof \SmartFridge\Kategorie) {
            return $this
                ->addUsingAlias(IngredienceTableMap::COL_KATEGORIEID, $kategorie->getId(), $comparison);
        } elseif ($kategorie instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(IngredienceTableMap::COL_KATEGORIEID, $kategorie->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByKategorie() only accepts arguments of type \SmartFridge\Kategorie or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Kategorie relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function joinKategorie($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Kategorie');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Kategorie');
        }

        return $this;
    }

    /**
     * Use the Kategorie relation Kategorie object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\KategorieQuery A secondary query class using the current class as primary query
     */
    public function useKategorieQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinKategorie($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Kategorie', '\SmartFridge\KategorieQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Produkt object
     *
     * @param \SmartFridge\Produkt|ObjectCollection $produkt The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByProdukt($produkt, $comparison = null)
    {
        if ($produkt instanceof \SmartFridge\Produkt) {
            return $this
                ->addUsingAlias(IngredienceTableMap::COL_PRODUKTID, $produkt->getId(), $comparison);
        } elseif ($produkt instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(IngredienceTableMap::COL_PRODUKTID, $produkt->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProdukt() only accepts arguments of type \SmartFridge\Produkt or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Produkt relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function joinProdukt($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Produkt');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Produkt');
        }

        return $this;
    }

    /**
     * Use the Produkt relation Produkt object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\ProduktQuery A secondary query class using the current class as primary query
     */
    public function useProduktQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProdukt($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Produkt', '\SmartFridge\ProduktQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Recepty object
     *
     * @param \SmartFridge\Recepty|ObjectCollection $recepty The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildIngredienceQuery The current query, for fluid interface
     */
    public function filterByRecepty($recepty, $comparison = null)
    {
        if ($recepty instanceof \SmartFridge\Recepty) {
            return $this
                ->addUsingAlias(IngredienceTableMap::COL_RECEPTID, $recepty->getId(), $comparison);
        } elseif ($recepty instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(IngredienceTableMap::COL_RECEPTID, $recepty->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRecepty() only accepts arguments of type \SmartFridge\Recepty or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Recepty relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function joinRecepty($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Recepty');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Recepty');
        }

        return $this;
    }

    /**
     * Use the Recepty relation Recepty object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\ReceptyQuery A secondary query class using the current class as primary query
     */
    public function useReceptyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRecepty($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Recepty', '\SmartFridge\ReceptyQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildIngredience $ingredience Object to remove from the list of results
     *
     * @return $this|ChildIngredienceQuery The current query, for fluid interface
     */
    public function prune($ingredience = null)
    {
        if ($ingredience) {
            $this->addUsingAlias(IngredienceTableMap::COL_ID, $ingredience->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Ingredience table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(IngredienceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            IngredienceTableMap::clearInstancePool();
            IngredienceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(IngredienceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(IngredienceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            IngredienceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            IngredienceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // IngredienceQuery
