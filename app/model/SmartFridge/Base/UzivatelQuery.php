<?php

namespace SmartFridge\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SmartFridge\Uzivatel as ChildUzivatel;
use SmartFridge\UzivatelQuery as ChildUzivatelQuery;
use SmartFridge\Map\UzivatelTableMap;

/**
 * Base class that represents a query for the 'Uzivatel' table.
 *
 *
 *
 * @method     ChildUzivatelQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUzivatelQuery orderByPrezdivka($order = Criteria::ASC) Order by the prezdivka column
 * @method     ChildUzivatelQuery orderByHeslo($order = Criteria::ASC) Order by the heslo column
 * @method     ChildUzivatelQuery orderByJmeno($order = Criteria::ASC) Order by the jmeno column
 * @method     ChildUzivatelQuery orderByPrijmeni($order = Criteria::ASC) Order by the prijmeni column
 * @method     ChildUzivatelQuery orderByUlice($order = Criteria::ASC) Order by the ulice column
 * @method     ChildUzivatelQuery orderByMesto($order = Criteria::ASC) Order by the mesto column
 * @method     ChildUzivatelQuery orderByCislopopisne($order = Criteria::ASC) Order by the cisloPopisne column
 * @method     ChildUzivatelQuery orderByPostovnismerovacicislo($order = Criteria::ASC) Order by the PostovniSmerovaciCislo column
 * @method     ChildUzivatelQuery orderByRole($order = Criteria::ASC) Order by the role column
 *
 * @method     ChildUzivatelQuery groupById() Group by the id column
 * @method     ChildUzivatelQuery groupByPrezdivka() Group by the prezdivka column
 * @method     ChildUzivatelQuery groupByHeslo() Group by the heslo column
 * @method     ChildUzivatelQuery groupByJmeno() Group by the jmeno column
 * @method     ChildUzivatelQuery groupByPrijmeni() Group by the prijmeni column
 * @method     ChildUzivatelQuery groupByUlice() Group by the ulice column
 * @method     ChildUzivatelQuery groupByMesto() Group by the mesto column
 * @method     ChildUzivatelQuery groupByCislopopisne() Group by the cisloPopisne column
 * @method     ChildUzivatelQuery groupByPostovnismerovacicislo() Group by the PostovniSmerovaciCislo column
 * @method     ChildUzivatelQuery groupByRole() Group by the role column
 *
 * @method     ChildUzivatelQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUzivatelQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUzivatelQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUzivatelQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUzivatelQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUzivatelQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUzivatelQuery leftJoinObjednavka($relationAlias = null) Adds a LEFT JOIN clause to the query using the Objednavka relation
 * @method     ChildUzivatelQuery rightJoinObjednavka($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Objednavka relation
 * @method     ChildUzivatelQuery innerJoinObjednavka($relationAlias = null) Adds a INNER JOIN clause to the query using the Objednavka relation
 *
 * @method     ChildUzivatelQuery joinWithObjednavka($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Objednavka relation
 *
 * @method     ChildUzivatelQuery leftJoinWithObjednavka() Adds a LEFT JOIN clause and with to the query using the Objednavka relation
 * @method     ChildUzivatelQuery rightJoinWithObjednavka() Adds a RIGHT JOIN clause and with to the query using the Objednavka relation
 * @method     ChildUzivatelQuery innerJoinWithObjednavka() Adds a INNER JOIN clause and with to the query using the Objednavka relation
 *
 * @method     ChildUzivatelQuery leftJoinPotraviny($relationAlias = null) Adds a LEFT JOIN clause to the query using the Potraviny relation
 * @method     ChildUzivatelQuery rightJoinPotraviny($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Potraviny relation
 * @method     ChildUzivatelQuery innerJoinPotraviny($relationAlias = null) Adds a INNER JOIN clause to the query using the Potraviny relation
 *
 * @method     ChildUzivatelQuery joinWithPotraviny($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Potraviny relation
 *
 * @method     ChildUzivatelQuery leftJoinWithPotraviny() Adds a LEFT JOIN clause and with to the query using the Potraviny relation
 * @method     ChildUzivatelQuery rightJoinWithPotraviny() Adds a RIGHT JOIN clause and with to the query using the Potraviny relation
 * @method     ChildUzivatelQuery innerJoinWithPotraviny() Adds a INNER JOIN clause and with to the query using the Potraviny relation
 *
 * @method     ChildUzivatelQuery leftJoinRecenze($relationAlias = null) Adds a LEFT JOIN clause to the query using the Recenze relation
 * @method     ChildUzivatelQuery rightJoinRecenze($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Recenze relation
 * @method     ChildUzivatelQuery innerJoinRecenze($relationAlias = null) Adds a INNER JOIN clause to the query using the Recenze relation
 *
 * @method     ChildUzivatelQuery joinWithRecenze($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Recenze relation
 *
 * @method     ChildUzivatelQuery leftJoinWithRecenze() Adds a LEFT JOIN clause and with to the query using the Recenze relation
 * @method     ChildUzivatelQuery rightJoinWithRecenze() Adds a RIGHT JOIN clause and with to the query using the Recenze relation
 * @method     ChildUzivatelQuery innerJoinWithRecenze() Adds a INNER JOIN clause and with to the query using the Recenze relation
 *
 * @method     ChildUzivatelQuery leftJoinUpozorneni($relationAlias = null) Adds a LEFT JOIN clause to the query using the Upozorneni relation
 * @method     ChildUzivatelQuery rightJoinUpozorneni($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Upozorneni relation
 * @method     ChildUzivatelQuery innerJoinUpozorneni($relationAlias = null) Adds a INNER JOIN clause to the query using the Upozorneni relation
 *
 * @method     ChildUzivatelQuery joinWithUpozorneni($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Upozorneni relation
 *
 * @method     ChildUzivatelQuery leftJoinWithUpozorneni() Adds a LEFT JOIN clause and with to the query using the Upozorneni relation
 * @method     ChildUzivatelQuery rightJoinWithUpozorneni() Adds a RIGHT JOIN clause and with to the query using the Upozorneni relation
 * @method     ChildUzivatelQuery innerJoinWithUpozorneni() Adds a INNER JOIN clause and with to the query using the Upozorneni relation
 *
 * @method     ChildUzivatelQuery leftJoinWatchdog($relationAlias = null) Adds a LEFT JOIN clause to the query using the Watchdog relation
 * @method     ChildUzivatelQuery rightJoinWatchdog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Watchdog relation
 * @method     ChildUzivatelQuery innerJoinWatchdog($relationAlias = null) Adds a INNER JOIN clause to the query using the Watchdog relation
 *
 * @method     ChildUzivatelQuery joinWithWatchdog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Watchdog relation
 *
 * @method     ChildUzivatelQuery leftJoinWithWatchdog() Adds a LEFT JOIN clause and with to the query using the Watchdog relation
 * @method     ChildUzivatelQuery rightJoinWithWatchdog() Adds a RIGHT JOIN clause and with to the query using the Watchdog relation
 * @method     ChildUzivatelQuery innerJoinWithWatchdog() Adds a INNER JOIN clause and with to the query using the Watchdog relation
 *
 * @method     \SmartFridge\ObjednavkaQuery|\SmartFridge\PotravinyQuery|\SmartFridge\RecenzeQuery|\SmartFridge\UpozorneniQuery|\SmartFridge\WatchdogQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUzivatel findOne(ConnectionInterface $con = null) Return the first ChildUzivatel matching the query
 * @method     ChildUzivatel findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUzivatel matching the query, or a new ChildUzivatel object populated from the query conditions when no match is found
 *
 * @method     ChildUzivatel findOneById(int $id) Return the first ChildUzivatel filtered by the id column
 * @method     ChildUzivatel findOneByPrezdivka(string $prezdivka) Return the first ChildUzivatel filtered by the prezdivka column
 * @method     ChildUzivatel findOneByHeslo(string $heslo) Return the first ChildUzivatel filtered by the heslo column
 * @method     ChildUzivatel findOneByJmeno(string $jmeno) Return the first ChildUzivatel filtered by the jmeno column
 * @method     ChildUzivatel findOneByPrijmeni(string $prijmeni) Return the first ChildUzivatel filtered by the prijmeni column
 * @method     ChildUzivatel findOneByUlice(string $ulice) Return the first ChildUzivatel filtered by the ulice column
 * @method     ChildUzivatel findOneByMesto(string $mesto) Return the first ChildUzivatel filtered by the mesto column
 * @method     ChildUzivatel findOneByCislopopisne(string $cisloPopisne) Return the first ChildUzivatel filtered by the cisloPopisne column
 * @method     ChildUzivatel findOneByPostovnismerovacicislo(string $PostovniSmerovaciCislo) Return the first ChildUzivatel filtered by the PostovniSmerovaciCislo column
 * @method     ChildUzivatel findOneByRole(string $role) Return the first ChildUzivatel filtered by the role column *

 * @method     ChildUzivatel requirePk($key, ConnectionInterface $con = null) Return the ChildUzivatel by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUzivatel requireOne(ConnectionInterface $con = null) Return the first ChildUzivatel matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUzivatel requireOneById(int $id) Return the first ChildUzivatel filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUzivatel requireOneByPrezdivka(string $prezdivka) Return the first ChildUzivatel filtered by the prezdivka column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUzivatel requireOneByHeslo(string $heslo) Return the first ChildUzivatel filtered by the heslo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUzivatel requireOneByJmeno(string $jmeno) Return the first ChildUzivatel filtered by the jmeno column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUzivatel requireOneByPrijmeni(string $prijmeni) Return the first ChildUzivatel filtered by the prijmeni column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUzivatel requireOneByUlice(string $ulice) Return the first ChildUzivatel filtered by the ulice column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUzivatel requireOneByMesto(string $mesto) Return the first ChildUzivatel filtered by the mesto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUzivatel requireOneByCislopopisne(string $cisloPopisne) Return the first ChildUzivatel filtered by the cisloPopisne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUzivatel requireOneByPostovnismerovacicislo(string $PostovniSmerovaciCislo) Return the first ChildUzivatel filtered by the PostovniSmerovaciCislo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUzivatel requireOneByRole(string $role) Return the first ChildUzivatel filtered by the role column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUzivatel[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUzivatel objects based on current ModelCriteria
 * @method     ChildUzivatel[]|ObjectCollection findById(int $id) Return ChildUzivatel objects filtered by the id column
 * @method     ChildUzivatel[]|ObjectCollection findByPrezdivka(string $prezdivka) Return ChildUzivatel objects filtered by the prezdivka column
 * @method     ChildUzivatel[]|ObjectCollection findByHeslo(string $heslo) Return ChildUzivatel objects filtered by the heslo column
 * @method     ChildUzivatel[]|ObjectCollection findByJmeno(string $jmeno) Return ChildUzivatel objects filtered by the jmeno column
 * @method     ChildUzivatel[]|ObjectCollection findByPrijmeni(string $prijmeni) Return ChildUzivatel objects filtered by the prijmeni column
 * @method     ChildUzivatel[]|ObjectCollection findByUlice(string $ulice) Return ChildUzivatel objects filtered by the ulice column
 * @method     ChildUzivatel[]|ObjectCollection findByMesto(string $mesto) Return ChildUzivatel objects filtered by the mesto column
 * @method     ChildUzivatel[]|ObjectCollection findByCislopopisne(string $cisloPopisne) Return ChildUzivatel objects filtered by the cisloPopisne column
 * @method     ChildUzivatel[]|ObjectCollection findByPostovnismerovacicislo(string $PostovniSmerovaciCislo) Return ChildUzivatel objects filtered by the PostovniSmerovaciCislo column
 * @method     ChildUzivatel[]|ObjectCollection findByRole(string $role) Return ChildUzivatel objects filtered by the role column
 * @method     ChildUzivatel[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UzivatelQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SmartFridge\Base\UzivatelQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SmartFridge\\Uzivatel', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUzivatelQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUzivatelQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUzivatelQuery) {
            return $criteria;
        }
        $query = new ChildUzivatelQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUzivatel|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UzivatelTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UzivatelTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUzivatel A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, prezdivka, heslo, jmeno, prijmeni, ulice, mesto, cisloPopisne, PostovniSmerovaciCislo, role FROM Uzivatel WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUzivatel $obj */
            $obj = new ChildUzivatel();
            $obj->hydrate($row);
            UzivatelTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUzivatel|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UzivatelTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UzivatelTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UzivatelTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UzivatelTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UzivatelTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the prezdivka column
     *
     * Example usage:
     * <code>
     * $query->filterByPrezdivka('fooValue');   // WHERE prezdivka = 'fooValue'
     * $query->filterByPrezdivka('%fooValue%', Criteria::LIKE); // WHERE prezdivka LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prezdivka The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByPrezdivka($prezdivka = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prezdivka)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UzivatelTableMap::COL_PREZDIVKA, $prezdivka, $comparison);
    }

    /**
     * Filter the query on the heslo column
     *
     * Example usage:
     * <code>
     * $query->filterByHeslo('fooValue');   // WHERE heslo = 'fooValue'
     * $query->filterByHeslo('%fooValue%', Criteria::LIKE); // WHERE heslo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $heslo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByHeslo($heslo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($heslo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UzivatelTableMap::COL_HESLO, $heslo, $comparison);
    }

    /**
     * Filter the query on the jmeno column
     *
     * Example usage:
     * <code>
     * $query->filterByJmeno('fooValue');   // WHERE jmeno = 'fooValue'
     * $query->filterByJmeno('%fooValue%', Criteria::LIKE); // WHERE jmeno LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jmeno The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByJmeno($jmeno = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jmeno)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UzivatelTableMap::COL_JMENO, $jmeno, $comparison);
    }

    /**
     * Filter the query on the prijmeni column
     *
     * Example usage:
     * <code>
     * $query->filterByPrijmeni('fooValue');   // WHERE prijmeni = 'fooValue'
     * $query->filterByPrijmeni('%fooValue%', Criteria::LIKE); // WHERE prijmeni LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prijmeni The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByPrijmeni($prijmeni = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prijmeni)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UzivatelTableMap::COL_PRIJMENI, $prijmeni, $comparison);
    }

    /**
     * Filter the query on the ulice column
     *
     * Example usage:
     * <code>
     * $query->filterByUlice('fooValue');   // WHERE ulice = 'fooValue'
     * $query->filterByUlice('%fooValue%', Criteria::LIKE); // WHERE ulice LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ulice The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByUlice($ulice = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ulice)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UzivatelTableMap::COL_ULICE, $ulice, $comparison);
    }

    /**
     * Filter the query on the mesto column
     *
     * Example usage:
     * <code>
     * $query->filterByMesto('fooValue');   // WHERE mesto = 'fooValue'
     * $query->filterByMesto('%fooValue%', Criteria::LIKE); // WHERE mesto LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mesto The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByMesto($mesto = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mesto)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UzivatelTableMap::COL_MESTO, $mesto, $comparison);
    }

    /**
     * Filter the query on the cisloPopisne column
     *
     * Example usage:
     * <code>
     * $query->filterByCislopopisne('fooValue');   // WHERE cisloPopisne = 'fooValue'
     * $query->filterByCislopopisne('%fooValue%', Criteria::LIKE); // WHERE cisloPopisne LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cislopopisne The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByCislopopisne($cislopopisne = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cislopopisne)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UzivatelTableMap::COL_CISLOPOPISNE, $cislopopisne, $comparison);
    }

    /**
     * Filter the query on the PostovniSmerovaciCislo column
     *
     * Example usage:
     * <code>
     * $query->filterByPostovnismerovacicislo('fooValue');   // WHERE PostovniSmerovaciCislo = 'fooValue'
     * $query->filterByPostovnismerovacicislo('%fooValue%', Criteria::LIKE); // WHERE PostovniSmerovaciCislo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postovnismerovacicislo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByPostovnismerovacicislo($postovnismerovacicislo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postovnismerovacicislo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UzivatelTableMap::COL_POSTOVNISMEROVACICISLO, $postovnismerovacicislo, $comparison);
    }

    /**
     * Filter the query on the role column
     *
     * Example usage:
     * <code>
     * $query->filterByRole('fooValue');   // WHERE role = 'fooValue'
     * $query->filterByRole('%fooValue%', Criteria::LIKE); // WHERE role LIKE '%fooValue%'
     * </code>
     *
     * @param     string $role The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByRole($role = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($role)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UzivatelTableMap::COL_ROLE, $role, $comparison);
    }

    /**
     * Filter the query by a related \SmartFridge\Objednavka object
     *
     * @param \SmartFridge\Objednavka|ObjectCollection $objednavka the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByObjednavka($objednavka, $comparison = null)
    {
        if ($objednavka instanceof \SmartFridge\Objednavka) {
            return $this
                ->addUsingAlias(UzivatelTableMap::COL_ID, $objednavka->getUzivatelid(), $comparison);
        } elseif ($objednavka instanceof ObjectCollection) {
            return $this
                ->useObjednavkaQuery()
                ->filterByPrimaryKeys($objednavka->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByObjednavka() only accepts arguments of type \SmartFridge\Objednavka or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Objednavka relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function joinObjednavka($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Objednavka');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Objednavka');
        }

        return $this;
    }

    /**
     * Use the Objednavka relation Objednavka object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\ObjednavkaQuery A secondary query class using the current class as primary query
     */
    public function useObjednavkaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinObjednavka($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Objednavka', '\SmartFridge\ObjednavkaQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Potraviny object
     *
     * @param \SmartFridge\Potraviny|ObjectCollection $potraviny the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByPotraviny($potraviny, $comparison = null)
    {
        if ($potraviny instanceof \SmartFridge\Potraviny) {
            return $this
                ->addUsingAlias(UzivatelTableMap::COL_ID, $potraviny->getUzivatelid(), $comparison);
        } elseif ($potraviny instanceof ObjectCollection) {
            return $this
                ->usePotravinyQuery()
                ->filterByPrimaryKeys($potraviny->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPotraviny() only accepts arguments of type \SmartFridge\Potraviny or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Potraviny relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function joinPotraviny($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Potraviny');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Potraviny');
        }

        return $this;
    }

    /**
     * Use the Potraviny relation Potraviny object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\PotravinyQuery A secondary query class using the current class as primary query
     */
    public function usePotravinyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPotraviny($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Potraviny', '\SmartFridge\PotravinyQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Recenze object
     *
     * @param \SmartFridge\Recenze|ObjectCollection $recenze the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByRecenze($recenze, $comparison = null)
    {
        if ($recenze instanceof \SmartFridge\Recenze) {
            return $this
                ->addUsingAlias(UzivatelTableMap::COL_ID, $recenze->getUzivatelid(), $comparison);
        } elseif ($recenze instanceof ObjectCollection) {
            return $this
                ->useRecenzeQuery()
                ->filterByPrimaryKeys($recenze->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRecenze() only accepts arguments of type \SmartFridge\Recenze or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Recenze relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function joinRecenze($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Recenze');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Recenze');
        }

        return $this;
    }

    /**
     * Use the Recenze relation Recenze object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\RecenzeQuery A secondary query class using the current class as primary query
     */
    public function useRecenzeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRecenze($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Recenze', '\SmartFridge\RecenzeQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Upozorneni object
     *
     * @param \SmartFridge\Upozorneni|ObjectCollection $upozorneni the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByUpozorneni($upozorneni, $comparison = null)
    {
        if ($upozorneni instanceof \SmartFridge\Upozorneni) {
            return $this
                ->addUsingAlias(UzivatelTableMap::COL_ID, $upozorneni->getUzivatelid(), $comparison);
        } elseif ($upozorneni instanceof ObjectCollection) {
            return $this
                ->useUpozorneniQuery()
                ->filterByPrimaryKeys($upozorneni->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUpozorneni() only accepts arguments of type \SmartFridge\Upozorneni or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Upozorneni relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function joinUpozorneni($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Upozorneni');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Upozorneni');
        }

        return $this;
    }

    /**
     * Use the Upozorneni relation Upozorneni object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\UpozorneniQuery A secondary query class using the current class as primary query
     */
    public function useUpozorneniQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpozorneni($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Upozorneni', '\SmartFridge\UpozorneniQuery');
    }

    /**
     * Filter the query by a related \SmartFridge\Watchdog object
     *
     * @param \SmartFridge\Watchdog|ObjectCollection $watchdog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUzivatelQuery The current query, for fluid interface
     */
    public function filterByWatchdog($watchdog, $comparison = null)
    {
        if ($watchdog instanceof \SmartFridge\Watchdog) {
            return $this
                ->addUsingAlias(UzivatelTableMap::COL_ID, $watchdog->getUzivatelid(), $comparison);
        } elseif ($watchdog instanceof ObjectCollection) {
            return $this
                ->useWatchdogQuery()
                ->filterByPrimaryKeys($watchdog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWatchdog() only accepts arguments of type \SmartFridge\Watchdog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Watchdog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function joinWatchdog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Watchdog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Watchdog');
        }

        return $this;
    }

    /**
     * Use the Watchdog relation Watchdog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SmartFridge\WatchdogQuery A secondary query class using the current class as primary query
     */
    public function useWatchdogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinWatchdog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Watchdog', '\SmartFridge\WatchdogQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUzivatel $uzivatel Object to remove from the list of results
     *
     * @return $this|ChildUzivatelQuery The current query, for fluid interface
     */
    public function prune($uzivatel = null)
    {
        if ($uzivatel) {
            $this->addUsingAlias(UzivatelTableMap::COL_ID, $uzivatel->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Uzivatel table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UzivatelTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UzivatelTableMap::clearInstancePool();
            UzivatelTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UzivatelTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UzivatelTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UzivatelTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UzivatelTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UzivatelQuery
