<?php

namespace SmartFridge\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use SmartFridge\Polozka;
use SmartFridge\PolozkaQuery;


/**
 * This class defines the structure of the 'Polozka' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PolozkaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'SmartFridge.Map.PolozkaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'Polozka';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\SmartFridge\\Polozka';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'SmartFridge.Polozka';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the id field
     */
    const COL_ID = 'Polozka.id';

    /**
     * the column name for the cenaPolozky field
     */
    const COL_CENAPOLOZKY = 'Polozka.cenaPolozky';

    /**
     * the column name for the pocet field
     */
    const COL_POCET = 'Polozka.pocet';

    /**
     * the column name for the ZboziID field
     */
    const COL_ZBOZIID = 'Polozka.ZboziID';

    /**
     * the column name for the ObjednavkaID field
     */
    const COL_OBJEDNAVKAID = 'Polozka.ObjednavkaID';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Cenapolozky', 'Pocet', 'Zboziid', 'Objednavkaid', ),
        self::TYPE_CAMELNAME     => array('id', 'cenapolozky', 'pocet', 'zboziid', 'objednavkaid', ),
        self::TYPE_COLNAME       => array(PolozkaTableMap::COL_ID, PolozkaTableMap::COL_CENAPOLOZKY, PolozkaTableMap::COL_POCET, PolozkaTableMap::COL_ZBOZIID, PolozkaTableMap::COL_OBJEDNAVKAID, ),
        self::TYPE_FIELDNAME     => array('id', 'cenaPolozky', 'pocet', 'ZboziID', 'ObjednavkaID', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Cenapolozky' => 1, 'Pocet' => 2, 'Zboziid' => 3, 'Objednavkaid' => 4, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'cenapolozky' => 1, 'pocet' => 2, 'zboziid' => 3, 'objednavkaid' => 4, ),
        self::TYPE_COLNAME       => array(PolozkaTableMap::COL_ID => 0, PolozkaTableMap::COL_CENAPOLOZKY => 1, PolozkaTableMap::COL_POCET => 2, PolozkaTableMap::COL_ZBOZIID => 3, PolozkaTableMap::COL_OBJEDNAVKAID => 4, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'cenaPolozky' => 1, 'pocet' => 2, 'ZboziID' => 3, 'ObjednavkaID' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Polozka');
        $this->setPhpName('Polozka');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\SmartFridge\\Polozka');
        $this->setPackage('SmartFridge');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('cenaPolozky', 'Cenapolozky', 'DOUBLE', false, null, null);
        $this->addColumn('pocet', 'Pocet', 'INTEGER', false, 10, null);
        $this->addForeignKey('ZboziID', 'Zboziid', 'INTEGER', 'Zbozi', 'id', false, 10, null);
        $this->addForeignKey('ObjednavkaID', 'Objednavkaid', 'INTEGER', 'Objednavka', 'id', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Objednavka', '\\SmartFridge\\Objednavka', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ObjednavkaID',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Zbozi', '\\SmartFridge\\Zbozi', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ZboziID',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PolozkaTableMap::CLASS_DEFAULT : PolozkaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Polozka object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PolozkaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PolozkaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PolozkaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PolozkaTableMap::OM_CLASS;
            /** @var Polozka $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PolozkaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PolozkaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PolozkaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Polozka $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PolozkaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PolozkaTableMap::COL_ID);
            $criteria->addSelectColumn(PolozkaTableMap::COL_CENAPOLOZKY);
            $criteria->addSelectColumn(PolozkaTableMap::COL_POCET);
            $criteria->addSelectColumn(PolozkaTableMap::COL_ZBOZIID);
            $criteria->addSelectColumn(PolozkaTableMap::COL_OBJEDNAVKAID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.cenaPolozky');
            $criteria->addSelectColumn($alias . '.pocet');
            $criteria->addSelectColumn($alias . '.ZboziID');
            $criteria->addSelectColumn($alias . '.ObjednavkaID');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PolozkaTableMap::DATABASE_NAME)->getTable(PolozkaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PolozkaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PolozkaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PolozkaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Polozka or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Polozka object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PolozkaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \SmartFridge\Polozka) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PolozkaTableMap::DATABASE_NAME);
            $criteria->add(PolozkaTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PolozkaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PolozkaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PolozkaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the Polozka table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PolozkaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Polozka or Criteria object.
     *
     * @param mixed               $criteria Criteria or Polozka object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PolozkaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Polozka object
        }

        if ($criteria->containsKey(PolozkaTableMap::COL_ID) && $criteria->keyContainsValue(PolozkaTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PolozkaTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PolozkaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PolozkaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PolozkaTableMap::buildTableMap();
