<?php

namespace SmartFridge\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use SmartFridge\Ingredience;
use SmartFridge\IngredienceQuery;


/**
 * This class defines the structure of the 'Ingredience' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class IngredienceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'SmartFridge.Map.IngredienceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'Ingredience';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\SmartFridge\\Ingredience';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'SmartFridge.Ingredience';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the id field
     */
    const COL_ID = 'Ingredience.id';

    /**
     * the column name for the mnozstvi field
     */
    const COL_MNOZSTVI = 'Ingredience.mnozstvi';

    /**
     * the column name for the receptID field
     */
    const COL_RECEPTID = 'Ingredience.receptID';

    /**
     * the column name for the jednotkaID field
     */
    const COL_JEDNOTKAID = 'Ingredience.jednotkaID';

    /**
     * the column name for the kategorieID field
     */
    const COL_KATEGORIEID = 'Ingredience.kategorieID';

    /**
     * the column name for the produktID field
     */
    const COL_PRODUKTID = 'Ingredience.produktID';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Mnozstvi', 'Receptid', 'Jednotkaid', 'Kategorieid', 'Produktid', ),
        self::TYPE_CAMELNAME     => array('id', 'mnozstvi', 'receptid', 'jednotkaid', 'kategorieid', 'produktid', ),
        self::TYPE_COLNAME       => array(IngredienceTableMap::COL_ID, IngredienceTableMap::COL_MNOZSTVI, IngredienceTableMap::COL_RECEPTID, IngredienceTableMap::COL_JEDNOTKAID, IngredienceTableMap::COL_KATEGORIEID, IngredienceTableMap::COL_PRODUKTID, ),
        self::TYPE_FIELDNAME     => array('id', 'mnozstvi', 'receptID', 'jednotkaID', 'kategorieID', 'produktID', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Mnozstvi' => 1, 'Receptid' => 2, 'Jednotkaid' => 3, 'Kategorieid' => 4, 'Produktid' => 5, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'mnozstvi' => 1, 'receptid' => 2, 'jednotkaid' => 3, 'kategorieid' => 4, 'produktid' => 5, ),
        self::TYPE_COLNAME       => array(IngredienceTableMap::COL_ID => 0, IngredienceTableMap::COL_MNOZSTVI => 1, IngredienceTableMap::COL_RECEPTID => 2, IngredienceTableMap::COL_JEDNOTKAID => 3, IngredienceTableMap::COL_KATEGORIEID => 4, IngredienceTableMap::COL_PRODUKTID => 5, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'mnozstvi' => 1, 'receptID' => 2, 'jednotkaID' => 3, 'kategorieID' => 4, 'produktID' => 5, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Ingredience');
        $this->setPhpName('Ingredience');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\SmartFridge\\Ingredience');
        $this->setPackage('SmartFridge');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('mnozstvi', 'Mnozstvi', 'INTEGER', false, 10, null);
        $this->addForeignKey('receptID', 'Receptid', 'INTEGER', 'Recepty', 'id', false, 10, null);
        $this->addForeignKey('jednotkaID', 'Jednotkaid', 'INTEGER', 'Jednotka', 'id', false, 10, null);
        $this->addForeignKey('kategorieID', 'Kategorieid', 'INTEGER', 'Kategorie', 'id', false, 10, null);
        $this->addForeignKey('produktID', 'Produktid', 'INTEGER', 'Produkt', 'id', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Jednotka', '\\SmartFridge\\Jednotka', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':jednotkaID',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Kategorie', '\\SmartFridge\\Kategorie', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':kategorieID',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Produkt', '\\SmartFridge\\Produkt', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':produktID',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Recepty', '\\SmartFridge\\Recepty', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':receptID',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? IngredienceTableMap::CLASS_DEFAULT : IngredienceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Ingredience object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = IngredienceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = IngredienceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + IngredienceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = IngredienceTableMap::OM_CLASS;
            /** @var Ingredience $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            IngredienceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = IngredienceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = IngredienceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Ingredience $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                IngredienceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(IngredienceTableMap::COL_ID);
            $criteria->addSelectColumn(IngredienceTableMap::COL_MNOZSTVI);
            $criteria->addSelectColumn(IngredienceTableMap::COL_RECEPTID);
            $criteria->addSelectColumn(IngredienceTableMap::COL_JEDNOTKAID);
            $criteria->addSelectColumn(IngredienceTableMap::COL_KATEGORIEID);
            $criteria->addSelectColumn(IngredienceTableMap::COL_PRODUKTID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.mnozstvi');
            $criteria->addSelectColumn($alias . '.receptID');
            $criteria->addSelectColumn($alias . '.jednotkaID');
            $criteria->addSelectColumn($alias . '.kategorieID');
            $criteria->addSelectColumn($alias . '.produktID');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(IngredienceTableMap::DATABASE_NAME)->getTable(IngredienceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(IngredienceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(IngredienceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new IngredienceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Ingredience or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Ingredience object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(IngredienceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \SmartFridge\Ingredience) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(IngredienceTableMap::DATABASE_NAME);
            $criteria->add(IngredienceTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = IngredienceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            IngredienceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                IngredienceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the Ingredience table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return IngredienceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Ingredience or Criteria object.
     *
     * @param mixed               $criteria Criteria or Ingredience object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(IngredienceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Ingredience object
        }

        if ($criteria->containsKey(IngredienceTableMap::COL_ID) && $criteria->keyContainsValue(IngredienceTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.IngredienceTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = IngredienceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // IngredienceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
IngredienceTableMap::buildTableMap();
