<?php

namespace SmartFridge\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use SmartFridge\Objednavka;
use SmartFridge\ObjednavkaQuery;


/**
 * This class defines the structure of the 'Objednavka' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ObjednavkaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'SmartFridge.Map.ObjednavkaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'Objednavka';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\SmartFridge\\Objednavka';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'SmartFridge.Objednavka';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'Objednavka.id';

    /**
     * the column name for the datumZalozeni field
     */
    const COL_DATUMZALOZENI = 'Objednavka.datumZalozeni';

    /**
     * the column name for the datumZaplaceni field
     */
    const COL_DATUMZAPLACENI = 'Objednavka.datumZaplaceni';

    /**
     * the column name for the datumDorucení field
     */
    const COL_DATUMDORUCENí = 'Objednavka.datumDorucení';

    /**
     * the column name for the zpusobPlatby field
     */
    const COL_ZPUSOBPLATBY = 'Objednavka.zpusobPlatby';

    /**
     * the column name for the celkovaCena field
     */
    const COL_CELKOVACENA = 'Objednavka.celkovaCena';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'Objednavka.status';

    /**
     * the column name for the soubor field
     */
    const COL_SOUBOR = 'Objednavka.soubor';

    /**
     * the column name for the uzivatelID field
     */
    const COL_UZIVATELID = 'Objednavka.uzivatelID';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Datumzalozeni', 'Datumzaplaceni', 'Datumdorucení', 'Zpusobplatby', 'Celkovacena', 'Status', 'Soubor', 'Uzivatelid', ),
        self::TYPE_CAMELNAME     => array('id', 'datumzalozeni', 'datumzaplaceni', 'datumdorucení', 'zpusobplatby', 'celkovacena', 'status', 'soubor', 'uzivatelid', ),
        self::TYPE_COLNAME       => array(ObjednavkaTableMap::COL_ID, ObjednavkaTableMap::COL_DATUMZALOZENI, ObjednavkaTableMap::COL_DATUMZAPLACENI, ObjednavkaTableMap::COL_DATUMDORUCENí, ObjednavkaTableMap::COL_ZPUSOBPLATBY, ObjednavkaTableMap::COL_CELKOVACENA, ObjednavkaTableMap::COL_STATUS, ObjednavkaTableMap::COL_SOUBOR, ObjednavkaTableMap::COL_UZIVATELID, ),
        self::TYPE_FIELDNAME     => array('id', 'datumZalozeni', 'datumZaplaceni', 'datumDorucení', 'zpusobPlatby', 'celkovaCena', 'status', 'soubor', 'uzivatelID', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Datumzalozeni' => 1, 'Datumzaplaceni' => 2, 'Datumdorucení' => 3, 'Zpusobplatby' => 4, 'Celkovacena' => 5, 'Status' => 6, 'Soubor' => 7, 'Uzivatelid' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'datumzalozeni' => 1, 'datumzaplaceni' => 2, 'datumdorucení' => 3, 'zpusobplatby' => 4, 'celkovacena' => 5, 'status' => 6, 'soubor' => 7, 'uzivatelid' => 8, ),
        self::TYPE_COLNAME       => array(ObjednavkaTableMap::COL_ID => 0, ObjednavkaTableMap::COL_DATUMZALOZENI => 1, ObjednavkaTableMap::COL_DATUMZAPLACENI => 2, ObjednavkaTableMap::COL_DATUMDORUCENí => 3, ObjednavkaTableMap::COL_ZPUSOBPLATBY => 4, ObjednavkaTableMap::COL_CELKOVACENA => 5, ObjednavkaTableMap::COL_STATUS => 6, ObjednavkaTableMap::COL_SOUBOR => 7, ObjednavkaTableMap::COL_UZIVATELID => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'datumZalozeni' => 1, 'datumZaplaceni' => 2, 'datumDorucení' => 3, 'zpusobPlatby' => 4, 'celkovaCena' => 5, 'status' => 6, 'soubor' => 7, 'uzivatelID' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Objednavka');
        $this->setPhpName('Objednavka');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\SmartFridge\\Objednavka');
        $this->setPackage('SmartFridge');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('datumZalozeni', 'Datumzalozeni', 'TIMESTAMP', false, null, null);
        $this->addColumn('datumZaplaceni', 'Datumzaplaceni', 'TIMESTAMP', false, null, null);
        $this->addColumn('datumDorucení', 'Datumdorucení', 'TIMESTAMP', false, null, null);
        $this->addColumn('zpusobPlatby', 'Zpusobplatby', 'CHAR', false, null, null);
        $this->addColumn('celkovaCena', 'Celkovacena', 'DOUBLE', false, null, null);
        $this->addColumn('status', 'Status', 'CHAR', false, null, null);
        $this->addColumn('soubor', 'Soubor', 'VARCHAR', false, 50, null);
        $this->addForeignKey('uzivatelID', 'Uzivatelid', 'INTEGER', 'Uzivatel', 'id', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Uzivatel', '\\SmartFridge\\Uzivatel', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':uzivatelID',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Polozka', '\\SmartFridge\\Polozka', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':ObjednavkaID',
    1 => ':id',
  ),
), null, null, 'Polozkas', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ObjednavkaTableMap::CLASS_DEFAULT : ObjednavkaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Objednavka object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ObjednavkaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ObjednavkaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ObjednavkaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ObjednavkaTableMap::OM_CLASS;
            /** @var Objednavka $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ObjednavkaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ObjednavkaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ObjednavkaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Objednavka $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ObjednavkaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ObjednavkaTableMap::COL_ID);
            $criteria->addSelectColumn(ObjednavkaTableMap::COL_DATUMZALOZENI);
            $criteria->addSelectColumn(ObjednavkaTableMap::COL_DATUMZAPLACENI);
            $criteria->addSelectColumn(ObjednavkaTableMap::COL_DATUMDORUCENí);
            $criteria->addSelectColumn(ObjednavkaTableMap::COL_ZPUSOBPLATBY);
            $criteria->addSelectColumn(ObjednavkaTableMap::COL_CELKOVACENA);
            $criteria->addSelectColumn(ObjednavkaTableMap::COL_STATUS);
            $criteria->addSelectColumn(ObjednavkaTableMap::COL_SOUBOR);
            $criteria->addSelectColumn(ObjednavkaTableMap::COL_UZIVATELID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.datumZalozeni');
            $criteria->addSelectColumn($alias . '.datumZaplaceni');
            $criteria->addSelectColumn($alias . '.datumDorucení');
            $criteria->addSelectColumn($alias . '.zpusobPlatby');
            $criteria->addSelectColumn($alias . '.celkovaCena');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.soubor');
            $criteria->addSelectColumn($alias . '.uzivatelID');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ObjednavkaTableMap::DATABASE_NAME)->getTable(ObjednavkaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ObjednavkaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ObjednavkaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ObjednavkaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Objednavka or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Objednavka object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ObjednavkaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \SmartFridge\Objednavka) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ObjednavkaTableMap::DATABASE_NAME);
            $criteria->add(ObjednavkaTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ObjednavkaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ObjednavkaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ObjednavkaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the Objednavka table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ObjednavkaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Objednavka or Criteria object.
     *
     * @param mixed               $criteria Criteria or Objednavka object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ObjednavkaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Objednavka object
        }

        if ($criteria->containsKey(ObjednavkaTableMap::COL_ID) && $criteria->keyContainsValue(ObjednavkaTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ObjednavkaTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ObjednavkaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ObjednavkaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ObjednavkaTableMap::buildTableMap();
