<?php

namespace SmartFridge\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use SmartFridge\Uzivatel;
use SmartFridge\UzivatelQuery;


/**
 * This class defines the structure of the 'Uzivatel' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UzivatelTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'SmartFridge.Map.UzivatelTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'Uzivatel';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\SmartFridge\\Uzivatel';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'SmartFridge.Uzivatel';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'Uzivatel.id';

    /**
     * the column name for the prezdivka field
     */
    const COL_PREZDIVKA = 'Uzivatel.prezdivka';

    /**
     * the column name for the heslo field
     */
    const COL_HESLO = 'Uzivatel.heslo';

    /**
     * the column name for the jmeno field
     */
    const COL_JMENO = 'Uzivatel.jmeno';

    /**
     * the column name for the prijmeni field
     */
    const COL_PRIJMENI = 'Uzivatel.prijmeni';

    /**
     * the column name for the ulice field
     */
    const COL_ULICE = 'Uzivatel.ulice';

    /**
     * the column name for the mesto field
     */
    const COL_MESTO = 'Uzivatel.mesto';

    /**
     * the column name for the cisloPopisne field
     */
    const COL_CISLOPOPISNE = 'Uzivatel.cisloPopisne';

    /**
     * the column name for the PostovniSmerovaciCislo field
     */
    const COL_POSTOVNISMEROVACICISLO = 'Uzivatel.PostovniSmerovaciCislo';

    /**
     * the column name for the role field
     */
    const COL_ROLE = 'Uzivatel.role';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Prezdivka', 'Heslo', 'Jmeno', 'Prijmeni', 'Ulice', 'Mesto', 'Cislopopisne', 'Postovnismerovacicislo', 'Role', ),
        self::TYPE_CAMELNAME     => array('id', 'prezdivka', 'heslo', 'jmeno', 'prijmeni', 'ulice', 'mesto', 'cislopopisne', 'postovnismerovacicislo', 'role', ),
        self::TYPE_COLNAME       => array(UzivatelTableMap::COL_ID, UzivatelTableMap::COL_PREZDIVKA, UzivatelTableMap::COL_HESLO, UzivatelTableMap::COL_JMENO, UzivatelTableMap::COL_PRIJMENI, UzivatelTableMap::COL_ULICE, UzivatelTableMap::COL_MESTO, UzivatelTableMap::COL_CISLOPOPISNE, UzivatelTableMap::COL_POSTOVNISMEROVACICISLO, UzivatelTableMap::COL_ROLE, ),
        self::TYPE_FIELDNAME     => array('id', 'prezdivka', 'heslo', 'jmeno', 'prijmeni', 'ulice', 'mesto', 'cisloPopisne', 'PostovniSmerovaciCislo', 'role', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Prezdivka' => 1, 'Heslo' => 2, 'Jmeno' => 3, 'Prijmeni' => 4, 'Ulice' => 5, 'Mesto' => 6, 'Cislopopisne' => 7, 'Postovnismerovacicislo' => 8, 'Role' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'prezdivka' => 1, 'heslo' => 2, 'jmeno' => 3, 'prijmeni' => 4, 'ulice' => 5, 'mesto' => 6, 'cislopopisne' => 7, 'postovnismerovacicislo' => 8, 'role' => 9, ),
        self::TYPE_COLNAME       => array(UzivatelTableMap::COL_ID => 0, UzivatelTableMap::COL_PREZDIVKA => 1, UzivatelTableMap::COL_HESLO => 2, UzivatelTableMap::COL_JMENO => 3, UzivatelTableMap::COL_PRIJMENI => 4, UzivatelTableMap::COL_ULICE => 5, UzivatelTableMap::COL_MESTO => 6, UzivatelTableMap::COL_CISLOPOPISNE => 7, UzivatelTableMap::COL_POSTOVNISMEROVACICISLO => 8, UzivatelTableMap::COL_ROLE => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'prezdivka' => 1, 'heslo' => 2, 'jmeno' => 3, 'prijmeni' => 4, 'ulice' => 5, 'mesto' => 6, 'cisloPopisne' => 7, 'PostovniSmerovaciCislo' => 8, 'role' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Uzivatel');
        $this->setPhpName('Uzivatel');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\SmartFridge\\Uzivatel');
        $this->setPackage('SmartFridge');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('prezdivka', 'Prezdivka', 'VARCHAR', true, 50, null);
        $this->addColumn('heslo', 'Heslo', 'VARCHAR', true, 4096, null);
        $this->addColumn('jmeno', 'Jmeno', 'VARCHAR', false, 50, null);
        $this->addColumn('prijmeni', 'Prijmeni', 'VARCHAR', false, 50, null);
        $this->addColumn('ulice', 'Ulice', 'VARCHAR', false, 50, null);
        $this->addColumn('mesto', 'Mesto', 'VARCHAR', false, 50, null);
        $this->addColumn('cisloPopisne', 'Cislopopisne', 'VARCHAR', false, 50, null);
        $this->addColumn('PostovniSmerovaciCislo', 'Postovnismerovacicislo', 'VARCHAR', false, 50, null);
        $this->addColumn('role', 'Role', 'CHAR', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Objednavka', '\\SmartFridge\\Objednavka', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':uzivatelID',
    1 => ':id',
  ),
), null, null, 'Objednavkas', false);
        $this->addRelation('Potraviny', '\\SmartFridge\\Potraviny', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':uzivatelID',
    1 => ':id',
  ),
), null, null, 'Potravinies', false);
        $this->addRelation('Recenze', '\\SmartFridge\\Recenze', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':uzivatelID',
    1 => ':id',
  ),
), null, null, 'Recenzes', false);
        $this->addRelation('Upozorneni', '\\SmartFridge\\Upozorneni', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':uzivatelID',
    1 => ':id',
  ),
), null, null, 'Upozornenis', false);
        $this->addRelation('Watchdog', '\\SmartFridge\\Watchdog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':uzivatelID',
    1 => ':id',
  ),
), null, null, 'Watchdogs', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UzivatelTableMap::CLASS_DEFAULT : UzivatelTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Uzivatel object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UzivatelTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UzivatelTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UzivatelTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UzivatelTableMap::OM_CLASS;
            /** @var Uzivatel $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UzivatelTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UzivatelTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UzivatelTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Uzivatel $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UzivatelTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UzivatelTableMap::COL_ID);
            $criteria->addSelectColumn(UzivatelTableMap::COL_PREZDIVKA);
            $criteria->addSelectColumn(UzivatelTableMap::COL_HESLO);
            $criteria->addSelectColumn(UzivatelTableMap::COL_JMENO);
            $criteria->addSelectColumn(UzivatelTableMap::COL_PRIJMENI);
            $criteria->addSelectColumn(UzivatelTableMap::COL_ULICE);
            $criteria->addSelectColumn(UzivatelTableMap::COL_MESTO);
            $criteria->addSelectColumn(UzivatelTableMap::COL_CISLOPOPISNE);
            $criteria->addSelectColumn(UzivatelTableMap::COL_POSTOVNISMEROVACICISLO);
            $criteria->addSelectColumn(UzivatelTableMap::COL_ROLE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.prezdivka');
            $criteria->addSelectColumn($alias . '.heslo');
            $criteria->addSelectColumn($alias . '.jmeno');
            $criteria->addSelectColumn($alias . '.prijmeni');
            $criteria->addSelectColumn($alias . '.ulice');
            $criteria->addSelectColumn($alias . '.mesto');
            $criteria->addSelectColumn($alias . '.cisloPopisne');
            $criteria->addSelectColumn($alias . '.PostovniSmerovaciCislo');
            $criteria->addSelectColumn($alias . '.role');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UzivatelTableMap::DATABASE_NAME)->getTable(UzivatelTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UzivatelTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UzivatelTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UzivatelTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Uzivatel or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Uzivatel object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UzivatelTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \SmartFridge\Uzivatel) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UzivatelTableMap::DATABASE_NAME);
            $criteria->add(UzivatelTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UzivatelQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UzivatelTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UzivatelTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the Uzivatel table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UzivatelQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Uzivatel or Criteria object.
     *
     * @param mixed               $criteria Criteria or Uzivatel object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UzivatelTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Uzivatel object
        }

        if ($criteria->containsKey(UzivatelTableMap::COL_ID) && $criteria->keyContainsValue(UzivatelTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UzivatelTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = UzivatelQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UzivatelTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UzivatelTableMap::buildTableMap();
