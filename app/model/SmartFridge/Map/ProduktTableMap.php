<?php

namespace SmartFridge\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use SmartFridge\Produkt;
use SmartFridge\ProduktQuery;


/**
 * This class defines the structure of the 'Produkt' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ProduktTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'SmartFridge.Map.ProduktTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'Produkt';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\SmartFridge\\Produkt';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'SmartFridge.Produkt';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the id field
     */
    const COL_ID = 'Produkt.id';

    /**
     * the column name for the nazev field
     */
    const COL_NAZEV = 'Produkt.nazev';

    /**
     * the column name for the fotka field
     */
    const COL_FOTKA = 'Produkt.fotka';

    /**
     * the column name for the popis field
     */
    const COL_POPIS = 'Produkt.popis';

    /**
     * the column name for the kategorieID field
     */
    const COL_KATEGORIEID = 'Produkt.kategorieID';

    /**
     * the column name for the jednotkaID field
     */
    const COL_JEDNOTKAID = 'Produkt.jednotkaID';

    /**
     * the column name for the mnozstvi field
     */
    const COL_MNOZSTVI = 'Produkt.mnozstvi';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Nazev', 'Fotka', 'Popis', 'Kategorieid', 'Jednotkaid', 'Mnozstvi', ),
        self::TYPE_CAMELNAME     => array('id', 'nazev', 'fotka', 'popis', 'kategorieid', 'jednotkaid', 'mnozstvi', ),
        self::TYPE_COLNAME       => array(ProduktTableMap::COL_ID, ProduktTableMap::COL_NAZEV, ProduktTableMap::COL_FOTKA, ProduktTableMap::COL_POPIS, ProduktTableMap::COL_KATEGORIEID, ProduktTableMap::COL_JEDNOTKAID, ProduktTableMap::COL_MNOZSTVI, ),
        self::TYPE_FIELDNAME     => array('id', 'nazev', 'fotka', 'popis', 'kategorieID', 'jednotkaID', 'mnozstvi', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Nazev' => 1, 'Fotka' => 2, 'Popis' => 3, 'Kategorieid' => 4, 'Jednotkaid' => 5, 'Mnozstvi' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'nazev' => 1, 'fotka' => 2, 'popis' => 3, 'kategorieid' => 4, 'jednotkaid' => 5, 'mnozstvi' => 6, ),
        self::TYPE_COLNAME       => array(ProduktTableMap::COL_ID => 0, ProduktTableMap::COL_NAZEV => 1, ProduktTableMap::COL_FOTKA => 2, ProduktTableMap::COL_POPIS => 3, ProduktTableMap::COL_KATEGORIEID => 4, ProduktTableMap::COL_JEDNOTKAID => 5, ProduktTableMap::COL_MNOZSTVI => 6, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'nazev' => 1, 'fotka' => 2, 'popis' => 3, 'kategorieID' => 4, 'jednotkaID' => 5, 'mnozstvi' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Produkt');
        $this->setPhpName('Produkt');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\SmartFridge\\Produkt');
        $this->setPackage('SmartFridge');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('nazev', 'Nazev', 'VARCHAR', false, 50, null);
        $this->addColumn('fotka', 'Fotka', 'VARCHAR', false, 50, null);
        $this->addColumn('popis', 'Popis', 'CLOB', false, null, null);
        $this->addForeignKey('kategorieID', 'Kategorieid', 'INTEGER', 'Kategorie', 'id', false, 10, null);
        $this->addForeignKey('jednotkaID', 'Jednotkaid', 'INTEGER', 'Jednotka', 'id', false, 10, null);
        $this->addColumn('mnozstvi', 'Mnozstvi', 'INTEGER', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Jednotka', '\\SmartFridge\\Jednotka', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':jednotkaID',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Kategorie', '\\SmartFridge\\Kategorie', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':kategorieID',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Ingredience', '\\SmartFridge\\Ingredience', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':produktID',
    1 => ':id',
  ),
), null, null, 'Ingrediences', false);
        $this->addRelation('Potraviny', '\\SmartFridge\\Potraviny', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':produktID',
    1 => ':id',
  ),
), null, null, 'Potravinies', false);
        $this->addRelation('Upozorneni', '\\SmartFridge\\Upozorneni', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':produktID',
    1 => ':id',
  ),
), null, null, 'Upozornenis', false);
        $this->addRelation('Watchdog', '\\SmartFridge\\Watchdog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':produktID',
    1 => ':id',
  ),
), null, null, 'Watchdogs', false);
        $this->addRelation('Zbozi', '\\SmartFridge\\Zbozi', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':produktID',
    1 => ':id',
  ),
), null, null, 'Zbozis', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProduktTableMap::CLASS_DEFAULT : ProduktTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Produkt object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProduktTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProduktTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProduktTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProduktTableMap::OM_CLASS;
            /** @var Produkt $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProduktTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProduktTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProduktTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Produkt $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProduktTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProduktTableMap::COL_ID);
            $criteria->addSelectColumn(ProduktTableMap::COL_NAZEV);
            $criteria->addSelectColumn(ProduktTableMap::COL_FOTKA);
            $criteria->addSelectColumn(ProduktTableMap::COL_POPIS);
            $criteria->addSelectColumn(ProduktTableMap::COL_KATEGORIEID);
            $criteria->addSelectColumn(ProduktTableMap::COL_JEDNOTKAID);
            $criteria->addSelectColumn(ProduktTableMap::COL_MNOZSTVI);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.nazev');
            $criteria->addSelectColumn($alias . '.fotka');
            $criteria->addSelectColumn($alias . '.popis');
            $criteria->addSelectColumn($alias . '.kategorieID');
            $criteria->addSelectColumn($alias . '.jednotkaID');
            $criteria->addSelectColumn($alias . '.mnozstvi');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProduktTableMap::DATABASE_NAME)->getTable(ProduktTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProduktTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProduktTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProduktTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Produkt or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Produkt object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduktTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \SmartFridge\Produkt) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProduktTableMap::DATABASE_NAME);
            $criteria->add(ProduktTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ProduktQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProduktTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProduktTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the Produkt table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProduktQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Produkt or Criteria object.
     *
     * @param mixed               $criteria Criteria or Produkt object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduktTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Produkt object
        }

        if ($criteria->containsKey(ProduktTableMap::COL_ID) && $criteria->keyContainsValue(ProduktTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProduktTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ProduktQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProduktTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProduktTableMap::buildTableMap();
