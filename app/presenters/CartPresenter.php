<?php

namespace App\Presenters;


use SmartFridge\ObjednavkaQuery;
use SmartFridge\PolozkaQuery;

class CartPresenter extends BasePresenter
{

    /** CartPresenter - renderDefault
     * Zobrazí hlavní stránku s košíkem pro přihlášeného uživatele
     */
    public function renderDefault() {
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $this->template->shop = PolozkaQuery::create()->leftJoinWithObjednavka()->where('Objednavka.uzivatelID = ' . $this->getUser()->getId())->find();
        $this->template->totalPrice = 0;

        foreach ($this->template->shop as $s) {
            $this->template->totalPrice += ($s->getZbozi()->getCena() * $s->getPocet());
        }
    }

    /** CartPresenter - renderDelete
     * Zobrazí dotaz, zda chce uživatel zboží opravdu chce smazat.
     * @param $id - id produkty v košíku
     */
    public function renderDelete($id) {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $this->template->shop = PolozkaQuery::create()->findOneById($id);
    }


    /** CartPresenter - handleDeleteFromFridge
     * Smaže zboží z košíku
     * @param $id - id zboží v košíku
     */
    public function handleDeleteFromCart($id) {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $shop = PolozkaQuery::create()->findOneById($id);
        $shop->delete();

        $this->flashMessage('Toto zboží bylo úspěšně odebráno.', 'success');
        $this->redirect('Cart:default');
    }


    /** CartPresenter - handleDeleteAll
     * Smaže všechny zboží
     */
    public function handleDeleteAll() {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $order = ObjednavkaQuery::create()->where('status = "vytvorena"')->findOneByUzivatelid($this->getUser()->getId());
        $shop = PolozkaQuery::create()->findByObjednavkaid($order->getId());

        foreach ($shop as $s) {
            $s->delete();
        }

        $this->flashMessage('Všechno zboží bylo odebráno.', 'success');
        $this->redirect('Cart:default');
    }
 }