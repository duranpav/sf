<?php

namespace App\Presenters;

use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\Utils\DateTime;
use SmartFridge\Base\ObjednavkaQuery;
use SmartFridge\Base\Watchdog;
use SmartFridge\Base\WatchdogQuery;
use SmartFridge\KategorieQuery;
use SmartFridge\Objednavka;
use SmartFridge\Polozka;
use SmartFridge\PolozkaQuery;
use SmartFridge\Potraviny;
use SmartFridge\Produkt;
use SmartFridge\Upozorneni;
use SmartFridge\Zbozi;
use SmartFridge\ZboziQuery;
use SmartFridge\PotravinyQuery;
use SmartFridge\ProduktQuery;
use SmartFridge\JednotkaQuery;

class ProductsPresenter extends Presenter
{

    /** ProductsPresenter - beforeRender
     * Tato metoda před načtením stránky render naplní formulářové prvky daty.
     */
    protected function beforeRender() {
        parent::beforeRender();

        if ($this->getAction() == 'edit') {
            $id = $this->getParameter('id');
            $product = ProduktQuery::create()->findOneById($id);
            $form = $this->getComponent('editForm');
            $form['kategorieID']->setDefaultValue($product->getKategorieid());
            $form['jednotkaID']->setDefaultValue($product->getJednotkaid());
            $form['popis']->setDefaultValue($product->getPopis());
            $form['nazev']->setDefaultValue($product->getNazev());
            $form['mnozstvi']->setDefaultValue($product->getMnozstvi());
            $form['id']->setDefaultValue($product->getId());
        }
        if ($this->getAction() == 'editProperty') {
            $id = $this->getParameter('zid');
            $zbozi = ZboziQuery::create()->findOneById($id);
            $form = $this->getComponent('editPropertyForm');
            $form['cena']->setDefaultValue($zbozi->getCena());
            $form['pocet']->setDefaultValue($zbozi->getPocet());
            $form['trvanlivost']->setDefaultValue($zbozi->getTrvanlivost()->format('Y-m-d'));
            $form['id']->setDefaultValue($zbozi->getId());
        }

    }

    /** ProductsPresenter - renderList
     * Tato metoda bude vypisovat seznam všech produktů v systému.
     * @param $id - id categorie
     */
    public function renderList($id) {
        $this->template->products = ProduktQuery::create()->getAllProductsFromCID($id);
        $this->template->categories = KategorieQuery::create()->findByKategorieid($id);
        $this->template->category = KategorieQuery::create()->findOneById($id);
        $this->template->cid = $id;
        $this->template->countProducts = count($this->template->products);
    }

    /** ProductsPresenter - renderAdd
     * Tato metoda zobrazí formulář pro vložení nového produktu.
     */
    public function renderAdd() {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
    }

    /** ProductsPresenter - renderEdit
     * Tato metoda zobrazí předvyplnění formulář pro editaci existujícího produktu.
     * @param $id - id produktu
     */
    public function renderEdit($id) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $this->template->product = ProduktQuery::create()->findOneById($id);
    }

    /** ProductsPresenter - createComponentEditForm
     * Tato metoda vytvoří formulář pro přidávání a editaci
     */
    public function createComponentEditForm() {
        $form = new Form();

        $dbkat = KategorieQuery::create()->find();
        $dbjed = JednotkaQuery::create()->find();

        $kategorie = array();
        foreach ($dbkat as $kid){
            $kategorie[$kid->getId()] = $kid->getNazev();
        }

        $jednotky = array();
        foreach ($dbjed as $jid){
            $jednotky[$jid->getId()] = $jid->getNazev();
        }

        $form->addText('nazev','Název produktu')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Zadejte název produktu');
        $form->addUpload('fotka','Nahrát foto')
            ->setAttribute('class', 'form-control')
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, 'Soubor musí být ve formátu JPEG, PNG nebo GIF.');
        $form->addTextArea('popis','Popis produktu')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Napište popisek produktu');
        $form->addText('mnozstvi','Množství')
            ->setType('number')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Napište množství');
        $form->addSelect('kategorieID','Kategorie',$kategorie)
            ->setAttribute('class', 'form-control')
            ->setPrompt('-- Vyberte kategorii --');
        $form->addSelect('jednotkaID','Jednotka',$jednotky)
            ->setAttribute('class', 'form-control')
            ->setPrompt('-- Vyberte jednotku --');
        $form->addHidden('id');

        $form->addSubmit('submit','Uložit')
            ->setAttribute('class', 'btn btn-block btn-success product-light category-a');

        $form->onSuccess[] = [$this, 'editFormSucceeded'];
        return $form;
    }

    /** ProductsPresenter - editFormSucceeded
     * Tato metoda vloží editované produkty a přesměruje na stránku produktů
     * @param Form $form
     */
    public function editFormSucceeded(Form $form) {
        $values = $form->getValues();

        $toIns = $values->id != NULL ? ProduktQuery::create()->findOneById($values->id) : new Produkt();

        $toIns
            ->setNazev($values['nazev'])
            ->setPopis($values['popis'])
            ->setKategorieId($values['kategorieID'])
            ->setJednotkaId($values['jednotkaID'])
            ->setMnozstvi($values['mnozstvi']);
        $toIns->save();


        $arr = explode( '.', $values->fotka->name );

        if($values->fotka->name) {
            $doc = $values->fotka;
            $doc->move(__DIR__ . "/../../www/img/" . 'product' . $toIns->getId() . '.' . $arr[(count($arr)-1)]);
        }

        $toIns->setFotka('review'.$toIns->getId().'.'.$arr[(count($arr)-1)]);
        $toIns->save();

        $this->flashMessage('Produkt byl úšpěšně vytvořen.', 'success');
        $this->redirect('Products:view', $toIns->getId());

    }

    /** ProductsPresenter - handleDelete
     * Odebere produkt ze systému.
     * @param $id - id produktu
     */
    public function handleDelete($id) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $zbozi = ZboziQuery::create()->findOneByProduktid($id);
        if($zbozi != NULL) {
            $this->flashMessage('Produkt nemůže být smazán.', 'danger');
            $this->redirect('Products:list');
        }

        $produkt = ProduktQuery::create()->findOneById($id);
        $produkt->delete();

        // přesměrování
        $this->flashMessage('Produkt byl úspěšně smazán.', 'success');
        $this->redirect('Products:list');
    }

    /** ProductsPresenter - renderDelete
     * Zobrazí dotaz, zda chce uživatel produkt opravdu smazat.
     * @param $id - id produktu
     */
    public function renderDelete($id) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $zbozi = ZboziQuery::create()->findOneByProduktid($id);
        if($zbozi != NULL) {
            $this->flashMessage('Produkt nemůže být smazán.', 'danger');
            $this->redirect('Products:list');
        }

        $this->template->produkt = ProduktQuery::create()->findOneById($id);
    }
    /** ProductsPresenter - renderView
     * Tato metoda bude zobrazovat detail produktu $id.
     * @param $id - id produktu
     */
    public function renderView($id) {
        $this->template->product = ProduktQuery::create()->findOneById($id);
        $this->template->property = ZboziQuery::create()->findByProduktid($id);
    }

    /** ProductsPresenter - handleAddToFridge
     * Tato metoda přidá produkt do lednice
     * @param $id - id kategorie odkud jsme přišli - pro hodnotu return this
     * @param $pid - id produktu
     * @param $zid - id zboží
     * @param $count - množství
     * @redirect - Products:list
     */
    public function handleAddToFridge($id, $pid, $zid = NULL, $count = 1) {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $property = ZboziQuery::create()->orderByCena()->findOneByProduktid($pid);

        if ($zid)
            $property = ZboziQuery::create()->findOneById($zid);

        if (!$property) {
            $this->flashMessage('Produkt nelze přidat do lednice.', 'danger');
            $this->redirect('Products:list');
        }

        $existujiciPotravina = PotravinyQuery::create()
            ->where('produktID = ' . $pid . ' AND spotrebovatDo = "' . $property->getTrvanlivost()->format('Y-m-d') . '"')
            ->findOneByUzivatelid($this->getUser()->getId());

        if (!$existujiciPotravina) {
            $potravina = new Potraviny();
            $potravina->setPocet($count)
                ->setSpotrebovatdo($property->getTrvanlivost())
                ->setProduktid($pid)
                ->setUzivatelid($this->getUser()->getId());
            $potravina->save();
        } else {
            $count = $existujiciPotravina->getPocet();
            $existujiciPotravina->setPocet(++$count)
                ->save();
        }

        $this->flashMessage('Produkt byl úspěšně přidán do lednice.', 'success');
        $this->redirect('this');
    }

    /** ProductsPresenter - handleAddToCart
     * Tato metoda přidá produkt do košíku
     * @param $id - id kategorie odkud jsme přišli - pro hodnotu return this
     * @param $pid - id produktu
     * @param $zid - id zboží
     * @param $count - množství
     * @redirect - this
     */
    public function handleAddToCart($id, $pid, $zid = NULL, $count = 1) {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $property = ZboziQuery::create()->orderByCena()->findOneByProduktid($pid);

        if ($zid)
            $property = ZboziQuery::create()->findOneById($zid);

        if (! $property) {
            $this->flashMessage('Produkt nelze přidat do košíku.', 'danger');
            $this->redirect('Products:list');
        }

        $kosik = ObjednavkaQuery::create()->where('uzivatelID = ' . $this->getUser()->getId() . ' AND status = "vytvorena"')->findOne();

        if ($kosik == NULL) {
            $kosik = new Objednavka();
            $kosik->setStatus('vytvorena')
                ->setUzivatelid($this->getUser()->getId());
            $kosik->save();
        }

        $existujiciPolozka = PolozkaQuery::create()->where('zboziID = ' . $property->getId())->findOneByObjednavkaid($kosik->getId());

        if (!$existujiciPolozka) {
            $polozka = new Polozka();
            $polozka->setPocet($count)
                ->setCenapolozky($property->getCena())
                ->setZboziid($property->getId())
                ->setObjednavkaid($kosik->getId());
            $polozka->save();
        } else {
            $count = $existujiciPolozka->getPocet();
            $existujiciPolozka->setPocet(++$count)
                ->save();
        }


        $this->flashMessage('Produkt byl úspěšně přidán do košíku.', 'success');
        $this->redirect('this');
    }

    /** ProductsPresenter - handleAddToCart
     * Tato metoda přidá produkt do košíku
     * @param $id - id produktu
     * @param $count - množství
     * @redirect - this
     */
    public function handleAddWatchdog($id, $pid) {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $zbozi = ZboziQuery::create()->orderByCena()->findOneByProduktid($pid);

        $existujiciWatchdog = WatchdogQuery::create()->where('produktID = ' . $pid)->findOneByUzivatelid($this->getUser()->getId());

        if (!$existujiciWatchdog) {
            $watchdog = new \SmartFridge\Watchdog();

            $watchdog
                ->setProduktid($pid)
                ->setUzivatelid($this->getUser()->getId());
                //->setCena($zbozi->getCena());
            $watchdog->save();
        } else {
            $this->flashMessage('Produkt už sledujete.', 'warning');
            $this->redirect('this');
        }


        $this->flashMessage('Produkt byl úspěšně přidán do sledování.', 'success');
        $this->redirect('this');
    }


    /** ProductsPresenter - renderAddProperty
     * Tato metoda zobrazí formulář pro vložení nového zboží.
     * @param $pid - id produktu
     */
    public function renderAddProperty($pid) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $this->template->product = ProduktQuery::create()->findOneById($pid);
    }

    /** ProductsPresenter - renderEditProperty
     * Tato metoda zobrazí předvyplnění formulář pro editaci existujícího zboží.
     * @param $pid - id produktu
     * @param $zid - id zboží
     */
    public function renderEditProperty($pid, $zid) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $this->template->product = ProduktQuery::create()->findOneById($pid);
        $this->template->property = ZboziQuery::create()->findOneById($zid);
    }

    /** ProductsPresenter - createComponentEditPropertyForm
     * Tato metoda vytvoří formulář pro přidávání a editaci zboží
     */
    public function createComponentEditPropertyForm() {
        $form = new Form();

        $form->addText('cena','Cena')
            ->setType('number')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Napište cenu v Kč');
        $form->addText('pocet','Množství')
            ->setType('number')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Napište počet zboží na skladě');
        $form->addText('trvanlivost','Trvanlivost')
            ->setType('date')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Napište trvanlivost zboží');

        $form->addHidden('produktID');
        $form->addHidden('id');

        $form->addSubmit('submit','Uložit')
            ->setAttribute('class', 'btn btn-block btn-success product-light category-a');

        $form->onSuccess[] = [$this, 'editPropertyFormSucceeded'];
        return $form;
    }

    /** ProductsPresenter - editPropertyFormSucceeded
     * Tato metoda vloží editované zboží a přesměruje na stránku produktu
     * @param Form $form
     */
    public function editPropertyFormSucceeded(Form $form) {
        $values = $form->getValues();

        $toIns = $values->id != NULL ? ZboziQuery::create()->findOneById($values->id) : new Zbozi();

        $toIns
            ->setCena($values['cena'])
            ->setPocet($values['pocet'])
            ->setTrvanlivost(new DateTime($values['trvanlivost']))
            ->setProduktid($values['produktID']);
        $toIns->save();

        // kontrola watchdogu + přidání upozornění
        $watchdog = \SmartFridge\WatchdogQuery::create()->findByProduktid($toIns->getProduktid());

        foreach ($watchdog as $w) {
            if ($w->getProduktid() == $values['produktID']) {
                $upozorneni = new Upozorneni();

                $upozorneni->setTyp('zmena_ceny')
                    ->setAktivni(1)
                    ->setUzivatelid($w->getUzivatelid())
                    ->setProduktid($values['produktID'])
                    ->setCena($values['cena'])
                    ->setDatum(new DateTime());
                $upozorneni->save();
            }
        }

        $this->flashMessage('Zboží bylo úšpěšně přidáno k produktu ' . $toIns->getProdukt()->getNazev() . '.', 'success');
        $this->redirect('Products:view', $toIns->getProduktid());

    }

    /** ProductsPresenter - handleDeleteProperty
     * Odebere zboží ze systému.
     * @param $pid - id produktu
     * @param $zid - id zboží
     */
    public function handleDeleteProperty($pid, $zid) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $zbozi = ZboziQuery::create()->findOneById($zid);
        $zbozi->delete();

        // přesměrování
        $this->flashMessage('Zboží bylo úspěšně smazáno.', 'success');
        $this->redirect('Products:view', $pid);
    }

    /** ProductsPresenter - renderDeleteProperty
     * Zobrazí dotaz, zda chce uživatel zboží opravdu smazat.
     * @param $pid - id produktu
     * @param $zid - id zboží
     */
    public function renderDeleteProperty($pid, $zid) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $this->template->zbozi = ZboziQuery::create()->findOneById($zid);
        $this->template->produkt = ProduktQuery::create()->findOneById($pid);
    }

}
