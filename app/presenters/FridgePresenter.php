<?php

namespace App\Presenters;

use SmartFridge\Base\PotravinyQuery;

class FridgePresenter extends BasePresenter {

    /** FridgePresenter - renderList
     * Tato metoda zobrazí seznam všech potravin v lednici
     */
    public function renderList() {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $this->template->foods = PotravinyQuery::create()->where('uzivatelID = ' . $this->getUser()->getId())->find();
        $this->template->countFood = count($this->template->foods);
    }

    /** FridgePresenter - renderDelete
     *  Zobrazí dotaz, zda chce uživatel potravinu opravdu smazat.
     *  @param $id - id potraviny v lednici
     */
    public function renderDelete($id) {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $this->template->food = PotravinyQuery::create()->findOneById($id);
    }


    /** FridgePresenter - handleDeleteFromFridge
     *  Smaže produkt z lednice
     *  @param $id - id potraviny v lednici
     */
    public function handleDeleteFromFridge($id) {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $food = PotravinyQuery::create()->findOneById($id);
        $food->delete();

        $this->flashMessage('Tato potravina byla úspěšně smazána.', 'success');
        $this->redirect('Fridge:list');
    }

}
