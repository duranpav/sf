<?php

namespace App\Presenters;

use Nette;


abstract class BasePresenter extends Nette\Application\UI\Presenter {

    public $basePath = __DIR__;

    public function startup() {
        parent::startup();
    }

    /** BasePresenter - handleLogOut
     * Odhlásí uživatele a vrátí na hlavní stránku
     */
    public function handleLogOut() {
        $this->getUser()->logout();
        $this->flashMessage('Byli jste odhlášeni.');
        $this->redirect("Default:homepage");

    }
}
