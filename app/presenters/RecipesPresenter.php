<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;
use Nette\Application\UI\Form;
use Nette\Caching\Storages\FileStorage;
use SmartFridge\Base\ProduktQuery;
use SmartFridge\Ingredience;
use SmartFridge\IngredienceQuery;
use SmartFridge\JednotkaQuery;
use SmartFridge\KategorieQuery;
use SmartFridge\PotravinyQuery;
use SmartFridge\Recenze;
use SmartFridge\RecenzeQuery;
use SmartFridge\Recepty;
use SmartFridge\ReceptyQuery;
use Nette\Caching\Cache;

class RecipesPresenter extends Presenter
{
    /** RecipesPresenter - beforeRender
     * Tato metoda před načtením stránky render naplní formulářové prvky daty.
     */
    protected function beforeRender() {
        parent::beforeRender();

        if ($this->getAction() == 'edit') {
            $id = $this->getParameter('id');
            $recept = ReceptyQuery::create()->findOneById($id);
            $form = $this->getComponent('editForm');
            $form['nazev']->setDefaultValue($recept->getNazev());
            $form['postup']->setDefaultValue($recept->getPostup());
            $form['pocetPorci']->setDefaultValue($recept->getPocetporci());
            $form['pribliznyCas']->setDefaultValue($recept->getPribliznycas());
            $form['priorita']->setDefaultValue($recept->getPriorita());
            $form['slozitost']->setDefaultValue($recept->getSlozitost());
            $form['id']->setDefaultValue($recept->getId());
        }

        if ($this->getAction() == 'editIngredience') {
            $iid = $this->getParameter('iid');
            $ingredience = IngredienceQuery::create()->findOneById($iid);
            $form = $this->getComponent('editIngredienceForm');
            $form['mnozstvi']->setDefaultValue($ingredience->getMnozstvi());
            $form['jednotkaID']->setDefaultValue($ingredience->getJednotkaid());
            $form['produktID']->setDefaultValue($ingredience->getProduktid());
            $form['kategorieID']->setDefaultValue($ingredience->getKategorieid());
            $form['id']->setDefaultValue($ingredience->getId());
        }

    }

    /** RecipesPresenter - renderList
     * Vypíše seznam všech receptů v systému.
     */
    public function renderList()  {
        // ulozeny filtr
        $filter = $this->getSession('filter');

        // pro nahrání produktů z cache ( popřípadě dání produktů z lednice do cache)
        $products = $this->loadProductsFromCache();

        $this->template->recipes = ReceptyQuery::create()->getRecipes($this->getUser()->getId(), $filter);
        $this->template->countRecipes = count($this->template->recipes);
        $this->template->averageStar = 0;
        $this->template->countStars = 0;

        // počet chybějících ingrediencí
        $this->template->countMissingIngredience = ReceptyQuery::create()->getCountMissingIngredience($this->getUser()->getId(), $filter);

    }

    /** RecipesPresenter - renderAdd
     * Zobrazí formulář pro vložení nového receptu.
     */
    public function renderAdd() {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
    }

    /** RecipesPresenter - renderEdit
     * Tato metoda zobrazí formulář pro editaci existujícího receptu.
     * @param $id - id receptu
     */
    public function renderEdit($id) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }
        $this->template->recipe = ReceptyQuery::create()->findOneById($id);

    }

    /** RecipesPresenter - createComponentEditForm
     * Tato metoda vytvoří formulář pro přidávání a editaci
     */
    public function createComponentEditForm() {
        $moznosti = [
            'zacatecnik' => 'Začátečník',
            'pokrocily' => 'Pokročilý',
            'expert' => 'Expert'
        ];
        $form = new Form();

        $form->addText('nazev', 'Název:')
            ->setAttribute('class', 'form-control')
             ->setRequired('Zadejte, prosím, název receptu')
            ->setAttribute('placeholder', 'Napište název');
        $form->addText('pocetPorci', 'Počet porcí:')
            ->setAttribute('placeholder', 'Napište počet porcí')
            ->setAttribute('class', 'form-control')
             ->setType('number')
             ->setRequired('Zadejte, prosím, počet porcí')
             ->addRule( Form::RANGE, 'Počet porcí musí být od %d do %d.', [1,50]);
        $form->addTextArea('postup', 'Postup:')
            ->setAttribute('class', 'form-control')
            ->setRequired(false)
            ->setAttribute('placeholder', 'Napište postup')
             ->addRule( Form::MAX_LENGTH, 'Postup přípravy je příliš dlouhý.', 10000);
        $form->addText('pribliznyCas', 'Přibližný čas přípravy')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Napište přibližný čas přípravy')
             ->setType('number');
        $form->addText('priorita', 'Priorita:')
            ->setAttribute('class', 'form-control')
            ->setAttribute('placeholder', 'Napište prioritu')
             ->setType('number');
        $form->addUpload('fotka', 'Fotka:')
            ->setAttribute('class', 'form-control')
             ->setRequired(FALSE)
             ->addRule(Form::IMAGE, 'Soubor musí být ve formátu JPEG, PNG nebo GIF.');
        $form->addSelect('slozitost', 'Složitost:', $moznosti)
            ->setAttribute('class', 'form-control')
             ->setPrompt('-- Zvolte složitost --');
        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložit')
            ->setAttribute('class', 'btn btn-block btn-success recipes-light recipes-a');
        
        $form->onSuccess[] = [$this, 'editFormSucceeded'];
        return $form;
    }

    /** RecipesPresenter - editFormSucceeded
     * Tato metoda vloží editované recepty a přesměruje na stránku receptů
     * @param Form $form
     */
    public function editFormSucceeded(Form $form) {
        $values = $form->getValues();

        $toIns = $values->id != NULL ? ReceptyQuery::Create()->findOneById( $values->id) : new Recepty();
        
        $toIns 
            ->setNazev($values['nazev'])
            ->setPocetPorci($values['pocetPorci'])
            ->setPostup($values['postup'])
            ->setPribliznyCas($values['pribliznyCas'])
            ->setPriorita($values['priorita'])
            ->setSlozitost($values['slozitost']);
        $toIns->save();

        $arr = explode( '.', $values->fotka->name );

        if($values->soubor->name) {
            $doc = $values->fotka;
            $doc->move(__DIR__ . "/../../www/img/" . 'recipes' . $toIns->getId() . '.' . $arr[(count($arr)-1)]);
        }

        $toIns->setFotka('review'.$toIns->getId().'.'.$arr[(count($arr)-1)]);
        $toIns->save();

        $this->flashMessage('Recept byl úšpěšně vytvořen.', 'success');
        $this->redirect('Recipes:view', $toIns->getId());

    }

    /** RecipesPresenter - handleDelete
     * Odebere recept ze systému.
     * @param $id - id receptu
     */
    public function handleDelete($id) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $recenze =  RecenzeQuery::create()->findOneByReceptid($id);
        if($recenze != NULL) {
            $this->flashMessage('Recept nemůže být smazán.', 'danger');
            $this->redirect('Recipes:list');
        }

        $recept = ReceptyQuery::create()->findOneById($id);
        $recept->delete();

        // přesměrování
        $this->flashMessage('Recept byl úspěšně smazán.', 'success');
        $this->redirect('Recipes:list');
    }

    /** RecipesPresenter - renderDelete
     * Zobrazí dotaz, zda chce uživatel recept opravdu smazat.
     * @param $id - id receptu
     */
    public function renderDelete($id) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $recenze =  RecenzeQuery::create()->findOneByReceptid($id);
        if($recenze != NULL) {
            $this->flashMessage('Recept nemůže být smazán.', 'danger');
            $this->redirect('Recipes:list');
        }

        $this->template->recipe = ReceptyQuery::create()->findOneById($id);

    }


    /** RecipesPresenter - renderView
     * Zobrazí detail daného receptu.
     * @param $id - id receptu
     */
    public function renderView($id) {
        $this->template->recipe = ReceptyQuery::create()->findOneById($id);
        $this->template->reviews = RecenzeQuery::create()->findByReceptid($id);
        $this->template->ingredience = IngredienceQuery::create()->findByReceptid($id);
        $this->template->averageStar = 0;
        $this->template->countStars = 0;
        $this->template->CntReviews = count($this->template->reviews);

        $fridge = array();
        if ($this->getUser()->isLoggedIn()) {
            $food = PotravinyQuery::create()->findByUzivatelid($this->getUser()->getId());

            foreach($food as $f) {
                $fridge[$f->getProduktid()] = $f->getProduktid();
            }
        }

        $this->template->isInFridge = $fridge;

    }

    /** RecipesPresenter - renderAddReview
     * Zobrazí přidání recenze k receptu.
     * @param $id - id receptu
     */
    public function renderAddReview($id) {
        $this->template->id = $id;
    }

    /** RecipesPresenter - createComponentFilterForm
     * Tato metoda vytvoří formulář pro přidávání recenzí
     */
    public function createComponentAddReviewForm() {
        $form = new Form();

        $form->addTextArea('textRecenze', 'Recenze')
            ->setAttribute('class', 'form-control')
             ->setRequired(false)
             ->setAttribute('placeholder', 'Zadejte text recenze')
             ->addRule(Form::MAX_LENGTH, 'Příliš dlouhý text recenze', 1000);
        $form->addText('pocetHvezdicek', 'Počet hvězdiček')
            ->setAttribute('class', 'form-control')
             ->setType('number')
            ->setRequired('Zadejte počet hvězdiček')
            ->setDefaultValue(0)
             ->addRule(Form::RANGE, 'Počet hvězdiček musí být od %d do %d', [0,5]);
        $form->addUpload('soubor', 'Nahrajte obrázek')
            ->setAttribute('class', 'form-control')
             ->setRequired(FALSE)
             ->addRule(Form::IMAGE, 'Soubor musí být ve formátu JPEG, PNG nebo GIF.');
        $form->addHidden('receptID');
        $form->addHidden('uzivatelID');

        $form->addSubmit('submit', 'Uložit')
            ->setAttribute('class', 'btn btn-block btn-success recipes-light recipes-a');

        $form->onSuccess[] = [$this, 'addReviewFormSucceeded'];
        return $form;
    }

    /** RecipesPresenter - addReviewFormSucceeded
     * Tato metoda vloží novou recenzi k receptu
     * @param Form $form
     */
    public function addReviewFormSucceeded(Form $form) {
        $values = $form->getValues();

        $recenze = new Recenze();
        $recenze->setUzivatelid($values['uzivatelID'])
            ->setPocethvezdicek($values['pocetHvezdicek'])
            ->setReceptid($values['receptID'])
            ->setTextrecenze($values['textRecenze']);

        $recenze->save();

        $arr = explode( '.', $values->soubor->name );

        if($values->soubor->name) {
            $doc = $values->soubor;
            $doc->move(__DIR__ . "/../../www/img/" . 'review' . $recenze->getId() . '.' . $arr[(count($arr)-1)]);
        }

        $recenze->setSoubor('review'.$recenze->getId().'.'.$arr[(count($arr)-1)]);
        $recenze->save();

        $this->flashMessage('Recenze byla úspěšně přidána.', 'success');
        $this->redirect('Recipes:view', $values['receptID']);

    }


    /** RecipesPresenter - handleDeleteIngredience
     * Smaže ingredienci k receptu
     * @param $id - id receptu
     * @param $iid - id ingredience
     */
    public function handleDeleteReview($id, $rid) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $recenze = RecenzeQuery::create()->findOneById($rid);
        $recenze->delete();

        // přesměrování
        $this->flashMessage('Recenze byla úspěšně smazána.', 'success');
        $this->redirect('Recipes:view', $id);

    }

    /** RecipesPresenter - renderAddIngredience
     * Zobrazí přidání recenze k receptu.
     * @param $id - id receptu
     */
    public function renderAddIngredience($id) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $this->template->id = $id;
    }

    /** RecipesPresenter - renderEditIngredience
     * Zobrazí editaci recenze k receptu.
     * @param $id - id receptu
     * @param $iid - id ingredience
     */
    public function renderEditIngredience($id, $iid) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $this->template->id = $id;
    }

    /** RecipesPresenter - handleDeleteIngredience
     * Smaže ingredienci k receptu
     * @param $id - id receptu
     * @param $iid - id ingredience
     */
    public function handleDeleteIngredience($id, $iid) {
        // ochrana pokud uživatel není přihlášený a nemá roli zaměstnanec
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'zamestnanec') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $ingredience = IngredienceQuery::create()->findOneById($iid);
        $ingredience->delete();

        // přesměrování
        $this->flashMessage('Ingredience byla úspěšně smazána.', 'success');
        $this->redirect('Recipes:view', $id);

    }

    /** RecipesPresenter - createComponentFilterForm
     * Tato metoda vytvoří formulář pro přidávání recenzí
     */
    public function createComponentEditIngredienceForm() {
        $form = new Form();


        $dbkat = KategorieQuery::create()->find();
        $dbpro = ProduktQuery::create()->find();
        $dbjed = JednotkaQuery::create()->find();

        $kategorie = array();
        foreach ($dbkat as $kid){
            $kategorie[$kid->getId()] = $kid->getNazev();
        }

        $produkt = array();
        foreach ($dbpro as $pid){
            $produkt[$pid->getId()] = $pid->getNazev();
        }

        $jednotky = array();
        foreach ($dbjed as $jid) {
            $jednotky[$jid->getId()] = $jid->getNazev();
        }

        $form->addText('mnozstvi', 'Množství')
            ->setAttribute('class', 'form-control')
            ->setType('number')
            ->setRequired('Zadejte množství')
            ->setDefaultValue(1);
            $form->addSelect('kategorieID','Kategorie',$kategorie)
                ->setAttribute('class', 'form-control')
                ->setPrompt('-- Vyberte kategorii --');
            $form->addSelect('produktID','Produkt',$produkt)
                ->setAttribute('class', 'form-control')
                ->setPrompt('-- Vyberte produkt --');
            $form->addSelect('jednotkaID','Jednotka',$jednotky)
                ->setAttribute('class', 'form-control')
                ->setPrompt('-- Vyberte jednotku --');
        $form->addHidden('id');
        $form->addHidden('receptID');

        $form->addSubmit('submit', 'Uložit')
            ->setAttribute('class', 'btn btn-block btn-success recipes-light recipes-a');

        $form->onSuccess[] = [$this, 'editIngredienceFormSucceeded'];
        return $form;
    }

    /** RecipesPresenter - addReviewFormSucceeded
     * Tato metoda vloží novou recenzi k receptu
     * @param Form $form
     */
    public function editIngredienceFormSucceeded(Form $form) {
        $values = $form->getValues();

        $ingredience = $values->id != NULL ? IngredienceQuery::create()->findOneById($values->id) : new Ingredience();
        $ingredience
            ->setMnozstvi($values['mnozstvi'])
            ->setJednotkaid($values['jednotkaID'])
            ->setProduktid($values['produktID'])
            ->setKategorieid($values['kategorieID'])
            ->setReceptid($values['receptID']);

        $ingredience->save();


        $this->flashMessage('Ingredience byla úspěšně přidána.', 'success');
        $this->redirect('Recipes:view', $values['receptID']);

    }

    /** RecipesPresenter - getProductsFromArray
     * Tato metoda získá produkty z pole
     *
     * @param $arr
     * @return array|mixed|null|\Propel\Runtime\ActiveRecord\ActiveRecordInterface[]|\Propel\Runtime\Collection\ObjectCollection|\SmartFridge\Produkt[]
     */
    public function getProductsFromArray($arr) {
        $string = \SmartFridge\ProduktQuery::create()->getINOperator($arr);

        if ($string == '()') {
            return NULL;
        }

        $products = ProduktQuery::create()
            ->where("id IN " . $string)
            ->find();

        return $products;

    }

    /** RecipesPresenter - loadProductsFromCache
     * Tato metoda získá produkty z cache
     * @return array|mixed|null
     *
     * @param null $return
     * @return array|mixed|null|\Propel\Runtime\ActiveRecord\ActiveRecordInterface[]|\Propel\Runtime\Collection\ObjectCollection|\SmartFridge\Produkt[]
     */
    protected function loadProductsFromCache($return = NULL) {
        $session = $this->getSession();
        $filter = $session->getSection('filter');

        // pokud jestě není nic v cache
        if (!isset($filter->products)) {
            // pokud není užvatel přihlášený
            if (!$this->getUser()->isLoggedIn()) {
                return NULL;
            }
            $foodInFridge = PotravinyQuery::create()->where('uzivatelID = ' . $this->getUser()->getId())->find();
            $arr = array();
            foreach ($foodInFridge as $f) {
                $arr[$f->getProduktid()] = $f->getProduktid();
            }

            // ulozeni do cache
            $filter->products = $arr;
            $filter->setExpiration(0, 'products');

            if ($return == 'array')
                return $arr;
            return $this->getProductsFromArray($arr);
        }
        // cache
        if ($return == 'array')
            return $filter->products;
        return $this->getProductsFromArray($filter->products);
    }

    /** RecipesPresenter - renderFilter
     * Vypíše seznam všech produktů ze kterých se hledají recepty
     */
    public function renderFilter()  {
        $this->template->products = $this->loadProductsFromCache();
        $this->template->countProducts = count($this->template->products);
    }

    /** RecipesPresenter - createComponentFilterForm
     * Tato metoda vytvoří formulář pro filtrování receptů
     */
    public function createComponentFilterForm() {
        $form = new Form();

        $allProducts = \SmartFridge\ProduktQuery::create()->find();
        $filter = $this->loadProductsFromCache('array');

        $products = array();

        foreach ($allProducts as $a) {
            if (!isset($filter[$a->getId()])) {
                $products[$a->getId()] = $a->getNazev();
            }
        }

        $form->addSelect('products', 'Produkt', $products)
            ->setAttribute('class', 'form-control')
            ->setPrompt('-- Vyberte produkt --');
        $form->addSubmit('submit', 'Uložit')
            ->setAttribute('class', 'btn btn-block btn-success recipes-light recipes-a');

        $form->onSuccess[] = [$this, 'filterFormSucceeded'];
        return $form;
    }

    /** RecipesPresenter - filterFormSucceeded
     * Tato metoda uloží filtr do cache
     * @param Form $form
     */
    public function filterFormSucceeded(Form $form) {
        $values = $form->getValues();

        $session = $this->getSession();
        $filter = $session->getSection('filter');

        $products = array();
        if (isset($filter->products) || count($filter->products) == 0) {
            $products = $filter->products;
        } else if ($this->getUser()->isLoggedIn() && $this->getUser()->getRoles()[0] == 'uzivatel') {
            $foodInFridge = PotravinyQuery::create()->where('uzivatelID = ' . $this->getUser()->getId())->find();
            foreach ($foodInFridge as $f) {
                $products[$f->getProduktid()] = $f->getProduktid();
            }
        }

        // ulozeni do cache
        $products[$values->products] = $values->products;
        $filter->products = $products;
        $filter->setExpiration(0, 'products');

        $this->flashMessage('Produkt byl úspěšně přidán do filtru.', 'success');
        $this->redirect('Recipes:filter');

    }

    /** RecipesPresenter - handleDeleteFromFilter
     * Tato metoda smaže produkt z filtru
     * @param $id
     */
    public function handleDeleteFromFilter($id) {

        $session = $this->getSession();
        $filter = $session->getSection('filter');

        unset($filter->products[$id]);


        // přesměrování
        $this->flashMessage('Produkt z filtr byl smazán.', 'success');
        $this->redirect('Recipes:filter');

    }

    /** RecipesPresenter - handleDeleteFromFilter
     * Tato metoda resetuje filtr do výchozího stavu
     * - pro přihlášeného obsah lednice
     * - pro nepřihlášeného prázdný filtr
     */
    public function handleResetFilter() {

        $session = $this->getSession();
        $filter = $session->getSection('filter');

        unset($filter->products);

        // přesměrování
        $this->flashMessage('Filtr byl resetován.', 'success');
        $this->redirect('Recipes:filter');

    }
}
