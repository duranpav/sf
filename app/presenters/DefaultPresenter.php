<?php

namespace App\Presenters;

use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use SmartFridge\PolozkaQuery;
use SmartFridge\PotravinyQuery;
use SmartFridge\ProduktQuery;
use SmartFridge\ReceptyQuery;
use SmartFridge\Upozorneni;

class DefaultPresenter extends BasePresenter
{

    /** DefaultPresenter - renderHomepage
     * Zobrazí hlavní stránku s podstránkami
     */
    public function renderHomepage() {
        $this->template->countNotif = 0;
        $this->template->countFridge = 0;
        $this->template->countCart = 0;
        $this->template->countProduct = count(ProduktQuery::create()->find());
        $this->template->countRecipes = count(ReceptyQuery::create()->find());

        if ($this->getUser()->isLoggedIn() && $this->getUser()->getRoles()[0] == 'uzivatel') {
            $this->template->countNotif = count(Upozorneni::findActive($this->getUser()->getId()));
            $this->template->countFridge = count(PotravinyQuery::create()->findByUzivatelid($this->getUser()->getId()));
            $cart = PolozkaQuery::create()->leftJoinWithObjednavka()->where('Objednavka.uzivatelID = ' . $this->getUser()->getId())->find();
            $this->template->countCart = count($cart);
        }
    }

    /** DefaultPresenter - renderLogin
     * Zobrazí formálář s přihlášením
     */
    public function renderLogin() {
        //$this->getUser()->getAuthenticator()->add('eliska.dostalova', 'uzivatel', 'eliska');
        //$this->getUser()->getAuthenticator()->add('pepa.novak', 'uzivatel', 'pepa');
        //$this->getUser()->getAuthenticator()->add('petr.adam', 'admin', 'petr');
    }

    /** DefaultPresenter - createComponentLoginForm
     * Tato metoda vytvoří formulář pro login
     */
    public function createComponentLoginForm() {
        $form = new Form();

        $form->addText('prezdivka', 'Uživatelské jméno: ')
            ->setAttribute('placeholder', 'Zadejte uživatelké jméno')
            ->setAttribute('class', 'form-control')
            ->setRequired();

        $form->addPassword('heslo', 'Heslo: ')
            ->setAttribute('placeholder', 'Zadejte heslo')
            ->setAttribute('class', 'form-control')
            ->setRequired();

        $form->addSubmit('login', 'Přihlásit se')
            ->setAttribute('class', 'btn btn-success');

        $form->onSuccess[] = [$this, 'loginFormSucceeded'];

        return $form;
    }

    /** DefaultPresenter - loginFormSucceeded
     * Tato metoda zkontroluje přihlášení uživatele pokud souhlasí jméno a heslo tak ho přihlásí
     * @param Form $form
     */
    public function loginFormSucceeded(Form $form) {
        $values = $form->getValues();

        try {
            $this->getUser()->login($values->prezdivka, $values->heslo);
            $this->flashMessage('Byl jste úspěšně přihlášen.', 'success');
            $this->redirect('Default:homepage');

        } catch (AuthenticationException $e) {
            $this->flashMessage($e->getMessage(), 'danger');
            $this->redirect('Default:login');
        }

    }
}
