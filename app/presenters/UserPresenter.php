<?php

namespace App\Presenters;


use SmartFridge\WatchdogQuery;
use SmartFridge\UzivatelQuery;

class UserPresenter extends BasePresenter
 {

        /** UserPresenter - renderDefault
         * Tato metoda bude vypisovat stránku uživatele
         * @param $id - id watchdogu
         * @redirect - this
         */
        public function renderDefault() {
            // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
            if (!$this->user->isLoggedIn() || ($this->user->getRoles()[0] != 'uzivatel' && $this->user->getRoles()[0] != 'zamestnanec')) {
                $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
                $this->redirect('Default:login');
            }

            $this->template->uzivatel = UzivatelQuery::create()->findOneById($this->getUser()->getId());
            $this->template->watchdog = WatchdogQuery::create()->findByUzivatelid($this->getUser()->getId());
            
        }


    /** UserPresenter - handleDeleteWatchdog
     * Tato metoda přestane sledovat produkt
     * @param $id - id watchdogu
     * @redirect - this
     */
    public function handleDeleteWatchdog($id) {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $existujiciWatchdog = WatchdogQuery::create()->findOneById($id);

        if ($existujiciWatchdog) {
            $existujiciWatchdog->delete();
        } else {
            $this->flashMessage('Nemůžete přestat slevdovat produkt, který nesledujete.', 'danger');
            $this->redirect('this');
        }


        $this->flashMessage('Produkt byl úspěšně přestán sledován.', 'success');
        $this->redirect('this');
    }
 }