<?php

namespace App\Presenters;

use Propel\Runtime\ActiveQuery\Criteria;
use SmartFridge\Upozorneni;

class NotificationPresenter extends BasePresenter
{
    /** NotificationPresenter - renderList
     * Tato metoda zobrazí seznam všech notifikací pro přihlášeného uživatele
     */
    public function renderList() {
        // ochrana pokud uživatel není přihlášený a nebo nemá roli uživatel
        if (!$this->user->isLoggedIn() || $this->user->getRoles()[0] != 'uzivatel') {
            $this->flashMessage('Pro tuto akci se prosím přihlašte.', 'danger');
            $this->redirect('Default:login');
        }

        $userId = $this->user->getId();
        $notifications = Upozorneni::findAll($userId);

        $this->template->notificationsActive = Upozorneni::findActive($userId);
        $this->template->notificationsNonActive = Upozorneni::findNonActive($userId);

        $activeNotif = Upozorneni::findActive($userId);
        foreach ($activeNotif as $n) {
            $n->setAktivni(0);
            $n->save();
        }
        $this->template->countNoti = count($this->template->notificationsActive);
        $this->template->countAllNoti = count($this->template->notificationsActive) + count($this->template->notificationsNonActive);

    }
}
