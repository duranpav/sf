-- --------------------------------------------------------
-- Hostitel:                     xsky.cz
-- Verze serveru:                5.6.34-79.1-log - Percona Server (GPL), Release 79.1, Revision 1c589f9
-- OS serveru:                   debian-linux-gnu
-- HeidiSQL Verze:               9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportování dat pro tabulku c0smartfridge.Ingredience: ~21 rows (přibližně)
DELETE FROM `Ingredience`;
/*!40000 ALTER TABLE `Ingredience` DISABLE KEYS */;
INSERT INTO `Ingredience` (`id`, `mnozstvi`, `receptID`, `jednotkaID`, `kategorieID`, `produktID`) VALUES
	(4, 1, 1, 2, 4, 11),
	(5, 1, 1, 2, 8, 1),
	(6, 100, 1, 2, 4, 12),
	(7, 2, 1, 2, 4, 14),
	(8, 500, 3, 3, NULL, 20),
	(9, 10, 9, 1, 3, 17),
	(10, 300, 9, 3, 5, 18),
	(11, 300, 9, 3, 3, 19),
	(12, 250, 9, 1, 2, 4),
	(13, 50, 9, 3, 5, 21),
	(14, 100, 9, 3, 25, 22),
	(15, 10, 9, 3, 25, 23),
	(17, 1, 6, 2, 4, 10),
	(18, 100, 6, 3, 4, 12),
	(19, 400, 6, 3, 4, 15),
	(20, 300, 6, 3, 4, 16),
	(21, 400, 8, 3, 1, 2),
	(22, 1, 8, 2, 4, 10),
	(23, 100, 8, 3, 25, 15),
	(24, 500, 2, 3, NULL, 19),
	(25, 500, 2, 3, NULL, 22);
/*!40000 ALTER TABLE `Ingredience` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Jednotka: ~4 rows (přibližně)
DELETE FROM `Jednotka`;
/*!40000 ALTER TABLE `Jednotka` DISABLE KEYS */;
INSERT INTO `Jednotka` (`id`, `nazev`) VALUES
	(1, 'ml'),
	(2, 'kg'),
	(3, 'g'),
	(4, 'l');
/*!40000 ALTER TABLE `Jednotka` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Kategorie: ~25 rows (přibližně)
DELETE FROM `Kategorie`;
/*!40000 ALTER TABLE `Kategorie` DISABLE KEYS */;
INSERT INTO `Kategorie` (`id`, `ikona`, `nazev`, `kategorieID`) VALUES
	(1, NULL, 'Maso a ryby', NULL),
	(2, NULL, 'Mléčné výrobky', NULL),
	(3, NULL, 'Ovoce', NULL),
	(4, NULL, 'Zelenina', NULL),
	(5, NULL, 'Trvanlivé výrobky', NULL),
	(6, NULL, 'Nápoje', NULL),
	(7, NULL, 'Grilování', 1),
	(8, NULL, 'Drůbeží a králičí', 1),
	(9, NULL, 'Vepřové', 1),
	(10, NULL, 'Hovězí a telecí', 1),
	(11, NULL, 'Zvěřina', 1),
	(12, NULL, 'Ryby a mořské plody', 1),
	(13, NULL, 'Uzená masa a slaniny', 1),
	(14, NULL, 'Mléko a mléčné výrobky', 2),
	(15, NULL, 'Jogurty a mléčné dezerty', 2),
	(16, NULL, 'Smetany a šlehačky', 2),
	(17, NULL, 'Sýry', 2),
	(18, NULL, 'Tvarohy', 2),
	(19, NULL, 'Máslo, tuky a margaríny', 2),
	(20, NULL, 'Vejce a droždí', 2),
	(21, NULL, 'Majonézy a dresingy', 2),
	(22, NULL, 'Sojové a rostlinné produkty', 2),
	(23, NULL, 'Maso na gril', 7),
	(24, NULL, 'Sýry na gril', 7),
	(25, NULL, 'Koření', NULL);
/*!40000 ALTER TABLE `Kategorie` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Objednavka: ~2 rows (přibližně)
DELETE FROM `Objednavka`;
/*!40000 ALTER TABLE `Objednavka` DISABLE KEYS */;
INSERT INTO `Objednavka` (`id`, `datumZalozeni`, `datumZaplaceni`, `datumDorucení`, `zpusobPlatby`, `celkovaCena`, `status`, `soubor`, `uzivatelID`) VALUES
	(1, NULL, NULL, NULL, NULL, NULL, 'vytvorena', NULL, 2),
	(2, NULL, NULL, NULL, NULL, NULL, 'vytvorena', NULL, 3);
/*!40000 ALTER TABLE `Objednavka` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Polozka: ~9 rows (přibližně)
DELETE FROM `Polozka`;
/*!40000 ALTER TABLE `Polozka` DISABLE KEYS */;
INSERT INTO `Polozka` (`id`, `cenaPolozky`, `pocet`, `ZboziID`, `ObjednavkaID`) VALUES
	(6, 25, 1, 22, 1),
	(7, 20, 1, 10, 1),
	(8, 15, 1, 9, 1),
	(10, 50, 1, 8, 1),
	(11, 25, 1, 14, 1),
	(12, 89, 1, 2, 1),
	(13, 99, 1, 1, 1),
	(14, 25, 1, 18, 1),
	(15, 48, 1, 16, 1);
/*!40000 ALTER TABLE `Polozka` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Potraviny: ~12 rows (přibližně)
DELETE FROM `Potraviny`;
/*!40000 ALTER TABLE `Potraviny` DISABLE KEYS */;
INSERT INTO `Potraviny` (`id`, `spotrebovatDo`, `trvanlivostDo`, `uzivatelID`, `produktID`, `pocet`) VALUES
	(1, '2017-05-20', NULL, 3, 1, 3),
	(3, '2017-05-20', NULL, 3, 2, 1),
	(5, '2017-06-10', NULL, 2, 22, 2),
	(6, '2017-06-10', NULL, 2, 23, 1),
	(7, '2017-06-10', NULL, 2, 17, 2),
	(8, '2017-06-10', NULL, 2, 18, 1),
	(9, '2017-06-10', NULL, 2, 19, 2),
	(10, '2017-08-04', NULL, 2, 4, 2),
	(11, '2017-05-20', NULL, 2, 1, 2),
	(12, '2017-06-10', NULL, 2, 9, 1),
	(13, '2017-06-10', NULL, 2, 11, 1),
	(14, '2017-06-10', NULL, 2, 20, 2);
/*!40000 ALTER TABLE `Potraviny` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Produkt: ~19 rows (přibližně)
DELETE FROM `Produkt`;
/*!40000 ALTER TABLE `Produkt` DISABLE KEYS */;
INSERT INTO `Produkt` (`id`, `nazev`, `fotka`, `popis`, `kategorieID`, `jednotkaID`, `mnozstvi`) VALUES
	(1, 'Vodňanské Kuře', 'kure.jpg', '<ul>\r\n	<li>Pravé kuře z Vodňan</li>\r\n	<li>Vychované s láskou</li>\r\n	<li>Váha 2 kg</li>\r\n</ul>', 8, 2, NULL),
	(2, 'Klobásy na Grilování', 'klobasy.jpg', NULL, 23, 2, NULL),
	(3, 'Plody moře', 'plody.jpg', NULL, 12, 3, NULL),
	(4, 'Mléko Tatra', 'mleko.png', NULL, 14, 4, NULL),
	(5, 'Selské mléko', 'selske.jpg', NULL, 14, 4, NULL),
	(9, 'Pomeranč', 'pomeranc.jpg', '<p>citrusov&eacute; ovoce</p>', 3, 2, 1),
	(10, 'Rajče', 'rajce.jpg', '<p>červen&eacute; rajče</p>', 4, 2, 1),
	(11, 'Cibule', 'cibule.jpg', '<p>česk&aacute; cibule</p>', 4, 2, 1),
	(12, 'Česnek', 'cesnek.jpg', '<p>Česnek od česk&yacute;ch farm&aacute;řů.</p>', 4, 3, 100),
	(14, 'Brambory', 'brambory.jpg', '<p>polsk&eacute; brambory s minimem &scaron;krobu</p>', 4, 2, 5),
	(15, 'Červená paprika', 'paprika.png', '<p>Skvěl&aacute; paprika z Polska.</p>', 4, 2, 1),
	(16, 'Chilli papričky', 'chilli.jpg', '<p>Extra p&aacute;liv&eacute; chilli papričky z Argentiny.</p>', 4, 3, 200),
	(17, 'Citron', 'citron.jpg', '<p>Citron ze středn&iacute; Asie.</p>', 3, 2, 1),
	(18, 'Hladká mouka', 'hlm.jpg', '<p>P&scaron;eničn&aacute; hladk&aacute; mouka z čech.</p>', 5, 2, 1),
	(19, 'Čerstvé maliny', 'maliny.jpg', '<p>Jak natrhan&eacute; od babičky.</p>', 3, 3, 300),
	(20, 'Mražená pizza Hawaii', 'pizza.png', '<p>Hluboce zamražen&aacute; pizza z čerstv&eacute;ho ananasu.</p>', 5, 3, 500),
	(21, 'Vanilkový cukr', 'vanilin.jpg', '<p>cukr sm&iacute;chan&yacute; s vanilkov&yacute;m extraktem</p>', 25, 2, 1),
	(22, 'Cukr krupice', 'krc.jpg', '<p>Česk&yacute; kvallitn&iacute; cukr.</p>', 25, 2, 1),
	(23, 'Kypřící prášek do pečiva', 'kpdp.jpg', '<p>mal&yacute; bal&iacute;ček&nbsp;kypř&iacute;c&iacute; pr&aacute;&scaron;ek do pečiva</p>', 5, 3, 50);
/*!40000 ALTER TABLE `Produkt` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Recenze: ~11 rows (přibližně)
DELETE FROM `Recenze`;
/*!40000 ALTER TABLE `Recenze` DISABLE KEYS */;
INSERT INTO `Recenze` (`id`, `textRecenze`, `pocetHvezdicek`, `soubor`, `receptID`, `uzivatelID`) VALUES
	(1, 'Recept je úžasný', 5, NULL, 2, NULL),
	(2, 'Recept je úžasný, ale má pár chyb.', 4, NULL, 3, NULL),
	(3, 'Recept je na nic', 2, NULL, 1, NULL),
	(4, 'Recept je úžasný, ale drahý.', 4, NULL, 2, NULL),
	(5, 'Recept je úžasný!!!', 5, NULL, 2, NULL),
	(8, '<p>V&yacute;born&yacute; recept, ide&aacute;ln&iacute; pro rodinou večeři.</p>', 5, NULL, 1, 4),
	(15, '<p>nejľepsia vejca čo som kedy jedala</p>', 5, NULL, 7, 2),
	(16, '<p>ani ty prasata to nezraly</p>', 0, NULL, 8, 3),
	(17, '<p>Cel&aacute; rodina se pochutnala, velmi kvalitn&iacute; postup receptu, jsme v&scaron;ichni velmi spokojeni.</p>\n<p>Petra</p>', 5, NULL, 9, 4),
	(18, '<p>Velmi p&aacute;liv&eacute;, nic pro d&aacute;mičky.</p>\n<p>Andreas</p>', 5, NULL, 6, 4),
	(20, '<p>prilis palive</p>', 1, NULL, 6, 2);
/*!40000 ALTER TABLE `Recenze` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Recepty: ~7 rows (přibližně)
DELETE FROM `Recepty`;
/*!40000 ALTER TABLE `Recepty` DISABLE KEYS */;
INSERT INTO `Recepty` (`id`, `nazev`, `pocetPorci`, `postup`, `pribliznyCas`, `priorita`, `slozitost`, `fotka`) VALUES
	(1, 'Prirodni rizek na cesneku s parmezanem', 3, '<ol>\r\n	<li>Rozpalime cibulku do zlatova, a pridame veprove rizky predem okorenene.</li>\r\n	<li>Pridame cesnek a parmezan a 100ml vody a varime na mirnem ohni.</li>\r\n	<li>Podavame s brambory.</li>\r\n</ol>', 45, 5, 'zacatecnik', 'rizek.jpg'),
	(2, 'Domácí marmeláda', 4, '<p>1. Maliny omyjeme, nech&aacute;me okapat. P&aacute;r plodů si odlož&iacute;me stranou.</p>\n<p>2. Ovoce rozmačk&aacute;me, rozmixujeme nebo rozmačk&aacute;me lisem na brambory. Přid&aacute;me cukr a vař&iacute;me 5 minut, průběžně sb&iacute;r&aacute;me pěnu z povrchu marmel&aacute;dy. Je&scaron;tě horkou ji přeced&iacute;me a vař&iacute;me, dokud marmel&aacute;da nezačne rosolovatět.</p>\n<p>3. Do skoro hotov&eacute; marmel&aacute;dy vsypeme několik plodů, kter&eacute; jsme neprolisovali, převař&iacute;me tři minuty a přelijeme ji do sklenic.</p>\n<p>4. Zav&iacute;čkujeme a sklenice obr&aacute;t&iacute;me dnem vzhůru.</p>', 60, 5, 'pokrocily', 'jam.jpg'),
	(3, 'Pizza', 4, '<ol>\n<li>Rozmrazime Pizzu.</li>\n<li>Vlož&iacute;me do trouby.</li>\n<li>Pečeme 20 min na 150&deg;C.</li>\n<li>Pod&aacute;v&aacute;me nakr&aacute;jen&eacute;.</li>\n</ol>', 20, 10, 'zacatecnik', 'pizza1.jpg'),
	(6, 'Indický Phal', 2, '<p>1. Na p&aacute;nvi upraž&iacute;ma rajčata a papriku.</p>\n<p>2. Opepř&iacute;me a přid&aacute;me v&scaron;echny papričky a česnek.&nbsp;</p>\n<p>3. Hototvo přejeme př&iacute;jemnou chuť</p>', 60, 14, 'pokrocily', 'gulas.jpg'),
	(7, 'Slovenská vejce', 2, '<p>roz&scaron;leh&aacute;me uveden&eacute; množstv&iacute; vajec a sm&iacute;ch&aacute;me se sol&iacute; a pepřem podle potřeby. Pot&eacute; d&aacute;me tuto směs do igelitov&yacute;ch pytl&iacute;čků velikosti vět&scaron;&iacute;ho vejce a ty d&aacute;me va 5 minut vačit do vrouc&iacute; vody</p>', 10, 1, 'zacatecnik', 'skvajca.jpg'),
	(8, 'Fitness guláš', 4, '<p>Cibule nakr&aacute;j&iacute;me nadrobno a d&aacute;me do pek&aacute;če. Nadrobno nakr&aacute;j&iacute;me i maso a prom&iacute;ch&aacute;me s cibul&iacute;. L&aacute;k připrav&iacute;me rozpu&scaron;těn&iacute;m dvou kostek masoxu. Do litrov&eacute; n&aacute;doby d&aacute;me lž&iacute;ci km&iacute;nu, 3 lž&iacute;ce solamylu a 3 lžičky červen&eacute; papriky. Nesol&iacute;me! Přilijeme rozpu&scaron;těn&yacute; masox, dolijeme studenou vodou a prom&iacute;ch&aacute;me. L&aacute;k vylijeme na maso s cibul&iacute;, nem&iacute;ch&aacute;me, pouze přikryjeme a vlož&iacute;me do trouby. Na 250 &deg;C pečeme 1,5 hodiny. Do litrov&eacute; n&aacute;doby si d&aacute;me lž&iacute;ci km&iacute;nu, 3 lž&iacute;ce solamylu a 3 lžičky červen&eacute; papriky. V ž&aacute;dn&eacute;m př&iacute;padě nesol&iacute;me.&nbsp;<span style="background-color: #dddddd; color: #666666; font-family: \'Open Sans\'; font-size: 16px;">Zdroj: https://hruskovyrecepty.cz/fitness-gulas-129/</span>&nbsp;</p>', 120, 0, 'pokrocily', 'gulas_dom.jpg'),
	(9, 'Muffiny s čerstvými malinami', 15, '<p style="margin: 0px 0px 1em; color: #343434; font-family: arial, tahoma, sans-serif;">Ve vět&scaron;&iacute; m&iacute;se sm&iacute;ch&aacute;me hladkou mouku s kypř&iacute;c&iacute;m pr&aacute;&scaron;kem a čerstv&yacute;mi malinami. Do mal&eacute; m&iacute;sy nabereme jogurt, sm&iacute;ch&aacute;me s cukrem, citronovou kůrou a ml&eacute;kem. Směs přid&aacute;me do velk&eacute; m&iacute;sy s moukou a vypracujeme v těsto.</p>\n<p style="margin: 0px 0px 1em; color: #343434; font-family: arial, tahoma, sans-serif;">Keramick&eacute; nebo teflonov&eacute; formičky vymažeme m&aacute;slem a napln&iacute;me do 3/4 připraven&yacute;m těstem. Muffiny s čerstv&yacute;mi malinami pečeme v troubě na 200 &deg;C asi 20 minut, nebo dokud povrch nevyběhne a nezezl&aacute;tne.</p>\n<p style="margin: 0px 0px 1em; color: #343434; font-family: arial, tahoma, sans-serif;">Hotov&eacute; muffiny s čerstv&yacute;mi malinami můžeme pocukrovat moučkov&yacute;m cukrem.</p>', 120, 15, 'expert', 'muffins.jpg');
/*!40000 ALTER TABLE `Recepty` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Upozorneni: ~10 rows (přibližně)
DELETE FROM `Upozorneni`;
/*!40000 ALTER TABLE `Upozorneni` DISABLE KEYS */;
INSERT INTO `Upozorneni` (`id`, `produktID`, `uzivatelID`, `aktivni`, `typ`, `pocetDni`, `datum`, `cena`) VALUES
	(1, 4, 2, 0, 'trvanlivost', 3, '2017-05-15 22:30:00', NULL),
	(2, 4, 2, 0, 'trvanlivost', 5, '2017-05-14 22:30:00', NULL),
	(3, 1, 2, 0, 'zmena_ceny', NULL, '2017-05-15 22:39:44', 89),
	(4, 1, 3, 0, 'zmena_ceny', NULL, '2017-05-17 15:40:57', 98),
	(5, 1, 3, 1, 'zmena_ceny', NULL, '2017-05-17 16:10:25', 99),
	(6, 1, 3, 1, 'zmena_ceny', NULL, '2017-05-17 16:15:51', 100),
	(7, 1, 3, 1, 'zmena_ceny', NULL, '2017-05-17 18:29:20', 98),
	(8, 1, 2, 0, 'zmena_ceny', NULL, '2017-05-17 18:29:20', 98),
	(9, 1, 3, 1, 'zmena_ceny', NULL, '2017-05-18 12:54:29', 97),
	(10, 1, 2, 0, 'zmena_ceny', NULL, '2017-05-18 12:54:29', 97);
/*!40000 ALTER TABLE `Upozorneni` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Uzivatel: ~3 rows (přibližně)
DELETE FROM `Uzivatel`;
/*!40000 ALTER TABLE `Uzivatel` DISABLE KEYS */;
INSERT INTO `Uzivatel` (`id`, `prezdivka`, `heslo`, `jmeno`, `prijmeni`, `ulice`, `mesto`, `cisloPopisne`, `PostovniSmerovaciCislo`, `role`) VALUES
	(2, 'eliska.dostalova', '$2y$10$O37NTzkIYRfWptBLx0X6i.ppTuEbyisbpB.lfUbf/QuZ7iNakEoBm', 'Eliška', 'Dostálová', NULL, NULL, NULL, NULL, 'uzivatel'),
	(3, 'pepa.novak', '$2y$10$sYvBasE1K5z35cNjSjKdte7kbfGCHpQcM81wKh/dKQvS.1EEFUwOe', 'Pepa', 'Novák', NULL, NULL, NULL, NULL, 'uzivatel'),
	(4, 'petr.adam', '$2y$10$zUaGxrwFOcoT40gRrPoSYOkt1.lOhXvc08bUsQxSVm44dnDZQCeI6', 'Petr', 'Adam', NULL, NULL, NULL, NULL, 'zamestnanec');
/*!40000 ALTER TABLE `Uzivatel` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Watchdog: ~5 rows (přibližně)
DELETE FROM `Watchdog`;
/*!40000 ALTER TABLE `Watchdog` DISABLE KEYS */;
INSERT INTO `Watchdog` (`id`, `produktID`, `uzivatelID`, `cena`) VALUES
	(3, 2, 3, NULL),
	(4, 1, 3, NULL),
	(6, 1, 2, NULL),
	(7, 2, 2, NULL),
	(8, 4, 2, NULL);
/*!40000 ALTER TABLE `Watchdog` ENABLE KEYS */;

-- Exportování dat pro tabulku c0smartfridge.Zbozi: ~24 rows (přibližně)
DELETE FROM `Zbozi`;
/*!40000 ALTER TABLE `Zbozi` DISABLE KEYS */;
INSERT INTO `Zbozi` (`id`, `cena`, `pocet`, `trvanlivost`, `produktID`) VALUES
	(1, 97, 200, '2017-06-04 00:00:00', 1),
	(2, 89, 50, '2017-05-20 01:54:54', 1),
	(3, 49, 50, '2017-05-20 01:54:54', 2),
	(4, 49, 20, '2017-05-20 01:54:54', 3),
	(5, 21, 50, '2017-08-04 01:55:56', 4),
	(6, 21, 50, '2017-08-04 01:55:56', 5),
	(7, 35, 50, '2017-06-10 11:13:57', 14),
	(8, 50, 50, '2017-06-10 11:13:57', 19),
	(9, 15, 50, '2017-06-10 11:13:57', 15),
	(10, 20, 50, '2017-06-10 11:13:57', 12),
	(11, 30, 50, '2017-06-10 11:13:57', 16),
	(12, 15, 50, '2017-06-10 11:13:57', 11),
	(13, 15, 50, '2017-06-10 11:13:57', 17),
	(14, 25, 50, '2017-06-10 11:13:57', 22),
	(15, 10, 50, '2017-06-10 11:13:57', 18),
	(16, 48, 50, '2017-06-10 11:13:57', 2),
	(17, 5, 50, '2017-06-10 11:13:57', 23),
	(18, 25, 50, '2017-06-10 11:13:57', 4),
	(19, 50, 50, '2017-06-10 11:13:57', 20),
	(20, 69, 50, '2017-06-10 11:13:57', 3),
	(21, 30, 50, '2017-06-10 11:13:57', 9),
	(22, 25, 50, '2017-06-10 11:13:57', 10),
	(23, 25, 50, '2017-06-10 11:13:57', 5),
	(24, 5, 50, '2017-06-10 11:13:57', 21);
/*!40000 ALTER TABLE `Zbozi` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
