var indexSectionsWithContent =
{
  0: "$5_abcdefghijklnoprstuwz",
  1: "bcdefijknopruwz",
  2: "aps",
  3: "5bcdefijknopruwz",
  4: "_abcdefghijlprstu",
  5: "$cdnot"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables"
};

