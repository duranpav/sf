-- --------------------------------------------------------
-- Hostitel:                     xsky.cz
-- Verze serveru:                5.6.34-79.1-log - Percona Server (GPL), Release 79.1, Revision 1c589f9
-- OS serveru:                   debian-linux-gnu
-- HeidiSQL Verze:               9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Exportování struktury databáze pro
CREATE DATABASE IF NOT EXISTS `c0smartfridge` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci */;
USE `c0smartfridge`;

-- Exportování struktury pro tabulka c0smartfridge.Ingredience
CREATE TABLE IF NOT EXISTS `Ingredience` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mnozstvi` int(10) unsigned DEFAULT NULL COMMENT 'Množství ingredience',
  `receptID` int(10) unsigned DEFAULT NULL,
  `jednotkaID` int(10) unsigned DEFAULT NULL,
  `kategorieID` int(10) unsigned DEFAULT NULL,
  `produktID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Jednotka_ingredience` (`jednotkaID`),
  KEY `FK_Kategorie_Ingredience` (`kategorieID`),
  KEY `FK_Produkt_Ingredience` (`produktID`),
  KEY `FK_Recepty_Ingredience` (`receptID`),
  CONSTRAINT `FK_Jednotka_ingredience` FOREIGN KEY (`jednotkaID`) REFERENCES `Jednotka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Kategorie_Ingredience` FOREIGN KEY (`kategorieID`) REFERENCES `Kategorie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Produkt_Ingredience` FOREIGN KEY (`produktID`) REFERENCES `Produkt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Recepty_Ingredience` FOREIGN KEY (`receptID`) REFERENCES `Recepty` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje ingredience v receptu';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Jednotka
CREATE TABLE IF NOT EXISTS `Jednotka` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(50) DEFAULT NULL COMMENT 'Název jednotky',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje seznam všech jednotek';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Kategorie
CREATE TABLE IF NOT EXISTS `Kategorie` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ikona` varchar(50) DEFAULT NULL COMMENT 'Cesta k ikone',
  `nazev` varchar(50) DEFAULT NULL COMMENT 'Název kategorie',
  `kategorieID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Kategorie_Produkty` (`kategorieID`),
  CONSTRAINT `FK_Kategorie_Produkty` FOREIGN KEY (`kategorieID`) REFERENCES `Kategorie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje kategorie, každá kategorie muže mít svou podkategorii vázanou pomocí idRodice. ';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Objednavka
CREATE TABLE IF NOT EXISTS `Objednavka` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datumZalozeni` datetime DEFAULT NULL COMMENT 'Datum založení objednávky',
  `datumZaplaceni` datetime DEFAULT NULL COMMENT 'Datum zaplacení objednávky',
  `datumDorucení` datetime DEFAULT NULL COMMENT 'Datum dorucení objednávky',
  `zpusobPlatby` enum('dobirka','predem') DEFAULT NULL COMMENT 'Zpusob platby, možnosti hotove pri predání a online',
  `celkovaCena` double DEFAULT NULL COMMENT 'Celková cena objednávky',
  `status` enum('vytvorena','expandovana','zrusena','dokoncena') DEFAULT NULL COMMENT 'Status objednávky, možnosti: vytvorená, expandovaná, zrušená, dokoncená.',
  `soubor` varchar(50) DEFAULT NULL COMMENT 'Cesta k souboru s fakturou.',
  `uzivatelID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Uzivatel_Objednavka` (`uzivatelID`),
  CONSTRAINT `FK_Uzivatel_Objednavka` FOREIGN KEY (`uzivatelID`) REFERENCES `Uzivatel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje záznamy objednávek všech uživatelu.';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Polozka
CREATE TABLE IF NOT EXISTS `Polozka` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cenaPolozky` double unsigned DEFAULT NULL COMMENT 'Cena zboží pri objednávce.',
  `pocet` int(10) unsigned DEFAULT NULL COMMENT 'Pocet objednaných položek.',
  `ZboziID` int(10) unsigned DEFAULT NULL,
  `ObjednavkaID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Objednavka_Polozka` (`ObjednavkaID`),
  KEY `FK_Zbozi_Polozka` (`ZboziID`),
  CONSTRAINT `FK_Objednavka_Polozka` FOREIGN KEY (`ObjednavkaID`) REFERENCES `Objednavka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Zbozi_Polozka` FOREIGN KEY (`ZboziID`) REFERENCES `Zbozi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje položka objednávky, pro uchování historie s cenou, která byla aktuální pri objednávce, a poctem zboží.';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Potraviny
CREATE TABLE IF NOT EXISTS `Potraviny` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `spotrebovatDo` date DEFAULT NULL COMMENT 'Uvedené datum spotreby.',
  `trvanlivostDo` int(11) DEFAULT NULL COMMENT 'Trvanlivost po otevrení produktu. (ve dnech)',
  `uzivatelID` int(10) unsigned DEFAULT NULL,
  `produktID` int(10) unsigned DEFAULT NULL,
  `pocet` int(11) DEFAULT NULL COMMENT 'Počet potravin stejného druhu.',
  PRIMARY KEY (`id`),
  KEY `FK_Potraviny_Produkt` (`produktID`),
  KEY `FK_Potraviny_Uzivatel` (`uzivatelID`),
  CONSTRAINT `FK_Potraviny_Produkt` FOREIGN KEY (`produktID`) REFERENCES `Produkt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Potraviny_Uzivatel` FOREIGN KEY (`uzivatelID`) REFERENCES `Uzivatel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje potravinu, kterou má uživatel v lednici.';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Produkt
CREATE TABLE IF NOT EXISTS `Produkt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(50) DEFAULT NULL COMMENT 'Název produktu',
  `fotka` varchar(50) DEFAULT NULL COMMENT 'Cesta k fotce produktu',
  `popis` longtext COMMENT 'Popis k produktu',
  `kategorieID` int(10) unsigned DEFAULT NULL,
  `jednotkaID` int(10) unsigned DEFAULT NULL,
  `mnozstvi` int(11) DEFAULT NULL COMMENT 'Množství produktu.',
  PRIMARY KEY (`id`),
  KEY `FK_Jednotka` (`jednotkaID`),
  KEY `FK_Produkt_Kategorie` (`kategorieID`),
  CONSTRAINT `FK_Jednotka` FOREIGN KEY (`jednotkaID`) REFERENCES `Jednotka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Produkt_Kategorie` FOREIGN KEY (`kategorieID`) REFERENCES `Kategorie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje konecný produkt s názvem, fotkou a popiskem, který uživatel uvidí.  Tento produkt má cenu podle naskladneného zboží, ze kterého pochází.  Také tento produkt uživatel vidí ve své lednici s vlastnosmi potraviny.  Tento produkt uživatel uvidí i v ingredienci. ';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Recenze
CREATE TABLE IF NOT EXISTS `Recenze` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `textRecenze` longtext COMMENT 'Text recenze napsaný uživatelem.',
  `pocetHvezdicek` int(11) DEFAULT '0' COMMENT 'Zadaný pocet hvezdicek od 0 - 5',
  `soubor` varchar(50) DEFAULT NULL COMMENT 'Cesta k nahranému obrázku vytvoreného jídla.',
  `receptID` int(10) unsigned DEFAULT NULL,
  `uzivatelID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Recepty_Recenze` (`receptID`),
  KEY `FK_Uzivatel_Recenze` (`uzivatelID`),
  CONSTRAINT `FK_Recepty_Recenze` FOREIGN KEY (`receptID`) REFERENCES `Recepty` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Uzivatel_Recenze` FOREIGN KEY (`uzivatelID`) REFERENCES `Uzivatel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje hodnocení receptu od uživatelu.';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Recepty
CREATE TABLE IF NOT EXISTS `Recepty` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(50) DEFAULT NULL COMMENT 'Název receptu',
  `pocetPorci` int(11) DEFAULT NULL COMMENT 'Pocet porcí, které lze udelat.',
  `postup` longtext COMMENT 'Popsaný postup jak pripravit recept.',
  `pribliznyCas` int(11) DEFAULT NULL COMMENT 'Približný cas prípravy v minutách',
  `priorita` int(11) DEFAULT NULL COMMENT 'Zadaná priorita aby byl recept v razení co nejvýše.',
  `slozitost` enum('zacatecnik','pokrocily','expert') DEFAULT NULL COMMENT 'Složitost receptu, možnosti (zacátecník, pokrocilý, expert)',
  `fotka` varchar(50) DEFAULT NULL COMMENT 'cesta k uložené fotce k receptu',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje seznam všech receptu, které uživatel muže prohlížet a pripravit.';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Upozorneni
CREATE TABLE IF NOT EXISTS `Upozorneni` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produktID` int(10) unsigned DEFAULT NULL COMMENT 'Název jednotky',
  `uzivatelID` int(10) unsigned DEFAULT NULL,
  `aktivni` tinyint(1) DEFAULT '1',
  `typ` enum('trvanlivost','zmena_ceny') DEFAULT NULL COMMENT 'Typ upozornení ("trvanlivost", "zmena_ceny")',
  `pocetDni` int(11) DEFAULT NULL COMMENT 'Pocet dní do vypršení produktu - pokud 0 tak už vypršel.',
  `datum` datetime DEFAULT NULL COMMENT 'Datum kdy byla vygenerovana upozorneni',
  `cena` double DEFAULT NULL COMMENT 'Cena pri typu zmena ceny.',
  PRIMARY KEY (`id`),
  KEY `FK_Upozorneni_Uzivatel` (`uzivatelID`),
  KEY `FK_Upozorneni_Produkt` (`produktID`),
  CONSTRAINT `FK_Upozorneni_Produkt` FOREIGN KEY (`produktID`) REFERENCES `Produkt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Upozorneni_Uzivatel` FOREIGN KEY (`uzivatelID`) REFERENCES `Uzivatel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje upozorneni, ktera uzivatel dostane.';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Uzivatel
CREATE TABLE IF NOT EXISTS `Uzivatel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prezdivka` varchar(50) NOT NULL COMMENT 'Prezdívka pro prihlášení uživatele.',
  `heslo` varchar(4096) NOT NULL COMMENT 'Uložené heslo v hash podobe.',
  `jmeno` varchar(50) DEFAULT NULL COMMENT 'Jméno uživatele',
  `prijmeni` varchar(50) DEFAULT NULL COMMENT 'Príjmení uživatele',
  `ulice` varchar(50) DEFAULT NULL,
  `mesto` varchar(50) DEFAULT NULL,
  `cisloPopisne` varchar(50) DEFAULT NULL,
  `PostovniSmerovaciCislo` varchar(50) DEFAULT NULL,
  `role` enum('uzivatel','zamestnanec','admin') DEFAULT NULL COMMENT 'Role uživatele, možné hodnoty: uzivatel, technik, administrator',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Prezdivka` (`prezdivka`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje osobu, která má svou lednici, objednává potraviny a píše recenze na recepty.';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Watchdog
CREATE TABLE IF NOT EXISTS `Watchdog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `produktID` int(10) unsigned DEFAULT NULL COMMENT 'Název jednotky',
  `uzivatelID` int(10) unsigned DEFAULT NULL,
  `cena` double DEFAULT NULL COMMENT 'Cena pro hlídání zboží.',
  PRIMARY KEY (`id`),
  KEY `FK_Watchdog_Produkt` (`produktID`),
  KEY `FK_Watchdog_Uzivatel` (`uzivatelID`),
  CONSTRAINT `FK_Watchdog_Produkt` FOREIGN KEY (`produktID`) REFERENCES `Produkt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Watchdog_Uzivatel` FOREIGN KEY (`uzivatelID`) REFERENCES `Uzivatel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje seznam všech jednotek';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka c0smartfridge.Zbozi
CREATE TABLE IF NOT EXISTS `Zbozi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cena` double unsigned DEFAULT NULL COMMENT 'Cena naskladneného zboží',
  `pocet` int(10) unsigned DEFAULT NULL COMMENT 'Pocet kusu naskladneného zboží',
  `trvanlivost` datetime DEFAULT NULL COMMENT 'Trvanlivost zboží.',
  `produktID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Produkt_Zbozi` (`produktID`),
  CONSTRAINT `FK_Produkt_Zbozi` FOREIGN KEY (`produktID`) REFERENCES `Produkt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='Trída predstavuje zboží, které je na sklade.';

-- Export dat nebyl vybrán.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
